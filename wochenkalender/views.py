# -*- coding: utf-8 -*-
from datetime import datetime, timedelta
from heizmanager.render import render_response, render_redirect
from pytz.tzinfo import StaticTzInfo
from heizmanager.models import Haus
import pytz
import logging
from django.utils.datastructures import MultiValueDictKeyError
from heizmanager import cache_helper as ch


def get_name():
    return u'Wochenkalender'


def get_cache_ttl():
    return 1800


def is_togglable():
    return True


def is_hidden_offset():
    return False


def calculate_always_anew():
    return True


def get_global_settings_link(request, haus):
    pass


def get_global_description_link():
    desc = None
    desc_link = None
    return desc, desc_link


def get_global_settings_page(request, haus):
    pass


def get_global_settings_page_help(request, haus):
    pass


def fix_zero(num):
    # replace 0 to 7 for match part
    num = int(num)
    if num == 0:
        num = 7
    return num


def convert_date(lst):

    output = []
    berlin = pytz.timezone('Europe/Berlin')
    now = datetime.now(berlin)
    today = fix_zero(now.strftime('%w'))
    for item in lst:
        logging.warning('------------------start-------------------')

        end_day = fix_zero(item['end_day'])
        #next week
        if today > end_day:
            end_def = 7 - (today - end_day)
        else:
            end_def = end_day - today

        end = berlin.localize(datetime.strptime(now.strftime('%Y%m%d')+item['end_hour'], '%Y%m%d%H:%M'))
        end += timedelta(days=end_def)

        begin_dey = fix_zero(item['begin_day'])
        begin_def = begin_dey - end_day
        #next week
        if begin_def > 0:
            begin_def -= 7
        begin = berlin.localize(datetime.strptime(end.strftime('%Y%m%d')+item['begin_hour'], '%Y%m%d%H:%M'))
        begin += timedelta(days=begin_def)

        logging.warning("begin: %s, now: %s, end: %s" % (begin, now, end))

        output.append({
            'begin': begin,
            'end': end,
            'offset': item['offset']
        })

        if len(item['repeat']) > 0:
            for repeat in item['repeat']:
                if begin_dey == fix_zero(repeat):
                    logging.warning('skip the repeat ' + str(repeat))
                    continue
                repeat_begin_def = today - fix_zero(repeat)
                repeat_begin = berlin.localize(datetime.strptime(now.strftime('%Y%m%d')+item['begin_hour'], '%Y%m%d%H:%M'))
                repeat_begin -= timedelta(days=repeat_begin_def)

                repeat_begin_day_def = fix_zero(item['end_day']) - fix_zero(item['begin_day'])

                repeat_end = berlin.localize(datetime.strptime(repeat_begin.strftime('%Y%m%d')+item['end_hour'], '%Y%m%d%H:%M'))
                repeat_end += timedelta(days=repeat_begin_day_def)

                logging.warning("repeat_begin: %s, repeat_end: %s" % (repeat_begin, repeat_end))

                output.append({
                    'begin': repeat_begin,
                    'end': repeat_end,
                    'offset':  item['offset']
                })
                # past week
                output.append({
                    'begin': repeat_begin - timedelta(days=7),
                    'end': repeat_end - timedelta(days=7),
                    'offset':  item['offset']
                })

        logging.warning('------------------end-------------------')

    return output


def get_offset(haus, raum=None):

    params = raum.get_module_parameters()
    if not len(params.get('wochenkalender', list())):
        return 0.0

    try:
        lst = convert_date(params.get('wochenkalender', list()))
    except:
        return 0.0

    berlin = pytz.timezone('Europe/Berlin')
    now = datetime.now(berlin)

    offset = 0.0
    for item in lst:
        if item['begin'] < now < item['end']:
            logging.warning(item['begin'])
            logging.warning(now)
            logging.warning(item['end'])
            offset += float(item['offset'])

    return offset


def get_local_settings_link(request, raum):
    haus = raum.etage.haus
    offset = get_offset(haus,raum)
    if offset != 0.0:
        return "Wochenkalender", "%.2f" % offset, 75, "/m_raum/%s/wochenkalender/" % raum.id
    else:
        return "Wochenkalender", "deaktiviert", 75, "/m_raum/%s/wochenkalender/" % raum.id


def delete(index, raum):

    params = raum.get_module_parameters()

    if 'wochenkalender' in params:
        try:
            del params['wochenkalender'][index]
        except IndexError:
            pass

    raum.set_module_parameters(params)


def add(request, raum, index=0):

    params = raum.get_module_parameters()
    params.setdefault('wochenkalender', list())
    if index is None:
        index = len(params['wochenkalender'])

    week_name = ['So', 'Mo', 'Di', 'Mi', 'Do', 'Fr', 'Sa']

    try:
        entry = {
            'offset'         : request.POST['offset'],
            'begin_day'      : request.POST['begin-day'],
            'begin_day_name' : week_name[int(request.POST['begin-day'])],
            'begin_hour'     : request.POST['begin-hour'],
            'end_day'        : request.POST['end-day'],
            'end_day_name'   : week_name[int(request.POST['end-day'])],
            'end_hour'       : request.POST['end-hour'],
            'repeat'         : request.POST.getlist('repeat')
        }
    except MultiValueDictKeyError:
        return 'Bitte vervollst&auml;ndigen Sie Ihre Angaben.'

    lst = [entry]
    try:
        convert_date(lst)
    except:
        return 'Ung&uuml;ltiges Uhrzeitformat.'

    if index == len(params['wochenkalender']):
        params['wochenkalender'].append(lst[0])
    else:
        params['wochenkalender'][index] = lst[0]

    raum.set_module_parameters(params)
    activate(raum)

    return ''


def get_local_settings_page(request, raum):

    params = raum.get_module_parameters()
    lst = params.get('wochenkalender', list())

    if request.method == "GET":
        # update calendar row
        if 'edit' in request.GET:
            index = int(request.GET['edit'])
            return render_response(
                request, "m_raum_wochenkalender_add_edit.html", {"raum": raum, 'data': lst[index], 'index': index}
            )
        # add calander row for rome
        if 'add' in request.GET:
            return render_response(request, "m_raum_wochenkalender_add_edit.html", {"raum": raum})
        # delete calander row
        if 'delete' in request.GET:
            index = int(request.GET['delete'])
            delete(index, raum)
            try:
                del lst[index]
            except AttributeError:
                pass
            ch.delete("%s_roffsets_dict" % raum.id)
            return render_response(request, 'm_raum_wochenkalender.html', {'raum': raum, 'list': lst})

        return render_response(request, "m_raum_wochenkalender.html", {"raum": raum, 'list': lst})

    if request.method == "POST":
        # it mean we have the index and we should update it
        if 'index' in request.POST:
            index = int(request.POST['index'])
            ret = add(request, raum, index=index)
        else:
            ret = add(request, raum, index=None)  # len(lst))

        ch.delete("%s_roffsets_dict" % raum.id)

        if ret:
            return render_response(request, "m_raum_wochenkalender_add_edit.html", {'raum': raum, 'error': ret})

        return render_redirect(request, '/m_raum/%s/wochenkalender' % raum.id)


class OffsetTime(StaticTzInfo):
    def __init__(self, offset):
        """A dumb timezone based on offset such as +0530, -0600, etc."""
        hours = int(offset[:3])
        minutes = int(offset[0] + offset[3:])
        self._utcoffset = timedelta(hours=hours, minutes=minutes)


def activate(hausoderraum):
    if 'wochenkalender' not in hausoderraum.get_modules():
        hausoderraum.set_modules(hausoderraum.get_modules() + ['wochenkalender'])