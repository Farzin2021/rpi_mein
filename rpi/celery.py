from __future__ import absolute_import

import os
from celery import Celery
from django.conf import settings
from celery.schedules import crontab

os.environ.setdefault('DJANGO_SETTINGS_MODULE', 'rpi.settings')

app = Celery('rpi', broker="amqp://localhost")

app.config_from_object('django.conf:settings')
app.autodiscover_tasks(lambda: settings.INSTALLED_APPS, related_name='tasks')

try:
    from heizmanager.network_helper import get_mac
    mac = get_mac()
    tsleep = 60 + 60*(int(mac[-2:], 16) % 8) + (int(mac[-5:-3], 16) % 60)
except Exception as e:
    import logging
    logging.exception("exception going to sleep")
    tsleep = 60

app.conf.update(
    {
        'CELERYBEAT_SCHEDULE': {
            'logmonitor-periodic-update': {
                'task': 'logmonitor.tasks.update_service_monitor',
                'schedule': crontab(minute="*/20"),
            },
            'logmonitor-periodic-rf': {
                'task': 'logmonitor.tasks.periodic_calculate',
                'schedule': crontab(minute="*/3"),
            },
            'send-setup-every-10-minutes': {
                'task': 'dataslave.tasks.send_setup',
                'schedule': crontab(minute="%s-59/10" % int(tsleep/60)),
            },
            'general-setup-every-day': {
                'task': 'dataslave.tasks.general_functions',
                'schedule': crontab(hour=3, minute=0),
            },
            'nginx-errors': {
                'task': 'heizmanager.tasks.page_request',
                'schedule': crontab(minute="*/15")
            },
            'logger-clean-db': {
                'task': 'logger.tasks.clean_db',
                'schedule': crontab(hour=3, minute=0)
            },
            'atkorrektur-calc-offset': {
                'task': 'aussentemperaturkorrektur.tasks.calc_offset',
                'schedule': crontab(minute="*/60")
            },
            'backupcontrol-periodic-backup': {
                'task': 'backupcontrol.tasks.periodic_backup',
                'schedule': crontab(hour=3, minute=0),
            },
            'hfo-periodic-calculation': {
                'task': 'heizflaechenoptimierung.tasks.calc_offset',
                'schedule': crontab(minute="%s-59/10" % int(tsleep/60)),
            },
            'hfo-010v': {
                'task': 'heizflaechenoptimierung.tasks.calc_hftemps',
                'schedule': crontab(minute="*/3"),
            },
            'aha-pro': {
                'task': 'ahapro.tasks.aha_pro_calc',
                'schedule': crontab(minute="*/3"),
            },
            'vorlauftemperaturkorrektur-periodic-calculation': {
                'task': 'vorlauftemperaturkorrektur.tasks.calc_deviation',
                'schedule': crontab(minute="*/10"),
            },
            'every-minute-task-manager': {
                'task': 'heizmanager.tasks.every_minute_task_manager',
                'schedule': crontab(minute="*/1"),
            },
            'get-update-calculation': {
                'task': 'wetter_pro.tasks.periodic_calculation',
                'schedule': crontab(minute="*/60"),
            },
            'aha-testrun': {
                'task': 'aha.tasks.testrun',
                'schedule': crontab(minute="*/3"),
            },
            'ki-periodic-calc-data-point': {
                'task': 'ki.tasks.periodic_calc_data_point',
                'schedule': crontab(hour=2, minute=0),
            },
            'ki-periodic-evaluation-of-kilog': {
                'task': 'ki.tasks.periodic_evaluation_of_kilog',
                'schedule': crontab(hour=2, minute=15),
            },
            'periodic-kaminofen-calculation': {
                'task': 'kaminofen.tasks.periodic_kaminofen_calculation',
                'schedule': crontab(minute="%s-59/10" % int(tsleep/60)),
            },
            'ahk-analyze-logs': {
                'task': 'ki.tasks.analyze_logs',
                'schedule': crontab(hour=1, minute=5),
            },
            'ahk-adjust-curve': {
                'task': 'ki.tasks.adjust_curves',
                'schedule': crontab(hour=1, minute=55),
            },
            'periodic-betriebsarten-evaluation': {
                'task': 'betriebsarten.tasks.periodic_betriebsarten_evaluation',
                'schedule': crontab(hour='*/2'),
            },
            'periodic-query-execution': {
                'task': 'api_query.tasks.periodic_query_execution',
                'schedule': crontab(minute='*/1'),
            },
            'periodic-fps-email': {
                'task': 'benachrichtigung.tasks.send_notifications',
                'schedule': crontab(minute='*/5'),
            },
            'periodic-btle-handler': {
                'task': 'config.tasks.btle_handler',
                'schedule': crontab(minute='*/1'),
            },
            'periodic-zielwertkorrektur-handler': {
                'task': 'dataslave.tasks.zielwertkorrektur',
                'schedule': crontab(minute='*/10'),
            }
        }
    },
)
app.conf.update(
    {'CELERYBEAT_PID_FILE': '/var/run/celerybeat.pid'}
)

app.conf.CELERY_TIMEZONE = 'Europe/Berlin'
