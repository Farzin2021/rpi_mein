# -*- coding: utf-8 -*-
import pytz

from heizmanager.render import render_response, render_redirect
from heizmanager.models import Regelung, Gateway, GatewayAusgang, RPi, Raum
import logging
from django.http import HttpResponse, HttpRequest
from heizmanager import getandset
from heizmanager.models import sig_get_outputs_from_regelung, sig_get_all_outputs, sig_get_all_outputs_from_output
from rf.models import RFAktor, RFAusgang, RFController
from itertools import chain
from datetime import datetime, timedelta
import heizmanager.cache_helper as ch
from knx.models import KNXGateway
from benutzerverwaltung.decorators import has_regelung_access
import json
from django.views.decorators.csrf import csrf_exempt


logger = logging.getLogger('logmonitor')


def get_name():
    return u'Raumanforderung'


def managesoutputs():
    return True


def do_ausgang(regelung, ausgangno):

    params = regelung.get_parameters()
    modules = None
    for gwid, gwmac, ausgang in params['input'] + params['output']:
        try:
            ctrl = Gateway.objects.get(name=gwmac)
        except Gateway.DoesNotExist:
            pass
        else:
            modules = ctrl.haus.get_modules()
            break
        try:
            ctrl = RFController.objects.get(name=gwmac)
        except RFController.DoesNotExist:
            pass
        else:
            modules = ctrl.haus.get_modules()
            break

    if modules is None or 'pumpenlogik' not in modules:
        logging.warning("pl for %s returning default (deactivated)" % ausgangno)
        return HttpResponse('<%s>' % params.get('default', 1))

    inputs = []
    todel = []
    for gwid, gwmac, ausgang in params['input']:
        # sanity check ... nicht dass einer von den inputs geloescht wurde (kann z.b. bei solarpuffer passieren):
        if getandset.get_gwout_by_no(gwmac, ausgang):
            req = HttpRequest()
            req.path = '/get/%s/%s/' % (gwmac, ausgang)
            req.method = 'GET'
            req.META['HTTP_REFERER'] = '/m_raum/'  # wegen get/get_all Sperre
            response = getandset.get(req, gwmac, ausgang).content
            r = response.translate(None, '<>!')
            if len(r):
                inputs.append(int(float(r)))
        else:
            ret = params.get('default', 1)
            try:
                rfaktor = RFAktor.objects.get(controller_id=gwid, name=ausgang.split(' ')[0])
                for a in RFAusgang.objects.filter(controller_id=gwid):
                    if rfaktor.id in a.aktor:
                        ret = a.regelung.do_rfausgang(gwmac)  # das funktioniert nicht an fsr, das hat da eine andere schnittstelle als zpr und rlr
                        if isinstance(ret, int):
                            ret = str(ret)
                            break
                        else:
                            continue
                else:
                    if rfaktor.type.startswith("hkt") and rfaktor.raum_id:
                        val = rfaktor.raum.get_regelung().do_ausgang(None)
                        if isinstance(val, HttpResponse):
                            ret = val.content
                        else:
                            ret = val
                    else:
                        todel.append((gwid, gwmac, ausgang))
            except RFAktor.DoesNotExist:
                todel.append((gwid, gwmac, ausgang))
            try:
                ret = ret.translate(None, '<>!')
            except AttributeError:
                ret = str(ret)
            if len(ret):
                inputs.append(int(ret))
        if sum(inputs):
            break

    if todel:
        logging.warning("pl for %s has todel: %s with original input: %s" % (ausgangno, todel, params['input']))
        for td in todel:
            params['input'].remove(td)
        regelung.set_parameters(params)

    try:
        einvzg = int(params.get('einvzg', 0))
    except:
        params['einvzg'] = 0
        regelung.set_parameters(params)
        einvzg = 0

    try:
        ausvzg = int(params.get('ausvzg', 0))
    except:
        params['ausvzg'] = 0
        regelung.set_parameters(params)
        ausvzg = 0

    try:
        ra_min_valve_setting = int(params.get('ra_min_valve_setting', 0))
    except:
        ra_min_valve_setting = 0

    invertout = params.get('invertout', False)

    last = ch.get("pllast_%s" % regelung.id)

    # out = not bool(sum(inputs)) if invertout else bool(sum(inputs))
    out = any(value > ra_min_valve_setting for value in inputs)
    if out:
        logging.warning("pl for %s returning 1, inputs are %s" % (ausgangno, inputs))

        if last:

            if last[1] == 1:
                # cache nicht setzen, nichts machen
                pass
            else:
                # cache setzen, wenn ausschaltverzoegerung abgelaufen ist
                if datetime.now() > (last[0] + timedelta(seconds=ausvzg)):
                    last = (datetime.now(), 1)
                    ch.set("pllast_%s" % regelung.id, last)

            if datetime.now() > (last[0] + timedelta(seconds=einvzg)):
                logger.warning("%s|%s" % (regelung.id, u"Raumanforderung %s hat Wärmeanforderung, Ausgang auf 1." % (params.get('name', '-'))))
                return HttpResponse("<1>")

            else:
                logger.warning("%s|%s" % (regelung.id, u"Raumanforderung %s hat Wärmeanforderung, Ausgang wegen Einschaltverzögerung weiterhin auf 0." % (params.get('name', '-'))))
                return HttpResponse("<0>")

        else:
            if einvzg:
                logger.warning("%s|%s" % (regelung.id, u"Raumanforderung %s hat Wärmeanforderung, Ausgang wegen Einschaltverzögerung* weiterhin auf 0." % (params.get('name', '-'))))
                ch.set("pllast_%s" % regelung.id, (datetime.now(), 1))
                return HttpResponse("<0>")
            else:
                logger.warning("%s|%s" % (regelung.id, u"Raumanforderung %s hat Wärmeanforderung, Ausgang auf 1." % (params.get('name', '-'))))
                ch.set("pllast_%s" % regelung.id, (datetime.now(), 1))
                return HttpResponse("<1>")

    else:
        logging.warning("pl for %s returning 0" % ausgangno)

        if last:

            if last[1] == 0:
                # cache nicht setzen, nichts machen
                pass
            else:
                # cache setzen, wenn einschaltverzoegerung abgelaufen ist
                if datetime.now() > (last[0] + timedelta(seconds=einvzg)):
                    last = (datetime.now(), 0)
                    ch.set("pllast_%s" % regelung.id, last)

            if datetime.now() > (last[0] + timedelta(seconds=ausvzg)):
                logger.warning("%s|%s" % (regelung.id, u"Raumanforderung %s ohne Wärmeanforderung, Ausgang auf 0." % (params.get('name', '-'))))
                return HttpResponse("<0>")

            else:
                logger.warning("%s|%s" % (regelung.id, u"Raumanforderung %s ohne Wärmeanforderung, Ausgang wegen Ausschaltverzögerung weiterhin auf 1." % (params.get('name', '-'))))
                return HttpResponse("<1>")

        else:
            if ausvzg:
                logger.warning("%s|%s" % (regelung.id, u"Raumanforderung %s ohne Wärmeanforderung, Ausgang wegen Ausschaltverzögerung* weiterhin auf 1." % (params.get('name', '-'))))
                ch.set("pllast_%s" % regelung.id, (datetime.now(), 0))
                return HttpResponse("<1>")
            else:
                logger.warning("%s|%s" % (regelung.id, u"Raumanforderung %s ohne Wärmeanforderung, Ausgang auf 0." % (params.get('name', '-'))))
                ch.set("pllast_%s" % regelung.id, (datetime.now(), 0))
                return HttpResponse("<0>")


def do_rfausgang(regelung, controllerid):
    ret = do_ausgang(regelung, 0).content
    params = regelung.get_parameters()
    default = params.get('default', 0)
    return int(ret[1]) if ret[1] in ["0", "1"] else default


def get_config(request, macaddr):

    if request.method == 'GET':

        try:
            gw = Gateway.objects.get(name=macaddr.lower())
        except Gateway.DoesNotExist:
            return HttpResponse(status=405)

        ret = {}
        for pl in Regelung.objects.filter(regelung="pumpenlogik"):
            params = pl.get_parameters()
            for gwid, gwmac, ausgang in params.get('output', []):
                if gwmac == macaddr:
                    try:
                        val = int(do_ausgang(pl, ausgang).content.translate(None, '<>!'))
                    except:
                        val = params.get('default', 1)

                    ret[ausgang] = val
                    ch.set("%s_%s" % (gwmac, ausgang), val, time=300)

        return HttpResponse(json.dumps(ret))

    else:
        return HttpResponse(405)


def get_outputs(haus, raum, regelung):
    # gibt eine liste pro gateway mit den von dieser regelung belegten ausgaengen

    belegte_ausgaenge = dict()

    all_outs = sig_get_all_outputs_from_output.send('pl', haus=haus)
    for _, outs in all_outs:
        for out in outs:
            if out[2].regelung == 'pumpenlogik':
                belegte_ausgaenge.setdefault(out[0], list())
                for o in out[4]:
                    name = out[2].get_parameters().get('name', '')
                    id = out[2].id
                    belegte_ausgaenge[out[0]].append((o, name, 'Raumanforderung %s: %s' % (id, name), False))

    return belegte_ausgaenge


@csrf_exempt
def set_status(request, macaddr):

    if request.method == "POST":
        data = json.loads(request.body)
        try:
            gw = Gateway.objects.get(name=macaddr.lower())
        except Gateway.DoesNotExist:
            pass
        else:
            if 'error' in data:
                regs = Regelung.objects.filter(regelung="pumpenlogik")
                for reg in regs:
                    if reg.gatewayausgang and reg.gatewayausgang.gateway == gw:
                        logger.warning("%s|%s" % (reg.id, data['error']))

    return HttpResponse()


def get_global_settings_link(request, haus):
    return "<a href='/m_setup/%s/pumpenlogik/'>Raumanforderung</a>" % haus.id


def get_global_description_link():
    desc = "Schaltet Pumpen oder Wärmeerzeuger nur EIN, wenn wirklich notwendig."
    desc_link = "http://support.controme.com/auswahl-und-aktivierung-von-modulen/modul-raumanforderung/"
    return desc, desc_link


def get_ra(haus, objid=None):
    ret = {}
    for gateway in Gateway.objects.filter(haus=haus):
        gatewayausgaenge = GatewayAusgang.objects.filter(gateway=gateway)
        if objid is not None:
            gatewayausgaenge = gatewayausgaenge.filter(regelung_id=int(objid))
        for ausgang in gatewayausgaenge:
            if ausgang.regelung.regelung == 'pumpenlogik':
                params = ausgang.regelung.get_parameters()
                response = do_ausgang(ausgang.regelung, None).content
                response = response.translate(None, '<>!')
                if response == '0':
                    response = "Kein Wärmebedarf vorhanden, %s auf 0"
                else:
                    response = "Wärmebedarf vorhanden, %s auf 1"
                if len(params['input']) > 1:
                    response %= "Ausgänge"
                else:  # len(params['input']) == 1:
                    response %= "Ausgang"
                ret[str(ausgang.regelung.id)] = (params['name'], response)

    for rpi in RPi.objects.filter(haus=haus):
        for controller in RFController.objects.filter(rpi=rpi):
            rfs = RFAusgang.objects.filter(controller=controller)
            if objid is not None:
                rfs = rfs.filter(regelung_id=int(objid))
            for ausgang in rfs:
                if ausgang.regelung.regelung == "pumpenlogik":
                    params = ausgang.regelung.get_parameters()
                    response = do_ausgang(ausgang.regelung, None).content
                    response = response.translate(None, '<>!')
                    if response == '0':
                        response = "Zu beobachtende Ausgänge auf 0, kein Heizbedarf, Heizung/Pumpe nicht angefordert"
                    else:
                        response = "Zu beobachtende Ausgänge auf 1, Heizbedarf, Heizung/Pumpe angefordert"
                    ret[str(ausgang.regelung.id)] = (params['name'], response)

    for reg in Regelung.objects.filter(regelung="pumpenlogik"):
        if reg.get_ausgang() is None and not len(RFAusgang.objects.filter(regelung_id=reg.id)):
            reg.delete()

    if not len(ret):
        ret = None
    return ret


def get_global_settings_page(request, haus):
    if request.method == "GET":

        if "pldel" in request.GET and request.user.userprofile.get().role != 'K':
            try:
                pl = Regelung.objects.get(pk=long(request.GET['pldel']))
            except Regelung.DoesNotExist:  # falls veraltete seite angezeigt wurde
                return render_redirect(request, '/m_setup/%s/pumpenlogik/' % haus.id)

            # checken, ob diese PL von einer anderen PL verwendet wird
            for gateway in Gateway.objects.filter(haus=haus):
                for aausgang in gateway.ausgaenge.all():
                    if aausgang.regelung and \
                            aausgang.regelung.regelung == "pumpenlogik" and \
                            aausgang.regelung.id != pl.id:
                        params = aausgang.regelung.get_parameters()

                        # eine PL kann mehre AAusgaenge erzeugen, wenn sie Ausgaenge an versch. gateways ansteuert
                        for _aausgang in GatewayAusgang.objects.filter(regelung=pl):
                        #i_aausgang = pl.ausgang
                            _input = (_aausgang.gateway.id, _aausgang.gateway.name, _aausgang.ausgang)
                            if _input in params['input']:
                                params['input'].remove(_input)
                                _aausgang.regelung.set_parameters(params)
                                _aausgang.regelung.save()

                    elif aausgang.regelung.id == pl.id:
                        aausgang.delete()
            pl.delete()
            return render_redirect(request, '/m_setup/%s/pumpenlogik/' % haus.id)
            
        elif "getpl" in request.GET:
            import json
            from django.core.serializers.json import DjangoJSONEncoder
            ret = get_ra(haus)
            return HttpResponse(json.dumps(ret, cls=DjangoJSONEncoder), content_type='application/json')
        
        elif "plid" in request.GET:

            try:  # editieren
                pl = Regelung.objects.get(pk=long(request.GET['plid']))
                ausgaenge, verfuegbare_ausgaenge = _get_ausgaenge(haus, showforpl=long(request.GET['plid']))
                params = pl.get_parameters()
                name = params['name']
                _input = params['input']
                output = params['output']
                default = params.get('default', 1)
                try:
                    einvzg = int(params.get('einvzg', 0))
                except:
                    einvzg = 0
                try:
                    ausvzg = int(params.get('ausvzg', 0))
                except:
                    ausvzg = 0

                try:
                    ra_min_valve_setting = int(params.get('ra_min_valve_setting', 0))
                except:
                    ra_min_valve_setting = 0

                invertout = params.get('invertout', False)
                show_in_menu = params.get('show_in_menu', None)

            except ValueError:  # anlegen
                ausgaenge, verfuegbare_ausgaenge = _get_ausgaenge(haus)
                pl = Regelung(regelung="pumpenlogik", parameter=str({}))
                name = ''
                _input = []
                output = []
                default = 1
                einvzg = 0
                ausvzg = 0
                invertout = False
                show_in_menu = False
                ra_min_valve_setting = 0

            except Regelung.DoesNotExist:  # wenn noch was veraltetes angezeigt wurde
                return render_redirect(request, '/m_setup/%s/pumpenlogik/' % haus.id)

            gateways = list(chain(Gateway.objects.filter(haus=haus), KNXGateway.objects.filter(haus=haus)))

            show_ra_min_valve_setting = False
            for gateway in gateways:
                if (isinstance(gateway, Gateway) and "6.00" > gateway.version >= "5.00") or isinstance(gateway,
                                                                                                       KNXGateway):
                    show_ra_min_valve_setting = True
            return render_response(request, 'm_settings_pumpenlogik_edit.html',
                                   {'haus': haus, 'gwausgaenge': ausgaenge, 'vausgaenge': verfuegbare_ausgaenge,
                                    "ra_min_valve_setting": ra_min_valve_setting,
                                    "show_ra_min_valve_setting": show_ra_min_valve_setting,
                                    'pl': pl, 'name': name, 'input': _input, 'output': output, 'default': default,
                                    'einvzg': einvzg, 'ausvzg': ausvzg, "plid": request.GET['plid'],
                                    'invertout': invertout, "show_in_menu": show_in_menu, })

        else:
            return render_response(request, 'm_settings_pumpenlogik.html', {'haus': haus})

    if request.method == "POST" and request.user.userprofile.get().role != 'K':

        if 'pledit' in request.POST:

            logging.warning(request.POST)

            try:  # editieren
                pl = Regelung.objects.get(pk=long(request.POST['plid']))
            except (ValueError, Regelung.DoesNotExist):  # anlegen
                pl = Regelung(regelung="pumpenlogik", parameter=str({'name': '', 'input': [], 'output': [], 'default': 1}))
                pl.save()
            name, inputs, outputs, default, error = _parse_response(request)
            try:
                einvzg = int(request.POST.get('einvzg', 0))
            except:
                einvzg = 0
            try:
                ausvzg = int(request.POST.get('ausvzg', 0))
            except:
                ausvzg = 0
            try:
                invertout = bool(request.POST.get('invertout', 0))
            except:
                invertout = False
            try:
                show_in_menu = bool(request.POST.get('show_in_menu', 0))
            except:
                show_in_menu = False

            try:
                ra_min_valve_setting = int(request.POST.get('ra_min_valve_setting', 0))
            except:
                ra_min_valve_setting = 0

            outdict = dict()
            for dev, out in outputs:
                outdict.setdefault(dev, list())
                outdict[dev].append(out)

            outset = set()
            for dev in outdict.keys():
                if type(dev) in outset:
                    error += 'Es sind nur Ausgänge an <i>einem</i> Gerät pro Typ erlaubt! Bei mehreren Gateways kann die Raumanforderung nur Ausgänge an einem Gateway ansteuern.<br/>'
                    break
                else:
                    outset.add(type(dev))

            if pl_inconsistent(inputs, pl) or len(set(inputs) & set(outputs)):
                error += 'Eine Raumanforderung darf sich nicht (indirekt) selbst als Eingang enthalten.<br/>'
            if len(error):
                ausgaenge, verfuegbare_ausgaenge = _get_ausgaenge(haus, showforpl=pl.id)
                inputs = [(i[0].id, i[0].name, i[1]) for i in inputs]
                outputs = [(o[0].id, o[0].name, o[1]) for o in outputs]
                return render_response(request, 'm_settings_pumpenlogik_edit.html',
                                       {'haus': haus, 'gwausgaenge': ausgaenge, 'vausgaenge': verfuegbare_ausgaenge,
                                        'pl': pl, 'name': name, 'input': inputs, 'output': outputs, 'error': error,
                                        'default': default})
            else:
                params = pl.get_parameters()

                all_outs = set(chain(GatewayAusgang.objects.filter(regelung=pl), RFAusgang.objects.filter(regelung=pl)))
                for o in all_outs:
                    for dev in outdict.keys():
                        if type(o) == GatewayAusgang and dev == o.gateway:
                            break
                        elif type(o) == RFAusgang and dev == o.controller:
                            break
                    else:
                        o.delete()

                for dev, outs in outdict.items():
                    if type(dev) == Gateway:
                        try:
                            ausgang = GatewayAusgang.objects.get(gateway=dev, regelung=pl)
                        except GatewayAusgang.DoesNotExist:
                            ausgang = GatewayAusgang(gateway=dev, stellantrieb='NC', regelung=pl)
                        ausgang.ausgang = ', '.join([str(o) for o in sorted([int(_o) for _o in outs])])
                        if not len(ausgang.ausgang):
                            ausgang.delete()
                        else:
                            ausgang.save()
                    elif type(dev) == RFController:
                        try:
                            ausgang = RFAusgang.objects.get(controller=dev, regelung=pl)
                        except RFAusgang.DoesNotExist:
                            ausgang = RFAusgang(controller=dev, regelung=pl)
                        ausgang.aktor = [RFAktor.objects.get(controller=dev, name=rfa).id for rfa in outs]
                        if not len(ausgang.aktor):
                            ausgang.delete()
                        else:
                            ausgang.save()

                params['name'] = name
                params['input'] = [(i[0].id, i[0].name, i[1]) for i in inputs]
                params['output'] = [(o[0].id, o[0].name, o[1]) for o in outputs]
                params['default'] = default
                params['einvzg'] = einvzg
                params['ausvzg'] = ausvzg
                params['invertout'] = invertout
                params['show_in_menu'] = show_in_menu
                params['ra_min_valve_setting'] = ra_min_valve_setting
                pl.set_parameters(params)

                for reg in Regelung.objects.filter(regelung="pumpenlogik"):
                    Regelung.objects.invalidate(reg)

                return render_redirect(request, '/m_setup/%s/pumpenlogik/' % haus.id)


@has_regelung_access
def get_local_settings_page_haus(request, haus, action=None, objid=None):
    if request.method == "GET":
        regelung = Regelung.objects.get(pk=int(objid))
        params = regelung.get_parameters()
        if action == "show":
            context = {'haus': haus}
            context['config'] = params
            gateway_inputs = dict()
            if 'input' in params and params['input']:
                for item in params['input']:
                    gateway_inputs.setdefault(item[1], list()).append(item[2])

            gateway_outputs = dict()
            if 'output' in params and params['output']:
                for item in params['output']:
                    gateway_outputs.setdefault(item[1], list()).append(item[2])

            # sorting
            gateway_outputs = {x: sorted(gateway_outputs[x]) for x in gateway_outputs.keys()}
            gateway_inputs = {x: sorted(gateway_inputs[x]) for x in gateway_inputs.keys()}
            context['outputs'] = gateway_outputs.items()
            context['inputs'] = gateway_inputs.items()
            context['objid'] = objid
            last = ch.get('pllast_%s' % regelung.id)
            if last is not None and isinstance(last, tuple):
                berlin = pytz.timezone("Europe/Berlin")
                last = "%s um %s" % (int(last[1]), berlin.localize(last[0]).strftime("%H:%M %d.%m.%Y"))

            context['last'] = last
            ret = get_ra(haus, objid)
            try:
                context['response'] = ret[objid][1]
            except TypeError:  # ret is None
                context['response'] = None
            return render_response(request, "m_pumpenlogik_show.html", context)


def get_global_settings_page_help(request, haus):
    return render_response(request, "m_help_pumpenlogik.html", {'haus': haus})


def _get_ausgaenge(haus, showforpl=0L):
    belegte_ausgaenge = {}
    verfuegbare_ausgaenge = {}

    for etage in haus.etagen.all():
        for raum in Raum.objects.filter_for_user(haus.eigentuemer, etage_id=etage.id):
            outputs = sig_get_outputs_from_regelung.send('hardware', haus=haus, raum=raum, regelung=raum.get_regelung())
            for cbfunc, outs in outputs:
                for gw, o in outs.items():
                    belegte_ausgaenge.setdefault(gw, list())
                    if len(o) and len(o[0]) and o[0][0] not in [ba[0] for ba in belegte_ausgaenge[gw] if len(ba)]:
                        belegte_ausgaenge[gw] += o
                        try:
                            belegte_ausgaenge[gw] = sorted(belegte_ausgaenge[gw], key=lambda k: int(k[0]))
                        except ValueError:  # nicht-numerischer name
                            belegte_ausgaenge[gw] = sorted(belegte_ausgaenge[gw], key=lambda k: k[0])
    
    for cbfunc, outputs in sig_get_all_outputs_from_output.send(sender='pl', haus=haus):
        for ctrldev, raum, regelung, ausgang, outs in outputs:
            belegte_ausgaenge.setdefault(ctrldev, list())
            _ba = {ba[0] for ba in belegte_ausgaenge[ctrldev]}
            belegte_ausgaenge[ctrldev] += [(o, regelung.get_parameters().get('name', regelung.regelung.capitalize()), raum.get_mainsensor() if raum else None, True) for o in outs if o not in _ba]
            try:
                belegte_ausgaenge[ctrldev] = sorted(belegte_ausgaenge[ctrldev], key=lambda k: int(k[0]))
            except ValueError:  # s.o.
                belegte_ausgaenge[ctrldev] = sorted(belegte_ausgaenge[ctrldev], key=lambda k: k[0])

    b_a = dict((dev, sorted(list(set([f[0] for f in belegte_ausgaenge[dev]])))) for dev in belegte_ausgaenge)

    all_outs = sig_get_all_outputs.send('pl', haus=haus)
    for cbfunc, dev2outs in all_outs:
        for device, (devstr, outs) in dev2outs.items():
            if isinstance(device, Gateway) and device.is_heizraumgw():
                verfuegbare_ausgaenge[device] = [k for k in sorted(outs) if (device not in b_a or k not in b_a[device]) and k <= 12]
            else:
                verfuegbare_ausgaenge[device] = [k for k in sorted(outs) if device not in b_a or k not in b_a[device]]

    if showforpl:
        pl = Regelung.objects.get(pk=long(showforpl))
        for gw, outputs in verfuegbare_ausgaenge.items():
            for o in pl.get_parameters().get('output', list()):
                if gw.name == o[1]:
                    verfuegbare_ausgaenge[gw].append(o[2])
            try:
                verfuegbare_ausgaenge[gw] = sorted(verfuegbare_ausgaenge[gw], key=lambda a: int(a))
            except ValueError:  # s.o.
                verfuegbare_ausgaenge[gw] = sorted(verfuegbare_ausgaenge[gw])

    return belegte_ausgaenge, verfuegbare_ausgaenge


def _parse_response(request):
    inputs = list()
    outputs = list()
    for key in request.POST:
        if key.startswith('i_'):
            k = key.split('_')
            try:
                gw = Gateway.objects.get(name=k[1])
                inputs.append((gw, k[2]))
                continue
            except Gateway.DoesNotExist:
                pass

            try:
                gw = RFController.objects.get(name=k[1])
                inputs.append((gw, k[2].split(' ')[0]))
                continue
            except RFController.DoesNotExist:
                pass

        if key.startswith('o_'):
            k = key.split('_')
            try:
                gw = Gateway.objects.get(name=k[1])
                outputs.append((gw, k[2]))
                continue
            except Gateway.DoesNotExist:
                pass

            try:
                gw = RFController.objects.get(name=k[1])
                outputs.append((gw, k[2].split(' ')[0]))
                continue
            except RFController.DoesNotExist:
                pass

    name = request.POST['plname']
    default = 1 if request.POST['defval'] == 'on' else 0

    error = ''
    if not len(name):
        error += 'Bitte Namen eingeben.<br/>'
    if not len(inputs):
        error += 'Bitte Input Ausg&auml;nge ausw&auml;hlen.<br/>'
    if not len(outputs):
        error += 'Bitte Output Ausg&auml;nge ausw&auml;hlen.'
    
    return name, inputs, outputs, default, error


def pl_inconsistent(inputs, pl):
    for inp in inputs:
        ausgang = None

        if isinstance(inp[0], Gateway):
            ausgaenge = GatewayAusgang.objects.filter(gateway=inp[0])
        # elif isinstance(inp[0], RFController):  # auskommentiert, weil es im else vom loop behandelt wird
        #     ausgaenge = RFAusgang.objects.filter(controller=inp[0])
        else:
            ausgaenge = []

        for a in ausgaenge:
            if inp[1] in a.ausgang.split(', '):
                ausgang = a
                break
        else:
            try:
                rfaktor = RFAktor.objects.get(controller=inp[0], name=inp[1])
            except RFAktor.DoesNotExist:
                continue
            for a in RFAusgang.objects.filter(controller=inp[0]):
                if rfaktor.id in a.aktor:
                    ausgang = a
                    break
            else:
                continue

        if ausgang.regelung_id == pl.id:
            return True
        if ausgang.regelung and ausgang.regelung.regelung == 'pumpenlogik':
            ret = []
            for inp in ausgang.regelung.get_parameters()['input']:
                try:
                    dev = Gateway.objects.get(name=inp[1])
                    ret.append((dev, inp[2]))
                    continue
                except Gateway.DoesNotExist:
                    pass

                try:
                    dev = RFController.objects.get(name=inp[1])
                    ret.append((dev, inp[2]))
                    continue
                except RFController.DoesNotExist:
                    pass
                # ret.append((dev, inp[2]))
            return pl_inconsistent(ret, pl)
    else:
        return False


def get_jsonapi(haus, usr, entityid=None):
    
    def _get_val(_pl, _params):
        try:
            val = int(do_ausgang(_pl, 0).content.translate(None, '<>!'))
        except:
            val = _params.get('default', 1)
        return val
    
    ret = {}

    if entityid is None:
        for pl in Regelung.objects.filter(regelung="pumpenlogik"):
            params = pl.get_parameters()
            val = _get_val(pl, params)
            ret[pl.id] = {'name': params.get('name', ''), 'value': val}
    
    else:
        try:
            pl = Regelung.objects.get(pk=int(entityid))
            val = _get_val(pl, pl.get_parameters())
            ret[pl.id] = {'value': val}
        except Regelung.DoesNotExist:
            pass
    
    return ret
