# -*- coding: utf-8 -*-
import logging
import pytz
from datetime import datetime, timedelta
from heizmanager.models import AbstractSensor, Haus, Gateway, sig_new_temp_value, sig_new_output_value, Raum
from heizmanager.mobile.m_temp import get_module_offsets
import heizmanager.cache_helper as ch
from heizflaechenoptimierung.views import has_ms_and_rls, activate, line_intersection, get_strategy_mode_values
import wetter.views as wetter
from celery import shared_task
import time
from heizmanager.abstracts.base_mode import BaseMode
from math import fabs
from heizmanager.mobile.m_error import CeleryErrorLoggingTask
from knx.models import KNXGateway
from itertools import chain
from maximaldurchfluss.tasks import Openings, DHMACreator

logger = logging.getLogger('logmonitor')


def log_parameters(rlziel, rparams, raum, haus, now):
    hfo_dict = {"at_window_start": rparams.get('time_start', 0), "at_window_end": rparams.get('time_end', 5),
                "hf_temp_plustwentyat": rparams.get('heating_element_start', 22),
                "hf_temp_minustwentyat": rparams.get('heating_element_end', 40),
                "activity_threshold_below_target": rparams.get('before', -0.5),
                "activity_threshold_above_target": rparams.get('after', 0.5),
                "hf_temp_correction_per_degree_deviation": rparams.get('correction_heating_element', 3)}
    if raum.get_spec_module_params('betriebsarten').get(str(raum.id), '-1') == '1':
        hfo_dict['activity_threshold_below_target'] = 0.0
        hfo_dict['activity_threshold_above_target'] = 0.0

    value = 0
    name_params = 'hfo_params_raum_' + str(raum.id)
    modules = None
    sig_new_output_value.send(sender="hfo", name=name_params, value=value, ziel=rlziel,
                              parameters=hfo_dict, timestamp=now, modules=modules)


def log_onAT(haus, raum, now, rparams, forecast, average):
    if forecast is None or average is None:
        at_window_start = rparams['time_start']
        at_window_end = rparams['time_end']
        at_window_end = min(at_window_end, 72)
        forecast_now = wetter.get_10dayforecast(haus, int(at_window_end))
        forecast = forecast_now[int(at_window_start):int(at_window_end)]
        average = sum([float(f[1]) for f in forecast]) / float(len(forecast))
    if now is None:
        berlin = pytz.timezone('Europe/Berlin')
        now = datetime.now(berlin)
    cnttemp = forecast[0][1]
    name_avg = "averagetemp_raum_" + str(raum.id)
    name_cnt = "cnttemp_raum_" + str(raum.id)
    modules = []
    sig_new_temp_value.send(sender="hfo", name=name_avg, value=average, modules=modules, timestamp=now)
    sig_new_temp_value.send(sender="hfo", name=name_cnt, value=cnttemp, modules=modules, timestamp=now)


@shared_task(base=CeleryErrorLoggingTask)
def calc_offset(for_raum=None):

    haus = Haus.objects.first()
    params = haus.get_module_parameters()
    ahapro = params.get('aha_pro_status', {})
    ahapro_check = ahapro.get('status', False)
    if ahapro_check == 'true':
        return

    berlin = pytz.timezone('Europe/Berlin')
    now = datetime.now(berlin)

    for haus in Haus.objects.all():
        if 'heizflaechenoptimierung' not in haus.get_modules():
            continue

        for etage in haus.etagen.all():
            for raum in etage.raeume.all():

                if for_raum and raum != for_raum:
                    continue

                if raum.regelung.regelung == 'ruecklaufregelung':
                    if 'heizflaechenoptimierung' not in raum.get_modules():
                        activate(raum)
                    if raum.regelung.get_ausgang() and "6.00" > raum.regelung.gatewayausgang.gateway.version >= "5.00":
                        continue  # calc_hftemps will cover this room
                else:
                    continue

                try:
                    betriebs_mode = raum.get_betriebsart()
                    if betriebs_mode == '1' or betriebs_mode == '0':  # if mode is cooling or off deactivate module
                        vals = {'timer': None, 'offset': 0, 'step': 0, 'mode': 0, 'raumziel': None, 'ts': None, 'flushing_interval': None}
                        ch.set("hfo_%s_%s" % (haus.id, raum.id), vals, 3600)  # todo 600
                        if betriebs_mode == '1':
                            logger.warning("%s|%s" % (raum.id, u"Status: Betriebsart auf Modus 'kühlen' eingestellt. Heizflächenoptimierung wird für diesen Raum nicht ausgeführt."))
                        elif betriebs_mode == '0':
                            logger.warning("%s|%s" % (raum.id, u"Status: Betriebsart auf Modus 'aus' eingestellt. Heizflächenoptimierung wird für diesen Raum nicht ausgeführt."))
                        if for_raum:
                            return vals['offset']
                        continue
                except Exception as e:
                    logging.exception("Fehler: Betriebsmodus konnte nicht ermittelt werden %s" % str(e))
                    logger.warning(u"Fehler: Betriebsmodes konnte nicht ermittelt werden")

                rparams = get_strategy_mode_values(raum)

                regparams = raum.regelung.get_parameters()

                solltemp = raum.solltemp
                offset = get_module_offsets(raum, exclude='heizflaechenoptimierung', as_dict=True)
                try:
                    del offset['heizflaechenoptimierung']
                except:
                    pass
                offset = sum(offset.values())

                ms, rls = has_ms_and_rls(raum)

                if ms:
                    temp = raum.get_mainsensor().get_wert()
                    if temp:
                        temp = temp[0]
                else:
                    temp = regparams.get('offset', 21.0)

                if temp is None:
                    logger.warning("%s|%s" % (raum.id, u"Status: Keine Raumtemperatur verfügbar, Berechnung wird übersprungen."))
                    continue

                number_average = 0.0
                i = 0
                for ssn, outs in regparams.get('rlsensorssn', dict()).items():
                    sensor = AbstractSensor.get_sensor(ssn)
                    if not sensor:
                        continue
                    last_rl = sensor.get_wert()
                    if last_rl:
                        number_average += float(last_rl[0])
                        i += 1
                if i == 0:
                    logger.warning("%s|%s" % (raum.id, u"Status: Keine Rücklaufsensoren angelegt, Berechnung wird übersprungen."))
                    continue
                rltemp_average = number_average / i

                if not rltemp_average:
                    logger.warning("%s|%s" % (raum.id, u"Status: Keine Werte für Rücklaufsensoren verfügbar, Berechnung wird übersprungen."))
                    continue

                at_window_start = rparams['time_start']
                at_window_end = rparams['time_end']
                hf_temp_plustwentyat = rparams['heating_element_start']
                hf_temp_minustwentyat = rparams['heating_element_end']
                if raum.get_spec_module_params('betriebsarten').get(str(raum.id), '-1') == '1':
                    activity_threshold_below_target = 0.0
                    activity_threshold_above_target = 0.0
                else:
                    activity_threshold_below_target = rparams['before']
                    activity_threshold_above_target = rparams['after']
                flushing_interval_minustwentyat = rparams['flushing_interval_start']
                flushing_interval_plustwentyat = rparams['flushing_interval_end']
                hf_temp_correction_per_degree_deviation = rparams['correction_heating_element']

                last_vals = ch.get('hfo_%s_%s' % (haus.id, raum.id))
                if last_vals is None:
                    hf_timer = None
                    hf_offset = 0
                    hf_step = 0
                    hf_mode = 0
                    flushing_interval = None
                else:  # get active timers
                    hf_timer = last_vals['timer']
                    hf_offset = last_vals['offset']
                    hf_step = last_vals['step']
                    hf_mode = last_vals.get('mode', -1)
                    flushing_interval = last_vals['flushing_interval']

                hf_raum_ziel = solltemp + offset

                try:
                    diff_temp = temp - hf_raum_ziel
                except:
                    # wird komischerweise nicht immer oben mit dem temp is None check gefangen
                    logger.warning("%s|%s" % (raum.id, u"Status: Keine Raumtemperatur verfügbar. Berechnung wird übersprungen."))
                    continue
                _hf_offset = None
                if diff_temp <= activity_threshold_below_target: # ON
                    new_mode = 1
                elif 0 >= diff_temp >= activity_threshold_below_target: # Overswing protection
                    new_mode = 2
                    #  Offset = (room-target-temperature - current-Room-temperature + 0.1K) * -1 // +*-=-
                    _hf_offset = ((hf_raum_ziel - temp) + 0.1) * -1
                elif 0 < diff_temp <= activity_threshold_above_target: # Underswing protection
                    new_mode = 3
                    #  Offset = (room-target-temperature - current-Room-temperature - 0.1K) * -1 // -*-=+
                    _hf_offset = ((hf_raum_ziel - temp) - 0.1) * -1
                elif diff_temp > activity_threshold_above_target: # OFF
                    new_mode = 4

                if new_mode != hf_mode:  # if mode changed, remove old timers
                    hf_step = 0
                    hf_offset = 0
                    hf_timer = None
                    flushing_interval = None

                logging.warning("new_mode == %s" % new_mode)

                rlsoll_offset = (hf_raum_ziel - temp) * hf_temp_correction_per_degree_deviation

                hf_calc_ts = now
                rlsoll = _get_rlsoll(raum, rparams, hf_raum_ziel)
                try:
                    rlsoll += rlsoll_offset
                except TypeError:  # rlsoll is None, probably. sollte man vielleicht loggen.
                    continue

                ch.set("rlsoll_%s" % raum.id, rlsoll)

                if new_mode == 1:
                    hf_timer = None
                    hf_offset = 0
                    hf_step = 1
                    logger.warning("%s|%s" % (raum.id, u"Modus 1: Schnellstmöglich aufheizen. Genius beheizt den Raum gerade maximal, um schnellstmöglich deine Wunschtemperatur zu erreichen."))

                elif new_mode == 4:
                    hf_timer = None
                    hf_offset = 0
                    hf_step = 8
                    logger.warning("%s|%s" % (raum.id, u"Modus 8: Schnellstmöglich abkühlen. Genius beheizt den Raum gerade nicht, um schnellstmöglich auf deine Wunschtemperatur abzukühlen."))

                else:
                    try:
                        at_window_end = min(at_window_end, 72)
                        forecast_now = wetter.get_10dayforecast(haus, int(at_window_end))
                        forecast = forecast_now[int(at_window_start):int(at_window_end)]
                        average = sum([float(f[1]) for f in forecast]) / float(len(forecast))
                        log_onAT(haus, raum, now, rparams, forecast_now, average)
                    except Exception as e:
                        logging.exception("message")
                        average = None

                    if not average:
                        logger.warning("%s|%s" % (raum.id, u"Status: Keine Außentemperaturvorhersage vorhanden. Berechnung wird übersprungen."))
                        continue

                    a = (flushing_interval_plustwentyat, 20)
                    b = (flushing_interval_minustwentyat, -20)
                    c = (10, average)
                    d = (600, average)
                    try:
                        flushing_interval, y = line_intersection((a, b), (c, d))
                    except Exception as e:
                        flushing_interval = None

                    if new_mode == 2:  # overswing protection
                        if hf_mode == 3 or hf_mode == 4:  # if comming from mode 3 or 4 switch to step 3, no timer should be active right now
                            hf_offset = 0
                            hf_step = 3
                            logger.warning("%s|%s" % (raum.id, u"Modus 3.n: Intelligente Aufheizphase. Genius ermittelt die aktuelle Fußbodentemperatur."))
                            hf_timer = now + timedelta(minutes=19)
                        elif isinstance(hf_timer, datetime):
                            if hf_step == 2 and hf_timer < (now + timedelta(seconds=10)):  # flushing timer is abgelaufen
                                hf_timer = now + timedelta(minutes=19)  # set new timer to 20 mins (19 mins?)
                                hf_offset = 0
                                hf_step = 3
                                logger.warning("%s|%s" % (raum.id, u"Modus 3.1: Intelligente Aufheizphase. Genius ermittelt die aktuelle Fußbodentemperatur."))
                            elif hf_step == 3 and hf_timer < (now + timedelta(seconds=9)):  # timer done start from top
                                logger.warning("%s|%s" % (raum.id, u"Modus 3.2: Intelligente Aufheizphase. Genius hat die Ermittlung der Fußbodentemperatur abgeschlossen und startet neuen Zyklus."))
                                time.sleep(1.2)  # give next log entry another time
                                # check RLT > target
                                if rltemp_average > rlsoll:  # bild mode 2
                                    hf_offset = _hf_offset
                                    hf_step = 2
                                    flushing_interval /= 2
                                    hf_timer = now + timedelta(minutes=flushing_interval)
                                    logger.warning("%s|%s" % (raum.id, u"Modus 2: Intelligente Aufheizphase. Die Fußbodentemperatur ist zu hoch. Um zu verhindern, dass es wärmer wird als gewünscht, unterbricht Genius den Heizvorgang bis %s." % hf_timer.strftime("%H:%M %d.%m.%Y")))
                                # start from top
                                else:
                                    hf_timer = None
                                    hf_offset = 0
                                    hf_step = 4
                                    logger.warning("%s|%s" % (raum.id, u"Modus 4: Intelligente Aufheizphase. Genius hat ermittelt, dass die Fussbodentemperatur nicht übermäßig hoch ist. Der Raum wird weiter mit voller Leistung beheizt, damit schnellstmöglich deine Wunschtemperatur erreicht wird."))
                            else:
                                # logger.warning("%s|%s" % (raum.id, u"Modus 2, Schritt %s*. Offset=%s, Timer=%s." % (hf_step, hf_offset, hf_timer.strftime("%H:%M %d.%m.%Y"))))
                                pass
                        else:
                            # check RLT > target
                            if rltemp_average > rlsoll:  # bild mode 2
                                hf_offset = _hf_offset
                                hf_step = 2
                                flushing_interval /= 2
                                hf_timer = now + timedelta(minutes=flushing_interval)
                                logger.warning("%s|%s" % (raum.id, u"Modus 2: Intelligente Aufheizphase. Die Fußbodentemperatur ist zu hoch. Um zu verhindern, dass es wärmer wird als gewünscht, unterbricht Genius den Heizvorgang bis %s." % hf_timer.strftime("%H:%M %d.%m.%Y")))
                                # start from top
                            else:
                                hf_timer = None
                                hf_offset = 0
                                hf_step = 4
                                logger.warning("%s|%s" % (raum.id, u"Modus 4: Intelligente Aufheizphase. Genius hat ermittelt, dass die Fussbodentemperatur nicht übermäßig hoch ist. Der Raum wird weiter mit voller Leistung beheizt, damit schnellstmöglich deine Wunschtemperatur erreicht wird."))
                    if new_mode == 3:  # underswing protection
                        if hf_mode == 1 or hf_mode == 2:  # if comming from mode 1 or 2 switch to step 6, no timer should be active right now
                            hf_offset = 0
                            hf_step = 6
                            if hf_timer is not None:
                                logger.warning("%s|%s" % (raum.id, u"Modus 6.n: Intelligente Abkühlphase. Die Fussbodentemperatur ist warm genug. Genius startet kein zusätzliches vorwärmen des Fussbodens. Nächste Prüfung um %s." % hf_timer.strftime("%H:%M %d.%m.%Y")))
                            else:
                                logger.warning("%s|%s" % (raum.id, u"Modus 6.n: Intelligente Abkühlphase. Die Fussbodentemperatur ist warm genug. Genius startet kein zusätzliches vorwärmen des Fussbodens."))
                            hf_timer = now + timedelta(minutes=flushing_interval)
                        if isinstance(hf_timer, datetime):
                            if hf_timer < (now + timedelta(seconds=10)):
                                # after flushing_interval timer
                                if hf_step == 6:
                                    logger.warning("%s|%s" % (raum.id, u"Modus 6: Intelligente Abkühlphase. Die Fußbodentemperatur war zuletzt warm genug. Genius hat den Fußboden deshalb nicht zusätzlich vorgewärmt. Die Wartezeit ist nun vorüber und es startet eine neue Prüfung."))
                                    # go directly to step 5
                                    hf_offset = _hf_offset
                                    hf_timer = now + timedelta(minutes=19)
                                    hf_step = 5
                                    time.sleep(1.2)  # give next log entry another time
                                    logger.warning("%s|%s" % (raum.id, u"Modus 5: Intelligente Abkühlphase. Genius ermittelt die aktuelle Fußbodentemperatur um herauszufinden, ob der Fußboden etwas vorgewärmt werden sollte."))

                                else:  # current step = 5 or 7
                                    if rltemp_average > rlsoll:
                                        hf_offset = 0
                                        hf_step = 6
                                        hf_timer = now + timedelta(minutes=flushing_interval)
                                        logger.warning("%s|%s" % (raum.id, u"Modus 6: Intelligente Abkühlphase. Die Fussbodentemperatur ist warm genug. Genius startet kein zusätzliches vorwärmen des Fussbodens. Nächste Prüfung um %s." % hf_timer.strftime("%H:%M %d.%m.%Y")))
                                    else:
                                        hf_offset = _hf_offset
                                        hf_step = 7
                                        hf_timer = now + timedelta(minutes=9)
                                        logger.warning("%s|%s" % (raum.id, u"Modus 7: Intelligente Abkühlphase. Der Raum ist noch zu warm aber die Fussbodentemperatur ist recht niedrig. Genius wärmt den Fußboden für dich etwas vor."))
                            else:
                                # logger.warning("%s|%s" % (raum.id, u"Modus 3, Schritt %s*. Offset=%s, Timer=%s." % (hf_step, hf_offset, hf_timer.strftime("%H:%M %d.%m.%Y"))))
                                pass

                        else:  # underswing protection - first init
                            hf_offset = _hf_offset
                            hf_timer = now + timedelta(minutes=19)
                            hf_step = 5
                            logger.warning("%s|%s" % (raum.id, u"Modus 5: Intelligente Abkühlphase. Genius ermittelt die aktuelle Fußbodentemperatur um herauszufinden, ob der Fußboden etwas vorgewärmt werden sollte."))

                if new_mode == 1 or new_mode == 4:
                    try:
                        log_onAT(haus, raum, now, rparams, None, None)
                    except Exception as e:
                        logging.exception("message")
                if rlsoll is not None:
                    # nur loggen, wenn der wert auch berechnet wurde. ansonsten ist rlsoll ja auch irrelevant.
                    log_parameters(rlsoll, rparams, raum, haus, now)

                hf_mode = new_mode
                vals = {'timer': hf_timer, 'offset': hf_offset, 'step': hf_step, 'mode': hf_mode,
                        'raumziel': hf_raum_ziel, 'ts': hf_calc_ts, 'flushing_interval': flushing_interval}
                ch.set("hfo_%s_%s" % (haus.id, raum.id), vals, 3600)  # todo 600

                if for_raum:
                    return hf_offset


@shared_task(base=CeleryErrorLoggingTask)
def calc_hftemps(for_raum=None):

    haus = Haus.objects.first()
    ahapro = haus.get_spec_module_params('aha_pro')
    ahapro_check = ahapro.get('aha_pro_status', False)
    if ahapro_check and ahapro_check == 'true':
        logger.warning("%s|%s" % ("Systemstatus", u"RFR-PRO wird nicht ausgeführt, da AHA-PRO aktiv ist."))
        return

    open('maximaldurchfluss/dhma_log.txt', 'w').close()
    for haus in Haus.objects.all():
        # if 'heizflaechenoptimierung' not in haus.get_modules():
        #     continue
        hparams = haus.get_module_parameters()
        gateways = list(chain(Gateway.objects.filter(haus=haus), KNXGateway.objects.filter(haus=haus)))
        initial_state_is_on = hparams.get('aha', {}).get('initial_state_on', False)
        if hparams.get('aha', {}).get('is_active', False) or (hparams.get('aha', {}).get('is_active', False) and not initial_state_is_on):
            for gateway in gateways:
                if (isinstance(gateway, Gateway) and "6.00" > gateway.version >= "5.00") or isinstance(gateway, KNXGateway):
                    for ausgang in gateway.ausgaenge.all():
                        set_all_to_max_open(gateway, ausgang)
            continue
        dhma_inputs = DHMAParams()
        dhma_inputs.belowbounds = {}
        dhma_inputs.inbounds = {}
        dhma_inputs.activity_threshold = {}
        for gateway in gateways:
            if (isinstance(gateway, Gateway) and "6.00" > gateway.version >= "5.00") or isinstance(gateway, KNXGateway):
                if u'maximaldurchfluss' in haus.get_modules():
                    dhma_inputs.belowbounds[gateway] = {}
                    dhma_inputs.inbounds[gateway] = {}
                    dhma_inputs.activity_threshold[gateway] = {}
                for ausgang in gateway.ausgaenge.all():
                    try:
                        raum = ausgang.regelung.get_raum()
                        ms = raum.get_mainsensor()
                        mstemp = ms.get_wert()
                        mstemp = mstemp[0]
                        hfo_pro_gw = HFOProGateway(raum, mstemp, gateway, ausgang)
                        hfo_pro_gw.get_dhma_params_reset()
                    except:
                        pass
                for ausgang in gateway.ausgaenge.all():
                    raum = ausgang.regelung.get_raum()
                    if raum is None or ausgang.regelung.regelung not in ['ruecklaufregelung', 'zweipunktregelung']:
                        continue
                    if initial_state_is_on:
                        logger.warning("%s|%s" % (raum.id, u"Status: Initialzustand AHA: Öffne alle Ausgänge maximal."))
                        set_all_to_max_open(gateway, ausgang)
                        continue

                    betriebs_mode = raum.get_betriebsart()
                    if betriebs_mode == '1':
                        logger.warning("%s|%s" % (raum.id, u"Status: Betriebsart auf Modus 'kühlen' eingestellt. Heizflächenoptimierung wird für diesen Raum nicht ausgeführt."))

                    if isinstance(gateway, KNXGateway):
                        if not ausgang.is010v():
                            continue

                    ms = raum.get_mainsensor()
                    try:
                        mstemp = ms.get_wert()
                        mstemp = mstemp[0]
                    except (TypeError, AttributeError):
                        try:
                            raum_params = raum.get_module_parameters()
                            hfo_room_name = 'hfo_configurator_%s' % raum.id
                            hfo_configurator_data = raum_params.get(hfo_room_name, {})
                            roomsensor_failure_mode = hfo_configurator_data.get('roomsensor_failure_mode', True)

                            if roomsensor_failure_mode:
                                outside_temp = _get_weather_forecast_average(raum, get_strategy_mode_values(raum))
                                raum_offsets = get_module_offsets(raum, exclude='heizflaechenoptimierung', as_dict=True)
                                raum_offset = sum(raum_offsets.values())

                                try:
                                    mode = BaseMode.get_active_mode_raum(raum)
                                    raum.refresh_from_db()
                                except Exception as e:
                                    logging.exception("hfo: exception getting mode for raum %s" % raum.id)
                                    mode = None

                                if mode is None or mode.get('use_offsets', False):
                                    raum_ziel = raum.solltemp + raum_offset
                                else:
                                    raum_ziel = raum.solltemp

                                if raum_ziel - outside_temp > hfo_configurator_data.get('emergency_mode_threshold', 7.0):
                                    mstemp = raum_ziel
                                    logger.warning("%s|%s" % (raum.id, u"Status: Es steht keine Raumtemperatur zur Verfügung. Die Raumheizung erfolgt entsprechend der Rücklauftemperaturen."))
                                else:
                                    logger.warning("%s|%s" % (raum.id, u"Status: Es liegt keine Raumtemperatur vor. Aufgrund der gegenwärtig ausreichenden Außentemperatur wird auf eine Beheizung des Raums verzichtet."))
                                    for ausgangno in ausgang.ausgang.split(','):
                                        out = int(ausgangno.strip()) if str(ausgangno).strip().isdigit() else ausgangno.strip()
                                        ch.set("%s_%s" % (gateway.name, out), "<%s>" % (0))
                                    continue
                            else:
                                logger.warning("%s|%s" % (raum.id, u"Status: Es liegt keine Raumtemperatur vor. Aufgrund der gegenwärtig ausreichenden Außentemperatur wird auf eine Beheizung des Raums verzichtet."))
                                continue
                        except:
                            continue

                    hfo_pro_gw = HFOProGateway(raum, mstemp, gateway, ausgang)
                    hfo_pro_gw.run_hfo(naive_modus=False) # TODO: Create Button with naive_modus TRUE/FALSE and give it as a parameter "naive_modus" to run_hfo
                    if u'maximaldurchfluss' in haus.get_modules():
                        _belowbounds, _inbounds, _activity_threshold = hfo_pro_gw.get_dhma_params()
                        dhma_inputs.belowbounds[gateway].update(_belowbounds)
                        dhma_inputs.inbounds[gateway].update(_inbounds)
                        dhma_inputs.activity_threshold[gateway].update(_activity_threshold)
        if u'maximaldurchfluss' in haus.get_modules():
            dhma_creator = DHMACreator(haus, gateways, dhma_inputs)
            dhma_creator.run_dhma()
            log_dhma(gateways)
    exec_ein_mode()

def exec_ein_mode():
    raums = Raum.objects.all()
    for raum in raums:
        #try:
            betriebs_mode = raum.get_betriebsart()
            if betriebs_mode == '3':
                ausgang = raum.regelung.get_ausgang()
                if ausgang:
                    for ausgangno in ausgang.ausgang.split(','):
                        gateway = Gateway.objects.filter(id=ausgang.gateway_id).first()
                        if gateway:
                            if 6 >= float(str(gateway.version)) >= 5:
                                params = gateway.get_parameters()
                                max_open = params.get('max_opening', dict((i, 100) for i in range(1, 16)))
                                dhma_max_open = params.get('dhma_max_opening', dict((i, 1) for i in range(1, 16)))
                                max_open_room = max_open.get(int(ausgangno), u'99')
                                dhma_max_open_room = dhma_max_open.get(int(ausgangno), u'99')
                                out = max(float(str(max_open_room)), float(str(dhma_max_open_room)))
                                ch.set("%s_%s" % (gateway.name, ausgangno.strip()), "<%s>" % out)
                            else:
                                ch.set("%s_%s" % (gateway.name, ausgangno.strip()), "<%s>" % 1)
        #except:
         #   pass

# TODO: delete this function after creating own logmonitor for dhma
def log_dhma(gateways):
    for gateway in gateways:
        params = gateway.get_parameters()
        dhma_max_open = params.get('dhma_max_opening', dict((i, 100) for i in range(1, 16)))
        for ausgang in gateway.ausgaenge.all():
            raum = ausgang.regelung.get_raum()
            if raum is None or ausgang.regelung.regelung not in ['ruecklaufregelung', 'zweipunktregelung']:
                continue
            betriebs_mode = raum.get_betriebsart()
            for ausgangno in ausgang.ausgang.split(','):
                out = int(ausgangno.strip()) if str(ausgangno).strip().isdigit() else ausgangno.strip()
                dhma_max_open_out = dhma_max_open.get(out)
                if betriebs_mode != '1':
                    logger.warning("%s|%s" % (raum.id, u"Status (Ausgang %s): Dynamischer hydraulischer Maximaldurchfluss "
                                                       u"hat die maximal mögliche Öffnung für den nächsten Durchlauf berechnet. "
                                                       u"Ergebnis: %s%% ") % (out, dhma_max_open_out))

def set_all_to_max_open(gateway, ausgang):
    params = gateway.get_parameters()
    max_open = params.get('max_opening', dict((i, 100) for i in range(1, 16)))
    rb_min = max(params.get('regelbereich_min', 0), 0)
    rb_max = min(params.get('regelbereich_max', 100), 99)
    for ausgangno in ausgang.ausgang.split(','):
        val = regelbereich(int(max_open[int(ausgangno.strip()) if str(ausgangno).strip().isdigit() else ausgangno.strip()]), rb_min, rb_max)
        ch.set("%s_%s" % (gateway.name, ausgangno.strip()), "<%s>" % val)


class ActivityThreshold:
    activity_threshold_below_target = None
    activity_threshold_above_target = None


class Mode:
    ON = 1
    OVERSWING = 2 # Overswing Protection: Temperature lower than Target
    UNDERSWING = 3 # Underswing Protection: Temperature higher than Target
    OFF = 4


class STEP:
    FLUSHING_TIMER = 1
    DETERMINE_RLT = 2


class GatewayParameter:
    rb_min = None
    rb_max = None
    max_open = None
    aha_max_open = None


class CacheParams:
    cnt_mode = None
    cnt_step = {}
    timer = {}
    now = None
    flushing_interval = None


class DHMAParams:
    inbounds = {}
    belowbounds = {}
    activity_threshold = {}


# TODO: Raumziel und RL-Ziel Berechnung für V4 und V5 Gateways gleich
class HFOProGateway:
    def __init__(self, raum, mstemp, gateway, ausgang):
        self.raum = raum
        self.mstemp = mstemp
        self.gateway = gateway
        self.ausgang = ausgang
        self.hfo_params = get_strategy_mode_values(raum)
        self.activity_thresholds = ActivityThreshold()
        self.raum_ziel = self.calc_raum_ziel()
        self.rl_ziel = self.calc_rl_ziel()
        self.last_run_params = ch.get("hfo_%s_%s" % (gateway.haus_id, raum.id)) or {}
        self.cache_params = CacheParams()
        self.gw_params = GatewayParameter()
        self.betriebs_mode = None
        self.dhma_params = DHMAParams()
        self.log_det_rl = True

    def run_hfo(self, naive_modus=False):
        self.set_activity_thresholds()
        self.run_ki_code()
        self.calc_needed_params()
        if self.rl_ziel is None or naive_modus:
            if naive_modus:
                logger.warning("%s|%s" % (self.raum.id, u"Status: Es wird naiv geregelt, da so eingestellt."))
            else:
                logger.warning("%s|%s" % (self.raum.id, u"Status: Da keine Außentemperatur vorhanden ist, wird naiv geregelt."))
            self.run_naive_hfo()
            return
        # Start of hfo algorithm:
        if self.mstemp <= self.raum_ziel + self.activity_thresholds.activity_threshold_below_target: # ON
            self.run_on_mode()
        elif self.mstemp > self.raum_ziel + self.activity_thresholds.activity_threshold_above_target: # OFF
            self.run_off_mode()
        else: # Aktivitätsschwellen
            self.run_activity_threshold()
        self.set_cache_and_log_params()

    def run_naive_hfo(self):
        if self.mstemp <= self.raum_ziel: # ON
            self.run_on_mode(naive_mode=True) # TODO: überprüfen ob das Sinn macht
        elif self.mstemp > self.raum_ziel: # OFF
            self.run_off_mode(naive_mode=True)

    def calc_needed_params(self):
        params = self.gateway.get_parameters()
        self.gw_params.aha_max_open = params.get('max_opening', dict((i, 100) for i in range(1, 16)))
        self.gw_params.max_open = params.get('dhma_max_opening', self.gw_params.aha_max_open)
        self.gw_params.rb_min = max(params.get('regelbereich_min', 0), 0)
        self.gw_params.rb_max = min(params.get('regelbereich_max', 100), 99)
        self.betriebs_mode = self.raum.get_betriebsart()
        self.cache_params.timer = self.last_run_params.get('timer')
        if self.cache_params.timer is not dict:
            self.cache_params.timer = {}
        self.cache_params.now = datetime.now(pytz.timezone('Europe/Berlin'))
        self.cache_params.flushing_interval = _get_flushing_interval(self.raum, get_strategy_mode_values(self.raum))

    def set_activity_thresholds(self):
        if self.raum.regelung.regelung == 'zweipunktregelung':
            self.activity_thresholds.activity_threshold_below_target = 0
            self.activity_thresholds.activity_threshold_above_target = 0
        else:
            if self.raum.get_spec_module_params('betriebsarten').get(str(self.raum.id), '-1') == '1':
                self.activity_thresholds.activity_threshold_below_target = 0.0
                self.activity_thresholds.activity_threshold_above_target = 0.0
            else:
                self.activity_thresholds.activity_threshold_below_target = self.hfo_params.get('before', -0.5)  # 'before' ist negativ
                self.activity_thresholds.activity_threshold_above_target = self.hfo_params.get('after', 0.5)  # 'after' ist ein positiver wert

    def run_ki_code(self):
       # Anmerkung: aus simple_bounded_contol kopiert, nicht überprüft
        if fabs(self.raum_ziel - self.hfo_params.get('points_for', self.raum_ziel)) > 0.25:
            try:
                from ki.ahk import adjust_ondemand
                adjust_ondemand.delay(self.raum, self.raum_ziel)
            except Exception:
                logging.exception("exc raumid %s" % self.raum.id)

    def calc_raum_ziel(self):
        raum_offsets = get_module_offsets(self.raum, exclude='heizflaechenoptimierung', as_dict=True)
        raum_offset = sum(raum_offsets.values())
        try:
            mode = BaseMode.get_active_mode_raum(self.raum)
            self.raum.refresh_from_db()
        except Exception as e:
            logging.exception("hfo: exception getting mode for raum %s" % self.raum.id)
            mode = None
        if mode is None or mode.get('use_offsets', False):
            raum_ziel = self.raum.solltemp + raum_offset
        else:
            raum_ziel = self.raum.solltemp
        return raum_ziel

    def calc_rl_ziel(self):
        hf_temp_correction_per_degree_deviation = self.hfo_params['correction_heating_element']
        rlsoll_offset = (self.raum_ziel - self.mstemp) * hf_temp_correction_per_degree_deviation
        rlsoll = _get_rlsoll(self.raum, self.hfo_params, zielit=self.raum_ziel)
        try:
            rlsoll += rlsoll_offset #if 'ki' not in self.raum.get_module() else 0
        except:  # rlsoll kann ja None sein
            pass # dann soll naive Regelung durchgeführt werden
        ch.set("rlsoll_%s" % self.raum.id, rlsoll)
        return rlsoll

    def run_on_mode(self, naive_mode=False):
        if not naive_mode: # wenn der naive Modus an ist, muss kein Moduswechsel durchgeführt werden...
            self.cache_params.cnt_mode = Mode.ON
        for ausgangno in self.ausgang.ausgang.split(','):
            out = int(ausgangno.strip()) if str(ausgangno).strip().isdigit() else ausgangno.strip()
            self.cache_params.timer[out] = None
            # set new opening to max_opening or dhma max_opening:
            val = regelbereich(int(self.gw_params.max_open[out]), self.gw_params.rb_min, self.gw_params.rb_max)
            self.dhma_params.belowbounds[out] = val
            ch.set("%s_%s" % (self.gateway.name, out), "<%s>" % val)
            logger.warning("%s|%s" % (self.raum.id, u"Ausgang %s auf %s%%. Schnelles Aufheizen." % (out, val)))
        if self.betriebs_mode != '1':
            logger.warning("%s|%s" % (self.raum.id, u"Modus %s: Schnellstmöglich aufheizen. Genius beheizt den Raum gerade maximal, um schnellstmöglich deine Wunschtemperatur zu erreichen.")
                           % self.cache_params.cnt_mode)

    def run_off_mode(self, naive_mode=False):
        if not naive_mode:
            self.cache_params.cnt_mode = Mode.OFF
        val = max(self.gw_params.rb_min, 0)
        for ausgangno in self.ausgang.ausgang.split(','):
            out = int(ausgangno.strip()) if str(ausgangno).strip().isdigit() else ausgangno.strip()
            self.cache_params.timer[out] = None
            self.dhma_params.inbounds[out] = val
            ch.set("%s_%s" % (self.gateway.name, out), "<%s>" % val)
            logger.warning("%s|%s" % (self.raum.id, u"Ausgang %s auf %s%%. Raumtemperatur = %.2f, Ziel = %.2f."
                          % (ausgangno.strip(), val, self.mstemp, self.raum_ziel)))
        if self.betriebs_mode != '1':
            logger.warning("%s|%s" % (self.raum.id, u"Modus %s: Schnellstmöglich abkühlen. Genius beheizt den Raum gerade nicht, um schnellstmöglich auf deine Wunschtemperatur abzukühlen.")
                           % self.cache_params.cnt_mode)

    def run_activity_threshold(self):
        last_mode = self.last_run_params.get('mode', None)
        if self.mstemp < self.raum_ziel: # unter der Zieltemperatur
            self.cache_params.cnt_mode = Mode.OVERSWING
            if self.betriebs_mode != '1':
                logger.warning("%s|%s" % (self.raum.id, u"Modus 2.1: Modus-Check. Ergebnis: Überschwingschutz."))
        else: # über der Zieltemperatur
            self.cache_params.cnt_mode = Mode.UNDERSWING
            if self.betriebs_mode != '1':
                logger.warning("%s|%s" % (self.raum.id, u"Modus 3.1: Modus-Check. Ergebnis: Unterschwingschutz."))
        if self.betriebs_mode != '1':
            logger.warning("%s|%s" % (self.raum.id, u"Modus %s.2: Hauptmodus Change-Check wird ausgeführt.") % self.cache_params.cnt_mode)
        mode_has_changed = last_mode != self.cache_params.cnt_mode
        if mode_has_changed and self.betriebs_mode != '1':
            logger.warning("%s|%s" % (self.raum.id, u"Modus %s.3: Change-Check-Ergebnis: Ist-RT=%s; Ziel-RT=%s; Der Hauptmodus hat gewechselt. Nächster Schritt ist %s.4.") % (self.cache_params.cnt_mode, self.mstemp, self.raum_ziel, self.cache_params.cnt_mode))
        elif self.betriebs_mode != '1':
            logger.warning("%s|%s" % (self.raum.id, u"Modus %s.3: Change-Check-Ergebnis: Ist-RT=%s; Ziel-RT=%s; Der Hauptmodus hat NICHT gewechselt. Nächster Schritt ist %s.5.") % (self.cache_params.cnt_mode, self.mstemp, self.raum_ziel, self.cache_params.cnt_mode))
        if mode_has_changed: # if mode change
            if self.betriebs_mode != '1':
                logger.warning("%s|%s" % (self.raum.id, u"Modus %s.4: Aufgrund Wechsel des Hauptmodus: Reset aller Timer und Prozesse aus vorigen Durchläufen.") % self.cache_params.cnt_mode)
            self.last_run_params['timer'] = {} # reset timer
            self.last_run_params['step'] = {} # reset step
        for ausgangno in self.ausgang.ausgang.split(','):
            out = int(ausgangno.strip()) if str(ausgangno).strip().isdigit() else ausgangno.strip()
            try:
                cnt_opening = ch.get("%s_%s" % (self.gateway.name, out))
                cnt_opening = float(cnt_opening[1:-1])
            except:
                cnt_opening = 0
            if mode_has_changed and cnt_opening == 0:
                self.mode_changed(out) # determine rlt
            else: # mode has not changed
                timer = self.last_run_params.get('timer', {})
                if out in timer:
                    timer = timer[out]
                else:
                    timer = None
                if timer is None:
                    if self.betriebs_mode != '1':
                        logger.warning("%s|%s" % (self.raum.id, u"Modus %s.5 (Ausgang %s): Own-Timer-Active Check. Es ist kein Timer aktiv. Nächster Schritt ist %s.7.") % (self.cache_params.cnt_mode, out, self.cache_params.cnt_mode))
                    self.run_normal_algo(out, False)
                else: # timer exists
                    last_step = self.last_run_params.get('step', {})
                    if out in last_step:
                        last_step = last_step[out]
                    else:
                        last_step = None
                    self.check_if_timer_expired(last_step, out, timer)

    def mode_changed(self, out):
        if self.cache_params.timer is None:
            self.cache_params.timer = {}
        self.cache_params.timer[out] = self.cache_params.now + timedelta(minutes=19)
        self.cache_params.cnt_step[out] = STEP.DETERMINE_RLT
        self.determine_rlt(out)

    def check_if_timer_expired(self, last_step, out, timer):
        if last_step == STEP.FLUSHING_TIMER:
            if timer < (self.cache_params.now + timedelta(seconds=10)): # wenn flushing interval timer aktiv und abgelaufen
                self.cache_params.cnt_step[out] = STEP.DETERMINE_RLT
                self.cache_params.timer[out] = self.cache_params.now + timedelta(minutes=19)
                self.determine_rlt(out) #ermittle die RLT neu
                if self.betriebs_mode != '1':
                    logger.warning("%s|%s" % (self.raum.id, u"Modus %s.5 (Ausgang %s): Own-Timer-Active Check. Das Spülintervall ist abgelaufen.") % (self.cache_params.cnt_mode, out))
            else:
                self.renew_cache(last_step, timer, out)
                if self.betriebs_mode != '1':
                    logger.warning("%s|%s" % (self.raum.id, u"Modus %s.5 (Ausgang %s): Own-Timer-Active Check. Es ist der Spülintervall-Timer aktiv und läuft "
                                                            u"noch bis %s. Durchlauf wird deshalb beendet.") % (self.cache_params.cnt_mode, out, timer.strftime("%Y-%m-%d %H:%M:%S")))
        elif last_step == STEP.DETERMINE_RLT:
            if timer < (self.cache_params.now + timedelta(seconds=9)): # RLT-ermitteln timer abgelaufen
                if self.betriebs_mode != '1':
                    logger.warning("%s|%s" % (self.raum.id, u"Modus %s.5 (Ausgang %s): Own-Timer-Active Check. Heating-Element-Check-Timer abgelaufen.") % (self.cache_params.cnt_mode, out))
                self.run_normal_algo(out, True)
            else:
                self.renew_cache(last_step, timer, out)
                if self.betriebs_mode != '1':
                    logger.warning("%s|%s" % (self.raum.id, u"Modus %s.5 (Ausgang %s): Own-Timer-Active Check. Es ist der Heating-Element-Check-Timer aktiv "
                                                            u"und läuft bis %s. Durchlauf wird deshalb beendet.") % (self.cache_params.cnt_mode, out, timer.strftime("%Y-%m-%d %H:%M:%S")))
        else:
            self.cache_params.timer[out] = timer

    def renew_cache(self, last_step, timer, out):
        # Erneuere den cache, damit die bisherigen Parameter nicht verloren gehen
        self.cache_params.cnt_step[out] = last_step
        self.cache_params.timer[out] = timer

    def run_normal_algo(self, out, timer_expired):
        new_out = self.calc_new_opening(out)
        if new_out is None: # wenn RLT None ist
            logger.warning("%s|%s" % (self.raum.id, u"Status: Da kein RLT-Wert vorhanden ist, wird naiv geregelt."))
            self.run_naive_hfo()
        elif new_out == 0:
            if self.cache_params.cnt_mode == Mode.OVERSWING:
                self.cache_params.flushing_interval /= 2
                if self.betriebs_mode != '1':
                    logger.warning("%s|%s" % (self.raum.id, u"Modus %s.10 (Ausgang %s): Minimale Ventilöffnung unterschritten. Starte Timer mit %s Minuten (Spülintervall / 2).") % (self.cache_params.cnt_mode, out, self.cache_params.flushing_interval))
            else:
                if self.betriebs_mode != '1':
                    logger.warning("%s|%s" % (self.raum.id, u"Modus %s.10 (Ausgang %s): Minimale Ventilöffnung unterschritten. Starte Timer mit %s Minuten (Spülintervall).") % (self.cache_params.cnt_mode, out, self.cache_params.flushing_interval))
            self.cache_params.cnt_step[out] = STEP.FLUSHING_TIMER
            self.cache_params.timer[out] = self.cache_params.now + timedelta(minutes=self.cache_params.flushing_interval)
        elif timer_expired:
            self.cache_params.cnt_step[out] = None
            self.cache_params.timer[out] = None

    def determine_rlt(self, out):
        if self.cache_params.cnt_mode == Mode.OVERSWING:
            val = regelbereich(int(self.gw_params.max_open[out]), self.gw_params.rb_min, self.gw_params.rb_max)
        else: # UNDERSWING Mode
            thirty_percent_aha = int(int(self.gw_params.aha_max_open[out]) * 0.3) # TODO: überprüfen ob int gut ist evtl runden
            val = regelbereich(max(5, thirty_percent_aha), self.gw_params.rb_min, self.gw_params.rb_max)
        self.dhma_params.inbounds[out] = val
        self.dhma_params.activity_threshold[out] = val
        ch.set("%s_%s" % (self.gateway.name, out), "<%s>" % val)
        if self.betriebs_mode != '1':
            logger.warning("%s|%s" % (self.raum.id, u"Modus %s.6 (Ausgang %s): 20 Minuten Heating-Element-Check mit Ventilöffnung %s.") % (self.cache_params.cnt_mode, out, val))
            logger.warning("%s|%s" % (self.raum.id, u"Ausgang %s auf %s%%. Spülen." % (out, val)))

    def calc_new_opening(self, out):
        try:
            cnt_opening = ch.get("%s_%s" % (self.gateway.name, out))
            cnt_opening = float(cnt_opening[1:-1])
        except:
            cnt_opening = 0 #int(self.gw_params.max_open[out])
        last_rl = self.get_last_rl(out)
        if last_rl is None:
            return None # None wird in run_normal_algo abgefangen und naive regelung läuft
        dev_rl = self.rl_ziel - last_rl # wird negativ wenn zu warm im Raum, sodass er runterregelt
        if self.betriebs_mode != '1':
            logger.warning("%s|%s" % (self.raum.id, u"Modus %s.7 (Ausgang %s): Rücklauftemperatur-Check. Ziel-RLT=%s °C; Ist-RLT=%s °C. "
                                                    u"Die Rücklauftemperatur-Differenz ist im Moment %s °C.") % (self.cache_params.cnt_mode, out, self.rl_ziel, last_rl, dev_rl))
        raum_params = self.raum.get_module_parameters()
        hfo_room_name = 'hfo_configurator_%s' % self.raum.id
        hfo_configurator_data = raum_params.get(hfo_room_name, {})

        change_factor_gain = hfo_configurator_data.get('change_factor_gain', 0.1)
        max_change_factor = hfo_configurator_data.get('max_change_factor', 20)
        min_valve_threshold = hfo_configurator_data.get('min_valve_threshold', 5)
        min_valve_value = hfo_configurator_data.get('min_valve_value', 0)

        change_factor = change_factor_gain * dev_rl * cnt_opening
        if abs(change_factor) > max_change_factor:
            change_factor = max_change_factor if change_factor > 0 else - max_change_factor
        val = cnt_opening + change_factor
        if val <= min_valve_threshold:
            val = min_valve_value
            if self.betriebs_mode != '1':
                logger.warning("%s|%s" % (self.raum.id, u"Modus %s.9 (Ausgang %s): Minimum-opening-check: TRUE (die neue Ventiloeffnung ist nun kleiner <5%%). Der Stellantrieb wird deshalb komplett geschlossen und der Timer gestartet.") % (self.cache_params.cnt_mode, out))
        val = regelbereich(min(val, int(self.gw_params.max_open[out])), self.gw_params.rb_min, self.gw_params.rb_max)
        self.dhma_params.inbounds[out] = val
        self.dhma_params.activity_threshold[out] = val
        ch.set("%s_%s" % (self.gateway.name, out), "<%s>" % val)
        if self.betriebs_mode != '1':
            logger.warning("%s|%s" % (self.raum.id, u"Ausgang %s auf %s%%. RL-Temperatur = %.2f, RL-Ziel = %s." % (out, val, last_rl, self.rl_ziel)))
            logger.warning("%s|%s" % (self.raum.id, u"Modus %s.8 (Ausgang %s): Ziel-RLT=%s °C; Ist-RLT=%s °C; Aktuelle Ventilöffnung: %s%%. Neue Ventiloeffnung: %s%%.")
                           % (self.cache_params.cnt_mode, out, self.rl_ziel, last_rl, round(cnt_opening, 2), round(val, 2)))
        return val

    def get_last_rl(self, out):
        regparams = self.raum.regelung.get_parameters()
        for ssn, outs in regparams['rlsensorssn'].items():
            if out in outs:
                rlsensor = AbstractSensor.get_sensor(ssn)
                last_rl = None
                if rlsensor and len(outs):
                    try:
                        last_rl = rlsensor.get_wert()[0]
                    except TypeError:
                        if self.betriebs_mode != '1':
                            logger.warning("%s|%s" % (self.raum.id, u"Status: Keine Rücklauftemperatur für Sensor %s vorhanden. "
                                                                        u"Berechnung wird übersprungen." % rlsensor.name))
                        continue
                return last_rl

    def set_cache_and_log_params(self):
        ch.set("hfo_%s_%s" % (self.gateway.haus_id, self.raum.id),
               {'mode': self.cache_params.cnt_mode,
                'step': self.cache_params.cnt_step,
                'timer': self.cache_params.timer,
                'ts': self.cache_params.now,
                'raumziel': self.raum_ziel,
                'calc_hftemps': True,
                'flushing_interval': int(self.cache_params.flushing_interval) if self.cache_params.flushing_interval is not None else self.cache_params.flushing_interval,
                'offset': 0.0
                })
        log_parameters(self.rl_ziel, self.hfo_params, self.raum, None, self.cache_params.now)

    def get_dhma_params(self):
        return self.dhma_params.belowbounds, self.dhma_params.inbounds, self.dhma_params.activity_threshold

    def get_dhma_params_reset(self):
        self.dhma_params.belowbounds.clear()
        self.dhma_params.inbounds.clear()
        self.dhma_params.activity_threshold.clear()
        return self.dhma_params.belowbounds, self.dhma_params.inbounds, self.dhma_params.activity_threshold


def regelbereich(val, min_rb, max_rb):

    if val < 0:
        return 0
    elif 0 <= val < min_rb:
        return min_rb
    elif val > max_rb:
        return min(99, max_rb)
    else:
        return val


def _get_weather_forecast_average(raum, hfo_params):

    # todo die forecasts koennte man auch zwischenspeichern fuer jeden shared_task aufruf
    # todo weil dann muss man die wetter.get_10dayforecast nicht fuer jeden raum/ausgang einzeln neu aufrufen
    # todo weil sich fenster ja meistens wahrscheinlich identisch sind

    at_window_start = hfo_params.get('time_start', 0)
    at_window_end = hfo_params.get('time_end', 5)

    msg = u""
    try:
        at_window_end = min(at_window_end, 72)
        # forecast = wetter.get_10dayforecast(raum.etage.haus, int(at_window_end))[int(at_window_start):int(at_window_end)]
        forecast_now = wetter.get_10dayforecast(raum.etage.haus, int(at_window_end))
        forecast = forecast_now[int(at_window_start):int(at_window_end)]
        average = sum([float(f[1]) for f in forecast]) / float(len(forecast))
        log_onAT(None, raum, None, None, forecast_now, average)  # hier wird ein Fehler geschmissen in "/home/pi/rpi/logger/models.py", line 189, in write_temp_log
    except Exception as e:
        logging.exception("exc")
        msg += u"Status: Keine Außentemperaturvorhersage vorhanden."
        average = None
        haus = raum.etage.haus
        hparams = haus.get_module_parameters()
        s = AbstractSensor.get_sensor(hparams.get('ruecklaufregelung', {}).get('atsensor'))
        if s:
            average = s.get_wert()
            if average is not None:
                msg += u'Verwende stattdessen AT-Sensor.'
                average = average[0]
            else:
                msg += u'Berechnung wird übersprungen.'

    if len(msg):
        logger.warning("%s|%s" % (raum.id, msg))
    return average


def _get_flushing_interval(raum, hfo_params):
    try:
        flushing_interval_minustwentyat = hfo_params.get('flushing_interval_start', 60)
        flushing_interval_plustwentyat = hfo_params.get('flushing_interval_end', 300)
        average = _get_weather_forecast_average(raum, hfo_params)
        a = (flushing_interval_plustwentyat, 20)
        b = (flushing_interval_minustwentyat, -20)
        c = (10, average)
        d = (600, average)
        flushing_interval, y = line_intersection((a, b), (c, d))
    except Exception as e:
        logging.exception("exc")
        flushing_interval = None

    return flushing_interval


def _get_rlsoll(raum, hfo_params, zielit=None):

    average = _get_weather_forecast_average(raum, hfo_params)
    if average is None:
        return None

    if 'ki' in raum.get_modules():

        if hfo_params.get('ki_mode', 'proki') == 'proki':
            points = hfo_params.get('points', [])
            if len(points) == 41:
                intavg = int(average)
                if intavg < -19:
                    return points[0]
                elif intavg > 19:
                    return points[40]
                else:
                    idx = intavg + 20
                    decimal = average - intavg
                    if decimal < 0:
                        sm = points[idx] - decimal*(points[idx-1]-points[idx])
                    elif decimal == 0:
                        sm = points[idx]
                    else:
                        sm = points[idx] + decimal*(points[idx+1]-points[idx])
                    return sm

        elif hfo_params.get('ki_mode') == "on":
            m = hfo_params.get('slope', 0.33)
            itminusat = zielit - average
            rltminusit = m * itminusat
            rlsoll = rltminusit + zielit
            return rlsoll

    hf_temp_plustwentyat = hfo_params.get('heating_element_start', 22)
    hf_temp_minustwentyat = hfo_params.get('heating_element_end', 40)
    a = (hf_temp_plustwentyat, 20)
    b = (hf_temp_minustwentyat, -20)
    c = (20, average)
    d = (80, average)
    try:
        rlsoll, y = line_intersection((a, b), (c, d))
    except Exception as e:
        logging.exception("exc")
        rlsoll = None

    return rlsoll
