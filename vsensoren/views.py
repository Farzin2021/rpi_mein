# -*- coding: utf-8 -*-
from heizmanager.render import render_response, render_redirect
from heizmanager.models import Sensor, Raum, Regelung, GatewayAusgang
from models import VSensor
from django.http import HttpResponse
import heizmanager.cache_helper as ch
import pytz
from datetime import datetime
from itertools import chain
from heizmanager.models import sig_get_outputs_from_output, sig_get_outputs_from_regelung


def get_global_settings_link(request, haus):
    return "<a href='/m_setup/%s/vsensoren/'>Virtuelle Sensoren</a>" % haus.id


def get_global_description_link():
    desc = u"Verknüpft mehrere Sensoren und gibt den Durchschnittswert aus."
    desc_link = "http://support.controme.com/auswahl-und-aktivierung-von-modulen/modul-virtuellesensoren/"
    return desc, desc_link


def _get_eundr(request, haus):
    eundr = []
    for etage in haus.etagen.all():
        e = {etage: []}
        for raum in Raum.objects.filter_for_user(request.user, etage_id=etage.id):
            e[etage].append(raum)
        eundr.append(e)
    return eundr


def get_global_settings_page(request, haus):

    if request.method == "GET":
        if 'vscreate' in request.GET or 'vsedit' in request.GET:
            if 'vsedit' in request.GET:
                vs = VSensor.objects.get(pk=long(request.GET['vsedit']))
            else:
                vs = VSensor()

            sensoren, vsensoren = _get_sensoren(haus)

            rlsensor = False
            try:
                if vs.raum.regelung.regelung == "ruecklaufregelung" and \
                        vs.get_identifier() in vs.raum.regelung.get_parameters()['rlsensorssn']:
                    rlsensor = True
            except AttributeError:
                pass

            hparams = haus.get_module_parameters()
            atsensor = False
            if hparams.get('ruecklaufregelung', dict()).get('atsensor', -1) == vs.get_identifier():
                atsensor = True

            return render_response(request, 'm_settings_vsensoren_edit.html',
                                   {'haus': haus, 'etagen': haus.etagen.all(), 'sensoren': sensoren,
                                    'vsensoren': vsensoren, 'vs': vs, 'rlsensor': rlsensor, 'atsensor': atsensor,
                                    'input_sensoren': vs.input_sensoren, 'eundr': _get_eundr(request, haus)})

        elif 'vsdel' in request.GET and request.user.userprofile.get().role != 'K':
            vsid = request.GET['vsdel']

            try:
                vs = VSensor.objects.get(pk=long(vsid))
                err = False
                if vs.mainsensor and vs.raum and vs == vs.raum.get_mainsensor():
                    cb2outs = sig_get_outputs_from_output.send(
                        sender='hardware', raum=vs.raum, regelung=vs.raum.regelung
                    )
                    outs = [o for out in [_o[1] for _o in cb2outs] for o in out]
                    for out in outs:
                        if len(out[4]):
                            err = True
                elif vs.raum:
                    cb2outs = sig_get_outputs_from_regelung.send(
                        sender='hardware',
                        haus=haus,
                        raum=vs.raum,
                        regelung=vs.raum.regelung
                    )
                    outs = [o for out in [_o[1].items() for _o in cb2outs] for o in out]
                    for ctrldev, out in outs:
                        for o in out:
                            if o[1] and o[2] == vs:
                                err = True

                if err:
                    sensoren, vsensoren = _get_sensoren(haus)
                    rlsensor = False
                    try:
                        if vs.raum.regelung.regelung == "ruecklaufregelung" and \
                                vs.get_identifier() in vs.raum.regelung.get_parameters()['rlsensorssn']:
                            rlsensor = True
                    except AttributeError:
                        pass
                    atsensor = False
                    hparams = haus.get_module_parameters()
                    if hparams.get('ruecklaufregelung', dict()).get('atsensor', -1) == vs.get_identifier():
                        atsensor = True
                    error = u"Dem Sensor sind Ausgänge zugeordnet. Bitte entfernen Sie diese vor dem Löschen."
                    return render_response(request, "m_settings_vsensoren_edit.html",
                                           {'haus': haus, 'etagen': haus.etagen.all(), 'sensoren': sensoren,
                                            'vsensoren': vsensoren, 'vs': vs, 'rlsensor': rlsensor, 'atsensor': atsensor,
                                            'input_sensoren': vs.input_sensoren, "error": error,
                                            'eundr': _get_eundr(request, haus)})

                vsensoren = VSensor.objects.filter(haus=haus)
                for vsensor in vsensoren:
                    for s in vsensor.input_sensoren:
                        if s == vs.get_identifier():
                            vsensor.input_sensoren = [str(i) for i in vs.input_sensoren if i != vs.get_identifier()]
                            vsensor.save()
                            break

                if vs.mainsensor and vs.raum:
                    vs.raum.set_mainsensor(None)
                vs.delete()
            except VSensor.DoesNotExist:
                pass

            return render_redirect(request, '/m_setup/%s/vsensoren/' % haus.id)

        elif 'getvs' in request.GET:
            import json
            from django.core.serializers.json import DjangoJSONEncoder

            vsensoren = []
            for vsensor in VSensor.objects.all():
                ch.delete(vsensor.name)
                vs_marker = vsensor.get_marker()
                vs_wert = vsensor.get_wert()

                wert = vsensor.get_wert()
                _sensoren = u'<br/>'
                for sensor in vsensor.get_input_sensoren():
                    wert = sensor.get_wert()
                    marker = sensor.get_marker()
                    if marker == 'yellow':
                        marker = 'red'
                    _sensoren += u"<p><br/>&nbsp;&nbsp;&nbsp;<span style='color: %s'>&#8226;</span> %s - " % (marker, sensor.name)
                    if wert:
                        _sensoren += u"%.2f&deg;C am %s.%s. um %s:%s" % (wert[0], wert[1].day, wert[1].month, wert[1].hour, ("0%s" % wert[1].minute)[-2:])
                    else:
                        _sensoren += u"überträgt nicht"
                    if sensor.id == vsensor.backup_sensor_id:
                        _sensoren += u"<i> (Backup Sensor)</i>"
                    _sensoren += u'</p>'

                if not vsensor.backup_sensor_id:
                    _sensoren += u'<p><i>kein Backup Sensor angegeben!</i></p>'

                _sensoren += u'<p><br/>'
                if wert:
                    _sensoren += u'Wert: %s&deg;C (%s)' % ("%.2f" % vs_wert[0] if vs_wert else u'-', vsensor.get_verknuepfung())
                else:
                    _sensoren += u'Wert: -'
                _sensoren += u'</p>'

                if vsensor.raum:
                    vsensoren.append({
                        vsensor.id: [vsensor.id, u"%s in %s/%s %s"
                                     % (vsensor.description, vsensor.raum.etage.name, vsensor.raum.name, _sensoren), vs_marker]
                    })
                else:
                    vsensoren.append({vsensor.id: [vsensor.id, u"%s in -/- %s" % (vsensor.description, _sensoren), vs_marker]})
            return HttpResponse(json.dumps(vsensoren, cls=DjangoJSONEncoder), content_type='application/json')

        else:
            return render_response(request, 'm_settings_vsensoren.html', {'haus': haus})

    elif request.method == "POST" and request.user.userprofile.get().role != 'K':

        if 'vscreate' in request.POST or 'vsedit' in request.POST:
            error = ''
            alter_raum = None
            if 'vscreate' in request.POST:
                vs = VSensor()
                vs.haus = haus
            elif 'vsedit' in request.POST:
                vsid = request.POST['vsedit']
                vs = VSensor.objects.get(pk=long(vsid))
                alter_raum = vs.raum

            raumid = long(request.POST['raumid'])

            if raumid != 0:
                vs.raum = Raum.objects.get(pk=raumid)
            else:
                vs.raum = None

            backup = long(request.POST['backup'])
            if backup == 0:
                if vs.backup_sensor_id:
                    vs.backup_sensor = None
                    vs.gateway = None
            else:
                vs.backup_sensor = None
                try:
                    vs.backup_sensor = Sensor.objects.get(pk=backup)
                except Sensor.DoesNotExist:
                    pass

                # wenn der vsensor ms ist und bleibt und sein raum einen ausgang hat, dann muss der gateway passen
                # wenn der vsensor rlsensor ist und bleibt, dann gilt das gleiche wie fuer einen ms
                if vs.id and vs.backup_sensor_id and vs.backup_sensor.gateway != vs.gateway and vs.gateway_id and \
                        ((vs.mainsensor and request.POST['sensortyp'] == 'rt' and vs.raum.get_regelung().get_ausgang()) or
                        (request.POST['sensortyp'] == 'rl' and vs.raum.get_regelung().get_ausgang() and
                            vs.get_identifier() in vs.raum.get_regelung().get_parameters().get('rlsensorssn', dict()) and len(vs.raum.get_regelung().get_parameters()['rlsensorssn'][vs.get_identifier()]))):
                    error += 'Unzul&auml;ssiger Backupsensor, stimmt nicht mit dem Gateway bereits zu diesem ' \
                             'Virtuellen Sensor zugeordneten Ausg&auml;ngen &uuml;berein.<br/>'

                elif vs.backup_sensor_id:
                    vs.gateway = vs.backup_sensor.gateway

            description = request.POST['description']
            if not len(description):
                error += 'Bitte Namen eingeben.<br/>'
            if 'vscreate' in request.POST:
                if len(VSensor.objects.filter(haus=haus, description=description)):
                    error += 'Name bereits in Verwendung.<br/>'
            vs.description = description

            con = request.POST['con']
            vs.verknuepfung = con

            sensoren = list()
            for key in request.POST.items():
                if key[0].startswith('is'):
                    sensorid = key[0].split('is', 1)[1]
                    sensoren.append(sensorid)

            for sensorid in sensoren:
                if 'VSensor' in sensorid:
                    sensorid = sensorid.split('*')[1]
                else:
                    continue
                try:
                    vsensor = VSensor.objects.get(pk=long(sensorid))
                    if vsensor_inconsistent(vsensor, vs):
                        error += 'Virtueller Sensor kann sich nicht selbst enthalten, Verbindung entfernt. ' \
                                 'Speichern Sie, um zu best&auml;tigen.<br/>'
                        sensoren.remove(sensorid)
                except (VSensor.DoesNotExist, ValueError):
                    # valueerror in sensoren.remove()
                    pass
            vs.input_sensoren = sensoren

            if vs.raum is not None:
                if request.POST['sensortyp'] == 'rt':
                    if not vs.raum.get_mainsensor() or vs.raum.get_mainsensor() == vs:
                        vs.mainsensor = True
                        vs.raum.set_mainsensor(vs)
                    else:
                        link = u"<a style='color:#cb0963; white-space:normal;' target='_blank' href='/m_setup/%s/%s'>%s</a>"
                        text = u'Speichern nicht m&ouml;glich, weil %s bereits %s als Raumtemperatursensor' \
                               u' zugewiesen bekommen hat. Klicken Sie hier, um diesen Sensor zu bearbeiten.'
                        ms = vs.raum.get_mainsensor()
                        # ueberschreiben wir alle anderen Fehler, sehen wir ja beim naechsten Mal wieder
                        if isinstance(ms, Sensor):
                            text = text % (u"%s/%s" % (ms.raum.etage, ms.raum.name), u"%s %s" % (ms.name, ms.description))
                            error = link % (haus.id, 'hardware/sedit/%s/' % ms.id, text)
                        elif isinstance(ms, VSensor):
                            text = text % (u"%s/%s" % (ms.raum.etage, ms.raum.name), u"%s %s" % (ms.name, ms.description))
                            error = link % (haus.id, 'vsensoren/?vsedit=%s' % ms.id, text)
                        else:
                            text = text % (u"%s/%s" % (ms.raum.etage, ms.raum.name), u"%s %s" % (ms.name, ms.description))
                            error = link % (haus.id, 'setup_rf/', text)
                else:
                    if vs.raum.get_mainsensor() == vs and vs.raum.regelung.get_ausgang():
                        error += 'Sensor ist regelungsrelevant. Entfernen Sie bitte zuerst die zugeordneten Ausgänge.<br/>'
                    else:
                        vs.mainsensor = False
                        vs.raum.set_mainsensor(None)
                        try:
                            vs.raum.get_regelung().get_ausgang().delete()
                        except (GatewayAusgang.DoesNotExist, AttributeError):
                            pass

            if alter_raum and vs.raum != alter_raum:
                arreg = alter_raum.get_regelung()
                if (vs.mainsensor and alter_raum.get_mainsensor() == vs and arreg.get_ausgang()) \
                        or (arreg.regelung == 'ruecklaufregelung' and vs.get_identifier() in arreg.get_parameters().get('rlsensorssn', dict()) and len(arreg.get_parameters()['rlsensorssn'][vs.get_identifier()])):
                    error += u"Sensor ist regelungsrelevant. Entfernen Sie bitte zuerst die zugeordneten Ausgänge.<br/>"

            if len(error):
                input_sensoren = [s for s in sensoren]  # fuer den vergleich bei zusammenzufassende sensoren
                sensoren, vsensoren = _get_sensoren(haus)
                rlsensor = False
                try:
                    if vs.raum.regelung.regelung == "ruecklaufregelung":
                        if vs.get_identifier() in vs.raum.regelung.get_parameters()['rlsensorssn']:
                            rlsensor = True
                except AttributeError:
                    pass
                return render_response(request, 'm_settings_vsensoren_edit.html',
                                       {'haus': haus, 'etagen': haus.etagen.all(), 'sensoren': sensoren, 'vs': vs,
                                        'rlsensor': rlsensor, 'vsensoren': vsensoren, 'input_sensoren': input_sensoren,
                                        'error': error, 'eundr': _get_eundr(request, haus)})

            if vs.raum != alter_raum and alter_raum is not None:
                if alter_raum.get_mainsensor() == vs:
                    ausgang = alter_raum.regelung.get_ausgang()
                    if ausgang is not None:
                        ausgang.delete()

                # war sensor ms im alten raum?
                if alter_raum.get_mainsensor() is not None and alter_raum.get_mainsensor().id == vs.id:
                    alter_raum.set_mainsensor(None)

                # war sensor rlsensor im alten raum? -> rlr deaktivieren
                ar_regparams = alter_raum.get_regelung().get_parameters()
                if alter_raum.get_regelung().regelung == 'ruecklaufregelung' \
                        and vs.get_identifier() in ar_regparams['rlsensorssn']:
                    del ar_regparams['rlsensorssn'][vs.get_identifier()]
                    if not len(ar_regparams['rlsensorssn']):
                        ar_regparams['active'] = False
                    alter_raum.get_regelung().set_parameters(ar_regparams)

            # speichern, damit wir im naechsten if die seriennummer verwenden koennen
            vs.save()
            vs.name = str(vs.get_identifier())

            if vs.raum is not None:
                reg = vs.raum.get_regelung()
                regparams = vs.raum.get_regelung().get_parameters()
                if request.POST['sensortyp'] == 'rl':

                    # rlsensor neu setzen
                    if reg.regelung == 'ruecklaufregelung':
                        if vs.get_identifier() not in regparams['rlsensorssn']:
                            regparams['rlsensorssn'][vs.get_identifier()] = list()
                            regparams['active'] = True
                            reg.set_parameters(regparams)

                    # rlr neu anlegen
                    else:
                        ausgang = vs.raum.regelung.get_ausgang()
                        vs.raum.regelung.delete()
                        reg = Regelung(regelung='ruecklaufregelung',
                                       parameter=str({'active': True, 'rlsensorssn': {vs.get_identifier(): []},
                                                      'neigung': 5.5, 'offset': 20, 'kruemmung': 2.0, 'schaerfe': 0.5,
                                                      'delta': 0.0}))
                        reg.save()
                        vs.raum.regelung = reg
                        vs.raum.save()
                        if ausgang:
                            ausgang.regelung = reg
                            ausgang.save()

                else:
                    if reg.regelung == 'ruecklaufregelung':
                        # rlr deaktivieren, wenn dieser sensor nicht mehr rlsensor und davor diesen raum geregelt hat
                        if vs.get_identifier() in regparams['rlsensorssn']:
                            del regparams['rlsensorssn'][vs.get_identifier()]
                            if not len(regparams['rlsensorssn']):
                                regparams['active'] = False
                            reg.set_parameters(regparams)
                        if not reg.get_raum().get_mainsensor() and not len(regparams['rlsensorssn']):
                            try:
                                reg.get_ausgang().delete()
                            except AttributeError:
                                pass

            vs.save()

            hparams = haus.get_module_parameters()
            if request.POST['sensortyp'] == 'at':
                hparams.setdefault('ruecklaufregelung', dict())
                hparams['ruecklaufregelung']['atsensor'] = vs.get_identifier()
                haus.set_module_parameters(hparams)
                if vs.raum is not None:
                    vs.raum.set_mainsensor(vs)
            else:
                if vs.get_identifier() == hparams.get('ruecklaufregelung', dict()).get('atsensor', -1):
                    del hparams['ruecklaufregelung']['atsensor']
                    haus.set_module_parameters(hparams)

            return render_redirect(request, '/m_setup/%s/vsensoren/' % haus.id)


def get_global_settings_page_help(request, haus):
    return render_response(request, "m_help_vsensoren.html", {'haus': haus})


def _get_sensoren(haus):
    sensoren = []
    for s in list(chain(sorted(haus.sensor_related.all()),
                        sorted(haus.rfsensor_related.all()),
                        sorted(haus.rfaktor_related.filter(type='wt')),
                        sorted(haus.rfaktor_related.filter(type__startswith='hkt', protocol='btle')))):

        last = s.get_wert()
        marker = s.get_marker()
        sensoren.append((s, last, marker))
    sensoren = sorted(sensoren, key=lambda s: s[0].raum.etage.name + s[0].raum.name if s[0].raum else True)

    vsensoren = []
    for vs in sorted(haus.vsensor_related.all()):
        ch.delete(vs.name)
        wert = vs.get_wert()
        marker = vs.get_marker()
        vsensoren.append((vs, wert, marker))
    vsensoren = sorted(vsensoren, key=lambda vs: vs[0].raum.etage.name + vs[0].raum.name if vs[0].raum else True)

    return sensoren, vsensoren


def vsensor_inconsistent(vsensor, vs):
    if vs.id == vsensor.id:
        return True

    for input in vsensor.input_sensoren:
        try:
            if 'VSensor' in input:
                input = input.split('*')[1]
            else:
                continue
            i_s = VSensor.objects.get(pk=long(input))
            return vsensor_inconsistent(i_s, vs)
        except VSensor.DoesNotExist:
            pass
    else:
        return False
