# -*- coding: utf-8 -*-
from heizmanager.render import render_response, render_redirect
import pytz
import heizmanager.cache_helper as ch
from heizmanager.models import Haus, Raum, Etage, Regelung
import logging
from heizmanager.helpers import activate, deactivate


def get_cache_ttl():
    return 1800


def is_togglable():
    return True


def is_hidden_offset():
    return False


def calculate_always_anew():
    return True


def is_fps_active(reg):
    result = reg.do_ausgang(0).content
    if result == '<1>':
        return True
    return False


def offset_evaluation(haus, obj, obj_id):
    """
    :param haus: haus object
    :param obj: str, obj gets something like, dr, vtr, raum
    :param obj_id: gets object id of given object
    :return: if fps-reg result is true returns relevant defined offset-value
    """
    offsets_params = haus.get_spec_module_params('offsets')
    offsets_items = offsets_params.get('items', dict())
    offset = 0
    for item_id, item_params in offsets_items.items():
        try:
            reg = Regelung.objects.get(id=int(item_params.get('selected_fps', 0)))
            # evaluation of fps-result
            if is_fps_active(reg):
                offset += float(item_params.get('object_offsets', dict()).get(obj, dict()).get(str(obj_id), 0))
        except Regelung.DoesNotExist:
            continue
    return offset


def get_offset(haus, raum=None):
    if raum is None:
        return 0.0
    offset = offset_evaluation(haus, 'raum', raum.id)
    return offset


def get_offset_regelung(haus, module, objid):
    obj_mod_dict = dict(differenzregelung='dr', vorlauftemperaturregelung='vtr')
    return offset_evaluation(haus, obj_mod_dict[module], objid)


def get_name():
    return 'Offsets'


def get_global_settings_link(request, haus):
    return "<a href='/m_setup/%s/offsets/'>Offsets</a>" % haus.id


def get_global_description_link():
    desc = u"Offsets für Regelungen zu frei konfigurierbaren Ereignissen."
    desc_link = "https://support.controme.com/offsets/"
    return desc, desc_link


def get_local_settings_link(request, raum):
    offset = get_offset(raum.etage.haus, raum)
    if not offset:
        offset = "deaktiviert"
    else:
        offset = "%.2f" % offset
    return "Offsets", offset, 75, "/m_raum/%s/offsets/" % raum.id


def get_local_settings_link_regelung(haus, modulename, objid):
    offset = get_offset_regelung(haus, modulename, objid)
    return 'Offsets', "Offsets", "%.2f" % offset, 75,  "#"


def get_local_settings_page(request, raum):
    if request.method == "GET":
        pass

    if request.method == "POST":
        pass


def get_local_settings_page_haus(request, haus):

    if request.method == "GET":
        pass

    elif request.method == "POST":
        pass


def get_global_settings_page_help(request, haus):
    return None


def get_global_settings_page(request, haus, action=None, entityid=None):

    if not isinstance(haus, Haus):
        haus = Haus.objects.get(pk=long(haus))

    context = dict()
    context['haus'] = haus
    mod_params = haus.get_spec_module_params('offsets')
    offsets_items = mod_params.get('items', dict())
    if request.method == "GET":
        if action == 'edit':

            # collecting fps
            regs = Regelung.objects.filter(regelung="fps")
            reg_rows = []
            for reg in sorted(regs, key=lambda x: x.get_parameters()['name']):
                reg_rows.append((str(reg.pk), reg.get_parameters()['name']))
            context['fpss'] = reg_rows

            # collecting room
            eundr = list()
            for etage in haus.etagen.all():
                e = {etage: []}
                for _raum in Raum.objects.filter_for_user(request.user, etage_id=etage.id):
                    e[etage].append(
                        (_raum, offsets_items.get(entityid, dict()).get(
                                'object_offsets', dict()).get('raum', dict()).get(str(_raum.id), 0)))
                eundr.append(e)
            context['eundr'] = eundr

            # collecting regelungen
            hparams = haus.get_module_parameters()
            drs = hparams.get('differenzregelung', dict())
            vtrs = hparams.get('vorlauftemperaturregelung', dict())

            drs_rows = []
            vtrs_rows = []
            for regid, params in drs.items():
                drs_rows.append({
                    "name": params['name'],
                    "regid": regid,
                    "val": offsets_items.get(entityid, dict()).get(
                        'object_offsets', dict()).get('dr', dict()).get(str(regid), 0)
                })
            for regid, params in vtrs.items():
                vtrs_rows.append({
                    "name": params['name'],
                    "regid": regid,
                    "val": offsets_items.get(entityid, dict()).get(
                        'object_offsets', dict()).get('vtr', dict()).get(str(regid), 0)
                })

            context['drs'] = drs_rows
            context['vtrs'] = vtrs_rows

            if entityid:
                offset_param = offsets_items.get(entityid)
                context['offset_param'] = offset_param
                context['entity_id'] = entityid
            return render_response(request, "m_settings_offsets_edit.html", context)

        if action == 'delete':
            del offsets_items[entityid]
            mod_params['items'] = offsets_items
            haus.set_spec_module_params('offsets', mod_params)
            return render_redirect(request, '/m_setup/%s/offsets/' % haus.id)

        offsets_rows = list()
        for item_id, item_params in offsets_items.items():
            try:
                reg = Regelung.objects.get(id=int(item_params.get('selected_fps', 0)))
            except Regelung.DoesNotExist:
                continue
            # evaluation of fps-result
            if is_fps_active(reg):
                offsets_rows.append((item_id, item_params, True))
            else:
                offsets_rows.append((item_id, item_params, False))

            context['offset_params'] = offsets_rows
        return render_response(request, "m_settings_offsets.html", context)

    if request.method == "POST":
        
        # preparing data
        data_d = dict()
        data_d['name'] = request.POST.get('name', '')
        selected_fps = request.POST.get('fps_abfrage', '0')
        data_d['selected_fps'] = selected_fps
        # saving a dict of objects with its offsets
        obj_vals = {'raum': dict(), 'dr': dict(), 'vtr': dict()}

        for key in request.POST.keys():
            if key.startswith('raum') or key.startswith('dr') or key.startswith('vtr'):
                obj, obj_id = key.split('_')
                obj_vals[obj][obj_id] = request.POST[key]
                # activate module for the room
                if obj == 'raum':
                    try:
                        room = Raum.objects.get(id=int(obj_id))
                    except Raum.DoesNotExist:
                        continue

                    if request.POST[key] == '0':
                        # deactivating is possible if only in other records, given room's offset is 0
                        can_be_deactivated = True
                        for item_id, item_params in offsets_items.items():
                            # ignore current editing item because it's checked by post key
                            if item_id == entityid:
                                continue
                            offset = float(item_params.get('object_offsets', dict()).get('raum', dict()).get(obj_id, 0))
                            if offset:
                                can_be_deactivated = False

                        if can_be_deactivated:
                            deactivate(room, 'offsets')

                    else:
                        activate(room, 'offsets')

        data_d['object_offsets'] = obj_vals
        # get existing data or define new id
        if entityid is None:
            ids = [0] + [int(o) for o in offsets_items.keys()]
            entityid = str(max(ids) + 1)
            offsets_items[entityid] = dict()

        # saving data into database
        offsets_items[entityid].update(data_d)
        mod_params['items'] = offsets_items
        haus.set_spec_module_params('offsets', mod_params)
        return render_redirect(request, '/m_setup/%s/offsets/' % haus.id)


def get_local_settings_link_haus(request, haus):
    pass


def get_local_settings_page_regelung(request, haus, modulename, objid, params):

    if request.method == "GET":
        pass

    elif request.method == "POST":
        pass
