# -*- coding: utf-8 -*-
from heizmanager.render import render_response, render_redirect
from heizmanager.models import Raum, Haus, Regelung
from rf.models import RFSensor
from django.http import HttpResponse
import heizmanager.cache_helper as ch


def get_name():
    return u'Kontaktabsenkung'


def get_cache_ttl():
    return 1800


def is_togglable():
    return True


def is_hidden_offset():
    return False


def calculate_always_anew():
    return True


def get_offset(haus, raum=None):

    if raum is None:
        return 0.0

    cntcts = RFSensor.objects.filter(raum=raum, type='cntct')
    for cntct in cntcts:
        val = ch.get("enocean_cntct_%s" % cntct.name.replace(':', '').lower())
        if val is not None and isinstance(val, tuple) and len(val) == 2 and val[0] == 'open':
            break
    else:
        return 0.0

    offset = raum.get_module_parameters().get('kontaktabsenkung', dict()).get('offset', None)
    if offset is None:
        offset = raum.etage.haus.get_module_parameters().get('kontaktabsenkung', dict()).get('default', 0.0)
    return offset


def get_global_settings_link(request, haus):
    return "<a href='/m_setup/%s/kontaktabsenkung/'>Kontaktabsenkung</a>" % haus.id


def get_global_description_link():
    desc = u"Einstellbare Absenkung für Fensterkontakte oder Raum-Präsenzmelder."
    desc_link = "http://support.controme.com/kontaktabsenkung/"
    return desc, desc_link


def get_global_settings_page(request, haus):

    params = haus.get_module_parameters()
    if request.method == "GET":
        return render_response(request, "m_settings_kontaktabsenkung.html",
                               {"haus": haus, "default": params.get('kontaktabsenkung', dict()).get('default', 0.0)})

    if request.method == "POST":
        default = request.POST['defaultoffset']
        if not len(default):
            error = "Bitte Standard Offset eingeben."
            return render_response(request, 'm_settings_kontaktabsenkung.html', {"haus": haus, "error": error})
        params.setdefault('kontaktabsenkung', dict())
        params['kontaktabsenkung']['default'] = float(default)
        haus.set_module_parameters(params)
        return render_redirect(request, '/m_setup/%s/kontaktabsenkung/' % haus.id)


def get_local_settings_link(request, raum):

    cntcts = RFSensor.objects.filter(raum=raum, type='cntct')
    for cntct in cntcts:
        val = ch.get("enocean_cntct_%s" % cntct.name.replace(':', '').lower())
        if val is not None and len(val) == 2 and val[0] == 'open':
            break
    else:
        return "Kontaktabsenkung", "deaktiviert", 75, "/m_raum/%s/kontaktabsenkung/" % raum.id

    if 'kontaktabsenkung' not in raum.get_modules():
        raum.set_modules(raum.get_modules() + ['kontaktabsenkung'])

    offset = raum.get_module_parameters().get('kontaktabsenkung', dict()).get('offset', None)
    if offset is None:
        offset = raum.etage.haus.get_module_parameters().get('kontaktabsenkung', dict()).get('default', 0.0)
    return "Kontaktabsenkung", "%.2f" % offset, 75, "/m_raum/%s/kontaktabsenkung/" % raum.id


def get_local_settings_page(request, raum):

    if request.method == "GET":
        rparams = raum.get_module_parameters()
        offset = rparams.get('kontaktabsenkung', dict()).get('offset', 0.0)
        return render_response(request, "m_raum_kontaktabsenkung.html", {"raum": raum,"haus": raum.etage.haus, "offset": offset})

    elif request.method == "POST":
        if request.POST.get('singleroom', False):
            #name = request.POST['name']
            #obj_id = long(request.POST.get('id', 0))
            number = request.POST['number']
            raum = Raum.objects.get(id=long(request.POST.get('id', 0)))
            set_params(raum, number)
            return HttpResponse()
        else:
            offset = request.POST.get('offset', 0)
            try:
                offset = float(offset)
            except:
                return render_response(request, "m_raum_kontaktabsenkung.html", {"raum": raum, "error": "Illegaler Wert."})
            rparams = raum.get_module_parameters()
            rparams['kontaktabsenkung'] = dict()
            rparams['kontaktabsenkung']['offset'] = offset
            raum.set_module_parameters(rparams)
            return render_redirect(request, "/m_raum/%s/" % raum.id)



def get_local_settings_page_haus(request, haus):

    if request.method == "GET":

        eundr = []
        for etage in haus.etagen.all():
            e = {etage: []}
            for raum in Raum.objects.filter_for_user(request.user, etage_id=etage.id):
                rparams = raum.get_module_parameters()
                offset = rparams.get('kontaktabsenkung', dict()).get('offset', 0.0)
                e[etage].append((raum,offset))
            eundr.append(e)

        return render_response(request, "m_kontaktabsenkung.html", {"eundr": eundr, "haus": haus})

    elif request.method == "POST":
        
        if request.POST.get('singleroom', False):
            #name = request.POST['name']
            #obj_id = long(request.POST.get('id', 0))
            number = request.POST['number']
            raum = Raum.objects.get(id=long(request.POST.get('id', 0)))
            set_params(raum, number)
            return HttpResponse()
        else:
            absenkung = request.POST.get('absenkung', -1)
            if 'absenkung_checkbox' not in request.POST:
                absenkung = -1
            raeume = request.POST.getlist('rooms')
            for raum_id in raeume:
                raum = Raum.objects.get(id=long(raum_id))
                set_params(raum, absenkung)
            return render_redirect(request, "/m_kontaktabsenkung/%s/" % haus.id)



def get_params(hausoderraum):
    params = hausoderraum.get_module_parameters()
    offset = params.get('kontaktabsenkung', dict()).get('offset', 0.0)
    return offset


def set_params(hausoderraum, offset):
    rparams = hausoderraum.get_module_parameters()
    rparams['kontaktabsenkung'] = dict()
    rparams['kontaktabsenkung']['offset'] = float(offset)
    hausoderraum.set_module_parameters(rparams)
