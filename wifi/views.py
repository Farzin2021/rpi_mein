# -*- coding: utf-8 -*-

from heizmanager.render import render_response, render_redirect
from heizmanager.models import Haus
from fabric.operations import local
from config.Controme_ETRV.helper import _local_handler
import re
import socket
import fcntl
import struct


def get_wlan_interface():
    sock = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
    try:
        name = "wlan0"
        ip_address = socket.inet_ntoa(fcntl.ioctl(
            sock.fileno(),0x8915,
            struct.pack('256s', name[:15]))[20:24])
        return {name: ip_address}
    except:
        return {name: None}


def get_global_settings_link(request, haus):
    return "<a href='/m_setup/%s/wifi/'>WLAN</a>" % haus.id


def connect_wifi(ssid, password, status):
    # writing wifi config to related file
    if status == 'off':
        _local_handler('sudo rm /etc/wpa_supplicant/wpa_supplicant.conf')
        _local_handler('sudo ifconfig wlan0 down')
        _local_handler('sudo killall wpa_supplicant')
        return True
    try:
        config = local("sudo cat /etc/wpa_supplicant/wpa_supplicant.conf", capture=True)
    except:
        config = ''
    if 'country' not in config:
        config = "country=DE\n" + config

    # search for ssid and psk if exists otherwise change them
    if 'ssid' not in config:
        config = config + '\nnetwork={\nssid="%s"\npsk="%s"\n}\n' % (ssid, password)
    else:
        config = re.sub(r'ssid=".*"', 'ssid="%s"' % ssid, config)
        config = re.sub(r'psk=".*"', 'psk="%s"' % password, config)
    command = "printf '%s\n' '{}' | sudo tee /etc/wpa_supplicant/wpa_supplicant.conf".format(config)
    local(command)
    _local_handler('sudo ifconfig wlan0 down')
    _local_handler('sudo killall wpa_supplicant')
    _local_handler('sudo wpa_supplicant -B -c /etc/wpa_supplicant/wpa_supplicant.conf -i wlan0')
    return True


def get_global_settings_page(request, haus, action=None, entityid=None):

    if not isinstance(haus, Haus):
        haus = Haus.objects.get(pk=int(haus))

    if request.method == "GET":
        wifi_data = haus.get_spec_module_params('wifi')
        wifi_ip = get_wlan_interface().get('wlan0', None)
        if not wifi_data.get('status'):
            if wifi_ip:
                wifi_data['status'] = 'on'
            else:
                wifi_data['status'] = 'off'
        return render_response(request, 'm_install_wifi.html', {'haus': haus, 'params': wifi_data,
                                        'wifi_ip': wifi_ip})

    elif request.method == "POST":
        wifi_data = {
            'ssid': request.POST.get('ssid', ''),
            'password': request.POST.get('password', ''),
            'status': request.POST.get('wifi_status', '')
        }
        haus.set_spec_module_params('wifi', wifi_data)
        # prevent to leave ssid empty in config file
        if wifi_data['ssid'] == '':
            wifi_data['ssid'] = 'notset'
        connect_wifi(wifi_data['ssid'], wifi_data['password'], wifi_data['status'])
        return render_redirect(request, "/m_setup/%s/wifi/" % haus.id)
