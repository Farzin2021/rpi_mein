import sys, os, django

# Add django settings to run this script seperately!
try:
    sys.path.append('/home/pi/rpi/')
    os.environ.setdefault("DJANGO_SETTINGS_MODULE", "rpi.settings")
    django.setup()
except:
    print('Error in initialize django settings!')
    sys.exit()

from heizmanager.models import Sensor
import heizmanager.cache_helper as ch
import random
from datetime import datetime


def simulate():

    def set_new_temp(ssn, ist, hausid, sensor_offset=0.0, update_time=True):
        berlin = pytz.timezone('Europe/Berlin')
        tstamp = datetime.now(berlin)
        last = get_last_temp(ssn)
        if update_time:
            ltstamp = None
            if last:
                try:
                    ltstamp = last[1]
                except (TypeError, KeyError):
                    pass
        else:
            if last:
                try:
                    tstamp = last[1]
                    ltstamp = last[3]
                except TypeError:
                    ltstamp = None
            else:
                ltstamp = None
        try:
            set(key=ssn.lower(), value=(float(ist)+sensor_offset, tstamp, hausid, ltstamp, sensor_offset))
        except TypeError:
            return False
        return True

    sensor_queryset = Sensor.objects.all().filter(name__startswith='01_').values_list()
    for sensor in sensor_queryset:
        hausid = sensor[1]
        snn = sensor[2]
        snn_temp = snn.split('_')
        center_ssn_temp = float('%s.%s' % (snn_temp[-2], snn_temp[-1]))
        max_ssn_temp = center_ssn_temp + float('%s.%s' % (snn_temp[-4], snn_temp[-3]))
        min_ssn_temp = center_ssn_temp - float('%s.%s' % (snn_temp[-4], snn_temp[-3]))
        get_sensor_data = ch.get_last_temp(sensor[2])

        if not get_sensor_data:
            snn_ch = random.uniform(min_ssn_temp, max_ssn_temp)
            
        else:
            snn_ch = get_sensor_data[0]
            if snn_ch < min_ssn_temp or snn_ch > max_ssn_temp:
                snn_ch = random.uniform(min_ssn_temp, max_ssn_temp)

        operation = random.choice(['up', 'down'])
        final_value = snn_ch + 0.125 if operation == 'up' else snn_ch - 0.125
        if final_value < min_ssn_temp:
            final_value += 2*0.125
        if final_value > max_ssn_temp:
            final_value -= 2*0.125
        if min_ssn_temp == max_ssn_temp:
            final_value = center_ssn_temp
        ch.set_new_temp(sensor[2], final_value, hausid)


simulate()
