# coding=utf-8
from heizmanager.render import render_response, render_redirect
from fabric.api import local
import json
import os
import datetime
import pytz


def get_name():
    return 'SensorSimulator'


def get_cache_ttl():
    return 1800


def is_togglable():
    return True


def get_global_settings_link(request, haus):
    return "<a href='/m_setup/%s/sensor_simulator/'>Sensor-Simulator</a>" % haus.id


def get_global_description_link():
    desc = u"Generiert zufällige Temperaturwerte von Sensoren mit einstellbarem Bereich."
    desc_link = "https://support.controme.com/"
    return desc, desc_link


def write_json(json_file, json_data):
    with open(json_file, 'w') as outfile:
        json.dump(json_data, outfile)


def read_json(json_file):
    with open(json_file) as infile:
        data = json.load(infile)
    return data


def local_handler(cmd):
    try:
        res = local(cmd, capture= True)
    except:
        return
    return res


def get_global_settings_page(request, haus):

    # location of json as db
    filepath = '/home/pi/.sensor_simulate.json'
    # check if periodischer json is avalaible
    if not os.path.isfile(filepath):
        sensor_simulator_status = local_handler('echo $(sudo systemctl is-active sensor_simulator.timer)')
        sensor_status = u'on' if sensor_simulator_status == 'active' else u'off'
        sensor_repeat = local_handler(
            'cat /etc/systemd/system/sensor_simulator.timer | grep OnUnitActiveSec | cut -d "=" -f2 | cut -d " " -f1'
            )
        if not sensor_repeat:
            sensor_repeat = '30'
        berlin = pytz.timezone('Europe/Berlin')
        now = datetime.datetime.now(berlin)
        datenow = now.ctime()
        json_data = {
            'date': datenow,
            'minute_slider': sensor_repeat,
            'status': sensor_status,
        }
        write_json(filepath, json_data)
    module_params = read_json(filepath)

    if request.method == "GET":
        # get left time 
        left_time = local_handler("echo $(sudo systemctl list-timers sensor_simulator.timer | grep -i 'sensor_simulator.timer'"
                                  " | grep -o -P '.*(?= left)' | cut -d' ' -f 5-)")

        if left_time == '':
            left_time = 'Warten Sie mal'
        context = {}
        context['haus'] = haus
        context['params'] = module_params
        context['left_time'] = left_time
        return render_response(request, 'm_settings_sensor_simulator.html', context)

    elif request.method == "POST":
        if 'minute_slider' in request.POST:
            module_params['minute_slider'] = request.POST['minute_slider']
        if 'set_status' in request.POST:
            module_params['status'] = request.POST['set_status']
            if module_params['status'] == 'on':
                berlin = pytz.timezone('Europe/Berlin')
                now = datetime.datetime.now(berlin)
                datenow = now.ctime()
                module_params['date'] = datenow
        write_json(filepath, module_params)
        status = module_params.get('status')
        if status is not None:
            if status == 'on':
                # kill same existing process
                local_handler("sudo ps -ef | sudo grep 'bash ble_service_create.sh sensor_simulator'"
                             " | grep -v grep | awk '{print $2}' | sudo xargs kill > /dev/null 2>&1")
                # update timer according to slider value
                local_handler('cd /home/pi/rpi/heizmanager/scripts/ && sudo bash ble_service_create.sh '
                    'sensor_simulator "/usr/bin/sudo -H -u pi /usr/bin/python /home/pi/rpi/sensor_simulator/fake_simulation.py" %s'
                    % (module_params.get('minute_slider', 30)))
            else:
                local_handler('sudo systemctl stop sensor_simulator.timer && sudo systemctl disable sensor_simulator.timer')
        return render_redirect(request, "/m_setup/%s/sensor_simulator/" % haus.id)


def activate(hausoderraum):
    if 'sensor_simulator' not in hausoderraum.get_modules():
        hausoderraum.set_modules(hausoderraum.get_modules() + ['sensor_simulator'])


def deactivate(hausoderraum):
    modules = hausoderraum.get_modules()
    try:
        modules.remove('sensor_simulator')
        hausoderraum.set_modules(modules)
    except ValueError:  # nicht in modules
        pass


