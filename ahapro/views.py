# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from heizmanager.models import Raum, Haus, Regelung, Gateway
from heizmanager.render import render_response, render_redirect
from django.http import HttpResponse, HttpResponseRedirect, JsonResponse
from django.shortcuts import redirect
from ahapro.tasks import calculate_rsoll
import heizmanager.cache_helper as ch
from django.http import FileResponse
from itertools import chain
import datetime
import json
import requests
import re, os


def get_name():
    return u'AHA-PRO'

def get_global_settings_link(request, haus):
    return "<a href='/m_setup/%s/ahapro/'>AHA-PRO</a>" % haus.id

def get_global_description_link():
    desc = u"AHA-PRO"
    desc_link = "https://support.controme.com/"
    return desc, desc_link

def get_raum_params(request, haus):
    raumid = int(request.GET.get('raumid_get'))
    raum = Raum.objects.get(pk=long(raumid))
    raum_params = raum.get_module_parameters()
    data = raum_params.get('aha_pro', {}).get('value', None)
    room_check = raum_params.get('aha_pro', {}).get('status', None)
    room_status_all = haus.get_spec_module_params('aha_pro')
    room_status_all = room_status_all.get('all_rooms_status')
    return JsonResponse({'value': data, 'room_status': room_check, 'room_status_all':room_status_all})

def post_aha_pro_status(request, haus):
    params = haus.get_module_parameters()
    if request.POST.get('aha_pro_status') == 'on':
        aha_pro_status = 'true'
    else:
        aha_pro_status = None

    data = {
        'aha_pro_status': aha_pro_status,
    }
    if 'aha_pro' not in params:
        params['aha_pro'] = {}
    params['aha_pro'].update(data)
    haus.set_module_parameters(params)
    success_message = "Die Daten wurden erfolgreich gespeichert"
    return JsonResponse({'success_message': success_message})

def post_data(request, haus):
    raumid = request.POST.get('raumid')
    data = request.POST.get('data')
    raum = Raum.objects.get(pk=long(raumid))
    raum_params = raum.get_module_parameters()
    if not raum_params.get('aha_pro'):
        raum_params['aha_pro'] = dict()
    raum_params['aha_pro']['value'] = data
    raum.set_module_parameters(raum_params)
    return redirect(request.get_full_path())

def post_room_status(request, haus):
    raumid = request.POST.get('raumid')
    data = str(request.POST.get('room_status'))
    raum = Raum.objects.get(pk=long(raumid))
    raum_params = raum.get_module_parameters()
    if not raum_params.get('aha_pro'):
        raum_params['aha_pro'] = dict()
    raum_params['aha_pro']['status'] = data
    raum.set_module_parameters(raum_params)
    return redirect(request.get_full_path())

def post_all_rooms_status(request, haus):
    haus_params = haus.get_module_parameters()
    if 'aha_pro' not in haus_params:
        params['aha_pro'] = {}
    data = {'all_rooms_status': str(request.POST.get('all_rooms_status'))}
    haus_params['aha_pro'].update(data)
    haus.set_module_parameters(haus_params)
    return redirect(request.get_full_path())

def post_algorithm(request, haus):
    haus_params = haus.get_module_parameters()
    if 'aha_pro' not in haus_params:
        params['aha_pro'] = {}
    data = {'algorithm': str(request.POST.get('algorithm'))}
    haus_params['aha_pro'].update(data)
    haus.set_module_parameters(haus_params)
    return redirect(request.get_full_path())

def post_calculate(request, haus):
    method = str(request.POST.get('calculate'))
    ahapro = haus.get_spec_module_params('aha_pro')
    all_rooms_status = ahapro.get('all_rooms_status')
    if method == '2':
        data, calculation = calculate_rsoll(method, all_rooms_status)
    else:
        calculation = calculate_rsoll(method, all_rooms_status)
    calculation = calculation if calculation else 'Kein Data'
    calculation_message = 'Calculation: %s' % calculation
    return JsonResponse({'calculation_response': calculation_message})

# main page
def get_global_settings_page(request, haus):
    if not isinstance(haus, Haus):
        try:
            haus = Haus.objects.get(pk=long(haus))
        except Haus.DoesNotExist:
            return HttpResponse("Error", status=400)

    params = haus.get_module_parameters()

    if request.method == "GET":
        if 'raumid_get' in request.GET:
            return get_raum_params(request, haus)

        ahapro = params.get('aha_pro', {})
        aha_pro_status = ahapro.get('aha_pro_status', False)
        algorithm = ahapro.get('algorithm', '1')
        raums = Raum.objects.all()
        context = {
            'aha_pro_status': aha_pro_status,
            'algorithm': algorithm,
        }

    elif request.method == 'POST':
        if 'aha_pro_status' in request.POST:
            return post_aha_pro_status(request, haus)
        elif 'data' in request.POST:
            return post_data(request, haus)
        elif 'room_status' in request.POST:
            return post_room_status(request, haus)
        elif 'all_rooms_status' in request.POST:
            return post_all_rooms_status(request, haus)
        elif 'algorithm' in request.POST:
            return post_algorithm(request, haus)
        elif 'calculate' in request.POST:
            return post_calculate(request, haus)

    return render_response(request, "m_aha_pro.html", {'aha_pro': context, 'haus': haus, 'raums': raums,})


# configurator page
def configurator(request, haus):
    if not isinstance(haus, Haus):
        try:
            haus = Haus.objects.get(pk=long(haus))
        except Haus.DoesNotExist:
            return HttpResponse("Error", status=400)
    if request.method == "GET":
        ahapro = haus.get_spec_module_params('aha_pro')
        aha_pro_change_factor_gain = ahapro.get('aha_pro_change_factor_gain', 0.1)
        aha_pro_max_change_factor = ahapro.get('aha_pro_max_change_factor', 20.0)
        aha_pro_min_valve_threshold = ahapro.get('aha_pro_min_valve_threshold', 5.0)
        aha_pro_min_valve_value = ahapro.get('aha_pro_min_valve_value', 0)
        raums = Raum.objects.all()
        context = {
            'aha_pro_change_factor_gain': aha_pro_change_factor_gain,
            'aha_pro_max_change_factor': aha_pro_max_change_factor,
            'aha_pro_min_valve_threshold': aha_pro_min_valve_threshold,
            'aha_pro_min_valve_value': aha_pro_min_valve_value,
        }
        return render_response(request, "m_aha_pro_configurator.html", {'haus': haus, 'aha_pro': context})

    if request.method == 'POST':
        params = haus.get_module_parameters()
        data = {
            'aha_pro_change_factor_gain': float(request.POST.get('aha_pro_change_factor_gain', 0.1)),
            'aha_pro_max_change_factor': float(request.POST.get('aha_pro_max_change_factor', 20)),
            'aha_pro_min_valve_threshold': float(request.POST.get('aha_pro_min_valve_threshold', 5)),
            'aha_pro_min_valve_value': float(request.POST.get('aha_pro_min_valve_value', 0)),
        }

        if 'aha_pro' not in params:
            params['aha_pro'] = {}
        params['aha_pro'].update(data)
        haus.set_module_parameters(params)
        success_message = "Die Daten wurden erfolgreich gespeichert"
        return JsonResponse({'success_message': success_message})

# heizlastberechnung page
def heizlastberechnung(request, haus):
    if not isinstance(haus, Haus):
        try:
            haus = Haus.objects.get(pk=long(haus))
        except Haus.DoesNotExist:
            return HttpResponse("Error", status=400)
    if request.method == "GET":
        if 'raumid_get' in request.GET:
            raumid = int(request.GET.get('raumid_get'))
            raum = Raum.objects.get(pk=long(raumid))
            raum_params = raum.get_module_parameters()
            hlb_room_T = raum_params.get('aha_pro', {}).get('hlb_room_T', 0)
            hlb_room_Q = raum_params.get('aha_pro', {}).get('hlb_room_Q', 0)
            hlb_heat_loop_Q = raum_params.get('aha_pro', {}).get('hlb_heat_loop_Q', 0)
            hlb_heat_loop_flow_rate = raum_params.get('aha_pro', {}).get('hlb_heat_loop_flow_rate', 0)
            hlb_heat_loop_rlT = raum_params.get('aha_pro', {}).get('hlb_heat_loop_rlT', 0)
            return JsonResponse({'hlb_room_T': hlb_room_T, 'hlb_room_Q': hlb_room_Q, 'hlb_heat_loop_Q': hlb_heat_loop_Q,
                                 'hlb_heat_loop_flow_rate': hlb_heat_loop_flow_rate, 'hlb_heat_loop_rlT': hlb_heat_loop_rlT})

        raums = Raum.objects.all()
        ahapro = haus.get_spec_module_params('aha_pro')
        aha_pro_raum = raums[0].get_module_parameters().get('aha_pro', {})
        aha_pro_room_daten = ahapro.get('aha_pro_room_daten', False)
        hlb_house_atT = ahapro.get('hlb_house_atT', 0)
        hlb_house_vlT = ahapro.get('hlb_house_vlT', 0)
        hlb_house_rlT = ahapro.get('hlb_house_rlT', 0)

        context = {
            'aha_pro_room_daten': aha_pro_room_daten,
            'hlb_house_atT': hlb_house_atT,
            'hlb_house_vlT': hlb_house_vlT,
            'hlb_house_rlT': hlb_house_rlT,
        }
        return render_response(request, "m_aha_pro_heizlastberechnung.html", {'haus': haus, 'aha_pro': context, 'raums': raums})

    if request.method == "POST":
        if request.POST.get('aha_pro_room_daten') == 'on':
            aha_pro_room_daten = 'true'
        else:
            aha_pro_room_daten = None
        params = haus.get_module_parameters()
        data = {
            'aha_pro_room_daten': aha_pro_room_daten,
            'hlb_house_atT': float(request.POST.get('hlb_house_atT', 0)),
            'hlb_house_vlT': float(request.POST.get('hlb_house_vlT', 0)),
            'hlb_house_rlT': float(request.POST.get('hlb_house_rlT', 0)),
        }
        if 'aha_pro' not in params:
            params['aha_pro'] = {}
        params['aha_pro'].update(data)
        haus.set_module_parameters(params)

        raum_data = {
            'hlb_room_T': float(request.POST.get('hlb_room_T', 0)),
            'hlb_room_Q': float(request.POST.get('hlb_room_Q', 0)),
            'hlb_heat_loop_Q': float(request.POST.get('hlb_heat_loop_Q', 0)),
            'hlb_heat_loop_flow_rate': float(request.POST.get('hlb_heat_loop_flow_rate', 0)),
            'hlb_heat_loop_rlT': float(request.POST.get('hlb_heat_loop_rlT', 0)),
        }
        raumid = request.POST.get('raumid')
        if raumid:
            raum = Raum.objects.get(pk=long(raumid))
            raum_params = raum.get_module_parameters()
            if not raum_params.get('aha_pro'):
                raum_params['aha_pro'] = dict()
            raum_params['aha_pro'].update(raum_data)
            raum.set_module_parameters(raum_params)
        return redirect(request.get_full_path())

# maximaloeffnungen page
def maximaloeffnungen(request, haus):
    if not isinstance(haus, Haus):
        try:
            haus = Haus.objects.get(pk=long(haus))
        except Haus.DoesNotExist:
            return HttpResponse("Error", status=400)
    if request.method == "GET":
        directory = '/var/log/uwsgi/'
        file_list = os.listdir(directory)
        pattern = re.compile(r".*(_cnt_opening.json|_max_opening.json)$")
        matching_files = sorted([f for f in file_list if pattern.match(f)], reverse=True)
        return render_response(request, "m_aha_pro_maximal.html", {'haus': haus, 'files': matching_files})
    if request.method == "POST":
        if 'download_config' in request.POST:
            selected_file = request.POST.get('selected_file')
            file_path = '/var/log/uwsgi/' + selected_file
            response = FileResponse(open(file_path, 'rb'), content_type='application/json')
            response['Content-Disposition'] = 'attachment; filename="{}"'.format(selected_file)
            return response

        elif 'download' in request.POST:
            filename = "/var/log/uwsgi/.aha_pro.txt"
            with open(filename, 'r') as data_file:
                data = data_file.read()
            response = HttpResponse(data, content_type='text/plain')
            response['Content-Disposition'] = 'attachment; filename=aha_pro.txt'
            return response

        elif 'save_openings' in request.POST:
            gateways = list(chain(Gateway.objects.filter(haus=haus)))
            opening_dict = {}
            for gateway in gateways:
                opening_dict[gateway.id] = {}
                response = requests.get('http://localhost/get/%s/all/' % (gateway.name))
                cnt_data = response.text.strip('<>').split(';')
                cnt_data = [x for x in cnt_data]
                opening_dict = {gateway.id: {i+1: str(v) for i, v in enumerate(cnt_data)}}
            now = datetime.datetime.now()
            date_string = now.strftime("%Y%m%d%H%M%S")
            if opening_dict:
                with open('/var/log/uwsgi/%s_cnt_opening.json' % date_string, 'w') as f:
                    json.dump(opening_dict, f)
                success_message = "Die Daten wurden erfolgreich gespeichert"
            else:
                success_message = "Error!"
            return JsonResponse({'success_message': success_message})

        elif 'save_maximal' in request.POST:
            gateways = list(chain(Gateway.objects.filter(haus=haus)))
            opening_dict = {}
            for gateway in gateways:
                opening_dict[gateway.id] = {}
                params = gateway.get_parameters()
                max_open = params.get('max_opening', dict((i, 100) for i in range(1, 16)))
                opening_dict = {gateway.id: max_open}
            now = datetime.datetime.now()
            date_string = now.strftime("%Y%m%d%H%M%S")
            if opening_dict:
                with open('/var/log/uwsgi/%s_max_opening.json' % date_string, 'w') as f:
                    json.dump(opening_dict, f)
                success_message = "Die Daten wurden erfolgreich gespeichert"
            else:
                success_message = "Error!"
            return JsonResponse({'success_message': success_message})

        elif 'sync_openings' in request.POST:
            gateways = list(chain(Gateway.objects.filter(haus=haus)))
            opening_dict = {}
            for gateway in gateways:
                opening_dict[gateway.id] = {}
                response = requests.get('http://localhost/get/%s/all/' % (gateway.name))
                cnt_data = response.text.strip('<>').split(';')
                cnt_data = [int(x) if x.isdigit() else float(x) if x.replace('.','',1).isdigit() else None for x in cnt_data]
                params = gateway.get_parameters()
                max_open = params.get('max_opening', dict((i, 100) for i in range(1, 16)))
                ahapro = haus.get_spec_module_params('aha_pro')
                aha_pro_min_valve_value = ahapro.get('aha_pro_min_valve_value', 0)
                final_dict = {i+1: (unicode(val) if val is not None and val >= aha_pro_min_valve_value else max_open[i+1]) for i, val in enumerate(cnt_data)}
                params['max_opening'] = final_dict
                gateway.set_parameters(params)
                opening_dict[gateway.id] = final_dict
            now = datetime.datetime.now()
            date_string = now.strftime("%Y%m%d%H%M%S")
            if opening_dict:
                with open('/var/log/uwsgi/%s_max_opening.json' % date_string, 'w') as f:
                    json.dump(opening_dict, f)
                success_message = "Die Daten wurden erfolgreich gespeichert"
            else:
                success_message = "Error!"
            return JsonResponse({'success_message': success_message})

        elif 'selected_file' in request.POST:
            selected_file = request.POST.get('selected_file')
            try:
                with open('/var/log/uwsgi/%s' % selected_file, 'r') as json_file:
                    data = json.load(json_file)
            except:
                return HttpResponse(status=400)
            data = {int(k): {int(j): v for j, v in v_dict.items()} for k, v_dict in data.items()}
            gateways = list(chain(Gateway.objects.filter(haus=haus)))
            ahapro = haus.get_spec_module_params('aha_pro')
            aha_pro_min_valve_value = ahapro.get('aha_pro_min_valve_value', 0)
            success_message = "Error"
            for gateway in gateways:
                params = gateway.get_parameters()
                max_open = params.get('max_opening', dict((i, 100) for i in range(1, 16)))
                if 'mmss_cnt_opening' in selected_file:
                    if gateway.id in data:
                        for key, value in data.get(gateway.id, {}).items():
                            if value.isdigit() and int(value) >= aha_pro_min_valve_value:
                                max_open[key] = value
                else:
                    if gateway.id in data:
                        max_open = data.get(gateway.id)
                params['max_opening'] = max_open
                gateway.set_parameters(params)
                success_message = "Die Daten wurden erfolgreich gespeichert"
            return JsonResponse({'success_message': success_message})

# test_hlb page
def test_hlb(request, haus):
    try:
        with open('/var/log/uwsgi/.aha_pro_heizlastberechung.txt', 'r') as file:
            log_file_content = file.read()
    except IOError:
        log_file_content = "Error: File not found or could not be opened."

    return render_response(request, 'm_aha_pro_log.html', {'log_file_content': log_file_content})

