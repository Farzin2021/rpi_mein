from django.conf.urls import url
from . import views
urlpatterns = [
    url(r'^m_setup/(?P<haus>\d+)/ahapro/configurator/$', views.configurator),
    url(r'^m_setup/(?P<haus>\d+)/ahapro/heizlastberechnung/$', views.heizlastberechnung),
    url(r'^m_setup/(?P<haus>\d+)/ahapro/maximaloeffnungen/$', views.maximaloeffnungen),
    url(r'^m_setup/(?P<haus>\d+)/ahapro/test_hlb/$', views.test_hlb),
    ]
