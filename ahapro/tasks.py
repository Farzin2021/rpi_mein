# -*- coding: utf-8 -*-
import logging, sys
from celery import shared_task
from heizmanager.mobile.m_error import CeleryErrorLoggingTask
from heizmanager.models import AbstractSensor, Haus, Gateway, Raum
from itertools import chain
import heizmanager.cache_helper as ch
from fabric.api import local

logger = logging.getLogger('logmonitor')

def write_log(message, logfile='/var/log/uwsgi/.aha_pro.txt'):
    """Logs given message to specified log file"""
    if message == '$(date)':
        local('echo "%s" > %s' % (message, logfile))
    else:
        local('echo "%s" >> %s' % (message, logfile))

def calculate_rsoll(method, all_rooms_status):
    """Calculate statistics"""

    if method == '1':
        write_log('Manuall mode is active!')
        return

    # Method 2: Calculate Auslegungs-Rücklauftemperaturen aus der Heizlastberechung
    if method == '2':
        write_log('$(date)', '/var/log/uwsgi/.aha_pro_heizlastberechung.txt')
        rooms = Raum.objects.all()
        rl_values = {}
        all_hlb_heat_loop_rlT = {}
        hlb_last_rl_corr_factor = {}
        hlb_av_heat_loop_rlT = None

        for raum in rooms:
            raum_params = raum.get_module_parameters()
            room_check = raum_params.get('aha_pro', {}).get('status', False)
            if room_check == 'true' or all_rooms_status == 'true':
                hlb_heat_loop_rlT = raum_params.get('aha_pro', {}).get('hlb_heat_loop_rlT', 0)
                if hlb_heat_loop_rlT:
                    all_hlb_heat_loop_rlT[raum.id] = hlb_heat_loop_rlT
                regparams = raum.regelung.get_parameters()
                try:
                    for ssn, out in regparams['rlsensorssn'].items():
                        rlsensor = AbstractSensor.get_sensor(ssn)
                        last_rl = rlsensor.get_wert()[0]
                        rl_values[raum.id] = last_rl
                except:
                    pass

        if all_hlb_heat_loop_rlT:
            hlb_av_heat_loop_rlT = sum(all_hlb_heat_loop_rlT.values()) / float(len(all_hlb_heat_loop_rlT))
            hlb_av_heat_loop_rlT = round(hlb_av_heat_loop_rlT, 2)  # rounding to 2 decimal points
            write_log('All rooms:\nhlb_av_heat_loop_rlT: %s\n' % hlb_av_heat_loop_rlT, '/var/log/uwsgi/.aha_pro_heizlastberechung.txt')

            for room_id, hlb_heat_loop_rlT in all_hlb_heat_loop_rlT.items():
                hlb_last_rl_corr_factor[room_id] = round(hlb_av_heat_loop_rlT - hlb_heat_loop_rlT, 2)

        # calculating each elements of rl_values + hlb_last_rl_corr_factor according their raum.id
        result = {}
        for room_id in rl_values:
            if rl_values.get(room_id):
                result[room_id] = round(rl_values.get(room_id) + hlb_last_rl_corr_factor.get(room_id, 0), 2)

        # Logging the detailed information
        for room in rooms:
            room_id = room.id
            write_log(
                '\nRoom %s (%s):\nincluded: %s\nhlb_heat_loop_rlT: %s\nlast_rl: %s\nhlb_last_rl_corr_factor: %s\nhlb_corrected_last_rl: %s' % (
                    room_id, room.name, 'true' if room_id in all_hlb_heat_loop_rlT.keys() else 'false', all_hlb_heat_loop_rlT.get(room_id, 'None'), rl_values.get(room_id, 'None'),
                    hlb_last_rl_corr_factor.get(room_id, 'None'), result.get(room_id, 'None')), '/var/log/uwsgi/.aha_pro_heizlastberechung.txt'
            )

        return result, hlb_av_heat_loop_rlT

    rooms = Raum.objects.all()
    rl_values = []
    for raum in rooms:
        raum_params = raum.get_module_parameters()
        room_check = raum_params.get('aha_pro', {}).get('status', False)
        if room_check == 'true' or all_rooms_status == 'true':
            regparams = raum.regelung.get_parameters()
            try:
                for ssn, out in regparams['rlsensorssn'].items():
                    rlsensor = AbstractSensor.get_sensor(ssn)
                    last_rl = rlsensor.get_wert()[0]
                    rl_values.append(last_rl)
            except:
                pass

    # Check if rl_values is not empty
    if rl_values:

        # Method 3: Calculate Median
        if method == '3':
            rl_values.sort()  # first we sort the list
            length = len(rl_values)
            if (length % 2) == 0:  # if list length is even
                median_rl = (rl_values[length//2] + rl_values[length//2 - 1]) / 2.0
            else:
                median_rl = rl_values[length//2]
            write_log('Calculate Median of last_rl is: %s' % median_rl)
            return median_rl

        # Method 4: Calculate Average
        elif method == '4':
            average_rl = sum(rl_values) / float(len(rl_values))
            write_log('Calculate Average of last_rl is: %s' % average_rl)
            return average_rl

        # Method 5: Choose Min
        elif method == '5':
            min_rl = min(rl_values)
            write_log('Calculate Min of last_rl is: %s' % min_rl)
            return min_rl

        # Method 6: Choose Max
        elif method == '6':
            max_rl = max(rl_values)
            write_log('Calculate Max of last_rl is: %s' % max_rl)
            return max_rl
    else:
        write_log('No last_rl values found.')
        return


def calc_new_opening(gateway, out, last_rl, raum, rl_soll, haus):
    """Calculates and logs new opening"""
    cnt_opening = retrieve_cnt_opening(gateway, out)
    if last_rl is None:
        return
    dev_rl = rl_soll - last_rl
    write_log('6: Calculate dev_rl = %s & cnt_opening: %s.' % (dev_rl, cnt_opening))
    ahapro = retrieve_ahapro(haus)
    change_factor = calc_change_factor(cnt_opening, dev_rl, ahapro)
    val = cnt_opening + change_factor
    if val <= ahapro['min_valve_value']:
        val = ahapro['min_valve_value']
    if float(val) > 99:
        val = 99
    write_log('8: Calculate val: %s & change_factor: %s.' % (val, change_factor))
    ch.set("%s_%s" % (gateway.name, out), "<%s>" % val)
    log_final_opening_val(val)
    logger.warning("%s|%s" % ('aha_pro', u"AHA-PRO: Ausgang %s auf %s%%. RL-Temperatur = %.2f, RL-Ziel = %s, Raum=%s." % (out, val, last_rl, rl_soll, raum.name)))
    return val

def retrieve_cnt_opening(gateway, out):
    """Returns the cnt opening for given gateway and out"""
    try:
        cnt_opening = ch.get("%s_%s" % (gateway.name, out))
        cnt_opening = float(cnt_opening[1:-1])
    except:
        cnt_opening = 0
    return cnt_opening

def retrieve_ahapro(haus):
    """Returns the ahapro parameters for given haus"""
    ahapro = haus.get_spec_module_params('aha_pro')
    aha_pro_change_factor_gain = ahapro.get('aha_pro_change_factor_gain', 0.1)
    aha_pro_max_change_factor = ahapro.get('aha_pro_max_change_factor', 20.0)
    aha_pro_min_valve_threshold = ahapro.get('aha_pro_min_valve_threshold', 5.0)
    aha_pro_min_valve_value = ahapro.get('aha_pro_min_valve_value', 0)
    write_log('7: values: factor_gain:%s, max_change_factor:%s, min_valve_threshold:%s, min_valve_value:%s.' %
        (aha_pro_change_factor_gain, aha_pro_max_change_factor, aha_pro_min_valve_threshold, aha_pro_min_valve_value))
    return {
        'change_factor_gain': aha_pro_change_factor_gain,
        'max_change_factor': aha_pro_max_change_factor,
        'min_valve_threshold': aha_pro_min_valve_threshold,
        'min_valve_value': aha_pro_min_valve_value
    }

def calc_change_factor(cnt_opening, dev_rl, ahapro):
    """Calculates change factor"""
    change_factor = ahapro['change_factor_gain'] * dev_rl * cnt_opening
    if abs(change_factor) > ahapro['max_change_factor']:
        change_factor = ahapro['max_change_factor'] if change_factor > 0 else - ahapro['max_change_factor']
    return change_factor

def log_final_opening_val(val):
    """Logs final opening value"""
    write_log('9: final new opening val: %s.' % val)

def get_rlsoll(raum, custom_rsoll, method):
    """Returns rlsoll for given raum"""
    write_log('### custom_rsoll: %s, method: %s' % (custom_rsoll, method))
    if method == '1':
        raum_params = raum.get_module_parameters()
        room_value = 'aha_pro_value_%s' % raum.id
        rlsoll = raum_params.get(room_value, None)
    else:
        rlsoll = str(custom_rsoll)
    if rlsoll:
        ch.set("rlsoll_%s" % raum.id, rlsoll)
        return float(rlsoll)
    return

def get_last_rl(raum, out):
    """Returns the last rl for given raum and out"""
    regparams = raum.regelung.get_parameters()
    for ssn, outs in regparams['rlsensorssn'].items():
        if out in outs:
            rlsensor = AbstractSensor.get_sensor(ssn)
            last_rl = None
            if rlsensor and len(outs):
                try:
                    last_rl = rlsensor.get_wert()[0]
                except TypeError:
                    continue
            return last_rl

@shared_task(base=CeleryErrorLoggingTask)
def aha_pro_calc():
    """Main function for aha_pro_calc"""
    write_log('$(date)')
    haus = Haus.objects.first()
    ahapro = haus.get_spec_module_params('aha_pro')
    ahapro_check = ahapro.get('aha_pro_status', False)
    all_rooms_status = ahapro.get('all_rooms_status', False)
    if ahapro_check and ahapro_check == 'true':
        log_aha_pro_active()
        method = ahapro.get('algorithm', '1')
        if method == '2':
            custom_rsoll, av_calculation = calculate_rsoll(method, all_rooms_status)
        else:
            custom_rsoll = calculate_rsoll(method, all_rooms_status)
            av_calculation = None
        for haus in Haus.objects.all():
            gateways = list(chain(Gateway.objects.filter(haus=haus)))
            write_log('2: Iterate on Gateways.')
            for gateway in gateways:
                write_log('------------------------------------')
                if isinstance(gateway, Gateway) and "6.00" > gateway.version >= "5.00":
                    process_gateway_greater_than_5(gateway, haus, custom_rsoll, method, av_calculation)
                elif isinstance(gateway, Gateway) and gateway.version < "5.00":
                    process_gateway_less_than_5(gateway)
            continue
    else:
        sys.exit()

def log_aha_pro_active():
    """Logs the AHA-PRO is active"""
    write_log('1: AHA-PRO is Active.')
    logger.warning("%s|%s" % ("aha_pro", u"Modus AHA-PRO: Das Plugin AHA-PRO hat die Regelung des Raumes übernommen."))

def process_gateway_greater_than_5(gateway, haus, custom_rsoll, method, av_calculation):
    """Processes for gateways with version greater than 5"""
    write_log('3: Gateway: %s version > 5.' % gateway.name)
    for ausgang in gateway.ausgaenge.all():
        try:
            process_ausgang(ausgang, gateway, haus, custom_rsoll, method, av_calculation)
        except:
            write_log('3: Error in Gateway: %s version > 5.' % gateway.name)
            write_log('------------------------------------')

def process_gateway_less_than_5(gateway):
    """Processes for gateways with version less than 5"""
    write_log('3: Gateway: %s version < 5.' % gateway.name)
    write_log('------------------------------------')

def process_ausgang(ausgang, gateway, haus, custom_rsoll, method, av_calculation):
    """Processes each ausgang"""
    try:
        raum = ausgang.regelung.get_raum()
        raum_params = raum.get_module_parameters()
        room_check = raum_params.get('aha_pro', {}).get('status', False)
        ahapro = haus.get_spec_module_params('aha_pro')
        all_rooms_status = ahapro.get('all_rooms_status', False)
        if  all_rooms_status == 'true' or room_check == 'true':
            write_log('4: Calculate room: Raum:%s.' % raum.name)
            for ausgangno in ausgang.ausgang.split(','):
                out = int(ausgangno.strip()) if str(ausgangno).strip().isdigit() else ausgangno.strip()
                if method == '2':
                    rlsoll = av_calculation
                    last_rl = custom_rsoll.get(raum.id)
                    custom_rsoll = av_calculation
                else:
                    rlsoll = get_rlsoll(raum, custom_rsoll, method)
                    last_rl = get_last_rl(raum, out)
                write_log('5: calculate values: last_rl:%s, rlsoll:%s.' % (last_rl, rlsoll))
                if rlsoll and last_rl:
                    final_val = calc_new_opening(gateway, out, last_rl, raum, rlsoll, haus)
                    select_method = {'1': 'Manuell_RL', '2': 'Heizlastberechung', '3': 'Median-RL', '4': 'Average-RL', '5': 'Min-RL', '6': 'Max-RL'}
                    logger.warning("%s|%s" % ("aha_pro", u"%s: %s an Ausgang %s mit %s an Gateway %s in Raum %s." % (select_method.get(method), custom_rsoll, out, final_val, gateway.name, raum.name)))
                else:
                    write_log('6: Error: problem with rsoll or last_rl.')
                write_log('------------------------------------')
        else:
            write_log('4: Checkbox off for Raum:%s.' % raum.name)
    except:
        pass
