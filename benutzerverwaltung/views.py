# coding=utf-8
from django.db import IntegrityError
from heizmanager.render import render_response, render_redirect
from heizmanager.models import Haus, Etage, Raum, Regelung
from users.models import UserProfile
from django.contrib.auth.models import User
import logging
from benutzerverwaltung.models import ObjectPermission
from django.contrib.contenttypes.models import ContentType
from benutzerverwaltung.decorators import is_admin


def get_name():
    return 'benutzerverwaltung'


def get_cache_ttl():
    return 1800


def is_togglable():
    return True


def get_offset(haus, raum=None):
    offset = 0.0
    return offset


def is_hidden_offset():
    return False


def calculate_always_anew():
    return True


def get_global_settings_link(request, haus):
    return "Benutzerverwaltung"


def get_global_description_link():
    desc = None
    desc_link = None
    return desc, desc_link


def get_global_settings_page(request, haus):
    pass


def get_global_settings_page_help(request, haus):
    pass


def get_local_settings_page_haus(request, haus, action=None, objid=None):

    if request.user.userprofile.get().role != 'A' and \
            ((objid and int(objid) != request.user.id) or (action in {'add', 'access', 'delete'})):
        return render_redirect(request, '/m_benutzerverwaltung/%s/' % haus.id)

    if request.method == "GET":

        if action == 'add':
            return render_response(request, "m_benutzerverwaltung_add_user.html", {'haus': haus})

        elif action == 'profile':
            profile = UserProfile.objects.get(pk=long(objid))
            return render_response(request, "m_benutzerverwaltung_edit_profile.html",
                                   {'haus': haus, 'profile': profile})

        elif action == 'password':
            profile = UserProfile.objects.get(pk=long(objid))
            req_old_pw = False
            if profile.pk == request.user.userprofile.get().pk:
                req_old_pw = True
            return render_response(request, "m_benutzerverwaltung_edit_password.html",
                                   {'haus': haus, 'profile': profile, 'req_old_pw': req_old_pw})

        elif action == 'access':
            profile = UserProfile.objects.get(pk=long(objid))
            eundr = []
            for etage in haus.etagen.all():
                e = {etage: []}
                for raum in Raum.objects.filter_for_user(request.user, etage_id=etage.id):
                    e[etage].append(raum)
                eundr.append(e)

            ct = ContentType.objects.get_for_model(Raum)
            ct_regelung = ContentType.objects.get_for_model(Regelung)
            objperms = ObjectPermission.objects.filter(user=profile.user, content_type=ct)
            logging.warning(objperms)
            access_list = [objperm.object_id for objperm in objperms]
            params = haus.get_module_parameters()
            # get diff params
            user = profile.user
            # get list
            diff_params = []
            vor_params = []
            pl_params = []
            regs = Regelung.objects.filter(regelung__in=['differenzregelung', 'vorlauftemperaturregelung', 'pumpenlogik'])
            for reg in regs:
                reg_id = reg.get_parameters().values()[0]
                if reg.regelung == "differenzregelung":
                    diff_params.append((reg.id, reg_id, params.get(reg.regelung, {}).get(reg_id, {}))) #[reg.id] = params.get(reg.regelung, {}).get(reg_id, {})
                elif reg.regelung == "vorlauftemperaturregelung":
                    vor_params.append((reg.id, reg_id, params.get(reg.regelung, {}).get(reg_id, {})))
                elif reg.regelung == "pumpenlogik":
                    pl_params.append((reg.id, reg.get_parameters()['name']))
            diff_params.sort(key=lambda tup: tup[1])
            vor_params.sort(key=lambda tup: tup[1])
            pl_params.sort(key=lambda tup: tup[1])
            # checked list
            objperms = ObjectPermission.objects.filter(user=user, content_type=ct_regelung)
            access_list_reg = [objperm.object_id for objperm in objperms]

            u_params = profile.get_parameters()
            permissions = u_params.get('permissions', {})
            perm_params = {}

            # smart ui access
            perm_params['smartui_access'] = permissions.get('smartui_access', 'full_access')
            # default smart ui tab
            perm_params['smartui_default_tab'] = permissions.get('smartui_default_tab', 'temporal')

            # smart ui access
            perm_params['proui_access'] = permissions.get('proui_access', 'full_access')
            # default pro ui func
            perm_params['proui_default_func'] = permissions.get('proui_default_func', 'permanent')
            # multizone_showtemp
            if permissions.get('multizone_showtemp'):
                perm_params['proui_tempbar'] = True

            user_main_page = u_params.get('user_main_page', 'pro')
            return render_response(request, "m_benutzerverwaltung_edit_user.html",
                                {'haus': haus, 'profile': profile, 'eundr': eundr, 'user_main_page': user_main_page,
                                    'access_list': access_list, 'diff_params': diff_params,
                                    'vor_params': vor_params, 'pl_params': pl_params,
                                    'access_list_reg': access_list_reg, 'perm_params': perm_params})

        elif action == 'delete':
            profile = UserProfile.objects.get(pk=long(objid))
            user = profile.user
            if user.id == haus.eigentuemer_id:
                return render_redirect(request, "/m_benutzerverwaltung/%s/" % haus.id)
            profile.delete()
            user.delete()
            return render_redirect(request, "/m_benutzerverwaltung/%s/" % haus.id)

        else:
            h_params = haus.get_module_parameters()
            only_smart = h_params.get('only_smart', False)
            if request.user.userprofile.get().role != 'A' or only_smart:
                userlist = [request.user.userprofile.get()]
            else:
                userlist = UserProfile.objects.filter(user_id__in=haus.besitzer).order_by('id')
            return render_response(request, "m_benutzerverwaltung.html", {'userlist': userlist,
                                                                          'haus': haus, 'only_smart': only_smart })

    elif request.method == "POST":

        if action == 'add':
            vorname = request.POST.get('vorname')
            nachname = request.POST.get('nachname')
            email = request.POST.get('email').lower()
            passwort = request.POST.get('passwort')
            pw2 = request.POST.get('pw2')

            if not len(email) or '@' not in email or '.' not in email:
                error = u"Bitte gültige Emailadresse angeben."
                return render_response(request, "m_benutzerverwaltung_add_user.html",
                                       {'haus': haus, 'error': error, 'vorname': vorname, 'nachname': nachname,
                                        'email': email})

            if len(email) > 30:
                error = u"Emailadresse darf nicht länger als 30 Zeichen sein."
                return render_response(request, "m_benutzerverwaltung_add_user.html",
                                       {'haus': haus, 'error': error, 'vorname': vorname, 'nachname': nachname,
                                        'email': email})

            if passwort != pw2:
                error = u"Passwörter stimmen nicht überein."
                return render_response(request, "m_benutzerverwaltung_add_user.html",
                                       {'haus': haus, 'error': error, 'vorname': vorname, 'nachname': nachname,
                                        'email': email})

            try:
                usr = User.objects.create_user(email, email, passwort)
            except IntegrityError as e:
                if str(e) == "UNIQUE constraint failed: auth_user.username":
                    error = u"Benutzername/Email existiert bereits."
                else:
                    error = u"Ein Fehler ist aufgetreten, bitte versuchen Sie es erneut."
                    logging.exception("fehler:")
                return render_response(request, "m_benutzerverwaltung_add_user.html",
                                       {"haus": haus, 'error': error, 'vorname': vorname, 'nachname': nachname,
                                        "email": email})
            usr.is_active = True
            usr.save()

            profile = usr.userprofile.get()
            profile.vorname = vorname
            profile.nachname = nachname
            profile.role = 'A'
            profile.save()

            haus.besitzer.append(usr.id)
            haus.save()

            ct = ContentType.objects.get_for_model(Raum)
            for etage in haus.etagen.all():
                e = {etage: []}
                for raum in Raum.objects.filter_for_user(request.user, etage_id=etage.id):
                    o = ObjectPermission(content_type=ct, object_id=raum.id, permission='1', user=usr)
                    o.save()
            
            ct_regelung = ContentType.objects.get_for_model(Regelung)
            regs = Regelung.objects.filter(regelung__in=['differenzregelung', 'vorlauftemperaturregelung'])
            for reg in regs:
                o = ObjectPermission(content_type=ct_regelung, object_id=reg.id, permission='1', user=usr)
                o.save()

            u_params = profile.get_parameters()
            u_params['permissions'] = {}
            u_params['permissions']['multizone_quick'] = True
            u_params['permissions']['multizone_pro'] = True
            u_params['permissions']['multizone_raumregelung_smart'] = True
            u_params['permissions']['multizone_showtemp'] = True
            u_params['permissions']['multizone_permanent_tab'] = True
            profile.set_parameters(u_params)

            return render_redirect(request, "/m_benutzerverwaltung/%s/" % haus.id)

        elif action == 'profile':
            profile = UserProfile.objects.get(pk=long(objid))
            vorname = request.POST.get('vorname')
            nachname = request.POST.get('nachname')
            email = request.POST.get('email').lower()
            if not len(email) or '@' not in email or '.' not in email:
                error = u'Bitte gültige Emailadresse angeben.'
                return render_response(request, "m_benutzerverwaltung_edit_profile.html",
                                       {'haus': haus, 'profile': profile, 'error': error})
            profile.vorname = vorname
            profile.nachname = nachname
            profile.save()
            profile.user.username = email
            profile.user.email = email
            profile.user.save()

            return render_redirect(request, "/m_benutzerverwaltung/%s/access/%s/" % (haus.id, profile.id))

        elif action == 'password':
            req_old_pw = False
            profile = UserProfile.objects.get(pk=long(objid))
            if profile.pk == request.user.userprofile.get().pk:
                req_old_pw = True
                oldpw = request.POST.get('oldpw', '')
                if not request.user.check_password(oldpw):
                    error = "Falsches altes Passwort."
                    return render_response(request, "m_benutzerverwaltung_edit_password.html",
                                           {'haus': haus, 'profile': profile, 'req_old_pw': req_old_pw, 'error': error})

            newpw1 = request.POST.get('newpw1')
            newpw2 = request.POST.get('newpw2')
            if newpw1 != newpw2:
                error = u'Neue Passwörter stimmen nicht überein.'
                return render_response(request, "m_benutzerverwaltung_edit_password.html",
                                       {'haus': haus, 'profile': profile, 'req_old_pw': req_old_pw, 'error': error})

            profile.user.set_password(newpw1)
            profile.user.save()
            return render_redirect(request, '/m_benutzerverwaltung/%s/access/%s/' % (haus.id, profile.id))

        elif action == 'access':

            logging.warning(request.POST)

            profile = UserProfile.objects.get(pk=long(objid))
            usr = profile.user
            configaccess = bool(int(request.POST.get('configaccess', 0)))
            if configaccess:
                profile.role = 'A'
            else:
                if usr.id != haus.eigentuemer_id:
                    profile.role = 'K'
            profile.save()

            ct = ContentType.objects.get_for_model(Raum)
            ct_regelung = ContentType.objects.get_for_model(Regelung)
            ObjectPermission.objects.filter(user=usr, content_type=ct).delete()
            ObjectPermission.objects.filter(user=usr, content_type=ct_regelung).delete()

            for key in request.POST:
                if key.startswith('room-'):
                    try:
                        raum = Raum.objects.get(pk=long(key.split('-')[1]))
                    except Raum.DoesNotExist:
                        continue
                    else:
                        o = ObjectPermission(content_type=ct, object_id=raum.id, permission='1', user=usr)
                        o.save()

            for key in request.POST:
                if key.startswith('diff-') or key.startswith('vor-') or key.startswith('pl-'):
                    try:
                        regelung = Regelung.objects.get(pk=long(key.split('-')[1]))
                    except Regelung.DoesNotExist:
                        continue
                    else:
                        o = ObjectPermission(content_type=ct_regelung, object_id=regelung.id, permission='1', user=usr)
                        o.save()

            u_params = profile.get_parameters()
            accesses = {}
            # default interface
            u_params['user_main_page'] = request.POST.get('mainpage_selection', 'pro')

            # smart ui access
            accesses['smartui_access'] = request.POST.get('smartui_access', 'full_access')
            if accesses['smartui_access'] == 'full_access':
                # smart ui default smart tab
                accesses['smartui_default_tab'] = request.POST.get('smartui_default_tab')
            else:
                accesses['smartui_default_tab'] = 'temporal'

            accesses['proui_access'] = request.POST.get('proui_access', 'full_access')
            if accesses['proui_access'] == 'full_access':
                # pro ui access
                accesses['proui_default_func'] = request.POST.get('proui_default_func')
            else:
                accesses['proui_default_func'] = 'permanent'

            if 'multizone_showtemp' in request.POST:
                accesses['multizone_showtemp'] = True
            else:
                try:
                    accesses.pop('multizone_showtemp')
                except KeyError:
                    pass

            u_params['permissions'] = accesses
            profile.set_parameters(u_params)

            return render_redirect(request, "/m_benutzerverwaltung/%s/" % haus.id)
