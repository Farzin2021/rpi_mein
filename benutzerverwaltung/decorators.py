from functools import wraps
from django.db.models.query import QuerySet
from benutzerverwaltung.models import ObjectPermission
from django.contrib.auth.models import User, AnonymousUser
from heizmanager.render import render_redirect


def perm_req(qsmethod):
    def real_decorator(func):

        @wraps(func)
        def _wrapped_view(manager, *args, **kwargs):

            if not len(args) or not isinstance(args[0], User):
                return manager.none()

            qs = manager.get_queryset()
            qs = getattr(qs, qsmethod)(**kwargs)

            user = args[0]
            #role = user.userprofile.get().role
            is_admin = user.userprofile.get().role == 'A'
            if not is_admin:
                if isinstance(qs, QuerySet):
                    for obj in qs:
                        if not ObjectPermission.objects.has_perm('0', user, obj):
                            qs = qs.exclude(pk=obj.pk)

                elif not ObjectPermission.objects.has_perm('0', user, qs):
                    return manager.none()

            return qs

        return _wrapped_view
    return real_decorator


def is_admin(func):

    @wraps(func)
    def _wrapped_view(request, *args, **kwargs):

        if isinstance(request.user, AnonymousUser):
            from heizmanager.models import Haus
            if not len(Haus.objects.all()) and "dbul" in request.path_info:
                return func(request, *args, **kwargs)
            else:
                return render_redirect(request, '/')
        
        else:
            if request.user.userprofile.get().role != 'K':
                return func(request, *args, **kwargs)
            else:
                return render_redirect(request, '/')

    return _wrapped_view


def has_regelung_access(func):
    @wraps(func)
    def _wrapped_view(request, *args, **kwargs):
        objid = args[2]
        from heizmanager.models import Regelung
        if request.path_info.find('differenzregelung') > -1:
            regs = [r.get_parameters().values()[0] for r in Regelung.objects.filter_for_user(request.user).filter(regelung='differenzregelung')]
        elif request.path_info.find('vorlauftemperaturregelung') > -1:
            regs = [r.get_parameters().values()[0] for r in Regelung.objects.filter_for_user(request.user).filter(regelung='vorlauftemperaturregelung')]
        elif request.path_info.find('fps') > -1:
            regs = [str(r.id) for r in Regelung.objects.filter_for_user(request.user).filter(regelung='fps')]
        elif request.path_info.find('pumpenlogik') > -1:
            regs = [str(r.id) for r in Regelung.objects.filter_for_user(request.user).filter(regelung='pumpenlogik')]

        if objid not in regs:
            return render_redirect(request, '/')

        return func(request, *args, **kwargs)

    return _wrapped_view


def is_superuser(uid):
    from heizmanager.models import Haus
    if len(Haus.objects.all()) == 0:
        return True
    for haus in Haus.objects.all():
        if haus.eigentuemer_id == uid:
            return True


def has_access_pro(request):
    # only smartui
    from heizmanager.models import Haus
    haus = Haus.objects.first()
    if haus is None:
        return True
    h_params = haus.get_module_parameters()
    only_smart = h_params.get('only_smart', False)
    if only_smart:
        return False

    userid = request.user.id
    if is_superuser(userid):
        return True
    # if request.user.userprofile.get().role != 'K':
    #     return True
    profile = request.user.userprofile.get()
    u_params = profile.get_parameters()
    perm_list = u_params.get('permissions', {})
    if perm_list.get('proui_access', 'full_access') != 'no_access':
        return True
    else:
        return False


def has_access_quick(request):
    userid = request.user.id
    if is_superuser(userid):
        return True
    # if request.user.userprofile.get().role != 'K':
    #     return True
    profile = request.user.userprofile.get()
    u_params = profile.get_parameters()
    multizone_list = u_params.get('permissions', {}).keys()
    if 'multizone_quick' in multizone_list:
        return True
    else:
        return False


def has_access_raumregelung_smart(request):
    userid = request.user.id
    if is_superuser(userid):
        return True

    profile = request.user.userprofile.get()
    u_params = profile.get_parameters()
    perm_list = u_params.get('permissions', {})
    if perm_list.get('smartui_access', 'full_access') != 'no_access':
        return True
    else:
        return False


def smartui_tab_access(request):
    userid = request.user.id
    if is_superuser(userid):
        return 'full_access'

    profile = request.user.userprofile.get()
    u_params = profile.get_parameters()
    perm_d = u_params.get('permissions', {})
    return perm_d.get('smartui_access', 'full_access')


def smartui_default_tab(request):
    profile = request.user.userprofile.get()
    u_params = profile.get_parameters()
    perm_d = u_params.get('permissions', {})
    return perm_d.get('smartui_default_tab', 'temporal')


def proui_default_func(request):
    profile = request.user.userprofile.get()
    u_params = profile.get_parameters()
    perm_d = u_params.get('permissions', {})
    return perm_d.get('proui_default_func', 'permanent')


def proui_functionality_access(request):
    userid = request.user.id
    if is_superuser(userid):
        return 'full_access'

    profile = request.user.userprofile.get()
    u_params = profile.get_parameters()
    perm_d = u_params.get('permissions', {})
    return perm_d.get('proui_access', 'full_access')


def multizone_pro_access(func):

    @wraps(func)
    def _wrapped_view(request, *args, **kwargs):
        if has_access_pro(request):
            return func(request, *args, **kwargs)
        elif has_access_raumregelung_smart(request):  # todo hide quick
            return render_redirect(request, '/raumregelung-smart')
        else:
            return render_redirect(request, "/no-access")
    return _wrapped_view


def user_main_page_check(func):

    @wraps(func)
    def _wrapped_view(request, *args, **kwargs):
        profile = request.user.userprofile.get()
        u_params = profile.get_parameters()
        main_page = u_params.get('user_main_page', 'pro')

        # only smartui
        from heizmanager.models import Haus

        haus = Haus.objects.first()
        if haus is None:
            return func(request, *args, **kwargs)
        h_params = haus.get_module_parameters()
        only_smart = h_params.get('only_smart', False)
        if only_smart:
            if haus.eigentuemer_id == request.user.id:
                return render_redirect(request, '/raumregelung-smart')
            else:
                return render_redirect(request, '/no-access')

        if main_page == 'pro':
            return func(request, *args, **kwargs)
        else:
            relevant_path_d = {
                'smart': '/raumregelung-smart',
            }
            return render_redirect(request, relevant_path_d[main_page])

        return func(request, *args, **kwargs)
    return _wrapped_view


def multizone_raumregelung_smart_access(func):
    from heizmanager.models import Haus
    @wraps(func)
    def _wrapped_view(request, *args, **kwargs):
        userid = request.user.id
        for haus in Haus.objects.all():
            if haus.eigentuemer_id == userid:
                return func(request, *args, **kwargs)

        if has_access_raumregelung_smart(request):
            return func(request, *args, **kwargs)
        elif has_access_pro(request):
            return render_redirect(request, "/raumregelung-pro")
        else:
            return render_redirect(request, "/no-access")
    return _wrapped_view


def multizone_quick_access(func):
    @wraps(func)
    def _wrapped_view(request, *args, **kwargs):
        from heizmanager.models import Haus
        userid = request.user.id
        for haus in Haus.objects.all():
            if haus.eigentuemer_id == userid:
                return func(request, *args, **kwargs)
        # if request.user.userprofile.get().role != 'K':
        #     return func(request, *args, **kwargs)
        if has_access_quick(request):
            return func(request, *args, **kwargs)
        elif 'smartui' in request.POST and has_access_raumregelung_smart(request):
            return func(request, *args, **kwargs)
        elif has_access_pro(request):
            return render_redirect(request, '/')
        else:
            return render_redirect(request, "/no-access")
    return _wrapped_view


def multizone_pro_access_page(func):
    @wraps(func)
    def _wrapped_view(request, *args, **kwargs):
        from heizmanager.models import Haus
        userid = request.user.id
        for haus in Haus.objects.all():
            if haus.eigentuemer_id == userid:
                return func(request, *args, **kwargs)
                # if request.user.userprofile.get().role != 'K':
                #     return func(request, *args, **kwargs)
        if has_access_pro(request):
            return func(request, *args, **kwargs)
        else:
            return render_redirect(request, "/no-access")
    return _wrapped_view


def show_pro_visualbar(request):
    userid = request.user.id
    if is_superuser(userid):
        return True
    # if request.user.userprofile.get().role != 'K':
    #     return True
    profile = request.user.userprofile.get()
    u_params = profile.get_parameters()
    multizone_list = u_params.get('permissions', {})
    if 'multizone_showtemp' in multizone_list:
        return True
    else:
        return False