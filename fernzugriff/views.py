# -*- coding: utf-8 -*-
from fabric.api import local, settings
from heizmanager.render import render_response, render_redirect
from heizmanager.network_helper import get_mac
from rpi.aeshelper import aes_encrypt
import requests
import json


def get_name():
    return "Fernzugriff"


def get_global_settings_link(request, haus):
    return "<a href='/m_setup/%s/fernzugriff/'>Fernzugriff</a>" % haus.id


def get_global_description_link():
    desc = u"Einfacher Fernzugriff dem kostenlosen Controme Direct Connect Service."
    desc_link = "http://support.controme.com/installation-und-inbetriebnahme/fernzugriff-einrichten/"
    return desc, desc_link


def get_global_settings_page(request, haus):

    hparams = haus.get_module_parameters()
    params = hparams.get('fernzugriff', {'active': False})

    if request.method == "GET":
        mac = get_mac()
        mac = mac.lower().replace('-', '')[-6:]
        try:
            status = local("sudo systemctl status openvpn@openvpn.service", capture=True)
        except:
            status = None

        if params['active']:
            local("touch /home/pi/rpi/fernzugriff/run.pid")
            try:
                local("sudo systemctl start openvpn@openvpn.service")
            except:
                pass
            if status is None or "service; disabled)" in status:
                try:
                    local("sudo systemctl enable openvpn@openvpn.service")
                except:
                    pass
        else:
            try:
                local("rm /home/pi/rpi/fernzugriff/run.pid")
            except:
                pass
            try:
                local("sudo systemctl stop openvpn@openvpn.service")
            except:
                pass

        try:
            status = local("sudo systemctl status openvpn@openvpn.service", capture=True)
        except:
            status = None

        if status is None or "Active: active (running)" not in status:
            is_running = False
            if status is not None and "Unit file changed on disk" in status:
                local("sudo systemctl daemon-reload")
                local("sudo systemctl restart openvpn@openvpn.service")
            try:
                ret = local("sudo ls -al /etc/openvpn/client.crt", capture=True)
                if "root root 0 " in ret:
                    local("sudo rm /etc/openvpn/ca.crt /etc/openvpn/client.crt /etc/openvpn/client.key /etc/openvpn/ta.key")
            except:
                pass  # file doesnt exist
        else:
            is_running = True

        return render_response(request, "m_settings_fernzugriff.html", {'address': mac, 'active': params['active'], 'is_running': is_running, 'haus': haus})

    elif request.method == "POST":

        if 'disable' in request.POST:
            try:
                local("rm /home/pi/rpi/fernzugriff/run.pid")
            except:
                pass
            try:
                local("sudo systemctl stop openvpn@openvpn.service")
            except:
                pass
            params['active'] = False
            hparams['fernzugriff'] = params
            haus.set_module_parameters(hparams)

        elif 'enable' in request.POST:
            local("touch /home/pi/rpi/fernzugriff/run.pid")
            try:
                local("sudo python /home/pi/rpi/fernzugriff/helper.py")
            except:
                pass
            try:
                local("sudo systemctl start openvpn@openvpn.service")
            except:
                pass

            ret = {}
            try:
                # das geht nur hier, weil wir hier wissen, der fernzugriff gestartet sein sollte
                with settings(warn_only=True):
                    r = local("sudo systemctl status openvpn@openvpn.service", capture=True)
                    if "Active: failed (Result: exit-code)" in r:
                        try:
                            r = local("grep 'Error: private key password verification failed' /var/log/syslog", capture=True)
                            if "Binary file" in r:
                                raise Exception()
                            else:
                                ret = {"error": "please delete keys"}
                        except:
                            ret = {"error": "couldnt start openvpn service"}
                        raise Exception()
                    elif "Active: activating (auto-restart)" in r and "(code=exited, status=1/FAILURE)" in r:
                        ret = {"error": "please delete keys"}
                        raise Exception()
                    elif "Active: active (running)" in r:
                        try:
                            r = local("ping -c1 10.8.0.1 -i 0.2", capture=True)
                            if "1 packets transmitted, 0 received, 100" in r:
                                ret = {"error": "please add route on fwd"}
                                raise Exception()
                        except:
                            ret = {"error": "please add route on fwd"}
                            raise Exception()
            except:
                try:
                    local("sudo rm /etc/openvpn/ca.crt /etc/openvpn/client.crt /etc/openvpn/client.key /etc/openvpn/ta.key")
                except:
                    pass
                try:
                    local("sudo python /home/pi/rpi/fernzugriff/helper.py")
                except:
                    pass
                try:
                    local("sudo systemctl daemon-reload")
                    local("sudo systemctl start openvpn@openvpn.service")  # wenns bisher eh nicht ging, koennen wir einfach starten
                except:
                    pass
                macaddr = get_mac()
                requests.post("http://controme-main.appspot.com/set/rpilog/%s/" % macaddr, data="data=%s" % aes_encrypt(json.dumps(ret)), timeout=5)

            params['active'] = True
            hparams['fernzugriff'] = params
            haus.set_module_parameters(hparams)

        return render_redirect(request, "/m_setup/%s/fernzugriff/" % haus.id)