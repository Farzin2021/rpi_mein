from django.apps import AppConfig


class GatewayMonitorConfig(AppConfig):
    name = 'gatewaymonitor'
    verbose_name = 'Gateway Monitor'

    def ready(self):
        from . import signals
