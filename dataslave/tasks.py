# -*- coding: utf-8 -*-
from __future__ import absolute_import

from celery import shared_task
import requests
from fabric.api import local, lcd, settings
from rpi.aeshelper import aes_encrypt
from fabric.state import env
import heizmanager.cache_helper as ch
from heizmanager.models import RPi, CryptKeys
from rf.models import RFController
from datetime import datetime, timedelta
from heizmanager import pytz
from rf.views import set_zwave
import heizmanager.network_helper as nh
import json
import base64
import logging
import time
import pytz
import os
from config.Controme_ETRV.helper import _local_handler
from django.contrib.auth.models import User
from heizmanager.models import Haus
from heizmanager.models import Raum
from rf.models import RFAktor
from funksollregelung.views import calculate_new_offset
from wifi.views import get_wlan_interface


url = "http://controme-main.appspot.com"
env.hosts = ["localhost"]
logger = logging.getLogger("logmonitor")
p_logger = logging.getLogger("servicemonitorlog")


class SetEncoder(json.JSONEncoder):
    def default(self, obj):
        if isinstance(obj, set):
            return list(obj)
        return json.JSONEncoder.default(self, obj)


@shared_task
def send_setup():

    try:
        ret = local("ip route show default | awk '/default/ {print $3; exit}' | xargs ping -c1", capture=True)
    except:
        logger.warning(u"%s|%s" % (0, u"Netzwerkverbindung zum Router unterbrochen."))
        try:
            ret = local("cat /etc/network/interfaces", capture=True)
            if "iface eth0 inet manual" in ret:
                local('printf "auto lo\niface lo inet loopback\niface eth0 inet dhcp\nallow-hotplug eth0\n" | sudo tee /etc/network/interfaces')
                ret = local("sudo service networking restart", capture=True)
            local("sudo ifdown eth0")
            local("sudo ifup --force eth0")
        except Exception as e:
            pass

    try:
        ret = local("sudo ifconfig eth0 | grep -i MTU", capture=True)
        if "1400" in ret or "1450" in ret or datetime.now().minute < 10:
            local("sudo ifconfig eth0 mtu 1500")
            ret = local('size=1272; while ping -s $size -c1 -M do google.com >&/dev/null; do ((size+=4)); done; echo "$((size-4+28))"', capture=True, shell="/bin/bash")
            if ret.isdigit():
                local("sudo ifconfig eth0 mtu %s" % ret)
                try:
                    local("sudo ifconfig tun0 mtu %s" % ret)
                except:
                    pass
    except:
        pass

    try:
        ret = local("ping -c1 controme-main.appspot.com", capture=True)
        if "0 received" in ret or "o route to host" in ret:
            p_logger.warning(u"%s|%s" % (0, u"Internetverbindung unterbrochen."))
    except:
        p_logger.warning(u"%s|%s" % (0, u"Internetverbindung unterbrochen."))

    ret = local("uptime -s", capture=True)
    ts = datetime.strptime(ret, "%Y-%m-%d %H:%M:%S")
    if (datetime.now()-ts).total_seconds() < 600:
        ln = u"System neugestartet um %s." % ret
        try:
            ret = local("grep '%s' /var/log/uwsgi/uwsgi.log" % ln[:-4])
        except:
            logger.warning(u"%s|%s" % (0, ln))

    try:
        r = float(local("w", capture=True).split('\n')[0][-4:])
        if r > 2:
            logger.warning(u"%s|%s" % (0, u"Miniserver unter hoher Last. Das System reagiert nur langsam und auf manche Anfragen u.U. gar nicht."))
    except Exception as e:
        logging.exception("error logging system load")

    macaddr = nh.get_mac()
    lan_ip = nh.get_ip()
    try:
        data = {'ip': lan_ip, 'ver': 1}

        try:
            rev = local("cat /proc/cpuinfo | grep '^Revision'", capture=True)
            data['rpimodel'] = rev.split(':')[1].strip()
        except:
            pass

        try:
            data['offline_devices'] = []
            gws = local("sqlite3 /home/pi/rpi/db.sqlite3 'select name from heizmanager_gateway'", capture=True)
            for gwmac in gws.split('\n'):
                if ch.get_gw_ping(gwmac) is None:
                    data['offline_devices'].append(gwmac)
            rpis = local(""" sqlite3 /home/pi/rpi/db.sqlite3 "select name from heizmanager_rpi where name != '%s'" """ % macaddr, capture=True)
            for rpimac in rpis.split('\n'):
                if ch.get_gw_ping(rpimac) is None:
                    data['offline_devices'].append(rpimac)
            rcs = local(""" sqlite3 /home/pi/rpi/db.sqlite3 "select heizmanager_rfaktor.name, heizmanager_rfcontroller.name from heizmanager_rfaktor inner join heizmanager_rfcontroller on heizmanager_rfaktor.controller_id = heizmanager_rfcontroller.id where heizmanager_rfaktor.type = 'hktControme'" """, capture=True)
            for rc in rcs.split('\n'):
                rc = rc.split('|')
                last = ch.get_gw_ping("_".join(rc))
                berlin = pytz.timezone('Europe/Berlin')
                now = datetime.now(berlin)
                if last is None or (now-last[0]).total_seconds() > 900:
                    data['offline_devices'].append("rc_%s" % rc[0])
        except:
            pass

        try:
            usage_vals = ch.get("usage_data")
            if usage_vals is not None:
                ch.set("usage_data", None)
                try:
                    mods = local("sqlite3 /home/pi/rpi/db.sqlite3 'select modules from heizmanager_hausprofil where id = 1;'", capture=True)
                    usage_vals['mods'] = mods.strip()
                except:
                    pass

                data['usage'] = usage_vals
            uptime = local("cat /proc/uptime", capture=True)
            uptime = uptime.split(' ')[0]
            data['uptime'] = float(uptime)
            data['only_post_request'] = True
            haus = Haus.objects.all()[0]
            email_params = haus.get_spec_module_params('benachrichtigung')
            email_s_params = haus.get_spec_module_params('support_benachrichtigung')
            activated = haus.get_spec_module_params('wetter_pro').get('is_active', False)
            data['emails'] = ''
            if activated:
                data['emails'] = email_params.get('device_emails', '')
            data['emails'] += email_s_params.get('device_emails', '')
            data['device_status'] = 'on' if data['emails'] else 'off'
            try:
                user = User.objects.get(id=haus.eigentuemer_id)
                user_profile = user.userprofile.get()
                user_name = user_profile.vorname + " " + user_profile.nachname
            except User.DoesNotExist:
                user_name = ""
            data['customer_name'] = user_name
            r = requests.post("%s/get/update/%s/" % (url, macaddr), data="data=%s" % aes_encrypt(json.dumps(data, cls=SetEncoder)), timeout=20)
            if r is not None and len(r.content):
                try:
                    rpi = RPi.objects.get(name=macaddr)
                except RPi.DoesNotExist:
                    # noch nichts angelegt, also egal
                    return
                try:
                    resp = json.loads(rpi.crypt_keys.decrypt(r.content).rsplit('}', 1)[0] + '}')
                except:
                    resp = {}
                _handle_response(rpi.haus, resp)

                if r.status_code == 404:
                    local("sudo supervisorctl -c /etc/supervisor/supervisord.conf restart messageserver zwavehandler")
                elif r.status_code == 418:
                    local("sudo shutdown -r now")
                else:
                    _check_rf_processes(macaddr)

        except Exception as e:
            try:
                del data['usage']
            except:
                pass
            data['uverror'] = str(e)
            if 'Read timed out.' not in str(e):
                r = requests.post("%s/get/update/%s/" % (url, macaddr), data="data=%s" % aes_encrypt(json.dumps(data)), timeout=20)
            else:
                return
        finally:
            ch.set('usage_vals', None)

    except:
        from uuid import getnode
        import sys, traceback
        # exc_type, exc_obj, exc_tb = sys.exc_info()
        # err = "Exception %s / %s in tasks.send_setup: %s" % (exc_type, type(exc_obj).__name__, exc_tb.tb_lineno)
        # err = err.replace("'", "")
        typ, value, trace = sys.exc_info()
        ret = {
            'path': 'tasks.send_setup',
            'exc_type': typ.__name__,
            'exc_msg': typ.__doc__,
            'exc_value': str(value).translate(None, "'").translate(None, '"'),
            'exc_trace': traceback.extract_tb(trace)
        }
        with lcd("/home/pi/rpi/"):
            enc = local("""python -c "from rpi.aeshelper import aes_encrypt; import json; print aes_encrypt(json.dumps(%s))" """ % ret, capture=True)
        err = enc
        macaddr = nh.get_mac()
        try:
            requests.post("http://controme-main.appspot.com/set/rpilog/%s/" % macaddr, data="data=%s" % err, timeout=10)
        except:
            pass

    try:
        handler_version = local("[ -f /etc/supervisor/conf.d/btle.conf ] && echo yes || echo no", capture=True)
        if handler_version == 'yes':
            def bluetooth_devices():
                res = os.popen('hcitool dev')
                devices = res.readlines()
                connected_devices = '\n'.join(str(p.strip()) for p in devices[1:])
                try:
                    if len(connected_devices) != 0:
                        return True
                    else:
                        return False
                except:
                    return False

            sh_reset_status = _local_handler('[ -f /var/log/uwsgi/.ble_reset_status ] && echo on || echo off')
            wifi_on = get_wlan_interface().get('wlan0', None)
            etrv_devices = RFAktor.objects.filter(type='hkteTRV').count()
            if sh_reset_status != 'on' and not wifi_on and etrv_devices > 1:
                ble_auto_script = ch.get('ble_auto_script')
                if ble_auto_script:
                    counter = ch.get('ble_auto_script').get('counter')
                    timer = ch.get('ble_auto_script').get('time')
                else:
                    counter = 0
                    timer = None
                if counter >= 3 and not bluetooth_devices():
                    log_text = "%s|%s" % ("Systemstatus", u"Automatischer Neustart durch Funkchip-Healing-Prozess.|2b")
                    logger.warning(log_text)
                    p_logger.warning(log_text)
                    try:
                        with open('/proc/uptime', 'r') as f:
                            uptime_seconds = float(f.readline().split()[0])
                            if uptime_seconds > 10800:
                                _local_handler('sudo /sbin/reboot')
                    except:
                        pass
                if not bluetooth_devices():
                    _local_handler("sudo /etc/init.d/bluetooth restart")
                    logger.warning("%s|%s" % ("Systemstatus", u"Funkchip-SW-Healing-Prozess aktiv.|1b"))
                    time.sleep(3)
                    os.popen('sudo bash /home/pi/rpi/config/Controme_ETRV/utils/hard_ble_reset.sh')
                    logger.warning("%s|%s" % ("Systemstatus", u"Funkchip-HW-Healing-Prozess aktiv.|1b"))
                    if not bluetooth_devices():
                        time.sleep(3)
                        _local_handler("sudo /etc/init.d/bluetooth restart")
                        time.sleep(2)
                        os.popen('sudo bash /home/pi/rpi/config/Controme_ETRV/utils/hard_ble_reset.sh')
                    if timer:
                        if time.time() - timer < 3600:
                            ble_auto = {'time': timer, 'counter': counter+1}
                        else:
                            ble_auto = {'time': time.time(), 'counter': 1}
                    else:
                        ble_auto = {'time': time.time(), 'counter': 1}
                    ch.set('ble_auto_script', ble_auto)
                    _local_handler('sudo supervisorctl -c /etc/supervisor/supervisord.conf restart btlehandler')
                    _local_handler('sudo supervisorctl -c /etc/supervisor/supervisord.conf restart newbtle')
    except:
        pass

    _backup_db(macaddr)
    _set_networkgw_ntp(macaddr)
    _check_fernzugriff(macaddr)


def _handle_response(haus, resp):
    hparams = haus.get_module_parameters()
    for k, v in resp.items():
        if k == "ts": continue
        elif k == 'permissions':
            for perm, has_perm in v.items():

                if perm == 'alexa_interface':
                    is_active = hparams.get('alexa_interface', dict()).get('is_active', False)
                    if is_active and not has_perm:
                        from alexa_interface.views import stop_service
                        stop_service(haus)
                    elif not is_active and has_perm:
                        from alexa_interface.views import start_service
                        start_service(haus)

                elif perm == "wetter_pro":
                    is_active = hparams.get('wetter_pro', dict()).get('is_active', False)
                    if is_active and not has_perm:
                        hparams['wetter_pro']['is_active'] = False
                        haus.set_module_parameters(hparams)
                    elif not is_active and has_perm:
                        hparams.setdefault('wetter_pro', dict())
                        hparams['wetter_pro']['is_active'] = True
                        haus.set_module_parameters(hparams)

                elif perm == "rfonly":
                    current = hparams.get('rfonly', False)
                    if current != has_perm:
                        hparams['rfonly'] = has_perm
                        haus.set_module_parameters(hparams)

                elif perm == "knx":
                    current = hparams.get('knx_active', False)
                    if current != has_perm:
                        hparams['knx_active'] = has_perm
                        haus.set_module_parameters(hparams)

        elif k == 'visible_modules':
            try:
                from heizmanager.modules.visible_modules import visible_modules
            except ImportError:
                visible_modules = "None"
            if v != visible_modules:
                if v == "None" and visible_modules is not None:
                    local("""echo "visible_modules = %s" > /home/pi/rpi/heizmanager/modules/visible_modules.py""" % v)
                    local("sudo supervisorctl -c /etc/supervisor/supervisord.conf restart uwsgi")
                elif v != "None":
                    local("""echo "visible_modules = '%s'" > /home/pi/rpi/heizmanager/modules/visible_modules.py""" % v)
                    active_modules = local("sqlite3 /home/pi/rpi/db.sqlite3 'select modules from heizmanager_hausprofil'", capture=True)
                    active_modules = set(active_modules.split(','))
                    local("""sqlite3 /home/pi/rpi/db.sqlite3 'update heizmanager_hausprofil set modules = "%s"'""" % ','.join(list(active_modules & set(v.split(',')))))
                    local("sudo supervisorctl -c /etc/supervisor/supervisord.conf restart uwsgi")

        else:
            if k == "do_update":
                continue  # eigentlich unnoetig, aber wir muessen ja auch nicht unnoetig zur db
            try:
                from heizmanager.models import RPi
                _rpi = RPi.objects.get(name=k)
                if _rpi.local_ip != v:
                    from rpi.server import use_host, use_localhost
                    if use_host == _rpi.local_ip:
                        local("""echo "use_localhost = %s; use_host = '%s';" > /home/pi/rpi/rpi/server.py""" % (use_localhost, v))
                    _rpi.local_ip = v
                    _rpi.save()
            except:
                pass

    path = "/home/pi/rpi"
    if resp.get('do_update'):
        try:
            local("sudo service ntp stop")
            local("sudo ntpd -qg")
            local("sudo service ntp start")
        except:
            pass

        with lcd(path) and settings(warn_only=True):
            result = local("git show HEAD", capture=True)
            head = result.split("\n")[0].split(' ')[1]

            result = local("git pull origin $(git rev-parse --abbrev-ref HEAD)", capture=True)
            if result.return_code != 0:
                if 'fatal: loose object' in result:
                    while not revert():
                        time.sleep(1)
            else:
                try:
                    local("rm /home/pi/rpi/failed_pull")
                except:
                    pass

            if "Already up-to-date." in result:
                head = local("git show HEAD", capture=True)
                last = head.split("\n")[0].split(' ')[1]
                requests.get("http://controme-main.appspot.com/get/update/%s/?ver=%s" % (nh.get_mac(), last))
            else:
                local("fab -f config/fab_upgrade.py upgrade:%s" % head)

    else:
        with lcd(path):
            result = local("git fsck", capture=True)
            if "fatal: loose object" in result:
                while not revert():
                    time.sleep(1)
            try:
                local("rm OZW_Log.txt")
            except:
                pass

            res = local("git diff --stat", capture=True)
            if len(res):
                files = res.split('\n')
                for f in files[:-1]:
                    _file = f.split('|')[0].strip()
                    local("git checkout %s" % _file)

    local("touch /home/pi/rpi/last_update")


def revert():
    print "trying to revert ..."
    with lcd("/home/pi/"):
        local("cp rpi/rpi/aeskey.py .")
        local("cp rpi/rpi/secret_key.py .")
        local("cp rpi/rpi/verification_code.py .")
        local("cp rpi/db.sqlite3 .")
        local("sudo supervisorctl -c /etc/supervisor/supervisord.conf stop all")
        local("rm -rf rpi")
        local("git clone https://hcerny@bitbucket.org/hcerny/rpi.git rpi/")
        local("mv aeskey.py rpi/rpi/")
        local("mv secret_key.py rpi/rpi/")
        local("mv verification_code.py rpi/rpi/")
        local("mv db.sqlite3 rpi/")
        local("sudo supervisorctl -c /etc/supervisor/supervisord.conf start all")
    return True
        #result = local('git reset --hard @{1}')


def _check_rf_processes(macaddr):
    try:
        rpi = RPi.objects.get(name=macaddr)
    except RPi.DoesNotExist:
        # noch nichts angelegt, also egal
        return
    ret = []
    reboot = []
    controllers = RFController.objects.filter(rpi=rpi)
    for controller in controllers:
        params = controller.get_parameters()
        wakeups = params.get("wakeup_intervals", dict()).get(0x84, dict()).values()
        if not len(wakeups):
            wakeups = [600]
        smallest_wakeup = min(wakeups)
        berlin = pytz.timezone('Europe/Berlin')
        now = datetime.now(berlin)

        last = ch.get_gw_ping(controller.name)
        if not last:
            params = controller.get_parameters()
            last_data = params.get('last_data')
            if last_data:
                set_zwave(None, controller.rpi.name, controller.name, last_data)
                last = ch.get_gw_ping(controller.name)

        if last:
            if len(last) == 2 and last[0] and last[1] and \
                    (last[0].replace(tzinfo=berlin)-last[1].replace(tzinfo=berlin)).total_seconds() < smallest_wakeup*2.5 \
                    and (now-last[0].replace(tzinfo=berlin)).total_seconds() < smallest_wakeup*2.5:
                ret.append(controller.name)
            elif (now-last[0].replace(tzinfo=berlin)).total_seconds() < smallest_wakeup+60:
                ret.append(controller.name)
            else:
                pass
        else:
            last_data = params.get('last_data')
            if last_data and 'ls' in last_data:
                ts = datetime.strptime(last_data['ls'], "%Y-%m-%d %H:%M:%S")
                ts.replace(tzinfo=berlin)
                if (now-ts).total_seconds() > smallest_wakeup:
                    reboot.append(controller.name)

    if not len(controllers):
        last = ch.get_gw_ping(macaddr.replace('-', '_'))
        if last:
            ret.append(macaddr)

    if not len(ret):
        if len(reboot):
            local("sudo reboot")

        else:
            local("sudo supervisorctl -c /etc/supervisor/supervisord.conf restart messageserver zwavehandler btlehandler")


def _backup_db(macaddr):
    mnt = int(1440 * (int(macaddr[-2:], 16)/256.0))
    if datetime.now()-timedelta(seconds=300) < datetime.now().replace(hour=mnt/60, minute=mnt % 60) < datetime.now()+timedelta(seconds=300):
        local("sqlite3 /home/pi/rpi/db.sqlite3 '.dump' > dump.txt")
        base64.encode(open("/home/pi/rpi/dump.txt", "rb"), open("/home/pi/rpi/dump.b64", "wb"))
        f = open("/home/pi/rpi/dump.b64", "rb")
        a = ''.join(f.readlines())
        a = a.replace("\n", "")
        requests.post("https://controme-main.appspot.com/db/%s/" % macaddr, data={'db': a}, timeout=20)
        local("rm /home/pi/rpi/dump.txt")
        local("rm /home/pi/rpi/dump.b64")


def _set_networkgw_ntp(macaddr):
    mnt = int(1440 * (int(macaddr[-2:], 16)/256.0))
    if datetime.now()-timedelta(seconds=800) < datetime.now().replace(hour=mnt/60, minute=mnt % 60) < datetime.now()+timedelta(seconds=800):
        gateway_ip = local("ip route show default | awk '/default/ {print $3}'", capture=True).strip().split('\n')[0]
        try:
            local("grep '%s' /etc/ntp.conf" % gateway_ip)
            local('sudo sed -i /etc/ntp.conf -e "s/^server .*/server %s/"' % gateway_ip)
        except:
            local("echo 'server %s' | sudo tee -a /etc/ntp.conf" % gateway_ip)
        try:
            local("grep 'pool 0.debian.pool.ntp.org iburst' /etc/ntp.conf")
        except:
            local("echo 'pool 0.debian.pool.ntp.org iburst' | sudo tee -a /etc/ntp.conf")
            local("echo 'pool 1.debian.pool.ntp.org iburst' | sudo tee -a /etc/ntp.conf")
            local("echo 'pool 2.debian.pool.ntp.org iburst' | sudo tee -a /etc/ntp.conf")
            local("echo 'pool 3.debian.pool.ntp.org iburst' | sudo tee -a /etc/ntp.conf")
        try:
            local("sudo pkill ntpd")
            local("sudo systemctl start ntp")
        except:
            pass


def _check_fernzugriff(macaddr):
    mnt = int(1440 * (int(macaddr[-2:], 16)/256.0))
    if datetime.now()-timedelta(seconds=300) < datetime.now().replace(hour=mnt/60, minute=mnt % 60) < datetime.now()+timedelta(seconds=300):
        ret = {}
        try:
            with settings(warn_only=True):
                r = local("sudo systemctl status openvpn@openvpn.service", capture=True)
                if "Active: failed (Result: exit-code)" in r:
                    try:
                        r = local("grep 'Error: private key password verification failed' /var/log/syslog", capture=True)
                        if "Binary file" in r:
                            raise Exception()
                        else:
                            ret = {"error": "please delete keys"}
                    except:
                        ret = {"error": "couldnt start openvpn service"}
                    raise Exception()
                elif "Active: activating (auto-restart)" in r and "(code=exited, status=1/FAILURE)" in r:
                    ret = {"error": "please delete keys"}
                    raise Exception()
                elif "Active: active (running)" in r:
                    try:
                        r = local("ping -c1 10.8.0.1 -i 0.2", capture=True)
                        if "1 packets transmitted, 0 received, 100" in r:
                            ret = {"error": "please add route on fwd"}
                            raise Exception()
                    except:
                        ret = {"error": "please add route on fwd"}
                        raise Exception()
                elif "Active: inactive (dead)" in r:
                    try:
                        local("sudo systemctl start openvpn@openvpn.service")
                    except:
                        pass
        except Exception:
            try:
                local("sudo rm /etc/openvpn/ca.crt /etc/openvpn/client.crt /etc/openvpn/client.key /etc/openvpn/ta.key")
            except:
                pass
            try:
                local("sudo python /home/pi/rpi/fernzugriff/helper.py")
            except:
                pass
            try:
                local("sudo systemctl daemon-reload")
                local("sudo systemctl start openvpn@openvpn.service")  # wenns bisher eh nicht ging, koennen wir einfach starten
            except:
                pass
            requests.post("http://controme-main.appspot.com/set/rpilog/%s/" % macaddr, data="data=%s" % aes_encrypt(json.dumps(ret)), timeout=5)


@shared_task
def general_functions():
    try:
        uwsgi_status  = local("cat /etc/logrotate.d/uwsgi | grep -q '#2022-05-20' && echo yes || echo no", capture=True)
        if uwsgi_status == 'no':
            local('sudo cp /home/pi/rpi/config/logrotate-uwsgi-improves /etc/logrotate.d/uwsgi')
            local('sudo supervisorctl -c /etc/supervisor/supervisord.conf restart all &')
    except:
        pass
    try:
        hw_cmd = _local_handler('mount | grep usbmount')
        if hw_cmd != 'no':
            hw_cmd = _local_handler('ls -la /home/pi/rpi/usbmount/db_log.db')
            if hw_cmd != 'no':
                _local_handler("""sudo umount -l /home/pi/rpi/usbmount;
                                sudo rm -r /home/pi/rpi/usbmount;
                                mkdir /home/pi/.usbmount;
                                cp -n /home/pi/rpi/heizmanager/scripts/db_log.db /home/pi/.usbmount;
                                sudo ln -s /home/pi/.usbmount /home/pi/rpi/usbmount;
                                sudo chmod -R 777 /home/pi/rpi/usbmount""")
                _local_handler('sudo supervisorctl -c /etc/supervisor/supervisord.conf restart all &')
    except:
        pass
    try:
        super_status = _local_handler('echo $(sudo systemctl is-active super_check.timer)')
        if super_status != 'active':
            _local_handler('cd /home/pi/rpi/heizmanager/scripts/ && sudo bash ble_service_create.sh super_check "sudo python /home/pi/rpi/config/celery_auto_heal.py" 30')
    except:
        pass
    try:
        devices = RFController.objects.all()
        for device in devices:
            if str(device.protocol) == 'enocean':
                params = device.get_parameters()
                if params.get('discovery', True):
                    params['discovery'] = False
                    device.set_parameters(params)
    except:
        pass

    try:
        def get_attr_or_empty(obj, attr_name):
            return getattr(obj, attr_name, '') if obj else ''

        # Retrieve values from the database
        mac_address_obj = RPi.objects.first()
        aes_key_obj = CryptKeys.objects.first()

        secret_dbs = [
            get_attr_or_empty(mac_address_obj, 'name'),
            get_attr_or_empty(aes_key_obj, 'aeskey'),
        ]
        file_paths = [
            '/sys/class/net/eth0/address',
            '/home/pi/rpi/rpi/aeskey.py',
        ]

        file_contents = []

        for file_path in file_paths:
            try:
                with open(file_path, 'r') as file:
                    content = file.read().strip()
                    if content.startswith("key = "):
                        content = content.replace("key = '", "").replace("'", "")
                    if file_path == "/sys/class/net/eth0/address":
                        content = content.replace(':', '-')
                    file_contents.append(content)
            except IOError:
                log_text = u"Datei nicht gefunden: %s." % (file_path)
                logger.warning(u"%s|%s" % (0, log_text))
                file_contents.append('')
            except Exception as e:
                log_text = u"Fehler beim Lesen der Datei: %s." % (file_path)
                logger.warning(u"%s|%s" % (0, log_text))
                file_contents.append('')

        # Compare keys
        for i, (secret_db, file_content) in enumerate(zip(secret_dbs, file_contents), start=1):
            if secret_db != file_content:
                if i == 1:
                    log_text = u"Falscher MAC-Adresse erkannt. Bitte Controme Support kontaktieren."
                if i == 2:
                    log_text = u"Falscher AES-Schlüssel erkannt. Bitte Controme Support kontaktieren."
                logger.warning(u"%s|%s" % (0, log_text))
    except:
        pass

@shared_task
def zielwertkorrektur():
    try:
        for raum in Raum.objects.all():
            try:
                calculate_new_offset(raum)
            except:
                pass
    except:
        pass

    try:
        _local_handler('sudo python /home/pi/rpi/config/Controme_ETRV/scan_btle_rf.py')
    except:
        pass