# -*- coding: cp1252 -*-
from celery import shared_task
from heizmanager.models import Haus
from heizmanager.mobile.m_temp import get_module_offsets
from heizmanager.models import Gateway, AbstractSensor, Raum, Haus, Sensor
from itertools import chain
import heizmanager.cache_helper as ch
import pytz
from datetime import datetime
import logging
from heizmanager.mobile.m_error import CeleryErrorLoggingTask
from heizmanager.models import sig_send_notification
import math
from rf.models import RFController, RFAktor, RFSensor
from fabric.api import local

logger = logging.getLogger('servicemonitorlog')
l_logger = logging.getLogger("logmonitor")



@shared_task(base=CeleryErrorLoggingTask)
def update_service_monitor():

    for haus in Haus.objects.all():

        active_mods = haus.get_modules()

        if 'logmonitor' not in active_mods:
            return

    try:
        checks = {
            'gws': 'Gateway',
            'sens': 'Sensor'
        }
    
        currentCache = ch.get('ServiceMonitor')
        
        if currentCache is None:
            currentCache = dict()
        
        gws = Gateway.objects.all()
        
        for g in gws:
            last = ch.get_gw_ping(g.name)
            berlin = pytz.timezone('Europe/Berlin')
            now = datetime.now(berlin)               
            if last is not None:  #if gw send anything yet
                if len(last) and (now-last[0]).total_seconds() > 750: # last ping is old but was there before
                    if currentCache.get("gw" + g.name) is None: # if monitor has no entry yet, create one

                        log_text = "%s|%s|Gateway offline seit %1.0f Minuten|2b" % (checks['gws'],
                                                                                 g.name,
                                                                                 math.ceil((now-last[0]).total_seconds() / 60.0))
                        logger.warning(log_text)
                        l_logger.warning(log_text)
                        context = {'device_type': 'gateway'}
                        sig_send_notification.send('device-stat', haus_id=haus.id, message=log_text, context=context)

                        currentCache["gw" + g.name] = "Set"
                    else:
                        pass  #logger.warning("gw offline but was before %s, %s, %s, %1.0f" % (g.name,last[0], now,(now-last[0]).total_seconds()))
                else:
                    if currentCache.get("gw" + g.name) is not None: # if monitor has entry , delte key and write good in monitor
                        log_text = "%s|%s|Gateway wieder online|2b" %(checks['gws'], g.name)
                        logger.warning(log_text)
                        l_logger.warning(log_text)
                        context = {'device_type': 'gateway'}
                        sig_send_notification.send('device-stat', haus_id=haus.id, message=log_text, context=context)
                        del currentCache["gw" + g.name]
                    else:
                        pass #logger.warning("gw online but was not before %s, %s, %s, %%1.0f" % (g.name,last[0], now,(now-last[0]).total_seconds()))
            else:
                #gw did not send anything yet
                pass
                
        for haus in Haus.objects.all():
        
            gwdelay = haus.profil.get().get_gateway_delay()
            if int(gwdelay) == 1:
                gwdelay = 90

            liniendict = {
                1: "Klemme 32",
                2: "Klemme 33",
                3: "Klemme 29",
                4: "Klemme 30",
                5: "Klemme 31"
            }

            liniendict_hrgw = {
                -1: "-",
                0: "-", 1: "1", 2: "-", 3: "-", 4: "2", 5: "3", 6: "4", 7: "5"
            }

            berlin = pytz.timezone('Europe/Berlin')
            now = datetime.now(berlin)

            gwdict = dict((gw.id, gw) for gw in gws)
            gwdict[None] = "ohne Gateway"

            liniensort = ["unzugeordnet", "Klemme 31", "Klemme 30", "Klemme 29", "Klemme 33", "Klemme 32"]
            liniensort_hrgw = ["unzugeordnet", "-", "1", "2", "3", "4", "5"]
            sensoren = [{"%s - %s" % (gw, i): []} for gw in gws for i in liniensort if not gw.is_heizraumgw()]
            sensoren += [{"%s - %s" % (gw, i): []} for gw in gws for i in liniensort_hrgw if gw.is_heizraumgw()]
            sensoren.append({"ohne Gateway": []})
            l2idx = dict((d.keys()[0], i) for i, d in enumerate(sensoren))

            for sensor in list(chain(haus.sensor_related.values_list("id", "name", "haus_id", "linie", "raum_id", "description", "gateway_id", "mainsensor"),
                                     haus.luftfeuchtigkeitssensor_related.values_list("id", "name", "haus_id", "linie", "raum_id", "description", "gateway_id", "mainsensor"))):
                if sensor[6] is not None and gwdict[sensor[6]].is_heizraumgw():
                    last, isOffline = checkLast(sensor[1], liniendict_hrgw[sensor[3]] if sensor[3] else "",  gwdelay)
                else:
                    last, isOffline = checkLast(sensor[1], liniendict[sensor[3]] if sensor[3] and 0 < sensor[3] < 6 else "",  gwdelay)
                if 'pt1000' in sensor[1]:
                    sname = 'PT1000 an %s' % sensor[1].split('#')[0]
                elif 'ui10' in sensor[1]:
                    sname = 'Digitaleingang 1 an %s' % sensor[1].split('#')[0]
                elif 'ui20' in sensor[1]:
                    sname = 'Digitaleingang 2 an %s' % sensor[1].split('#')[0]
                else:
                    sname = sensor[1]

                if sensor[4]:
                    sd = {
                        sname: [
                            sensor[0],
                            sensor[4],
                            "TBD",
                            last,
                            isOffline,
                            gwdict[sensor[6]].name if sensor[6] is not None else "unzugeordnet"
                        ]
                    }
                elif sensor[5]:
                    sd = {
                        sname: [
                            sensor[0],
                            None,
                            u"- / - - '%s'" % sensor[5],
                            last,
                            isOffline,
                            gwdict[sensor[6]].name if sensor[6] is not None else "unzugeordnet"
                        ]
                    }
                else:
                    sd = {
                        sname: [
                            sensor[0] if 'pt1000' not in sensor[1] else '',
                            None,
                            "- / -",
                            last,
                            isOffline,
                            gwdict[sensor[6]].name if sensor[6] is not None else "unzugeordnet"
                        ]
                    }

              
                if sensor[3]:
                    if sensor[6] is None or sensor[6] not in gwdict:
                        linienname = "ohne Gateway"
                    else:
                        if sensor[6] is not None and gwdict[sensor[6]].is_heizraumgw():
                            linienname = liniendict_hrgw[sensor[3]] if sensor[3] else "-"
                        else:
                            linienname = liniendict[sensor[3]] if sensor[3] and 0 < sensor[3] < 6 else ""
                        linienname = "%s - %s" % (gwdict[sensor[6]], linienname)
                else:
                    if sensor[6] is None or sensor[6] not in gwdict:
                        linienname = "ohne Gateway"
                    else:
                        linienname = "%s - unzugeordnet" % gwdict[sensor[6]]
                sensoren[l2idx[linienname]][linienname].append(sd)
            for i, rdata in enumerate(sensoren): 
                for rstr, sensors in rdata.items(): # rstr = klemme xy, rdata sensorid: data
                    for ii in sensors:
                        for j in ii:
                            if ii[j][4] == False and (j in currentCache and currentCache[j] == True): # if online but was marked as offline before
                                log_text = "%s|%s|Sensor wieder online. (%s / %s)|2b" %(checks['sens'], j,ii[j][5], rstr)
                                logger.warning(log_text)
                                l_logger.warning(log_text)
                                context = {'device_type': 'sensor'}
                                sig_send_notification.send('device-stat', haus_id=haus.id, message=log_text, context=context)
                                currentCache[j] = False
                            elif ii[j][4] == True and (j not in currentCache or currentCache[j] == False): # if offline and was not offline or known before
                                log_text = "%s|%s|Sensor offline. (%s / %s)|2b" %(checks['sens'], j,ii[j][5],rstr)
                                logger.warning(log_text)
                                l_logger.warning(log_text)
                                context = {'device_type': 'sensor'}
                                sig_send_notification.send('device-stat', haus_id=haus.id, message=log_text, context=context)
                                currentCache[j] = True
                            elif ii[j][4] == True: # offline but was offline before aka still offline
                                currentCache[j] = True                            
                            else:
                                currentCache[j] = False
                            
        ch.set('ServiceMonitor', currentCache)
    except Exception as e:
        logger.exception("Service Monitor exception")
        
        
def checkLast(ssn, linie, gwdelay):
    last = ch.get_last_temp(ssn)
    isOffline =  True
    berlin = pytz.timezone('Europe/Berlin')
    now = datetime.now(berlin)               

    if last:
        if len(last) >= 2 and last[1] and (now-last[1]).total_seconds() < 605:
            isOffline = False           
        else:
            isOffline = True
        if len(last) >= 2:
            temp = last[0]
            last = last[1].strftime("am %d.%m. um %H:%M")
            if ssn.startswith("26_"):
                last += " mit %s%%" % temp
                if linie:
                    last += " auf Linie %s" % linie
            elif "ui" in ssn:
                last += " mit %d" % temp
            else:
                if temp == 85 or temp is None:
                    last = u"Fehler! Bitte Leitungen pruefen!"
                else:
                    last += " mit %.2f&deg;" % temp
                    if linie:
                        last += " auf Linie %s" % linie
        else:
            isOffline = False # if no cache is there, sensor was never seen since system start. This does not count as offline
    else: # if not in cache it was never seen since system start -> do nothing
        return "", False
    return last, isOffline


# add logs for RF
def rf_logmonitor(haus, berlin):
    logger = logging.getLogger("servicemonitorlog")
    haus_params = haus.get_module_parameters().get('rf_log_params', dict())
    previous_cache = haus_params.get('rf_exec_cache_device_%s' % haus.id)
    if previous_cache is None:
        previous_cache = dict()
    all_device = RFController.objects.all()
    now = datetime.now(berlin)
    try:
        local('sudo chmod 777 /var/log/uwsgi/rf*')
    except:
        pass
    local('echo "Last RF log: %s\n%s" > /var/log/uwsgi/rf_devices.log' % (datetime.now(berlin), '='*60))
    rf_log_default = 'log-off'
    try:
        model = local("cat /sys/firmware/devicetree/base/model", capture=True)
    except:
        model = ''
    if 'Pi 4' in model:
        rf_log_default = 'log-on'
    rf_log_status = haus_params.get('rf_log_status',  rf_log_default)
    for controller in all_device:
        aktoren = RFAktor.objects.filter(controller=controller)
        sensoren = RFSensor.objects.filter(controller=controller)
        devices = list(chain(aktoren, sensoren))
        for rfc in devices:
            device = AbstractSensor.get_sensor(rfc.get_identifier())
            local('echo "Device_ID: %s (%s)" >> /var/log/uwsgi/rf_devices.log' % (device.name, device.description))
            marker = device.get_marker(use_last_data=False)
            if device.type in ["2chrelais", "wp", "wpd2010b"]:
                shortest_wakeup = device.controller.get_shortest_wakeup()
                marker = RFController.get_marker(device.controller.name, shortest_wakeup)
            local('echo "marker: %s" >> /var/log/uwsgi/rf_devices.log' % (marker))
            if previous_cache.get(str(device.name)) is not True:
                if marker == 'red':
                    log_text = "%s|%s|Funkgerät offline. (%s / %s)|2b"\
                    % ('Sensor', (device.name).encode('utf8'), (controller.name).encode('utf8'), (device.description).encode('utf8'))
                    logger.warning(log_text)
                    l_logger.warning(log_text)
                    previous_cache[str(device.name)] = True
                    try:
                        if rf_log_status == 'log-on':
                            context = {'device_type': 'sensor'}
                            key = "dev_offline_%s" % device.name.encode('utf8')
                            dev_stat = ch.get(key)
                            if dev_stat is None:
                                ch.set(key, True, 300)
                                sig_send_notification.send('device-stat', haus_id=haus.id,
                                                           message=log_text.decode('utf8'),
                                                           context=context)
                            local('echo "Email sent." >> /var/log/uwsgi/rf_devices.log')
                        else:
                            local('echo "Email not sent(rf-log is off)." >> /var/log/uwsgi/rf_devices.log')
                    except:
                        pass
                    local('echo "Changes(offline) logged!" >> /var/log/uwsgi/rf_devices.log')
                else:
                    local('echo "Cache is not True and marker is not red." >> /var/log/uwsgi/rf_devices.log')
            else:
                if marker in ['green', 'yellow']:
                    log_text = "%s|%s|Funkgerät wieder online. (%s / %s)|2b"\
                    % ('Sensor', (device.name).encode('utf8'), (controller.name).encode('utf8'), (device.description).encode('utf8'))
                    logger.warning(log_text)
                    l_logger.warning(log_text)
                    previous_cache[str(device.name)] = False
                    try:
                        if rf_log_status == 'log-on':
                            context = {'device_type': 'sensor'}
                            sig_send_notification.send('device-stat', haus_id=haus.id,
                                                       message=log_text.decode('utf8'), context=context)
                            local('echo "Email sent." >> /var/log/uwsgi/rf_devices.log')
                        else:
                            local('echo "Email not sent(rf-log is off)." >> /var/log/uwsgi/rf_devices.log')
                    except:
                        pass
                    local('echo "Changes(online) logged!" >> /var/log/uwsgi/rf_devices.log')
                else:
                    local('echo "Cache is True and marker is not green or yellow." >> /var/log/uwsgi/rf_devices.log')
            local('echo "%s" >> /var/log/uwsgi/rf_devices.log' % ('-'*60))
    haus_params['rf_exec_cache_device_%s' % haus.id] = previous_cache
    haus.set_spec_module_params('rf_log_params', haus_params)
    local('mv /var/log/uwsgi/rf_devices.log /var/log/uwsgi/rf_logs.log')
    return


@shared_task(base=CeleryErrorLoggingTask)
def periodic_calculate():
    haus = Haus.objects.get()
    haus_params = haus.get_module_parameters().get('rf_log_params', dict())
    rf_exec_logmonitor = int(haus_params.get('rf_exec_logmonitor',  15))
    previuos_time = ch.get('last_rf_logmonitor_%s' % haus.id)
    berlin = pytz.timezone("Europe/Berlin")
    dtnow = datetime.now(berlin)
    if not previuos_time:
        previuos_time = dtnow
        ch.set('last_rf_logmonitor_%s' % haus.id, dtnow)
    duration = (dtnow - previuos_time).total_seconds() / 60
    haus_params['rf_exec_duration'] = duration
    haus.set_spec_module_params('rf_log_params', haus_params)
    if duration >= rf_exec_logmonitor:
        ch.set('last_rf_logmonitor_%s' % haus.id, dtnow)
        rf_logmonitor(haus, berlin)
    return
