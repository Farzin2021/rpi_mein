# -*- coding: utf-8 -*-
from heizmanager.models import Haus, Raum, Regelung, Gateway, Sensor, Luftfeuchtigkeitssensor
from heizmanager.render import render_response, render_redirect
from rf.models import RFSensor, RFAktor, RFDevice
from knx.models import KNXSensor
from heizmanager.render import render_response
from fabric.api import local
from django.http import HttpResponse
from django.http import JsonResponse
import json
from itertools import chain
import urllib


def get_name():
    return "Ereignisprotokoll"


def get_global_settings_link(request, haus):
    return "<a href='/m_setup/%s/logmonitor/'>Ereignisprotokoll</a>" % str(haus.id)


def get_global_description_link():
    desc = u"Nachvollziehbarkeit und Rückverfolgung, was das System, wann gemacht hat. "
    desc_link = "http://support.controme.com/logmonitor/"
    return desc, desc_link


def _local_handler(cmd):
    try:
        local_run = local(cmd, capture=True)
    except:
        local_run = 'no'
    return local_run


def get_global_settings_page(request, haus, action=None, entityid=None):

    if not isinstance(haus, Haus):
        haus = Haus.objects.get(pk=long(haus))

    context = dict()
    params = haus.get_module_parameters()

    if request.method == "GET":
        context['haus'] = haus
        context['r_m_active'] = params.get('logmonitor_right_menu', True)
        context['log_status'] = _local_handler("cat /var/log/uwsgi/.enable")
        return render_response(request, "m_settings_logmonitor.html", context)

    if request.method == "POST":
        params = haus.get_module_parameters()

        if 'logmonitor_status' in request.POST:
            if request.POST.get('logmonitor_status') == 'log-off':
                _local_handler('server="logto = \/var\/log\/uwsgi\/uwsgi.log" ; sed -i "/^$server/ c#$server" config/uwsgi-rpi.ini')
                _local_handler('echo "disabled" > /var/log/uwsgi/.enable')
                _local_handler("sudo supervisorctl -c /etc/supervisor/supervisord.conf restart all")
            elif request.POST.get('logmonitor_status') == 'log-on':
                _local_handler('server="logto = \/var\/log\/uwsgi\/uwsgi.log" ; sed -i "/^#$server/ c$server" config/uwsgi-rpi.ini')
                _local_handler('echo "enabled" > /var/log/uwsgi/.enable')
                _local_handler("sudo supervisorctl -c /etc/supervisor/supervisord.conf restart all")

        if 'right_menu_ajax' in request.POST:
            params['logmonitor_right_menu'] = bool(int(request.POST['right_menu_ajax']))
            haus.set_module_parameters(params)
            return JsonResponse({'message': 'done'})

        return render_redirect(request, '/m_setup/%s/logmonitor/' % haus.id)


def get_logs(user, haus, mod, obj, time=None, **kwargs):
    search_q = kwargs.get('search_q')
    ret = []
    # celery / cron haben vollst pfad, uwsgi faengt in ~/rpi an
    hprg = "\(/home/pi/rpi/temperaturszenen\|./temperaturszenen\)/views.py.get_local_settings_page\|"\
           "^/home/pi/rpi/heizprogramm/tasks.py.set_mode_for_raum\|"\
           "\(^/home/pi/rpi/heizprogramm\|^./heizprogramm\)/views.py.get_local_settings_page_haus\|"\
           "/home/pi/rpi/temperaturszenen/views.py.set_mode_raum\|"\
           "/home/pi/rpi/temperaturszenen/views.py.set_mode_all_rooms"
    modtrans = {
        'h': '^/home/pi/rpi/',
        'hfo': '\(^/home/pi/rpi/heizflaechenoptimierung/tasks.py\|^./heizflaechenoptimierung/tasks.py\).*\(|Status\||Modus\)',
        'doa': '\(^/home/pi/rpi/\|^./\)\(ruecklaufregelung\|zweipunktregelung\)/views.py\|Solltemperatur\|Ventilstellung\|Zieltemperatur\|heizflaechenoptimierung/tasks.py.*|Ausgang \|\(/home/pi/rpi/heizmanager\|./heizmanager\)/getandset.py.calc_opening.*|Ausgang ',
        'pl': '\(^/home/pi/rpi/pumpenlogik\|^./pumpenlogik\)/views.py',
        'mr': '\(^/home/pi/rpi/vorlauftemperaturregelung\|^./vorlauftemperaturregelung\)/views.py',
        'dr': '\(^/home/pi/rpi/differenzregelung\|^./differenzregelung\)/views.py',
        'fps': '\(^/home/pi/rpi/fps\|^./fps\)/views.py',
        'hprg': hprg,
        'sys': '^/home/pi/rpi/dataslave/tasks.py.send_setup\|\(^/home/pi/rpi/heizflaechenoptimierung\|^./heizflaechenoptimierung\)/tasks.py.\(calc_hftemps\|calc_offset\)|\(^/home/pi/rpi/gatewaymonitor\|^./gatewaymonitor\)/views.py.do_ausgang\|\(^/home/pi/rpi/heizmanager\|^./heizmanager\)/tasks.py.\(nginx_status\|reboot_system\)\|\(check_supervisor\)\|^.*Systemstatus\|\(^/home/pi/rpi/dataslave\|^./dataslave\)/tasks.py.general_functions',
        'aha': '\(^/home/pi/rpi/ahapro\|^./ahapro\)/tasks.py',
        'gws': '\(^/home/pi/rpi/heizmanager\|^./heizmanager\)/getandset.py.tset_all',
        'sens': '\(^/home/pi/rpi/heizmanager\|^./heizmanager\)/getandset.py.tset_all\|\(^/home/pi/rpi/rf\|^./rf\)/views.py.set_enocean\|\(^/home/pi/rpi/rf\|^./rf\)/models.py.update_device\|\(^/home/pi/rpi/knx\|^./knx\)/views.py.set_knx\|\(^/home/pi/rpi/rf\|^./rf\)/views.py.set_rc',
        'dbr': '^/home/pi/rpi/periodischer_reset/database_copy.py.db_replace',
        'gws_24': '^/home/pi/rpi/logmonitor/tasks.py.update_service_monitor|.*|Gateway|',
        'sens_24': '^/home/pi/rpi/logmonitor/tasks.py.update_service_monitor|.*|Sensor|',
    }

    if mod in ['sens_servm', 'gws_servm', 'sys_permanent']:
        return get_service_monitor_logs(user, haus, mod, obj, time, **kwargs)

    if obj == "h":
        raumdict = {"0": ""}
        if mod == "pl":
            for reg in Regelung.objects.filter(regelung="pumpenlogik"):
                raumdict[str(reg.id)] = reg.get_parameters().get('name') or "RA %s" % reg.id
        elif mod == "mr":
            for mid, params in haus.get_module_parameters().get('vorlauftemperaturregelung', dict()).items():
                raumdict[str(mid)] = params.get('name') or "VTR %s" % mid
        elif mod == "dr":
            for dregid, params in haus.get_module_parameters().get('differenzregelung', dict()).items():
                raumdict[str(dregid)] = params.get('name') or "DR %s" % dregid
        elif mod == "fps":
            for reg in Regelung.objects.filter(regelung="fps"):
                raumdict[str(reg.id)] = reg.get_parameters().get('name') or "FPS %s" % reg.id
        elif mod == "sys":
            pass
        elif mod == "aha":
            pass
        elif mod == "gws":
            pass
        elif mod == 'sens':
            pass
        elif mod == 'dbr':
            pass
        else:  # hfo / doa / hprg
            for etage in haus.etagen.all():
                for raum in Raum.objects.filter_for_user(user, etage_id=etage.id).only("name"):
                    raumdict[str(raum.id)] = u"%s / %s" % (etage.name, raum.name)
    try:
        new_uwsgi = local('grep -q "2022-05-20" /etc/logrotate.d/uwsgi && echo yes || echo no', capture=True)
    except:
        new_uwsgi = 'no'
    if new_uwsgi == 'yes':
        if str(time) == '1':
            try:
                log = local("grep '%s' /var/log/uwsgi/uwsgi.log | sort -r -k2 -t '|'" % modtrans[mod], capture=True)
            except:
                log = ''
            try:
                log1 = local("grep '%s' /var/log/uwsgi/uwsgi.log.1 | sort -r -k2 -t '|'" % modtrans[mod], capture=True)
                log += '\n' + log1
            except:
                pass
        else:
            try:
                log = local("grep '%s' /var/log/uwsgi/uwsgi.log.%s | sort -r -k2 -t '|'" % (modtrans[mod], str(time)), capture=True)
            except:
                log = ''
    else:
        try:
            log = local("grep '%s' /var/log/uwsgi/uwsgi.log | sort -r -k2 -t '|'" % modtrans[mod], capture=True)
        except:
            log = ''
        try:
            log1 = local("grep '%s' /var/log/uwsgi/uwsgi.log.1 | sort -r -k2 -t '|'" % modtrans[mod], capture=True)
            log += '\n' + log1
        except:
            pass
    log = log.split('\n')[:10000]

    import re
    if search_q:
        tmp = []
        for l in log:
            if re.findall(search_q, l, re.IGNORECASE):
                tmp.append(l)
        log = tmp
    
    if mod == "gws":
        for line in log:
            if not line.startswith("./") and not line.startswith("/home/pi/rpi/"):
                continue
            l = line.split('|')
            if obj == 'h':
                ret.append([l[1], l[3]])
            elif l[3] == obj.strip():
                ret.append([l[1], "%s Sensoren verbunden" % (l[4].count(';')+1)])

    elif mod == "sys":
        for line in log:
            if not line.startswith("./") and not line.startswith("/home/pi/rpi/") and '.check_supervisor' not in line:
                continue
            l = line.split('|')
            if obj == 'h':
                ret.append([l[1], l[3]])

    elif mod == "aha":
        for line in log:
            if not line.startswith("./") and not line.startswith("/home/pi/rpi/") or 'aha_pro' not in line:
                continue
            l = line.split('|')
            if obj == 'h':
                ret.append([l[1], l[3]])


    elif mod == "sens":
        obj = urllib.unquote(obj)
        if "#" in obj:
            gwmac = obj.split('#')[0]
            if len(obj.split('#')[1]) == 4:
                obj = obj[-4:-2] + '_' + obj[-2:]
            else:
                obj = obj[-6:-4] + '_' + obj[-4:-2] + '_' + obj[-2:]
        else:
            gwmac = None

        for line in log:
            if (not line.startswith("./") and not line.startswith("/home/pi/rpi/")) or 'Zieltemperatur' in line:
                continue

            l = line.decode('utf-8').split("|")

            if any(keyword in line for keyword in ['VOM', 'Dynamische']):
                _obj = obj.replace('\\', '')
                device_id, log_text= l[4].split(',')

                if device_id.startswith(_obj):
                    ret.append([l[1], log_text])
                continue

            if 'Solltemperatur' in line:
                continue

            if gwmac and l[3] != gwmac:
                continue

            if obj == "h":
                for s in l[4].split(';'):
                    ret.append([s.split(',')[0], l[1], l[3]])
            else:
                try:
                    _s = l[4].split(';')
                except IndexError:
                    _obj = obj.replace('\\', '')
                    if len(l) == 4 and l[3].startswith(_obj):
                        ret.append([l[1], l[3].split(' ', 1)[1]])
                    else:
                        continue
                else:  # u.a. 1wire
                    for s in _s:
                        sp = s.lower().split(',')
                        if sp[0] == obj.strip().replace('\\', ''):
                            try:
                                val = "Isttemperatur %.2f" % float(sp[1])
                            except:
                                val = '-'
                            ret.append([l[1], val])
    elif mod == "dbr":
       for line in log:
            l = line.split('|')
            ret.append(l[1::2])
           
    elif mod == 'gws_24' or mod == 'sens_24':
        for line in log:
            l = line.split('|')
            if len(l) > 3:
                if obj == 'h' or l[3] == obj.strip():
                    ret.append([l[1], l[2], l[3], l[4]])
    else:
        for line in log:
            if not line.startswith("./") and not line.startswith("/home/pi/rpi/"):
                continue
            l = line.split('|')
            if obj == 'h':
                try:
                    ret.append([raumdict[l[2]]] + l[1::2])
                except:
                    # wenn ein Raum wieder geloescht wurde
                    pass
            elif l[2] == obj:
                ret.append(l[1::2])

    return ret


def get_service_monitor_logs(user, haus, mod, obj, time, **kwargs):
    obj = str(obj.strip().replace('\\', ''))
    search_q = kwargs.get('search_q')
    ret = []
    modtrans = {
        'gws_servm': '^/home/pi/rpi/logmonitor/tasks.py.update_service_monitor|.*|Gateway|',
        'sens_servm': '^/home/pi/rpi/logmonitor/tasks.py.update_service_monitor|.*|Sensor|',
        'sys_permanent': '^.*Systemstatus',
    }

    rf_log_default = 'log-off'
    try:
        model = local("cat /sys/firmware/devicetree/base/model", capture=True)
    except:
        model = ''
    if 'Pi 4' in model:
        rf_log_default = 'log-on'
    haus_params = haus.get_module_parameters().get('rf_log_params', dict())
    rf_log_status = haus_params.get('rf_log_status',  rf_log_default)
    if rf_log_status == 'log-on':
        modtrans['sens_servm'] = '^/home/pi/rpi/logmonitor/tasks.py.update_service_monitor|.*|Sensor|\|^/home/pi/rpi/logmonitor/tasks.py.rf_logmonitor|.*|Sensor|'
    grepfilter = ''
    if search_q:
        for s in search_q.split(','):
            grepfilter += " | grep %s" % s
    try:
        log = local("grep -a '%s' /var/log/uwsgi/service_monitor.log2 %s | sort -r -k2 -t '|'"
                    % (modtrans[mod], grepfilter), capture=True)
    except:
        log = ''
    log = log.split('\n')[:10000]

    for line in log:
        l = line.split('|')
        if len(l) > 3:
            if mod == 'sys_permanent':
                ret.append([l[1], l[2], l[3]])
                continue
            if obj == 'h' or l[3] == obj.strip():
                ret.append([l[1], l[2], l[3], l[4]])

    return ret


def get_local_settings_page_haus(request, haus):
    if request.method == "GET":
        if 'mod' in request.GET and 'obj' in request.GET:

            ret = get_logs(request.user, haus, request.GET['mod'], request.GET['obj'], request.GET['time'],search_q=request.GET.get('filter', ''))

            return HttpResponse(json.dumps(ret))

        else:
            room_ids = []
            eundr = []
            for etage in haus.etagen.all():
                e = [etage.name, []]
                for raum in Raum.objects.filter_for_user(request.user, etage_id=etage.id).only("name"):
                    e[1].append((raum.id, raum.name))
                    room_ids.append(raum.id)
                eundr.append(e)
            module = haus.get_modules()
            modtrans = {
                'heizflaechenoptimierung': ('hfo', 'Heizflächenoptimierung (24h-Logfile)'),
                'pumpenlogik': ('pl', 'Raumanforderung (24h-Logfile)'),
                'differenzregelung': ('dr', 'Differenzregelung (24h-Logfile)'),
                'vorlauftemperaturregelung': ('mr', 'Vorlauftemperaturregelung (24h-Logfile)'),
                'fps': ('fps', 'FPS (24h-Logfile)'),
                'heizprogramm': ('hprg', 'Heizprogramm (24h-Logfile)'),
                'periodischer_reset': ('dbr', 'Periodischer Reset (24h-Logfile)')
            }  # supported modules
            modtrans_alwayson = [
                ('doa', 'ERR Ausgänge (24h-Logfile)'),
                ('sys', 'Systemmeldungen (24h-Logfile)'),
                ('aha', 'AHA-PRO'),
                ('sys_permanent', 'Systemmeldungen (Dauerhaftes-Logfile)'),
                ('sens', 'Sensoren (24h-Logfile)'),
                ('sens_servm', 'Ausfallanzeige Sensoren (Dauerhaftes-Logfile)'),
                ('gws_24', 'Ausfallanzeige Gateway (24h-Logfile)'),
                ('sens_24', 'Ausfallanzeige Sensoren (24h-Logfile)'),
            ]
            gws = Gateway.objects.all()
            if len(gws):
                modtrans_alwayson.append(('gws', 'Gateways (24h-Logfile)'))
                modtrans_alwayson.append(('gws_servm', 'Ausfallanzeige Gateways (Dauerhaftes-Logfile)'))
            lmodules = [modtrans[m] for m in module if m in modtrans]
            lmodules += modtrans_alwayson

            abbrs = {'differenzregelung': 'dr', 'fps': 'fps', 'vorlauftemperaturregelung': 'mr', 'pumpenlogik': 'pl'}
            modoptions = dict((k, []) for k in abbrs.values())
            hparams = haus.get_module_parameters()
            for reg in Regelung.objects.filter(regelung__in=abbrs.keys()):
                if reg.regelung in {'fps', 'pumpenlogik'}:
                    t = (reg.get_parameters().get('name'), reg.id)
                else:
                    t = (hparams[reg.regelung][reg.get_parameters().values()[0]]['name'], reg.get_parameters().values()[0])
                if t not in modoptions[abbrs[reg.regelung]]:
                    modoptions[abbrs[reg.regelung]].append(t)
            
            for gw in gws:
                modoptions.setdefault('gws', list())
                modoptions['gws'].append(("%s%s" % (gw.name, (" %s" % gw.description if len(gw.description) else "")), gw.name))

            modoptions['gws_servm'] = modoptions.get('gws')
            modoptions['gws_24'] = modoptions.get('gws')

            modoptions.setdefault('sens', list())
            for sensor in sorted(list(chain(Sensor.objects.all(), Luftfeuchtigkeitssensor.objects.all(), RFSensor.objects.all(), RFAktor.objects.filter(type='wt'), RFAktor.objects.filter(type__startswith="hkt"), RFAktor.objects.filter(type__startswith="relais", protocol="enocean"), KNXSensor.objects.all())), key=lambda x: (room_ids.index(x.raum.id) if x.raum else None, x.name)):
                modoptions['sens'].append((
                    u"%s%s%s" % (
                        u"%s - " % sensor.raum if sensor.raum else "",
                        u"%s - %s" % (sensor.raum, u"%s %s" % (sensor.controller.name, sensor.name))
                        if isinstance(sensor, RFDevice) and sensor.protocol == "zwave"
                        else sensor.name,
                        u" %s" % sensor.description if len(sensor.description) else ""
                    ),
                    u"%s_%s" % (sensor.controller.name, sensor.name)
                    if isinstance(sensor, RFDevice) and sensor.protocol == "zwave"
                    else sensor.name
                ))

            modoptions['sens_servm'] = modoptions.get('sens')
            modoptions['sens_24'] = modoptions.get('sens')
            try:
                logmonitor_status = local("cat /var/log/uwsgi/.enable", capture=True)
            except:
                logmonitor_status = 'disabled'
            try:
                new_uwsgi = local('grep -q "2022-05-20" /etc/logrotate.d/uwsgi && echo yes || echo no', capture=True)
            except:
                new_uwsgi = 'no'
            return render_response(request, "m_logmonitor.html", {'haus': haus, 'eundr': json.dumps(eundr), 'modules': sorted(lmodules, key=lambda x: x[1]), 'modoptions': json.dumps(modoptions), 'logmonitor_status': logmonitor_status, 'new_uwsgi': new_uwsgi, 'range': range(2, 15)})



def set_jsonapi(data, haus, usr, entityid):
    if 'wireless-sensor-time' in data:
        try:
            haus = Haus.objects.first()
            haus.set_spec_module_params('rf_exec_logmonitor', int(data['wireless-sensor-time']))
            return {'message': 'Done! rf timer set to: %s Minute' % data['wireless-sensor-time']}
        except ValueError:
            return {'message': 'rf timer must be integer value'}
    return {'error': 'parameters are not provided'}
