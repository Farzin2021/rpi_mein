import os
from datetime import datetime
import pytz
from heizmanager.models import Haus, sig_new_temp_value
from celery import shared_task
import heizmanager.cache_helper as ch
from views import get_list_of_assign_values
import imp
api_wetter = imp.load_compiled("api_wetter","/home/pi/rpi/wetter_pro/api_wetter.py")


def get_forecast(haus):
    cred_file = '/home/pi/rpi/wetter_pro/cred.yml'
    api_secret = 'JENvbnRyb21lIz8yMDIyJSR3ZWF0aGVyX2FwaV8mbmV3'
    api_data = 'forecast'
    package = "hourly"
    package_duration = 149
    apiVersion = 2.0

    params = haus.get_module_parameters()
    lat, lng, forecast = None, None, None
    is_active = params.get("wetter_pro", {}).get('is_active', False)
    if not is_active: # module is deactive
        return forecast

    if not lat:
        lat = params.get('wetter', {}).get('lat', None)
        lng = params.get('wetter', {}).get('lng', None)
    forecast = api_wetter.forecast(api_secret, cred_file, api_data, package, package_duration, apiVersion, lat, lng)
    forecast_today = []
    for d in forecast['data']:
        f_date = datetime.strptime(str(d['timestamp_local']), '%Y-%m-%dT%H:%M:%S')
        if not 'date' in d:
            d.update(date= f_date.strftime('%Y-%m-%d'))
            d.update(periodname= f_date.strftime('%H:00'))
        forecast_today.append(d)
    return forecast_today


# This task MUST execute every 60 minutes
@shared_task
def periodic_calculation(request=None):
    berlin = pytz.timezone('Europe/Berlin')
    now = datetime.now(berlin)

    for haus in Haus.objects.all():
        if 'wetter_pro' not in haus.get_modules():
            continue

        params = haus.get_module_parameters()
        is_active = params.get("wetter_pro", {}).get('is_active', False)
        if not is_active: # module is deactive
            continue

        # forecasts list
        f_list = ch.get('weatherpro_updating_forecast_%s' % haus.id)
        # check if cache has old version of data
        if f_list and any('tta_C' in v for v in enumerate(f_list)):
            f_list = None
            ch.set('weatherpro_assign_values_%s' % haus.id, None, time=3601)
            ch.set('weatherpro_updating_forecast_%s' % haus.id, None, time=36000)

        if f_list and len(f_list) > 72 and f_list[72]['date'] + f_list[72]['periodname'] < now.strftime("%Y-%m-%d%H:00"):
            del f_list[0:1]
            ch.set('weatherpro_updating_forecast_%s' % haus.id, f_list, time=36000)
            ch.set('weatherpro_lasttime_list_updated_%s' % haus.id, now, time=36000)

        # len(f_list) < 146 >>>> -72h + current time + +72h === 49h
        # last condition: for any reason (like deactivate and activate after 2h) if 72th of f_list that should be current time, is not equal with current time
        if not f_list or len(f_list) < 146 or (f_list[72]['date'] + f_list[72]['periodname'] < now.strftime("%Y-%m-%d%H:00")):
            f_list_api = get_forecast(haus)
            if not f_list or len(f_list) < 72:
                f_list = [None] * 151
            try:
                if f_list_api[0]['periodname'] == f_list[73]['periodname']:
                    f_list[73:] = f_list_api[:78]
                else:
                    f_list[72:] = f_list_api[:79] # the next 72h + extra 6h
            except:
                f_list[72:] = f_list_api[:79] # the next 72h + extra 6h
            ch.set('weatherpro_updating_forecast_%s' % haus.id, f_list, time=36000)
            ch.set('weatherpro_lasttime_list_updated_%s' % haus.id, now, time=36000)

        # calculate every 60 minutes
        wetters_assign_values = get_list_of_assign_values(haus)
        ch.set('weatherpro_assign_values_%s' % haus.id, wetters_assign_values, time=3601)

        # Log the actual degree of coverage
        percent = f_list[72]['clouds']
        sig_new_temp_value.send(sender="wetter_pro", name='coverage_degree', value=int(percent), modules=haus.get_modules(), timestamp=now)