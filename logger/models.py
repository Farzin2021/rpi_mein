# -*- coding: utf-8 -*-

from django.db import models
from datetime import date, timedelta, datetime
from heizmanager.models import sig_new_output_value, sig_new_temp_value, Haus
import sqlite3
import calendar
import logging
import pytz
from fabric.api import local
import heizmanager.cache_helper as ch


class SimpleLog(models.Model):
    name = models.CharField(max_length=32, db_index=True)
    timestamp = models.DateTimeField(db_index=True)  # auto_now_add=True
    epochts = models.BigIntegerField(db_index=True)

    class Meta:
        app_label = 'logger'
        abstract = True


class SimpleSensorLogManager(models.Manager):

    def getlogs(self, name, von, bis):
        try:
            conn = sqlite3.connect("usbmount/db_log.db")
        except sqlite3.OperationalError:
            return []
        cursor = conn.cursor()

        try:
            begin = datetime.strptime(von, "%Y-%m-%dT%H:%M")
            ende = datetime.strptime(bis, "%Y-%m-%dT%H:%M")
        except:
            return []
        td = int(((ende-begin).total_seconds() / 360) * 500)

        # local = pytz.timezone("Europe/Berlin")
        # begin = local.localize(begin, is_dst=None).astimezone(pytz.UTC)
        # ende = local.localize(ende, is_dst=None).astimezone(pytz.UTC)

        try:
            return cursor.execute(
                """SELECT epochts, value FROM logger_simplesensorlog WHERE name='%s' AND timestamp BETWEEN '%s' AND '%s' GROUP BY epochts / %s""" % (name.replace('\\', ''), begin, ende, td)
            ).fetchall()
        except sqlite3.OperationalError as e:
            if str(e) not in ["no such table: logger_simplesensorlog", "attempt to write a readonly database"]:
                raise e
            else:
                return []

    def downloadlogs(self, names, von, bis):
        try:
            conn = sqlite3.connect("usbmount/db_log.db")
        except sqlite3.OperationalError:
            return []
        cursor = conn.cursor()

        try:
            begin = datetime.strptime(von, "%Y-%m-%dT%H:%M")
            ende = datetime.strptime(bis, "%Y-%m-%dT%H:%M")
        except:
            return []

        # local = pytz.timezone("Europe/Berlin")
        # begin = local.localize(begin, is_dst=None).astimezone(pytz.UTC)
        # ende = local.localize(ende, is_dst=None).astimezone(pytz.UTC)

        instr = """(%s)""" % ', '.join(["'%s'" % name for name in names])

        try:
            return cursor.execute("""SELECT timestamp, name, value FROM logger_simplesensorlog WHERE timestamp BETWEEN '%s' AND '%s' AND name IN %s ORDER BY timestamp""" % (begin, ende, instr)).fetchall()
        except sqlite3.OperationalError as e:
            if str(e) != "no such table: logger_simplesensorlog":
                raise e
            else:
                return []


class SimpleSensorLog(SimpleLog):
    # name ist sensor.get_identifier()
    value = models.FloatField()

    objects = SimpleSensorLogManager()

    class Meta:
        app_label = 'logger'


class SimpleOutputLogManager(models.Manager):

    def getlogs(self, name, von, bis, val="ziel"):
        try:
            conn = sqlite3.connect("usbmount/db_log.db")
        except sqlite3.OperationalError:
            return []
        cursor = conn.cursor()

        try:
            begin = datetime.strptime(von, "%Y-%m-%dT%H:%M")
            ende = datetime.strptime(bis, "%Y-%m-%dT%H:%M")
        except:
            return []
        td = int(((ende-begin).total_seconds() / 360) * 500)

        # local = pytz.timezone("Europe/Berlin")
        # begin = local.localize(begin, is_dst=None).astimezone(pytz.UTC)
        # ende = local.localize(ende, is_dst=None).astimezone(pytz.UTC)

        try:
            return cursor.execute(
                """SELECT epochts, %s FROM logger_simpleoutputlog WHERE name='%s' AND timestamp BETWEEN '%s' AND '%s' GROUP BY epochts / %s""" % (val, name.replace('\\', ''), begin, ende, td)
            ).fetchall()
        except sqlite3.OperationalError as e:
            if str(e) != "no such table: logger_simpleoutputlog":
                raise e
            else:
                return []

    def downloadlogs(self, names, von, bis, val="value"):
        try:
            conn = sqlite3.connect("usbmount/db_log.db")
        except sqlite3.OperationalError:
            return []
        cursor = conn.cursor()

        try:
            begin = datetime.strptime(von, "%Y-%m-%dT%H:%M")
            ende = datetime.strptime(bis, "%Y-%m-%dT%H:%M")
        except:
            return []

        # local = pytz.timezone("Europe/Berlin")
        # begin = local.localize(begin, is_dst=None).astimezone(pytz.UTC)
        # ende = local.localize(ende, is_dst=None).astimezone(pytz.UTC)

        instr = """(%s)""" % ', '.join(["'%s'" % name for name in names])

        try:
            return cursor.execute("""SELECT timestamp, name, %s FROM logger_simpleoutputlog WHERE timestamp BETWEEN '%s' AND '%s' AND name IN %s ORDER BY timestamp""" % (val, begin, ende, instr)).fetchall()
        except sqlite3.OperationalError as e:
            if str(e) != "no such table: logger_simpleoutputlog":
                raise e
            else:
                return []


class SimpleOutputLog(SimpleLog):
    # name wird: ctrldev.name + '_' + aktor.name or ausgangno
    value = models.BooleanField(blank=True, default=None)
    ziel = models.FloatField()  # koennte bei reiner rlr verfaelscht sein
    parameters = models.TextField()  # koennen auch ein leeres dict sein, wenn kein neuer Wert berechnet wurde

    objects = SimpleOutputLogManager()

    class Meta:
        app_label = 'logger'


def write_temp_log(sender, **kwargs):
    modules = kwargs['modules']
    if 'logger' not in modules and sender not in {'vorlauftemperaturregelung', 'differenzregelung', 'hfo'}:
        sig_new_temp_value.disconnect(receiver=write_temp_log)
        sig_new_output_value.disconnect(receiver=write_output_log)
        return
    timestamp = kwargs['timestamp']
    if not timestamp.tzinfo:
        timestamp = timestamp.replace(tzinfo=pytz.UTC)
    s_int = ch.get("s_int")
    if s_int is None or not (isinstance(s_int, int) or isinstance(s_int, float)):
        haus = Haus.objects.all()[0]
        try:
            s_int = float(haus.get_module_parameters().get('logger', dict()).get('sensor_intervall', 180))
        except:
            s_int = 0
        ch.set("s_int", s_int)
    last_ts = ch.get("lastts_%s" % kwargs['name'])
    try:
        crit = last_ts is None or s_int == 0 or (s_int > 0 and (last_ts + timedelta(seconds=s_int)) < datetime.now())
    except Exception as e:
        logging.exception("error evaluating logging criterion / sensor")
        crit = True
        ch.delete("s_int")
    if crit:
        epochts = calendar.timegm(timestamp.timetuple()) * 1000
        sl = SimpleSensorLog(name=kwargs['name'], value=float(kwargs['value']), epochts=epochts, timestamp=timestamp)
        try:
            sl.save()
            ch.set("lastts_%s" % kwargs['name'], datetime.now())
        except Exception as e:
            logging.error("loggerexception1: %s" % str(e))
            if "no such table" in str(e):
                sig_new_temp_value.disconnect(receiver=write_temp_log)
                sig_new_output_value.disconnect(receiver=write_output_log)
            elif "disk I/O error" in str(e):
                sig_new_temp_value.disconnect(receiver=write_temp_log)
                sig_new_output_value.disconnect(receiver=write_output_log)


def write_output_log(sender, **kwargs):
    modules = kwargs['modules']
    if modules is not None and 'logger' not in modules and sender not in {'vorlauftemperaturregelung', 'differenzregelung'}:
        sig_new_temp_value.disconnect(receiver=write_temp_log)
        sig_new_output_value.disconnect(receiver=write_output_log)
        return
    if len(kwargs['parameters']):
        timestamp = kwargs['timestamp']
        if not timestamp.tzinfo:
            timestamp = timestamp.replace(tzinfo=pytz.UTC)
        s_int = ch.get("s_int")
        if s_int is None or not (isinstance(s_int, int) or isinstance(s_int, float)):
            haus = Haus.objects.all()[0]
            try:
                s_int = float(haus.get_module_parameters().get('logger', dict()).get('sensor_intervall', 180))
            except:
                s_int = 0
            ch.set("s_int", s_int)
        last_ts = ch.get("lastts_%s" % kwargs['name'])
        try:
            crit = last_ts is None or s_int == 0 or (s_int > 0 and (last_ts + timedelta(seconds=s_int)) < datetime.now())
        except Exception as e:
            logging.exception("error evaluating logging criterion / output")
            crit = True
            ch.delete("s_int")
        if crit:
            epochts = calendar.timegm(timestamp.timetuple()) * 1000
            ziel = kwargs['ziel']
            if not ziel:
                ziel = 0.0
            ol = SimpleOutputLog(name=kwargs['name'], value=bool(int(kwargs['value'])), ziel=ziel, parameters=kwargs['parameters'], epochts=epochts, timestamp=timestamp)
            try:
                ol.save()
                ch.set("lastts_%s" % kwargs['name'], datetime.now())
            except Exception as e:
                logging.error("loggerexception2: %s" % str(e))
