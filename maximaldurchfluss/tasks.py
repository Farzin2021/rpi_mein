class Openings:
    belowbounds = {}
    inbounds = {}
    activity_threshold = {}
    aha_max_openings = {}


class DynamicMaximalAbgleichParams:
    is_value = None
    all_open_outs = None
    all_outs = None
    relevant_values = None
    relevant_values_with_buffer = None
    highest_relevant_value = None


class DynamicMaximalAbgleich:
    def __init__(self, max_increase_dhma, openings, last_opening):
        self.last_opening = last_opening
        self.max_increase_dhma = max_increase_dhma
        self.openings = openings
        self.dhma_params = None
        self.increase_factor = None
        self.increase_factor_two = None
        self.new_max_opening_one = None
        self.new_max_opening_two = None
        self.last_moves = None
        self.stop_after_max_opening_one = False

    def run_dynamic_hydraulic_maximal_abgleich(self):
        self.calc_needed_params()
        max_open_is_out = self.check_if_max_open_aha_is_out()
        if max_open_is_out:
            self.calc_new_max_opening_one()
            self.calc_new_max_opening_two()
            self.set_zero_values_to_max_opening()
            self.print_params()
        else:
            self.new_max_opening_two = self.openings.aha_max_openings

    def check_if_max_open_aha_is_out(self):
        try:
            relevant_max_openings = {}
            for out, val in self.dhma_params.all_outs.items():
                relevant_max_openings[out] = self.openings.aha_max_openings[out]
            max_opened_key = [key for key, val in relevant_max_openings.items() if val == 100]
        except ValueError:
            max_opened_key = None
        if set(max_opened_key).issubset(set(self.openings.inbounds.keys())):
            return True
        else:
            with open('maximaldurchfluss/dhma_log.txt', 'a+') as log:
                log.write("\n In check_if_max_open_aha_is_out: Return False --> new max_openings are aha_openings")
            return False

    def calc_needed_params(self):
        self.dhma_params = DynamicMaximalAbgleichParams()
        self.dhma_params.all_open_outs = {key: value for d in (self.openings.activity_threshold,
                                                               self.openings.belowbounds) for key, value in d.items()}
        self.dhma_params.all_outs = {key: value for d in (self.openings.inbounds,
                                                          self.openings.belowbounds) for key, value in d.items()}
        self.dhma_params.relevant_values = self.calc_relevant_values()
        self.dhma_params.relevant_values_with_buffer = self.calc_relevant_values_with_buffer()
        self.dhma_params.highest_relevant_value = 0
        for out, val in self.dhma_params.relevant_values_with_buffer.items():
            if out in self.dhma_params.all_open_outs.keys() and val > self.dhma_params.highest_relevant_value:
                self.dhma_params.highest_relevant_value = val

    def calc_relevant_values(self):
        relevant_values = self.dhma_params.all_outs.copy()
        for out, val in self.openings.belowbounds.items():
            relevant_values[out] = self.openings.aha_max_openings[out]
        return relevant_values

    def calc_relevant_values_with_buffer(self):
        relevant_values_with_buffer = self.dhma_params.relevant_values.copy()
        for out, cnt_opening in self.openings.activity_threshold.items():
            buffer = cnt_opening - self.last_opening[out]
            new_val = cnt_opening
            if buffer > 0:
                new_val += 1.5 * buffer
            if new_val > self.openings.aha_max_openings[out]:
                new_val = self.openings.aha_max_openings[out]
            relevant_values_with_buffer[out] = new_val
        print("Rel values buffer", relevant_values_with_buffer)
        return relevant_values_with_buffer

    def calc_new_max_opening_one(self):
        highest_relevant_value = self.dhma_params.highest_relevant_value
        if highest_relevant_value == 0:
            self.new_max_opening_one = self.openings.aha_max_openings.copy()
            self.stop_after_max_opening_one = True
            return
        self.increase_factor = 100 / float(highest_relevant_value)
        if self.increase_factor > int(self.max_increase_dhma):
            self.increase_factor = int(self.max_increase_dhma)
            self.stop_after_max_opening_one = True
        self.new_max_opening_one = self.dhma_params.relevant_values_with_buffer.copy()
        for out, val in self.openings.belowbounds.items():
            self.new_max_opening_one[out] =  self.dhma_params.relevant_values_with_buffer[out] * self.increase_factor

    def calc_new_max_opening_two(self):
        # TODO: testfall 2 --> Abchecken ob man nicht noch weiter hoch kann
        self.new_max_opening_two = self.new_max_opening_one.copy()
        if self.stop_after_max_opening_one:
            return
        if self.increase_factor == self.max_increase_dhma:
            return
        highest_relevant_value_two = 0
        for out, val in self.new_max_opening_one.items():
            if val == 100 and out in self.dhma_params.all_open_outs.keys():
                return
            elif val > highest_relevant_value_two and out in self.dhma_params.all_open_outs.keys():
                highest_relevant_value_two = val
        print("Highest relevant value 2", highest_relevant_value_two)
        self.increase_factor_two = 100 / float(highest_relevant_value_two)
        overall_increasement = self.increase_factor * self.increase_factor_two
        if overall_increasement >= self.max_increase_dhma:
            self.increase_factor_two = self.max_increase_dhma / self.increase_factor
            if self.increase_factor_two < 1:
                return
        print("Increase factor 2", self.increase_factor_two)
        for out, val in self.new_max_opening_one.items():
            if out in self.dhma_params.all_open_outs.keys():
                new_val = round(val * self.increase_factor_two, 2)
                self.new_max_opening_two[out] = new_val

    def set_zero_values_to_max_opening(self):
        for out, val in self.openings.aha_max_openings.items():
            if not out in self.new_max_opening_two.keys():
                self.new_max_opening_two[out] = val
            elif self.new_max_opening_two[out] == 0 and val != self.new_max_opening_two[out]:
                self.new_max_opening_two[out] = self.openings.aha_max_openings.copy()[out]

    def get_cnt_openings(self):
        return self.dhma_params.all_outs

    def get_max_openings(self):
        for out, val in self.new_max_opening_two.items():
            self.new_max_opening_two[out] = round(val)
        print(self.new_max_opening_two)
        return self.new_max_opening_two

    def print_params(self):
        with open('maximaldurchfluss/dhma_log.txt', 'a+') as log:
            log.write("\n Maximaldurchfluss Durchlauf erfolgreich: ")
            #log.write("\n Results: " + str(self.new_max_opening_two))
            log.write("\n Intermediates:")
            log.write("\n Outs in belowbounds: " + str(self.openings.belowbounds))
            log.write("\n Outs in Aktivitaetsschwelle: " + str(self.openings.activity_threshold))
            log.write("\n Outs in inbounds: " + str(self.openings.inbounds))
            log.write("\n All outs: " + str(self.dhma_params.all_outs))
            log.write("\n All open outs: " + str(self.dhma_params.all_open_outs))
            log.write("\n Max openings aha: " + str(self.openings.aha_max_openings))
            log.write("\n Last opening: " + str(self.last_opening))
            log.write("\n Relevant values: " + str(self.dhma_params.relevant_values))
            log.write("\n Relevant values with buffer: " + str(self.dhma_params.relevant_values_with_buffer))
            log.write("\n Increase factor one: " + str(self.increase_factor))
            log.write("\n New max opening one: " + str(self.new_max_opening_one))
            log.write("\n Increase factor two: " + str(self.increase_factor_two))


class DHMACreator:
    def __init__(self, haus, gateways, dhma_inputs):
        self.haus = haus
        self.gateways = gateways
        self.dhma_inputs = dhma_inputs
        self.gateway_to_id_dict = self.create_gw_id_dict()
        self.last_openings = None

    def run_dhma(self):
        self.renew_dhma_inputs()
        openings = Openings()
        openings.belowbounds = self.dhma_inputs.belowbounds
        openings.inbounds = self.dhma_inputs.inbounds
        openings.activity_threshold = self.dhma_inputs.activity_threshold
        openings.aha_max_openings = dict((k, int(v)) for k, v in self.dhma_inputs.aha_max_openings.items())
        haus_params = self.haus.get_spec_module_params('maximaldurchfluss')
        max_increase_dhma = haus_params.get('max_increase_dhma', 100)
        dhma = DynamicMaximalAbgleich(max_increase_dhma, openings, self.last_openings)
        dhma.run_dynamic_hydraulic_maximal_abgleich()
        self.set_params_per_gateway(dhma)
        self.print_results()

    def create_gw_id_dict(self):
        gateway_to_id_dict = {}
        gw_id = 1
        for gateway in self.gateways:
            gateway_to_id_dict.update({gw_id: gateway})
            gw_id += 1
        return gateway_to_id_dict

    def renew_dhma_inputs(self):
        self.dhma_inputs.belowbounds = self.renew_keys(self.dhma_inputs.belowbounds)
        self.dhma_inputs.inbounds = self.renew_keys(self.dhma_inputs.inbounds)
        self.dhma_inputs.activity_threshold = self.renew_keys(self.dhma_inputs.activity_threshold)
        self.dhma_inputs.aha_max_openings = self.create_all_max_open()
        hparams = self.haus.get_module_parameters()
        self.last_openings = hparams.get('last_openings', {key: value for d in
                                                          (self.dhma_inputs.inbounds, self.dhma_inputs.belowbounds) for key, value in d.items()})

    def renew_keys(self, input_dict):
        new_dict = {}
        for gw_id, gateway in self.gateway_to_id_dict.items():
            value_dict = input_dict.get(gateway, None)
            if value_dict is None:
                continue
            for key, val in value_dict.items():
                new_key = str(key) + "_" + str(gw_id)
                new_dict.update({new_key:val})
        return new_dict

    def create_all_max_open(self):
        max_open_all_gw = {}
        for gateway in self.gateways:
            gw_id = [key for key, val in self.gateway_to_id_dict.items() if val is gateway][0]
            params = gateway.get_parameters()
            aha_max_open = params.get('max_opening', dict((i, 100) for i in range(1, 16)))
            for key, val in aha_max_open.items():
                new_key = str(key) + "_" + str(gw_id)
                max_open_all_gw.update({new_key: val})
        return max_open_all_gw

    def set_params_per_gateway(self, dhma):
        all_dhma_max_openings = dhma.get_max_openings()
        for gw_id, gateway in self.gateway_to_id_dict.items():
            max_openings = {}
            for key, val in all_dhma_max_openings.items():
                id = int(key.split('_')[-1])
                out = int(key.split('_')[0])
                if id == gw_id:
                    max_openings.update({out: val})
            params = gateway.get_parameters()
            params['dhma_max_opening'] = max_openings
            gateway.set_parameters(params)
        hparams = self.haus.get_module_parameters()
        hparams['last_openings'] = dhma.get_cnt_openings()

    def print_results(self):
        with open('maximaldurchfluss/dhma_log.txt', 'a+') as log:
            log.write("\n Results: ")
            for gateway in self.gateways:
                log.write("\n Gateway: " + str(gateway.name))
                log.write("\n" + str(gateway.get_parameters().get('dhma_max_opening')))
