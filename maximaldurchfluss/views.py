# -*- coding: utf-8 -*-
from heizmanager.models import Haus
from heizmanager.render import render_response, render_redirect
from django.http import HttpResponse

def get_name():
    return "Dynamischer hydraulischer Maximal-Abgleich"

def get_global_settings_link(request, haus):
    return "<a href='/m_setup/%s/maximaldurchfluss/'>Dynamischer hydraulischer Maximal-Abgleich</a>" % str(haus.id)

def get_global_description_link():
    desc = u"Öffnet Stellantriebe in Aufheizphasen wenn möglich über den Maximalwert"
    desc_link = "https://www.controme.com/hydraulischer-abgleich/"
    return desc, desc_link

def get_global_settings_page(request, haus):
    if not isinstance(haus, Haus):
        haus = Haus.objects.get(pk=long(haus))
    context = dict()
    params = haus.get_spec_module_params('maximaldurchfluss')
    if request.method == "GET":
        context['haus'] = haus
        context['max_increase_dhma'] = params.get('max_increase_dhma', 100)
        return render_response(request, "m_settings_maximaldurchfluss.html", context)
    if request.method == "POST":
        if 'max_increase_dhma' in request.POST:
            params['max_increase_dhma'] = request.POST.get('max_increase_dhma', 100)
            haus.set_spec_module_params('maximaldurchfluss', params)
        return render_redirect(request, '/m_setup/%s/maximaldurchfluss/' % haus.id)

def get_logging_page(request):
    f = open('maximaldurchfluss/dhma_log.txt', 'r')
    file_content = f.read()
    f.close()
    return HttpResponse(file_content, content_type="text/plain")
