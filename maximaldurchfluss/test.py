import unittest
from tasks import DynamicMaximalAbgleich, Openings, DHMACreator

''' 
belowbounds: values which are lower than Aktivitaetsschwelle and heat full
inbounds: Stellantriebe die aus sind, oder in Aktivitaetsschwelle 
aha_max_openings: Maximaloeffnungen von aha berechnet 
activity_threshold: Werte innerhalbd er Aktivitaetsschwelle
1) 100 Wert ist nicht 0 
2) highest_relevant_value is belowbounds (ein belowbounds ist der der 100 wird)
3) highest_relevant_value is in AS (belowbounds wird 100) 
4) highest_relevant_value is in AS (AS wird 100) 
5) Fall puffer geht ueber 100 checken 
6) Alles ein ausser der der 100 (alles ein)
7) All closed
8) All open and in belowbounds (mehrere sind ein und die sind alle belowbounds)
9) mehrere sind ein und die sind alle AS
10) maximum_increasment ist bereits bei new_max_opening 1 Berechnung ueberschritten 
11) maximum_increasement wird bei Berechnung von new_max_opening 2 ueberschritten

12) Wenn Oeffnung groesser als aha Oeffnung, aber buffer kleiner 0 und das bei 2 AS, sodass eine ueber 100 geht 
7) nur einer ist ein und der ist belowbounds
8) nur einer ist ein und der ist AS
9) mehrere sind ein und die sind alle belowbounds
10) mehrere sind ein und die sind alle AS
11) maximum_increasment ist bereits bei new_max_opening 1 Berechnung ueberschritten 
12) maximum_increasement wird bei Berechnung von new_max_opening 2 ueberschritten 
13) Testfaelle wo der ist-Wert in belowbounds hoeher ist als der max-aha Wert
Frage: welchen Wert bekommen die die aus sind in max_openings zugewiesen? --> aha max_opening
'''

class FakeGateway:
    def __init__(self, name):
        self.parameters = None
        self.name = None

    def set_parameters(self, parameters):
        self.parameters = parameters

    def get_parameters(self):
        return self.parameters


class FakeHaus:
    def __init__(self):
        self.params = None
        self.module_params = dict()

    def set_module_parameters(self, params):
        self.params = params

    def get_module_parameters(self):
        return self.params

    def get_spec_module_params(self, module):
        return self.module_params.get(module, dict())

    def set_spec_module_params(self, module, params):
        print("In haus set spec module params")
        self.module_params[module] = params
        return True


class TestDynamischerMaximalAbgleich(unittest.TestCase):
    def test_100_value_not_zero(self):
        openings = Openings()
        openings.aha_max_openings = {1: 15, 2: 100, 3: 35, 4: 90}
        openings.belowbounds = {1: 25, 2: 100} # out 1 ist hoeher als er laut max_opening darf
        openings.inbounds = {3: 0, 4: 50}
        openings.activity_threshold = {4: 50}
        max_increase_dhma = 100 # hier egal
        last_openings =  {1: 15, 2: 80, 3: 0, 4: 45} # letzte Oeffnung out 4 niedriger
        dhma = DynamicMaximalAbgleich(max_increase_dhma, openings, last_openings)
        dhma.run_dynamic_hydraulic_maximal_abgleich()
        new_openings = dhma.get_max_openings()
        expected_new_openings = {1: 15, 2: 100, 3: 35, 4: 90} # aha max_openings
        self.assertEqual(new_openings, expected_new_openings)

    def test_highest_rel_val_in_belowbounds(self):
        openings = Openings()
        openings.aha_max_openings = {1: 15, 2: 100, 3: 35, 4: 90}
        openings.belowbounds = {4: 90}
        openings.inbounds = {1: 0, 2: 0, 3: 20}
        openings.activity_threshold = {3: 20}
        max_increase_dhma = 100 # hier egal
        last_openings = {1: 0, 2: 0, 3: 15, 4: 90}
        dhma = DynamicMaximalAbgleich(max_increase_dhma, openings, last_openings)
        dhma.run_dynamic_hydraulic_maximal_abgleich()
        new_openings = dhma.get_max_openings()
        expected_new_openings = {1: 15, 2: 100, 3: 28, 4: 100}
        self.assertEqual(new_openings, expected_new_openings)

    def test_highest_rel_val_in_AS_one(self): # belowbounds (2) wird 100
        openings = Openings()
        openings.aha_max_openings = {1: 20, 2: 30, 3: 55, 4: 90, 5: 100, 6:100}
        openings.belowbounds = {1: 20, 2: 30}
        openings.inbounds = {3: 40, 4: 31, 5: 0, 6:0}
        openings.activity_threshold = {3: 40, 4: 31}
        max_increase_dhma = 10
        last_openings = {1: 20, 2: 30, 3: 38, 4: 34, 5: 100, 6:100}
        dhma = DynamicMaximalAbgleich(max_increase_dhma, openings, last_openings)
        dhma.run_dynamic_hydraulic_maximal_abgleich()
        new_openings = dhma.get_max_openings()
        expected_new_openings = {1: 67, 2: 100, 3: 62, 4: 44, 5: 100, 6:100}
        self.assertEqual(new_openings, expected_new_openings)

    def test_highest_rel_val_in_AS_two(self): # AS wird 100
        openings = Openings()
        openings.aha_max_openings = {1: 20, 2: 90, 3: 10, 4: 100, 5: 32, 6:90}
        openings.belowbounds = {1: 20, 3: 10}
        openings.inbounds = {2: 70, 4: 0, 5: 18, 6:0}
        openings.activity_threshold = {2: 70, 5: 18}
        max_increase_dhma = 10
        last_openings = {1: 20, 2: 65, 3: 50, 4: 20, 5: 100, 6:0}
        dhma = DynamicMaximalAbgleich(max_increase_dhma, openings, last_openings)
        dhma.run_dynamic_hydraulic_maximal_abgleich()
        new_openings = dhma.get_max_openings()
        expected_new_openings = {1: 33, 2: 100, 3: 17, 4: 100, 5: 23, 6:90}
        self.assertEqual(new_openings, expected_new_openings)

    def test_highest_rel_val_in_AS_puffer_greater_100(self): # AS wird 100, puffer ueber 100, increase factor < 0
        openings = Openings()
        openings.aha_max_openings = {1: 20, 2: 90, 3: 10, 4: 100, 5: 60, 6:90}
        openings.belowbounds = {1: 20, 3: 10}
        openings.inbounds = {2: 70, 4: 0, 5: 59, 6:0}
        openings.activity_threshold = {2: 70, 5: 59}
        max_increase_dhma = 10
        last_openings = {1: 20, 2: 65, 3: 50, 4: 20, 5: 0, 6:0}
        dhma = DynamicMaximalAbgleich(max_increase_dhma, openings, last_openings)
        dhma.run_dynamic_hydraulic_maximal_abgleich()
        new_openings = dhma.get_max_openings()
        expected_new_openings = {1: 33, 2: 100, 3: 17, 4: 100, 5: 77, 6:90}
        self.assertEqual(new_openings, expected_new_openings)

    def test_all_open_except_100(self):
        openings = Openings()
        openings.aha_max_openings = {1: 52, 2: 19, 3: 84, 4: 100, 5: 80, 6:33}
        openings.belowbounds = {1: 52, 6: 33}
        openings.inbounds = {2: 9, 3: 29, 4: 0, 5: 60}
        openings.activity_threshold = {2: 9, 3: 29, 5: 60}
        max_increase_dhma = 10
        last_openings = {1: 40, 2: 12, 3: 24, 4: 100, 5: 56, 6:33}
        dhma = DynamicMaximalAbgleich(max_increase_dhma, openings, last_openings)
        dhma.run_dynamic_hydraulic_maximal_abgleich()
        new_openings = dhma.get_max_openings()
        expected_new_openings = {1: 100, 2: 11, 3: 46, 4: 100, 5: 84, 6: 63}
        self.assertEqual(new_openings, expected_new_openings)

    def test_all_closed(self):
        openings = Openings()
        openings.aha_max_openings = {1: 17, 2: 85, 3: 100, 4: 23, 5: 19, 6: 45, 7: 88}
        openings.belowbounds = {}
        openings.inbounds = {1: 0, 2: 0, 3: 0, 4: 0, 5: 0, 6: 0, 7: 0}
        openings.activity_threshold = {}
        max_increase_dhma = 10
        last_openings = {1: 0, 2: 0, 3: 0, 4: 0, 5: 0, 6: 0, 7: 0}
        dhma = DynamicMaximalAbgleich(max_increase_dhma, openings, last_openings)
        dhma.run_dynamic_hydraulic_maximal_abgleich()
        new_openings = dhma.get_max_openings()
        expected_new_openings = {1: 17, 2: 85, 3: 100, 4: 23, 5: 19, 6: 45, 7: 88}
        self.assertEqual(new_openings, expected_new_openings)

    def test_all_open_and_in_belowbounds(self):
        openings = Openings()
        openings.aha_max_openings = {1: 88, 2: 22, 3: 19, 4: 76, 5: 35, 6: 100, 7: 84}
        openings.belowbounds = {1: 88, 2: 22, 3: 19, 4: 76, 5: 35, 6: 100, 7: 84}
        openings.inbounds = {}
        openings.activity_threshold = {}
        max_increase_dhma = 10
        last_openings = {1: 88, 2: 22, 3: 19, 4: 76, 5: 35, 6: 100, 7: 84}
        dhma = DynamicMaximalAbgleich(max_increase_dhma, openings, last_openings)
        dhma.run_dynamic_hydraulic_maximal_abgleich()
        new_openings = dhma.get_max_openings()
        expected_new_openings = {1: 88, 2: 22, 3: 19, 4: 76, 5: 35, 6: 100, 7: 84}
        self.assertEqual(new_openings, expected_new_openings)

    def test_all_open_in_AS(self):
        openings = Openings()
        openings.aha_max_openings = {1: 100, 2: 24, 3: 57, 4: 73}
        openings.belowbounds = {}
        openings.inbounds = {1: 0, 2: 20, 3: 30, 4: 10}
        openings.activity_threshold = {2: 20, 3: 30, 4: 10}
        max_increase_dhma = 100
        last_openings = {1: 50, 2: 18, 3: 32, 4: 10}
        dhma = DynamicMaximalAbgleich(max_increase_dhma, openings, last_openings)
        dhma.run_dynamic_hydraulic_maximal_abgleich()
        new_openings = dhma.get_max_openings()
        expected_new_openings = {1: 100, 2: 77, 3: 100, 4: 33}
        self.assertEqual(new_openings, expected_new_openings)

    def test_all_open_and_in_AS(self):
        openings = Openings()
        openings.aha_max_openings = {1: 100, 2: 24, 3: 57, 4: 73}
        openings.belowbounds = {}
        openings.inbounds = {1: 0, 2: 20, 3: 30, 4: 10}
        openings.activity_threshold = {2: 20, 3: 30, 4: 10}
        max_increase_dhma = 100
        last_openings = {1: 50, 2: 18, 3: 32, 4: 10}
        dhma = DynamicMaximalAbgleich(max_increase_dhma, openings, last_openings)
        dhma.run_dynamic_hydraulic_maximal_abgleich()
        new_openings = dhma.get_max_openings()
        expected_new_openings = {1: 100, 2: 77, 3: 100, 4: 33}
        self.assertEqual(new_openings, expected_new_openings)

    def test_max_increasement_after_max_opening_1(self):
        openings = Openings()
        openings.aha_max_openings = {1: 20, 2: 30, 3: 55, 4: 90, 5: 100, 6: 99}
        openings.belowbounds = {1: 20, 2: 30}
        openings.inbounds = {3: 40, 4: 31, 5: 0, 6: 0}
        openings.activity_threshold = {3: 40, 4: 31}
        max_increase_dhma = 2
        last_openings = {1: 20, 2: 30, 3: 38, 4: 34, 5: 0, 6: 0}
        dhma = DynamicMaximalAbgleich(max_increase_dhma, openings, last_openings)
        dhma.run_dynamic_hydraulic_maximal_abgleich()
        new_openings = dhma.get_max_openings()
        expected_new_openings = {1: 40, 2: 60, 3: 43, 4: 31, 5: 100, 6: 99}
        self.assertEqual(new_openings, expected_new_openings)

    def test_max_increasement_after_max_opening_2(self):
        openings = Openings()
        openings.aha_max_openings = {1: 20, 2: 30, 3: 55, 4: 90, 5: 100, 6: 99}
        openings.belowbounds = {1: 20, 2: 30}
        openings.inbounds = {3: 40, 4: 31, 5: 0, 6: 0}
        openings.activity_threshold = {3: 40, 4: 31}
        max_increase_dhma = 3
        last_openings = {1: 20, 2: 30, 3: 38, 4: 34, 5: 0, 6: 0}
        dhma = DynamicMaximalAbgleich(max_increase_dhma, openings, last_openings)
        dhma.run_dynamic_hydraulic_maximal_abgleich()
        new_openings = dhma.get_max_openings()
        expected_new_openings = {1: 60, 2: 90, 3: 55, 4: 40, 5: 100, 6: 99}
        self.assertEqual(new_openings, expected_new_openings)
        self.assertEqual(dhma.get_cnt_openings(), {1: 20, 2: 30, 3: 40, 4: 31, 5: 0, 6: 0})

    def test_is_value_in_belowbound_different_than_aha_val(self):
        openings = Openings()
        openings.aha_max_openings = {1: 20, 2: 10, 3: 48, 4: 11, 5: 89, 6: 91, 7: 100, 8: 100, 9: 100, 10: 100, 11: 100, 12: 100, 13: 100, 14: 100, 15: 100}
        openings.belowbounds = {2: 99}
        openings.inbounds =  {1: 0, 3: 0, 4: 0, 5: 0, 6: 0, 7: 0}
        openings.activity_threshold = {}
        max_increase_dhma = 100
        last_openings = {1: 20, 2: 30, 3: 38, 4: 34, 5: 0, 6: 0}
        dhma = DynamicMaximalAbgleich(max_increase_dhma, openings, last_openings)
        dhma.run_dynamic_hydraulic_maximal_abgleich()
        new_openings = dhma.get_max_openings()
        expected_new_openings = {1: 20, 2: 100, 3: 48, 4: 11, 5: 89, 6: 91, 7: 100, 8: 100, 9: 100, 10: 100, 11: 100, 12: 100, 13: 100, 14: 100, 15: 100}
        self.assertEqual(new_openings, expected_new_openings)

    def test_more_than_one_relevant_100_one_not_out(self):
        openings = Openings()
        openings.aha_max_openings = {1: 100, 2: 10, 3: 48, 4: 11, 5: 89, 6: 91, 7: 100, 8: 100, 9: 100, 10: 100, 11: 100, 12: 100, 13: 100, 14: 100, 15: 100}
        openings.belowbounds = {7: 100}
        openings.inbounds =  {1: 0, 2: 0, 3: 0, 4: 0, 5: 0, 6: 0}
        openings.activity_threshold = {}
        max_increase_dhma = 100
        last_openings = {1: 20, 2: 30, 3: 38, 4: 34, 5: 0, 6: 0}
        dhma = DynamicMaximalAbgleich(max_increase_dhma, openings, last_openings)
        dhma.run_dynamic_hydraulic_maximal_abgleich()
        new_openings = dhma.get_max_openings()
        expected_new_openings = {1: 100, 2: 10, 3: 48, 4: 11, 5: 89, 6: 91, 7: 100, 8: 100, 9: 100, 10: 100, 11: 100, 12: 100, 13: 100, 14: 100, 15: 100}
        self.assertEqual(new_openings, expected_new_openings)

    def test_more_gateways(self):
        openings = Openings()
        openings.aha_max_openings = {'1_1': 20, '2_1': 10, '3_1': 48, '4_1': 11, '5_1': 89, '6_1': 91, '7_1': 100,
                                     '1_2': 15, '2_2': 33, '3_2': 17, '4_2': 77, '5_2': 5, '6_3': 28, '7_4': 90}
        openings.belowbounds = {'1_1': 20}
        openings.inbounds =  {'2_1': 0, '3_1': 0, '4_1': 0, '5_1': 0, '6_1': 0, '7_1': 0,
                              '1_2': 0, '2_2': 0, '3_2': 0, '4_2': 0, '5_2': 0, '6_3': 0, '7_4': 0}
        openings.activity_threshold = {}
        max_increase_dhma = 100
        last_openings = {'1_1': 0, '2_1': 0, '3_1': 0, '4_1': 0, '5_1': 0, '6_1': 0, '7_1': 0,
                         '1_2': 0, '2_2': 0, '3_2': 0, '4_2': 0, '5_2': 0, '6_3': 0, '7_4': 0}
        dhma = DynamicMaximalAbgleich(max_increase_dhma, openings, last_openings)
        dhma.run_dynamic_hydraulic_maximal_abgleich()
        new_openings = dhma.get_max_openings()
        expected_new_openings = {'1_1': 100, '2_1': 10, '3_1': 48, '4_1': 11, '5_1': 89, '6_1': 91, '7_1': 100,
                                 '1_2': 15, '2_2': 33, '3_2': 17, '4_2': 77, '5_2': 5, '6_3': 28, '7_4': 90}
        self.assertEqual(new_openings, expected_new_openings)

    def test_dhma_creator(self):
        gateway_one = FakeGateway("GW1")
        gateway_two = FakeGateway("GW2")
        params_one = {'max_opening': {1: 20, 2: 30, 3: 100, 4: 45}}
        gateway_one.set_parameters(params_one)
        params_two = {'max_opening': {1: 10, 2: 90, 3: 77, 4: 80}}
        gateway_two.set_parameters(params_two)
        haus = FakeHaus()
        hparams = {'last_openings': {'1_1': 20, '2_1': 30, '3_1': 0, '4_1': 0,
                                     '1_2': 10, '2_2:': 0, '3_2': 0, '4_2': 80}}
        haus.set_module_parameters(hparams)
        gateways = [gateway_one, gateway_two]
        openings = Openings()
        openings.belowbounds = {gateway_one: {1: 20, 2: 30}, gateway_two: {1: 10, 4:80}}
        openings.inbounds =  {gateway_one: {3: 0, 4: 0}, gateway_two: {2: 0, 3: 0}}
        openings.activity_threshold = {}
        dhma_creator = DHMACreator(haus, gateways, openings)
        dhma_creator.run_dhma()
        self.assertEqual(gateway_one.get_parameters().get('dhma_max_opening'), {1: 25, 2: 38, 3: 100, 4: 45})
        self.assertEqual(gateway_two.get_parameters().get('dhma_max_opening'), {1: 13, 2: 90, 3: 77, 4: 100})

if __name__ == '__main__':
    unittest.main()
