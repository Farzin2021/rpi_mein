from fabric.api import local


# handle local commands
def _local_handler(cmd, state='capture'):
    try:
        local_run = local(cmd, capture=True)
        if state == 'status':
            status = 'yes'
        else:
            status = local_run
    except:
        status = 'no'
    return status
