import sys, os, django
# Add django settings to run this script seperately!
try:
    sys.path.append('/home/pi/rpi/')
    os.environ.setdefault("DJANGO_SETTINGS_MODULE", "rpi.settings")
    django.setup()
except:
    print('Error in initialize django settings!')
    sys.exit()


from bluepy.btle import Scanner, DefaultDelegate
from datetime import datetime
import heizmanager.cache_helper as ch
import requests
from rf.models import RFAktor, RFController
from heizmanager.models import RPi
import rpi


# detect if device is drop or master
def device_situation():
    try:
        reload(rpi.server)
    except:
        pass
    try:
        from rpi.server import use_localhost
    except ImportError:
        use_localhost = False
    try:
        from rpi.server import use_host
    except ImportError:
        use_host = 'localhost' if use_localhost else 'mycontromecom'
    return use_localhost, use_host


class ScanDelegate(DefaultDelegate):
    def __init__(self):
        DefaultDelegate.__init__(self)

    def handleDiscovery(self, dev, isNewDev, isNewData):
        if isNewDev:
            discovery = "Discovered device: %s"  % dev.addr
        elif isNewData:
            rec = "Received new data from: %s" % dev.addr
scanner = Scanner().withDelegate(ScanDelegate())
devices = scanner.scan(10.0)
rpi = RPi.objects.first()
controller = RFController.objects.filter(rpi_id= rpi.id, protocol='btle').first()
all_devices = [str(name) for name in RFAktor.objects.filter(controller_id=controller.id).values_list('name', flat=True)]
for dev in devices:
    if str(dev.addr) in all_devices:
        for (adtype, desc, value) in dev.getScanData():
            device_type = "  %s = %s" % (desc, value)
        if 'eTRV' in device_type:
            current_time = datetime.now().strftime('%d.%m.%Y - %H:%M')
            params = {
                    "pair_id": str(dev.addr),
                    "current_time": current_time,
                    "rssi": str(dev.rssi)
                    }
            use_localhost, use_host = device_situation()
            try:
                url = "http://localhost/set/rssi/"
                response = requests.get(url, params=params, timeout=10)
            except:
                pass
            try:
                if not use_localhost:
                    url = "http://%s/set/rssi/" % (use_host)
                    response = requests.get(url, params=params, timeout=10)
            except:
                pass
