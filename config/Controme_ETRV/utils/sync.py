from heizmanager import cache_helper as ch
from utils import connection, ui, controllers
import heizmanager.network_helper as nh
from heizmanager.models import RPi
from heizmanager.models import Raum
import memcache
from time import sleep


client = memcache.Client([('127.0.0.1', 11212)])


# get previous room temp
def pre_room_temp(device):
    room_data = ch.get('etrv__%s' % device)
    return room_data.get('r')

# get target of room
def target_room_temp(device):
    room_data = ch.get('etrv__%s' % device)
    return room_data.get('target')

# get eTRV data
def get_etrv_values(device, strsecret):
    locked = ui.lock_checkbox(device)
    room_data = ch.get('etrv__%s' % device)
    if not room_data:
        room_data = dict()
    temp_data, danfoss = connection.get_danfoss(device, strsecret)
    if locked:
        if room_data.get('r'):
            room_data['s'] = room_data['r']
        else:
            room_data['s'] = temp_data[0]
    else:
        room_data['s'] = temp_data[0]
    if not room_data.get('ps'):
        room_data['ps'] = temp_data[0]
    ch.set('etrv__%s' % device, room_data)
    return temp_data, danfoss

# syncing algorithm to decide target temp
def temp_sync(device, drop=False):
    room_data = ch.get('etrv__%s' % device)
    pre_r = room_data.get('r')
    pre_pr = room_data.get('pr')
    pre_s = room_data.get('s')
    if drop and pre_r and pre_pr and pre_r == pre_pr:
        room_data['target'] = pre_s
        room_data['ps'] = pre_s
    else:
        if room_data and room_data.get('target'):
            if room_data.get('r') == room_data.get('target'):
                room_data['pr'] = room_data.get('r')
            if room_data.get('s') == room_data.get('target'):
                room_data['ps'] = room_data.get('s')
        if room_data.get('r') == room_data.get('s'):
            room_data['target'] = room_data.get('r')
        elif room_data.get('r') == room_data.get('pr') and room_data.get('s') != room_data.get('ps'):
            room_data['target'] = room_data.get('s')
        elif room_data.get('s') == room_data.get('ps') and room_data.get('r') != room_data.get('pr'):
            room_data['target'] = room_data.get('r')
        else:
            room_data['target'] = room_data.get('r')
    ch.set('etrv__%s' % device, room_data)
    print('* Cache data: %s' % room_data)
    return room_data.get('target')

# room: sync data from dropserver to master
def drop_rooms_process(data):
    for device in data:
        try:
            sleep(0.2)
            drop_device = data.get(device)
            print('info: sync: device: master with dropserver', device)
            locked = ui.lock_checkbox(device)
            s_z_switch = ui.soll_ziel_checkbox(device)
            if locked:
                pass
            else:
                if drop_device.get('drop_soll') != drop_device.get('soll'):
                    raum = Raum.objects.get(id=int(drop_device.get('room_id')))
                    solltemp = ui.rounder(drop_device.get('soll') + drop_device.get('offset'))
                    mode = drop_device.get('mode')
                    target = drop_device.get('current_target')
                    if s_z_switch:
                        if mode == 'disable' and target != solltemp:
                            ui.set_ziel(target, raum)
                            print('info: sync: disable: write ziel to ui: %s.' % target)
                        elif mode == 'enable':
                            if target != solltemp:
                                ui.set_ziel(target, raum)
                                print('info: sync: enable: write ziel to ui: %s.' % target)
                    else:
                        if mode == 'disable' and target != solltemp:
                            ui.set_soll(target, raum)
                            print('info: sync: disable: write soll to ui: %s.' % target)
                        elif mode == 'enable':
                            if target != solltemp:
                                ui.set_soll(target, raum)
                                print('info: sync: enable: write soll to ui: %s.' % target)
        except:
            print('info: sync: error in device level of drop server.')

# sync data from dropserver to master
def sync_drop_rooms_master():
    try:
        sleep(5)
        macaddr = nh.get_mac()
        rpis = list(RPi.objects.all().values_list('name'))
        for rpi in rpis:
            try:
                sleep(0.2)
                if rpi[0] != macaddr:
                    print('info: sync: controller: master with dropserver', rpi[0])
                    data = client.get('%s' % rpi[0])
                    try:
                        if ui.get_encryption_level() == 'on':
                            data = eval(ui.decompress_string(data))
                    except:
                        pass
                    if data:
                        drop_rooms_process(data)
                        if ui.get_encryption_level() == 'on':
                            client.set('%s' % rpi[0], ui.compress_string('{}'))
                        else:
                            client.set('%s' % rpi[0], {})
                    ui.encrypt_zlib(rpi[0])
            except:
                print('info: sync: controller: error in controller level of drop server.')
    except:
        print('info: sync: sync: error in sync_drop_rooms_master function.')

# set new value to eTRV
def hardware_setpoint(target, strsecret, danfoss, device):
    try:
        connection.set_danfoss(target, danfoss)
        print('info: sync: hw: write ziel to device: %s' % target)
    except:
        try:
            print('info: sync: hw:  reconnect to write data.')
            connection.set_danfoss_raw(target, device, strsecret)
            print('info: sync: hw: write ziel to device: %s' % target)
        except:
            print('critical: sync: hw: error in writing to device')

# dropserver rpi function
def drop_server(device, data_set):
    print('info: main: drop: this device is dropserver.')
    drop_client, data = controllers.get_mem_master()
    print('info: main: drop:', data.get(str(device),{}))
    if data.get(str(device),{}) == {}:
        return None
    strsecret, mode = ui.get_secret_mode(device, data.get(str(device),{}))
    print('info: main: drop:', device, strsecret, mode)
    temp_data, danfoss = get_etrv_values(device, strsecret)
    target = temp_sync(device, True)
    solltemp = target - data.get(str(device),{}).get('offset')
    if data.get(str(device),{}).get('locked'):
        target = pre_room_temp(str(device))
    data[str(device)]['drop_soll'] = solltemp
    data[str(device)]['current_target'] = target
    data_set[str(device)] = data.get(str(device))
    controllers.set_mem_master(drop_client, data_set)
    return target, temp_data, strsecret, danfoss, data_set

# master rpi function
def master_server(device, device_data):
    strsecret, mode = ui.get_secret_mode(device, device_data)
    print('info: main: master:', device, strsecret, mode)
    temp_data, danfoss = get_etrv_values(device, strsecret)
    target = temp_sync(device)
    s_z_switch = ui.soll_ziel_checkbox(device)
    return target, temp_data, strsecret, danfoss, s_z_switch, mode

# master rpi function
def master_server_ui(target, s_z_switch, mode, raum, device):
    locked = ui.lock_checkbox(device)
    if locked:
        pass
    else:
        if s_z_switch:
            if mode == 'disable' and target != pre_room_temp(device):
                ui.set_ziel(target, raum)
                print('info: sync: disable: write ziel to ui: %s.' % target)
            elif mode == 'enable':
                if target != pre_room_temp(device):
                    ui.set_ziel(target, raum)
                    print('info: sync: enable: write ziel to ui: %s.' % target)
        else:
            if mode == 'disable' and target != pre_room_temp(device):
                ui.set_soll(target, raum)
                print('info: sync: disable: write soll to ui: %s.' % target)
            elif mode == 'enable':
                if target != pre_room_temp(device):
                    ui.set_soll(target, raum)
                    print('info: sync: enable: write soll to ui: %s.' % target)