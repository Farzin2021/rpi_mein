from time import sleep
from datetime import datetime, timedelta
from fabric.api import local
from heizmanager import cache_helper as ch
from config.Controme_ETRV.helper import _local_handler

btle_timer = 300
btle_calc = ch.set('btle_calc', datetime.now())


# check system uptime
def uptime():
    try:
        with open('/proc/uptime', 'r') as f:
            uptime_seconds = float(f.readline().split()[0])
            if uptime_seconds < 180:
                print('info: wait 3 minutes because of system uptime!')
                sleep(180)
            else:
                sleep(5)
    except:
        print('critical: error in getting uptime!')
        sleep(20)


# check bluetooth status
def health_check():
    service_calc = ch.get('service_calc')
    btle_calc = ch.get('btle_calc')
    exec_count = ch.get('exec_count') or 0

    if not service_calc or not btle_calc:
        ch.set('service_calc', datetime.now())
        ch.set('btle_calc', datetime.now())
        return
    if (datetime.now() - timedelta(seconds=btle_timer) > btle_calc):
        try:
            old_handler = local("sudo supervisorctl -c /etc/supervisor/supervisord.conf status btlehandler | grep -q 'RUNNING' && echo yes || echo no", capture=True)
            if old_handler == 'yes':
                _local_handler('sudo cp /home/pi/rpi/config/supervisor-newbtle.conf /etc/supervisor/conf.d/btle.conf')
                _local_handler('echo $(date) > /var/log/uwsgi/tempexec_v1')
                _local_handler('sudo supervisorctl -c /etc/supervisor/supervisord.conf stop btlehandler')
                _local_handler('sudo sed -i "/\[program:btlehandler\]/c\\#\[program:btlehandler\]" /etc/supervisor/conf.d/messaging.conf')
                _local_handler('sudo sed -i "/command = sudo \/usr\/bin\/python -u \/home\/pi\/rpi\/config\/btle_handler.py/c\\# command = sudo \/usr\/bin\/python -u \/home\/pi\/rpi\/config\/btle_handler.py" /etc/supervisor/conf.d/messaging.conf')
                _local_handler('sudo sed -i "/command = sudo \/usr\/bin\/python \/home\/pi\/rpi\/config\/btle_handler.py/c\\# command = sudo \/usr\/bin\/python \/home\/pi\/rpi\/config\/btle_handler.py" /etc/supervisor/conf.d/messaging.conf')
                _local_handler("sudo supervisorctl -c /etc/supervisor/supervisord.conf reread")
                _local_handler("sudo supervisorctl -c /etc/supervisor/supervisord.conf update")
                _local_handler('sudo supervisorctl -c /etc/supervisor/supervisord.conf restart btlehandler')
                _local_handler('sudo supervisorctl -c /etc/supervisor/supervisord.conf restart newbtle')
                _local_handler('sudo systemctl stop etrv_handler.service')
                _local_handler('sudo systemctl disable etrv_handler.service')
                _local_handler("sudo sed -i '/-l 127.0.0.1/c\-l 0.0.0.0' /etc/memcached.conf ; sudo systemctl restart memcached.service")
        except:
            pass
        try:
            local('sudo chmod 777 /var/log/uwsgi/.btle_health')
        except:
            pass
        local('sudo echo $(date) > /var/log/uwsgi/.btle_health')
        def ble_init(i):
            with open('/proc/uptime', 'r') as f:
                uptime_seconds = float(f.readline().split()[0])
                if uptime_seconds > 7200:
                    local('sudo echo "* uptime greater than 2 hours" >> /var/log/uwsgi/.btle_health')
                    hci_status = local('sudo systemctl status hciuart.service | grep -q "Active: failed" && echo yes || echo no', capture=True)
                    local('sudo echo "** hci down? %s" >> /var/log/uwsgi/.btle_health' % hci_status)
                    local('sudo echo "** hci count? %s" >> /var/log/uwsgi/.btle_health' % exec_count)
                    if hci_status == 'yes':
                        if exec_count > 4:
                            ch.set('exec_count', 0)
                            local('sudo echo "*** hci wants reboot." >> /var/log/uwsgi/.btle_health')
                            local('sudo reboot now')
                        else:
                            local('sudo bash /home/pi/rpi/config/Controme_ETRV/utils/hci_service.sh')
                            local('sudo echo "*** hci executed script." >> /var/log/uwsgi/.btle_health')
                            ch.set('exec_count', exec_count+1)
        try:
            ble_init(1)
            print('info: check bluetooth device.')
        except:
            print('critical: error restarting bluetooth device')
        try:
            sleep(3)
            print('info: restart bluetooth service.')
            local("sudo /etc/init.d/bluetooth restart")
            ch.set('btle_calc', datetime.now())
            try:
                local("echo 'flush_all' | nc localhost 11212")
            except:
                pass
            try:
                local("echo 'flush_all' | netcat localhost 11212")
            except:
                pass
        except:
            print('critical: error restarting bluetooth service.')
    sleep(5)
