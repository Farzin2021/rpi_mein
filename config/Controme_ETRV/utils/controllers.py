from rf.models import RFController, RFAktor
from heizmanager.models import Haus
from heizmanager.mobile.m_temp import get_module_offsets
import memcache
import ast
import ui
import heizmanager.network_helper as nh
from heizmanager.models import RPi
from heizmanager.abstracts.base_mode import BaseMode
from time import sleep


macaddr = nh.get_mac()
haus = Haus.objects.all()[0]
client = memcache.Client([('127.0.0.1', 11212)])


# get all controllers and set devices into memcache
def get_controllers():
    sleep(2)
    rf_dict = dict()
    rf_devices = []
    rf_devices = list(RFAktor.objects.all().values())
    for rf in rf_devices:
        sleep(0.2)
        try:
            if rf.get('protocol', None) == 'btle' and rf.get('type', None) == 'hkteTRV':
                controller = list(RFController.objects.filter(id=rf.get('controller_id', None)).values_list('rpi__name', 'rpi__local_ip'))[0]
                if len(rf_dict.get(str(controller[0]), [])) != 0:
                    rf_dict[str(controller[0])].append(rf)
                else:
                    rf_dict[str(controller[0])] = []
                    rf_dict[str(controller[0])].append(rf)
        except:
            pass
    sorted_data = dict()
    for controller in rf_dict:
        sorted_data[str(controller)] = {}
        for device in rf_dict[controller]:
            try:
                sleep(0.2)
                raum = ui.get_room(device.get('name'))
                try:
                    ret = BaseMode.get_active_mode_raum(raum)
                    if ret:
                        if ret.get('use_offsets', False):
                            offset = get_module_offsets(raum)
                        else:
                            offset = 0
                        solltemp = ret.get('soll')
                        mode = 'enable'
                    else:
                        solltemp = raum.solltemp
                        offset = get_module_offsets(raum)
                        mode = 'disable'
                except:
                    solltemp = raum.solltemp
                    offset = get_module_offsets(raum)
                    mode = 'disable'

                device_dict = {'secret': ast.literal_eval(device.get('parameters')).get('secret'), 
                            'locked': ui.lock_checkbox(device.get('name')),'room_id': device.get('raum_id'), 'soll': solltemp, 'offset': offset, 'mode': mode}
                sorted_data[str(controller)][device.get('name')]= device_dict
                try:
                    interval = ui.get_intervall({'data': {device.get('name'): sorted_data[str(controller)][device.get('name')]}})
                except:
                    interval = 60
                    print('critical: error getting interval.')
                sorted_data[str(controller)]['interval']= interval
            except:
                pass
    try:
        sleep(1)
        client.set('controllers', sorted_data, time=2000)
        print('info: controllers have been set!')
        compress_data = ui.compress_string(str(sorted_data))
        client.set('controllers_zlib', compress_data, time=2000)
        print('info: controllers_zlib have been set!')
    except:
        print('critical: error in loading controllers!')

# get data from master
def get_mem_master():
    try:
        rpis = list(RPi.objects.all().values_list('local_ip'))
        for rpi in rpis:
            sleep(1)
            if rpi[0]:
                drop_client = memcache.Client([(rpi[0], 11212)])
                if ui.get_encryption_level() == 'on':
                    data = eval(ui.decompress_string(drop_client.get('controllers_zlib')))
                    data = data.get(macaddr, {})
                else:
                    data = drop_client.get('controllers').get(macaddr, {})
        return drop_client, data
    except:
        return None, None

# save data to master
def set_mem_master(drop_client, data):
    sleep(1)
    if ui.get_encryption_level() == 'on':
        drop_client.set(macaddr, ui.compress_string(str(data)), time=2000)
    else:
        drop_client.set(macaddr, data, time=2000)