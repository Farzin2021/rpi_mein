import requests
import json
from rpi.aeshelper import aes_encrypt, aes_decrypt
from heizmanager.models import Raum
from rf.models import RFAktor, RFController
import pytz
from datetime import datetime
from quickui.views import QuickuiMode
from heizmanager.models import Haus
from heizmanager.abstracts.base_mode import BaseMode
import controllers
from time import sleep
from heizmanager.mobile.m_temp import get_module_offsets
from heizmanager import cache_helper as ch
import rpi
import zlib
import binascii
import memcache


with open('/sys/class/net/eth0/address') as f:
    macaddr = f.read().strip().replace(':', '-')

# return if data is encypted
def get_encryption_level():
    haus = Haus.objects.first()
    handler_zlib = haus.get_module_parameters().get('encryption', {}).get('handler_zlib', 'off')
    return handler_zlib

# compress data using zlib
def compress_string(data):
    compressed_data = zlib.compress(data.encode("utf-8"))
    return binascii.hexlify(compressed_data)

# decompress data using zlib
def decompress_string(data):
    compressed_data = binascii.unhexlify(data)
    decompressed_data = zlib.decompress(compressed_data).decode("utf-8")
    return str(decompressed_data)

# encrypt drops data
def encrypt_zlib(macaddr):
    try:
        client = memcache.Client([('127.0.0.1', 11212)])
        r = requests.get('http://%s/get/%s/%s/' % ('localhost', 'btle', macaddr), timeout=20)
        msg = r.content
        d = compress_string(msg)
        client.set('zlib__%s' % macaddr, d)
    except:
        print('info: ui: encrypt_zlib: %s error in data.' % macaddr)

# detect if device is drop or master
def device_situation():
    try:
        reload(rpi.server)
    except:
        pass
    try:
        from rpi.server import use_localhost
    except ImportError:
        use_localhost = False
    try:
        from rpi.server import use_host
    except ImportError:
        use_host = 'localhost' if use_localhost else 'mycontromecom'
    return use_localhost, use_host

# get devices data from ui
def receive_get_data():
    sleep(2)
    use_localhost, use_host = device_situation()
    try:
        if use_localhost:
            r = requests.get('http://%s/get/%s/%s/' % ('localhost', 'btle', macaddr), timeout=20)
            try:
                msg = aes_decrypt(r.content).rsplit('}', 1)[0] + '}'
            except:
                msg = r.content
        else:
            try:
                if get_encryption_level() == 'on':
                    print('info: ui: receive: master: network limit.')
                    client = memcache.Client([(use_host, 11212)])
                    r = client.get('zlib__%s' % macaddr)
                    msg = decompress_string(r)
                    print('info: ui: receive: master: data recieved with memcache')
                else:
                    r = requests.get('http://%s/get/%s/%s/' % (use_host, 'btle', macaddr), timeout=20)
                    msg = r.content
            except:
                print('info: ui: receive: else: error in getting data.')
        print('info: ui: receive: master:', msg)
        return json.loads(msg)
    except:
        print('info: ui: receive: master: error in getting data receive error.')
        return None

# send data to /rf
def send_data_rf(data):
    sleep(1)
    use_localhost, use_host = device_situation()
    try:
        d = aes_encrypt(json.dumps(data.values()[0]))
        requests.post("http://localhost/set/%s/%s/%s/" % ('btle', macaddr, macaddr),data={'data': d})
        print('info: ui: send: master: localhost:', data)
    except:
        print('info: ui: send: master: error in localhost.')
    try:
        if not use_localhost:
            requests.post("http://%s/set/%s/%s/%s/" % (use_host, 'btle', macaddr, macaddr), data={'data': json.dumps(data.values()[0])}, timeout=10)
            print('info: ui: send: master: use_host:', data)
    except:
        print('info: ui: send: master: error in use_host.')

# get switch mode
def soll_ziel_checkbox(device_name):
    try:
        device = list(RFAktor.objects.filter(name = str(device_name)).values())[0]
        switch = eval(device.get('parameters', None)).get('switchmode', False)
        return switch
    except:
        return False

# check if device is locked in ui
def lock_checkbox(device_name):
    try:
        device = list(RFAktor.objects.filter(name = str(device_name)).values())[0]
        lock = device.get('locked', False)
        return lock
    except:
        return False

# read data one by one
def read_one(device, soll, offset, mode, device_name):
        locked = lock_checkbox(device_name)
        s_z_switch = soll_ziel_checkbox(device_name)
        if mode == 'enable':
            if locked or s_z_switch:
                ziel = soll + offset
            else:
                ziel = soll + offset
        else:
            zieloffset = device.get('zieloffset', 0)
            if locked or s_z_switch:
                ziel = soll + offset + zieloffset
            else:
                ziel = soll + offset + zieloffset
        secret = device['secret']
        x = [unichr(ord(u)) for u in list(secret)]
        strsecret = ''.join(x).encode('latin-1')
        return ziel, strsecret

# send string of secret
def return_strsecret(device):
    secret = device['secret']
    x = [unichr(ord(u)) for u in list(secret)]
    strsecret = ''.join(x).encode('latin-1')
    return strsecret

# return room id
def device_room(device):
    try:
        raum_id = list(RFAktor.objects.filter(name = device).values())
        raum_id = raum_id[0].get('raum_id')
    except:
        raum_id = None
    return raum_id

# get room
def get_room(device):
    try:
        room_id = device_room(device)
        raum = Raum.objects.get(id=int(room_id))
    except:
        raum = None
    return raum

# update rf data to send into ui
def update_rf_ui(device, temp_data, secret, soll):

    # define max value that solltemp can recieve
    try:
        haus = Haus.objects.first()
        haus_params = haus.get_module_parameters()
        max_allowed_etrv = haus_params.get('max_allowed_etrv_soll', 30)
        if float(soll) >= float(max_allowed_etrv):
            soll = max_allowed_etrv
    except:
        pass

    s_data = dict()
    s_data[(device)] = {}
    s_data[(device)]['soll'] = soll
    s_data[(device)]['ist'] = int(temp_data[1])
    s_data[(device)]['device'] = 'eTRV'
    s_data[(device)]['secret'] = secret
    s_data[(device)]['manufacturer'] = 'Danfoss'
    berlin = pytz.timezone("Europe/Berlin")
    now = datetime.now(berlin)
    s_data[(device)]['last_seen'] = now.strftime("%Y-%m-%d %H:%M")
    s_data['last_seen'] = now.strftime("%Y-%m-%d %H:%M")
    print('info: ui: update_rf: %s' % s_data)
    print(temp_data, len(temp_data))
    if len(temp_data) == 5:
        _, _, mode, mounted, battery_percentage = temp_data
        setting_data = ','.join([str(mode), str(mounted), str(battery_percentage)])
        print('info: ui: update_rf: battery:{0}%, mode:{1}, mounted:{2}'.format(battery_percentage, mode, mounted))
        params = {
                "pair_id": device,
                "tupple": setting_data,
                }
        use_localhost, use_host = device_situation()
        try:
            url = "http://localhost/set/etrvsetting/"
            response = requests.get(url, params=params, timeout=10)
            print('info: ui: send: master: localhost:', response.text)
        except:
            print('info: ui: send: master: error in localhost.')
        try:
            if not use_localhost:
                url = "http://%s/set/etrvsetting/" % (use_host)
                response = requests.get(url, params=params, timeout=10)
                print('info: ui: send: master: use_host:', response.text)
        except:
            print('info: ui: send: master: error in use_host.')
    return s_data

# update rf data with no connected room
def update_rf_ui_no_room(device, temp_data, secret):

    # define max value that solltemp can recieve
    try:
        haus = Haus.objects.first()
        haus_params = haus.get_module_parameters()
        max_allowed_etrv = haus_params.get('max_allowed_etrv_soll', 30)
        if float(temp_data[0]) >= float(max_allowed_etrv):
            temp_data[0] = max_allowed_etrv
    except:
        pass

    s_data = dict()
    s_data[(device)] = {}
    s_data[(device)]['soll'] = temp_data[0]
    s_data[(device)]['ist'] = int(temp_data[1])
    s_data[(device)]['device'] = 'eTRV'
    s_data[(device)]['secret'] = secret
    s_data[(device)]['manufacturer'] = 'Danfoss'
    berlin = pytz.timezone("Europe/Berlin")
    now = datetime.now(berlin)
    s_data[(device)]['last_seen'] = now.strftime("%Y-%m-%d %H:%M")
    s_data['last_seen'] = now.strftime("%Y-%m-%d %H:%M")
    print('info: ui: update_rf: %s' % s_data)
    print(temp_data, len(temp_data))
    if len(temp_data) == 5:
        _, _, mode, mounted, battery_percentage = temp_data
        setting_data = ','.join([str(mode), str(mounted), str(battery_percentage)])
        print('info: ui: update_rf: battery:{0}%, mode:{1}, mounted:{2}'.format(battery_percentage, mode, mounted))
        params = {
                "pair_id": device,
                "tupple": setting_data,
                }
        use_localhost, use_host = device_situation()
        try:
            url = "http://localhost/set/etrvsetting/"
            response = requests.get(url, params=params, timeout=10)
            print('info: ui: send: master: localhost:', response.text)
        except:
            print('info: ui: send: master: error in localhost.')
        try:
            if not use_localhost:
                url = "http://%s/set/etrvsetting/" % (use_host)
                response = requests.get(url, params=params, timeout=10)
                print('info: ui: send: master: use_host:', response.text)
        except:
            print('info: ui: send: master: error in use_host.')
    return s_data

# calculate interval for master and drop
def get_intervall(data):
    try:
        try:
            _, device_data = controllers.get_mem_master()
            interval = device_data.get('interval')
            if interval:
                return interval
        except:
            pass
        for device in data.get('data'):
            try:
                controller = list(RFAktor.objects.filter(name = str(device)).values())[0].get('controller_id')
                interval = RFController.objects.get(pk=long(controller)).get_parameters().get('wakeup_intervals').get('intervall')
            except:
                pass
            if interval:
                break
        if interval <= 60:
            interval = 30
        if interval > 60 and interval <= 800:
            interval = 750
        return interval
    except:
        return None

# get loop interval
def loop_interval(loop_timer, r_data):
    new_loop_timer = get_intervall(r_data)
    if new_loop_timer:
        loop_timer = new_loop_timer
    print('info: ui: interval:', loop_timer)
    return loop_timer

# set ziel value to ui
def set_ziel(target, raum):
    haus = Haus.objects.all()[0]
    hparams = haus.get_module_parameters()
    duration = hparams.get('quickui', {}).get('default_duration', 180)
    QuickuiMode(float(target), duration).set_mode_raum(raum)
    print('info: ui: set_ziel: write ziel to ui: %s' % target)

# set soll value to ui
def set_soll(target, raum):
    soll = float(target)
    ret = BaseMode.get_active_mode_raum(raum)
    if ret:
        ret['activated_mode'].set_mode_temperature_raum(raum, soll)
    else:
        raum.solltemp = soll
        raum.save()
    print('info: ui: set_soll: write soll to ui: %s' % soll)

# round values for eTRV
def rounder(num):
    dec = round(num - int(num),2)
    if dec >= 0.25 and dec < 0.75:
        return int(num) + 0.5
    elif dec >= 0.75:
        return int(num) + 1
    return int(num)

# return mode and secret of device
def get_secret_mode(device, device_data):
    try:
        room_id = device_room(device)
        if not room_id and device_data.get('room_id',None):
            solltemp = device_data.get('soll', 21)
            offset = device_data.get('offset', 0)
            mode = 'disable'
            room_id = device_data.get('room_id',None)
        else:
            raum = get_room(device)
            ret = BaseMode.get_active_mode_raum(raum)
            if ret:
                mode = 'enable'
                if ret.get('use_offsets', False):
                    offset = get_module_offsets(raum)
                else:
                    offset = 0
                solltemp = ret.get('soll')
            else:
                mode = 'disable'
                solltemp = raum.solltemp
                offset = get_module_offsets(raum)
    except:
        solltemp = device_data.get('soll', 21)
        offset = device_data.get('offset', 0)
        room_id = device_data.get('room_id',None)
        mode = 'enable'
    ziel, strsecret = read_one(device_data, float(solltemp), float(offset), mode, device)
    if ziel and strsecret and room_id:
        room_data = ch.get('etrv__%s' % device)
        if not room_data:
            room_data = dict()
        room_data['r'] = rounder(ziel)
        if not room_data.get('pr'):
            room_data['pr'] = rounder(ziel)
        ch.set('etrv__%s' % device, room_data)
    return strsecret, mode
