#!/bin/bash

# Kill any running hciattach processes
for i in {1..3}; do
  sudo killall hciattach
  sleep 1
done

# Stop the Bluetooth services
sudo systemctl stop bluetooth.service
sudo systemctl stop hciuart.service

# Reset the Bluetooth module
if grep -q "Zero" /proc/device-tree/model; then
  sudo raspi-gpio set 45 op dl dh
else
  sudo vcmailbox 0x38041 8 8 128 0
  sleep 1
  sudo vcmailbox 0x38041 8 8 128 1
fi

# Start the Bluetooth services
sudo systemctl start bluetooth.service
sudo systemctl start hciuart.service

# Restart the newbtle process
sudo supervisorctl -c /etc/supervisor/supervisord.conf restart newbtle
