from etrv_lib import etrv
from time import sleep


# connect to danfoss to get data
def get_danfoss(device, strsecret):
    for x in range(3):
        try:
            danfoss = etrv.Controme(str(device), encryption_key= strsecret)
            with danfoss:
                temp_data = danfoss.getTemperature()
                return temp_data, danfoss
        except:
            sleep(2)
            print('critical: connection: get: try to connect for %s' % x, str(device))

# connect to danfoss to set data
def set_danfoss(target, danfoss):
    for x in range(3):
        try:
            with danfoss:
                danfoss.setTemperature(target)
                return
        except:
            sleep(2)
            print('critical: connection: set: try to connect for %s' % x)

# connect to danfoss to set raw data
def set_danfoss_raw(target, device, strsecret):
    for x in range(3):
        try:
            danfoss = etrv.Controme(str(device), encryption_key= strsecret)
            with danfoss:
                danfoss.setTemperature(target)
                return
        except:
            sleep(2)
            print('critical: connection: set: try to connect for %s' % x, str(device))
