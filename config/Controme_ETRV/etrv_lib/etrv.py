import xxtea
import sys
import threading
from bluepy import btle
import time
import struct


class Controme(btle.DefaultDelegate):

    pin_uuid = "10020001274900010000-00805F9B042F"
    temp_uuid = "1002000527490001000000805F9B042F"
    name_uuid = "1002000627490001000000805F9B042F"
    battery_service_uuid = "0000180F-0000-1000-8000-00805F9B34FB"
    battery_level_uuid = "00002A19-0000-1000-8000-00805F9B34FB"
    battery_uuid = '2A19'
    MODE_MAP = {
        0: 'Manuell',
        1: 'Schedule',
        2: 'Vacation',
        4: 'Reserved'
    }

    def __init__(self, mac=None, encryption_key=None, pin=None, connect_time=10):
        self.mac = mac
        self.pin = pin
        self.key = encryption_key
        self.connect_time = connect_time
        self.pin_handle = None
        self.temperature_handle = None
        self.device_name_handle = None
        self.battery_handle = None
        self.device = None
        self.battery = 0
        self.name = ""
        self.current_temperature = 0
        self.target_temperature = 0
        self.mutex = threading.Lock()
        btle.DefaultDelegate.__init__(self)

    def __enter__(self):
        self.mutex.acquire()
        try:
            self.device = btle.Peripheral()
            self.device.withDelegate(self)
            start_time = time.time()
            connection_try_count = 0
            while True:
                connection_try_count += 1
                if connection_try_count > 1:
                    print("Retrying to connect to device with mac: " + self.mac + ", try number: " + str(connection_try_count))
                    time.sleep(0.5)
                try:
                    time.sleep(1)
                    self.device.connect(self.mac, addrType=btle.ADDR_TYPE_PUBLIC)
                    break
                except:
                    if time.time() - start_time >= self.connect_time:
                        print("Connection timeout")
                        return None
            handles = self.device.getCharacteristics()
            for handle in handles:
                if handle.uuid == self.pin_uuid:
                    self.pin_handle = handle
                elif handle.uuid == self.temp_uuid:
                    self.temperature_handle = handle
                elif handle.uuid == self.name_uuid:
                    self.device_name_handle = handle
                elif handle.uuid == self.battery_uuid:
                    self.battery_handle = handle
            if self.pin_handle == None or self.temperature_handle == None or self.device_name_handle == None or self.battery_handle == None:
                self.device = None
                raise Exception("Unable to find all handles")
            self.login()
        except:
            self.__exit__()
            raise
        return self

    def __exit__(self, exc_type=None, exc_val=None, exc_tb=None):
        try:
            if self.device:
                self.device.disconnect()
        except Exception as ex:
            print("Error disconnecting: " + str(ex))
        finally:
            self.device = None
            self.pin_handle = None
            self.temperature_handle = None
            self.device_name_handle = None
            self.battery_handle = None
            self.mutex.release()

    def login(self):
        return self.send_BLE_packet(self.pin_handle, b'\x00\x00\x00\x00')

    def getTemperature(self):
        data = self.read_encrypted_handle(self.temperature_handle)
        if data[1] != 0:
            self.current_temperature = data[1] / 2
        self.target_temperature = float(data[0]) / 2

        try:
            # Retrieve settings data
            settings_handle = self.device.getCharacteristics(uuid="1002000327490001000000805F9B042F")[0]
            settings_data = self.read_encrypted_handle(settings_handle)

            # Extract individual settings values
            #child_lock = (settings_data[0] >> 7) & 0b1
            mode = self.MODE_MAP.get(settings_data[4] & 0b111, 'Unknown')
            # calibrated = (settings_data[0] >> 5) & 0b1
            # reaction_time = (settings_data[0] >> 4) & 0b1
            # display_rotation = (settings_data[0] >> 3) & 0b1
            # thermostat_orientation = (settings_data[0] >> 2) & 0b1
            # daylight_saving = (settings_data[0] >> 1) & 0b1
            # forecast = settings_data[0] & 0b1
            # minimum = settings_data[1]
            # maximum = settings_data[2]
            # frost = settings_data[3]
            mounted = (settings_data[0] >> 6) & 0b1
            # vacation = settings_data[5]
            # vacation_start = struct.unpack('<I', settings_data[6:10])[0]
            # vacation_end = struct.unpack('<I', settings_data[10:14])[0]

            if mounted == 1:
                mounted = 'Ja'
            else:
                mounted = 'Nein'

            battery_service = self.device.getServiceByUUID('0000180F-0000-1000-8000-00805F9B34FB')
            battery_level_char = battery_service.getCharacteristics('00002A19-0000-1000-8000-00805F9B34FB')[0]
            battery_level_data = battery_level_char.read()
            battery_percentage = struct.unpack('<B', battery_level_data)[0]

            return (
                self.target_temperature,
                self.current_temperature,
                mode,
                mounted,
                battery_percentage,
            )

        except:
            return (self.target_temperature, self.current_temperature)

    def getDeviceName(self):
        data = self.read_encrypted_handle(self.device_name_handle)
        for i, char in enumerate(data):
            if char == 0:
                data = data[:i]
                break
        self.device_name = data.decode("utf-8", 'replace')
        return self.device_name

    def setTemperature(self, temp):
        temp = int(temp*2)
        data = bytearray([temp]) + bytearray(7)
        return self.send_encrypted_packet(self.temperature_handle, data)

    def update(self):
        if self.getTemperature() == False:
            return False
        else:
            return True

    def send_encrypted_packet(self, handle, unencrypted_data):
        return self.send_BLE_packet(handle, self.encrypt(unencrypted_data))

    def read_encrypted_handle(self, handle):
        return self.decrypt(handle.read())

    def send_BLE_packet(self, handle, data):
        return handle.write(bytes(data), withResponse=True)

    def encrypt(self, data):
        return self.reverse_byte_order(xxtea.encrypt(bytes(self.reverse_byte_order(data)), self.key, padding=False))

    def decrypt(self, encrypted_data):
        data = self.reverse_byte_order(xxtea.decrypt(bytes(self.reverse_byte_order(encrypted_data)), self.key, padding=False))
        return data

    def reverse_byte_order(self, data):
        ba_data = bytearray(data)
        l = len(ba_data)
        padding = bytearray(0)
        if l%4 != 0:
            padding = bytearray(4 - (l%4))
        ba_data = ba_data + padding
        l = len(ba_data)
        for i in range(l >> 2):
            s = ba_data[i*4:(i+1)*4]
            s.reverse()
            ba_data[i*4:(i+1)*4] = s
        return ba_data
