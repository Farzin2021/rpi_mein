# ETRV_LIB V1
import sys, os, django
# Add django settings to run this script seperately!
try:
    sys.path.append('/home/pi/rpi/')
    os.environ.setdefault("DJANGO_SETTINGS_MODULE", "rpi.settings")
    django.setup()
except:
    print('# Error in initialize django settings!')
    sys.exit()
from utils import ui, sync, connection
from datetime import datetime, timedelta
from heizmanager import cache_helper as ch
from utils import ui, sync, controllers, connection
from utils import system_check


r_data = {}
loop_timer = 60
ch.set('loop_calc', datetime.now())
loop_calc = datetime.now()


system_check.uptime()
while True:
    data_set = {}
    system_check.health_check()
    controllers.get_controllers()
    sync.sync_drop_rooms_master()
    r_data = ui.receive_get_data()
    loop_timer = ui.loop_interval(loop_timer, r_data)

    loop_calc = ch.get('loop_calc')
    if not loop_calc:
        ch.set('loop_calc', datetime.now())
        loop_calc = datetime.now()

    if r_data and 'data' in r_data and datetime.now() - timedelta(seconds=loop_timer) > loop_calc:

        device_list = []
        for device in r_data.get('data', {}):
            if device in device_list:
                continue
            else:
                device_list.append(device)

        for device in device_list:
            device_data = r_data.get('data', {}).get(str(device),{})
            try:
                if device_data.get('typ','unknown') == u'eTRV' and device_data.get('secret',None):
                    raum = ui.get_room(device)
                    print('-'*70)
                    print('info: main: data:', device, device_data)

                    # check if this device is dropserver
                    if not raum and device_data.get('room_id',None):
                        ch.set('loop_calc', datetime.now())
                        try:
                            target, temp_data, strsecret, danfoss, data_set = sync.drop_server(device, data_set)
                            print('info: main: drop: device with room.')
                            s_data = ui.update_rf_ui(device, temp_data, device_data.get('secret',None), target)
                            ui.send_data_rf({'btle': s_data})
                            if target != (temp_data[0]):
                                sync.hardware_setpoint(target, strsecret, danfoss, device)
                        except:
                            strsecret = ui.return_strsecret(device_data)
                            temp_data, danfoss = connection.get_danfoss(device, strsecret)
                            print('info: main: drop: device without room.')
                            if temp_data:
                                s_data = ui.update_rf_ui_no_room(device, temp_data, device_data.get('secret',None))
                            ui.send_data_rf({'btle': s_data})
                    
                    # check if this device is master
                    else:
                        s_z_switch_init = ui.soll_ziel_checkbox(device)
                        ch.set('loop_calc', datetime.now())
                        try:
                            if not device_data.get('room_id',None):
                                raise TypeError
                            target, temp_data, strsecret, danfoss, s_z_switch, mode = sync.master_server(device, device_data)
                            print('info: main: master: device with room.')
                            if s_z_switch_init == s_z_switch:
                                sync.master_server_ui(target, s_z_switch, mode, raum, device)
                                s_data = ui.update_rf_ui(device, temp_data, device_data.get('secret',None), target)
                                ui.send_data_rf({'btle': s_data})
                                if target != (temp_data[0]):
                                    sync.hardware_setpoint(target, strsecret, danfoss, device)
                        except:
                            strsecret = ui.return_strsecret(device_data)
                            temp_data, danfoss = connection.get_danfoss(device, strsecret)
                            print('info: main: master: device without room.')
                            if temp_data:
                                s_data = ui.update_rf_ui_no_room(device, temp_data, device_data.get('secret',None))
                            ui.send_data_rf({'btle': s_data})

            except:
                print('critical: main: error in loop.')
        r_data = {}
        device_list = []