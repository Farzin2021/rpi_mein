from bluepy.btle import Scanner, DefaultDelegate
from helper import _local_handler


class ScanDelegate(DefaultDelegate):
    def __init__(self):
        DefaultDelegate.__init__(self)

    def handleDiscovery(self, dev, isNewDev, isNewData):
        if isNewDev:
            discovery = "Discovered device: %s"  % dev.addr
            _local_handler('echo "%s" >> /var/log/uwsgi/scan_log' % discovery)
        elif isNewData:
            rec = "Received new data from: %s" % dev.addr
            _local_handler('echo "%s" >> /var/log/uwsgi/scan_log' % rec)

_local_handler('sudo supervisorctl -c /etc/supervisor/supervisord.conf stop btlehandler')
_local_handler('sudo supervisorctl -c /etc/supervisor/supervisord.conf stop newbtle')
_local_handler('echo $(date) > /var/log/uwsgi/scan_log')
_local_handler('echo "================================================" >> /var/log/uwsgi/scan_log')

scanner = Scanner().withDelegate(ScanDelegate())
devices = scanner.scan(10.0)
_local_handler('echo "------------------------------------------------" >> /var/log/uwsgi/scan_log')
for dev in devices:
    str1 = "Device %s (%s), RSSI=%d dB" % (dev.addr, dev.addrType, dev.rssi)
    _local_handler('echo "%s" >> /var/log/uwsgi/scan_log' % str1)
    for (adtype, desc, value) in dev.getScanData():
        str2 = "  %s = %s" % (desc, value)
        _local_handler('echo "%s" >> /var/log/uwsgi/scan_log' % str2)
    _local_handler('echo "------------------------------------------------" >> /var/log/uwsgi/scan_log')
_local_handler('echo "================================================" >> /var/log/uwsgi/scan_log')
_local_handler('echo "finish" >> /var/log/uwsgi/scan_log')
_local_handler('sudo supervisorctl -c /etc/supervisor/supervisord.conf restart btlehandler')
_local_handler('sudo supervisorctl -c /etc/supervisor/supervisord.conf restart newbtle')