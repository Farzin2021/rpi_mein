from celery import shared_task
from fabric.api import local, settings


@shared_task
def btle_handler():
    try:
        if local('[ -f /etc/supervisor/conf.d/btle.conf ] && echo yes || echo no', capture=True) == 'yes':
            with settings(warn_only=True):
                local('echo $(date) > /home/pi/.cet')
                r = local("ps -C bluepy-helper -o %cpu", capture=True)
                r = float(r.split('\n')[1])
                if r > 90:
                    try:
                        local("sudo killall bluepy-helper")
                    except:
                        pass
                    try:
                        local("sudo hciconfig hci0 down")
                        local("sudo hciconfig hci0 up")
                        local("sudo systemctl restart bluetooth.service")
                        local('sudo supervisorctl -c /etc/supervisor/supervisord.conf restart newbtle')
                    except:
                        pass
    except:
        pass
