import zmq
try:
    from zmq.core.error import ZMQError
except ImportError:
    from zmq import ZMQError
import requests
from rpi.aeshelper import aes_encrypt, aes_decrypt
from fabric.api import local
import sys
from datetime import datetime, timedelta
import traceback
import json
from time import sleep


class MessageServer():

    def __init__(self):

        self.server = "http://controme-main.appspot.com"

        self.context = zmq.Context()

        self.handler_insocket = self.context.socket(zmq.ROUTER)
        self.handler_insocket.bind("tcp://127.0.0.1:%s" % 5557)

        self.handler_outsocket = self.context.socket(zmq.PUB)
        self.handler_outsocket.bind("tcp://127.0.0.1:%s" % 5558)

        self.poller = zmq.Poller()
        self.poller.register(self.handler_insocket, zmq.POLLIN)

        self.network = "None"

        try:
            from rpi.server import use_localhost
            self.use_localhost = use_localhost
        except ImportError:
            self.use_localhost = False
        try:
            from rpi.server import use_host
            self.use_host = use_host
        except ImportError:
            self.use_host = 'localhost' if self.use_localhost else 'mycontromecom'

    def run(self):
        print "msgsrv ready"

        try:
            
            with open('/sys/class/net/eth0/address') as f:
                macaddr = f.read().strip().replace(':', '-')

            zwave_keepalive_ts = datetime.now()
            
            while True:

                try:
                    from rpi.server import use_localhost
                except ImportError:
                    use_localhost = False
                try:
                    from rpi.server import use_host
                except ImportError:
                    use_host = 'localhost' if use_localhost else 'mycontromecom'

                if use_localhost != self.use_localhost or use_host != self.use_host:
                    raise Exception("restarting because use_localhost changed")

                socks = dict(self.poller.poll(10))

                if self.handler_insocket in socks and socks[self.handler_insocket] == zmq.POLLIN:
                    try:
                        msg_mp = self.handler_insocket.recv_multipart()
                        msg = json.loads(msg_mp[3])
                    except ZMQError, e:
                        if e.errno == zmq.EAGAIN:  # no message ready
                            pass
                        else:
                            raise e
                    else:
                        protocol = msg_mp[1]

                        if protocol.startswith('server'):
                            recipient = protocol[6:]
                            msg = "%s %s" % (recipient, json.dumps(msg))
                            self.handler_outsocket.send(msg)

                        else:
                            try:
                                network_id = msg.keys()[0]
                            except IndexError:
                                if protocol == 'zwave':
                                    zwave_keepalive_ts = datetime.now()
                                continue
                            self.network = network_id
                            if protocol == 'btle':
                                network_id = macaddr
                                self.network = macaddr

                            try:
                                d = aes_encrypt(json.dumps(msg.values()[0]))
                                r = requests.post("http://localhost/set/%s/%s/%s/" % (protocol, macaddr, network_id),
                                                  data={'data': d})

                                if protocol == "zwave":
                                    if self.use_host == 'mycontromecom':
                                        r = requests.post("%s/set/%s/%s/%s/" % (self.server, protocol, macaddr, network_id), data="data=%s" % d, timeout=10)
                                    elif self.use_host == 'localhost':
                                        # damit update nicht neustartet
                                        # r = requests.post("%s/set/%s/%s/%s/" % (self.server, protocol, macaddr, macaddr.replace('-', '_')), data=str({'1': {}}))
                                        pass
                                    else:
                                        single_zwave = False
                                        try:
                                            single_zwave = local('[ -f /var/log/uwsgi/.zwave_single ] && echo yes || echo no', capture=True)
                                            if single_zwave == 'yes':
                                                single_zwave = True
                                        except:
                                            pass
                                        if len(msg.values()[0]) > 1 and single_zwave:
                                            for key in msg.values()[0]:
                                                sleep(0.2)
                                                try:
                                                    sp_data = {key: msg.values()[0].get(key)}
                                                    r = requests.post("http://%s/set/%s/%s/%s/" % (self.use_host, protocol, macaddr, network_id), data={'data': json.dumps(sp_data)}, timeout=10)
                                                except:
                                                    pass
                                        else:
                                            r = requests.post("http://%s/set/%s/%s/%s/" % (self.use_host, protocol, macaddr, network_id), data={'data': json.dumps(msg.values()[0])}, timeout=10)
                                elif protocol in {'btle', 'enocean', 'knx'}:
                                    if self.use_host not in {'mycontromecom', 'localhost'}:
                                        r = requests.post("http://%s/set/%s/%s/%s/" % (self.use_host, protocol, macaddr, network_id), data={'data': json.dumps(msg.values()[0])}, timeout=10)

                            except (requests.ConnectionError, requests.Timeout):
                                print "connectionerror"

                            try:
                                if protocol == 'enocean' and len(msg.values()) == 1 and len(msg.values()[0].values()) and 'ack' in msg.values()[0].values()[0]:
                                    pass
                                else:
                                    r = None
                                    if self.use_localhost:
                                        r = requests.get("http://localhost/get/%s/%s/" % (protocol, network_id), timeout=20)
                                    elif protocol == "zwave":
                                        if self.use_host == 'mycontromecom':
                                            r = requests.get("%s/get/%s/%s/" % (self.server, protocol, network_id), timeout=20)
                                        elif self.use_host == 'localhost':
                                            pass
                                        else:
                                            r = requests.get('http://%s/get/%s/%s/' % (self.use_host, protocol, network_id), timeout=20)
                                    elif protocol in {'btle', 'enocean', 'knx'}:
                                        if self.use_host not in {'mycontromecom', 'localhost'}:
                                            r = requests.get('http://%s/get/%s/%s/' % (self.use_host, protocol, network_id), timeout=20)

                                    if r and r.status_code == 200:
                                        if not self.use_localhost and self.use_host not in ['mycontromecom', 'localhost']:
                                            msg = "%s %s" % (protocol, r.content)
                                            self.handler_outsocket.send(msg)
                                        else:
                                            if len(r.content):
                                                msg = "%s %s" % (protocol, aes_decrypt(r.content).rsplit('}', 1)[0] + '}')
                                                self.handler_outsocket.send(msg)
                            except (requests.ConnectionError, requests.Timeout):
                                print "connectionerror"

                            if protocol == 'zwave':
                                zwave_keepalive_ts = datetime.now()

                if (datetime.now() - zwave_keepalive_ts) > timedelta(seconds=1320):
                    # noch ein fallback, falls die zmq sockets irgendwie sterben
                    # seconds=1320, weil wir damit ueber zweimal /get/update/ drueberkommen
                    local("sudo supervisorctl -c /etc/supervisor/supervisord.conf restart all")

        except (KeyboardInterrupt, Exception) as e:
            exc_type, exc_obj, exc_tb = sys.exc_info()
            err = "%s - Exception %s / %s in msgsrv.run: %s" % (str(datetime.now()), exc_type, exc_obj, exc_tb.tb_lineno)
            print err

            print traceback.format_exc()
            self.handler_insocket.close(linger=0)
            self.handler_outsocket.close(linger=0)
            self.context.term()

            try:
                requests.post("%s/get/zwave/%s/" % (self.server, self.network), data=err, timeout=5)
            except Exception:
                pass


def main():
    ms = MessageServer()
    ms.run()

if __name__ == "__main__":
    main()
