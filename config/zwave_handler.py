from louie import dispatcher
import zmq
try:
    from zmq.core.error import ZMQError
except ImportError:
    from zmq import ZMQError
import time
import sys
import json
import copy
from datetime import datetime
import requests
from fabric.api import local

import openzwave
from openzwave.node import ZWaveNode
from openzwave.value import ZWaveValue
from openzwave.scene import ZWaveScene
from openzwave.controller import ZWaveController
from openzwave.network import ZWaveNetwork
from openzwave.option import ZWaveOption
import pyudev


class ZWaveHandler():

    def __init__(self):

        self.error_server = "http://controme-main.appspot.com"

        self.udevcontext = pyudev.Context()

        self.zmqcontext = zmq.Context()

        self.outsocket = self.zmqcontext.socket(zmq.DEALER)
        self.outsocket.connect("tcp://127.0.0.1:%s" % 5557)

        timer = None
        self.device = None
        while not self.device:
            try:
                devs = local("ls /dev", capture=True)
                devs = devs.split('\n')
                for dev in ['ttyUSB0', 'ttyUSB1', 'ttyACM0', 'ttyACM1']:
                    if dev in devs:
                        self.device = pyudev.Device.from_device_file(self.udevcontext, "/dev/%s" % dev)
                        # if 'Sigma Designs, Inc.' in self.device.values() and self.device['ID_MODEL_ID'] == '0200':
                        if 'EnOcean_GmbH' not in self.device.values() and self.device['ID_MODEL_ID'] != '6001':
                            print "using device: %s" % self.device
                            break
                else:
                    self.device = None
                    raise OSError
            except OSError:
                if timer is None or (datetime.now() - timer).seconds > 600:
                    timer = datetime.now()
                    with open('/sys/class/net/eth0/address') as f:
                        macaddr = f.read().strip().replace(':', '-')
                    r = requests.post("%s/set/zwave/%s/%s/" % (self.error_server, macaddr, macaddr.replace('-', '_')), data=str({'1': {}}), timeout=5)
                    self.outsocket.send_multipart(['zwave', '', json.dumps({})])
                time.sleep(60)

        self.monitor = pyudev.Monitor.from_netlink(self.udevcontext)
        self.monitor.filter_by("tty")
        self.observer = pyudev.MonitorObserver(self.monitor, self.usb_event_handler)
        self.observer.start()

        self.insocket = self.zmqcontext.socket(zmq.SUB)
        self.insocket.connect("tcp://127.0.0.1:%s" % 5558)
        self.insocket.setsockopt(zmq.SUBSCRIBE, 'zwave')

        self.poller = zmq.Poller()
        self.poller.register(self.insocket, zmq.POLLIN)

        self.network_data_cnt = dict()
        self.network_data_prev = dict()
        self.network_data_update = dict()
        self.server_data = dict()

        self.cc_defaults = {0x84: {0x08: 60}, 0x43: 21.0}
        self.timer_intervals = self.cc_defaults[0x84]

        self.network = None
        self._init_network()

    def _init_network(self):

        options = ZWaveOption(str(self.device.device_node),
                              config_path="openzwave_config",
                              user_path="/home/pi/rpi",
                              cmd_line="")
        options.addOptionBool("AssumeAwake", True)
        options.set_logging(False)
        options.set_suppress_value_refresh(False)
        options.set_console_output(False)
        options.lock()
        self.network = ZWaveNetwork(options, log=None)

        print "starting zwave network "
        for i in range(300):
            if self.network.state >= self.network.STATE_STARTED:
                break

            sys.stdout.write(".")
            sys.stdout.flush()
            time.sleep(1)
        else:
            raise Exception("unable to start network")

        for node in self.network.nodes:
            self.network_data_update.setdefault(node, dict())
            self.network_data_cnt.setdefault(node, dict())
            self.network_data_prev.setdefault(node, dict())

        self.zws_resethard(self.network)

    def run(self):

        try:

            initdata = {self.network.home_id_str: self.network_data_update}
            print "%s - initialising (default) values - with %s" % (datetime.now(), str(initdata))
            self.outsocket.send_multipart(['zwave', '', json.dumps(initdata)])
     
            # 10s auf eine Antwort warten
            got_response = False
            for i in range(30):
                socks = dict(self.poller.poll(100))
                if self.insocket in socks and socks[self.insocket] == zmq.POLLIN:
                    try:
                        message = self.insocket.recv()
                        message = json.loads(message.split('zwave ')[1])
                    except ZMQError, e:
                        if e.errno == zmq.EAGAIN:  # no message ready
                            pass 
                        else:
                            raise e
                    else:
                        print "%s - received (default) values: %s" % (datetime.now(), str(message))
                        got_response = True
                        self._update_settings(message)
                        self._update_values(message)
                        try:
                            requests.post("http://localhost/get/zwave/%s/" % self.network.home_id_str, data={"error": "restarted zwave_handler"})
                        except Exception:
                            pass
                        break
                else:
                    time.sleep(1)

            if not got_response:
                # raise Exception('no config data received')
                pass
            
            timer = time.time()

            print "%s - starting loop" % datetime.now()
            try:
                requests.post("http://localhost/get/zwave/%s/" % self.network.home_id_str, data={"error": "entering loop"})
            except Exception:
                pass
            while True:
                try:

                    socks = dict(self.poller.poll(10))

                    if self.insocket in socks and socks[self.insocket] == zmq.POLLIN:
                        try:
                            message = self.insocket.recv()
                            message = json.loads(message.split('zwave ')[1])
                        except ZMQError, e:
                            if e.errno == zmq.EAGAIN:  # no message ready
                                pass
                            else:
                                raise e
                        else:

                            print "%s - received message: %s" % (datetime.now(), str(message))

                            self._update_settings(message)
                            self._update_values(message)

                    cnttime = int(time.time())
                    if timer + min(min(self.timer_intervals.values()), 10*60) < cnttime:

                        for node_id in self.network_data_update.keys():
                            try:
                                self.network_data_update[node_id]['manufacturer_id'] \
                                    = self.network.nodes[int(node_id)].manufacturer_id[-4:]
                                self.network_data_update[node_id]['product_id'] \
                                    = self.network.nodes[int(node_id)].product_id[-4:]
                                self.network_data_update[node_id]['product_type'] \
                                    = self.network.nodes[int(node_id)].product_type[-4:]
                                self.network_data_update[node_id]['neighbors'] \
                                    = list(self.network.nodes[int(node_id)].neighbors)
                                if int(node_id) == 1:
                                    self.network_data_update[node_id]['networkstate'] \
                                        = self.network.state
                            except KeyError:
                                self.network_data_update[node_id] = dict()
                                try:
                                    requests.post("http://localhost/get/zwave/%s/" % self.network.home_id_str, data={"error": "KeyError in gathering data for transmission"})
                                except Exception:
                                    pass

                        self._scrub_update_data()
                        print "%s - sending: %s" % (datetime.now(), str(self.network_data_update))

                        self.outsocket.send_multipart(['zwave', '', json.dumps({self.network.home_id_str: self.network_data_update})])

                        self.network_data_prev = copy.deepcopy(self.network_data_cnt)
                        self.network_data_cnt = copy.deepcopy(self.network_data_update)
                        for node in self.network.nodes:
                            self.network_data_update[node] = dict()

                        timer = cnttime

                except KeyError:  # from callbacks
                    try:
                        requests.post("http://localhost/get/zwave/%s/" % self.network.home_id_str, data={"error": "KeyError (from callbacks)"})
                    except Exception:
                        pass
                except ZMQError as e:
                    exc_type, exc_obj, exc_tb = sys.exc_info()
                    err = "zmqerror: %s / %s at %s, reconfiguring sockets..." % (exc_type, exc_obj, exc_tb.tb_lineno)
                    try:
                        requests.post("%s/get/zwave/%s/" % (self.error_server, self.network.home_id_str), data=err, timeout=5)
                    except Exception:
                        try:
                            requests.post("http://localhost/get/zwave/%s/" % self.network.home_id_str, data={"error": "Error transmitting ZMQError to remote"}, timeout=5)
                        except Exception:
                            pass

                    self.insocket.setsockopt(zmq.LINGER, 0)
                    self.insocket.close()
                    self.poller.unregister(self.insocket)
                    self.insocket = self.zmqcontext.socket(zmq.DEALER)
                    self.insocket.bind("tcp://127.0.0.1:%s" % 5558)
                    self.poller.register(self.insocket, zmq.POLLIN)
                    
                    self.outsocket.setsockopt(zmq.LINGER, 0)
                    self.outsocket.close()
                    self.outsocket = self.zmqcontext.socket(zmq.DEALER)
                    self.outsocket.connect("tcp://127.0.0.1:%s" % 5557)
        except (KeyboardInterrupt, Exception) as e:
            try:
                requests.post("http://localhost/get/zwave/%s/" % self.network.home_id_str, data={"error": "%s" % str(e)})
            except Exception as e:
                requests.post("http://localhost/get/zwave/%s/" % self.network.home_id_str, data={"error": "error sending exception %s" % str(e)})
            try:
                exc_type, exc_obj, exc_tb = sys.exc_info()
                err = "exception %s / %s in run:%s" % (exc_type, exc_obj, exc_tb.tb_lineno)
                print err
            except Exception as e:
                err = "error %s extracting exception information" % str(e)
            try:
                requests.post("%s/get/zwave/%s/" % (self.error_server, self.network.home_id_str), data=err, timeout=5)
            except Exception:
                try:
                    requests.post("http://localhost/get/zwave/%s/" % self.network.home_id_str, data={"error": "Error transmitting general error to remote"}, timeout=5)
                except Exception:
                    pass
            self.network.stop()
            if not self.zmqcontext.closed:
                self.zmqcontext.destroy()

    def _update_settings(self, message):
        timer_intervals = dict()
        if 'settings' in message:
            if 'wakeup_intervals' in message['settings']:
                _ti = message['settings']['wakeup_intervals']
                for k, v in _ti.items():  # assuming a max. depth of 1
                    if type(v) == dict and int(k) == 132:
                        for nk, nv in v.items():
                            for node in self.network.nodes:
                                if self.network.nodes[node].generic == int(nk):
                                    if self.network.nodes[node].product_type[-4:] == "0002" and self.network.nodes[node].product_id[-4:] == "0064" and self.network.nodes[node].manufacturer_id[-4:] == "0086" and int(nv) == 300:
                                        nv = 240
                                    for value_id in self.network.nodes[node].get_values(class_id=int(k), genre='All'):
                                        self.network.nodes[node].values[value_id].data = int(nv)
                            
                            timer_intervals[int(nk)] = int(nv)
                    else:
                        for node in self.network.nodes:
                            for value_id in self.network.nodes[node].get_values(class_id=int(k), genre='All'):
                                self.network.nodes[node].values[value_id].data = v

            if 'reset' in message['settings']:
                if message['settings']['reset'] == 'hard':
                    dispatcher.connect(self.zws_resethard, ZWaveNetwork.SIGNAL_NETWORK_RESETTED)
                    dispatcher.disconnect(self.zws_valuesignal)
                    self.network.controller.hard_reset()

                elif message['settings']['reset'] == 'soft':
                    self.network.controller.soft_reset()

            if 'remove' in message['settings']:
                for node in message['settings']['node']:
                    self.network.controller.begin_command_remove_failed_node(int(node))

            del message['settings']

        if len(timer_intervals):
            self.timer_intervals = copy.deepcopy(timer_intervals)

    def _update_values(self, message):

        if not len(self.server_data):  # init

            for node_id, values in message.items():
                self.server_data[node_id] = copy.deepcopy(values)  # node_id ist hier ein string!

            for node in self.network.nodes:

                # CC WAKEUP
                for value_id in self.network.nodes[node].get_values(class_id=0x84, genre='All'):
                    if self.network.nodes[node].generic in self.timer_intervals:
                        self.network.nodes[node].values[value_id].data = self.timer_intervals[self.network.nodes[node].generic]

                # CC THERMOSTAT SETPOINT
                if self.server_data.get(node, dict()).get('67'):
                    for value_id in self.network.nodes[node].get_values(class_id=0x43, genre='User'):
                        if isinstance(self.server_data[node]['67'][1], str):
                            self.network.nodes[node].values[value_id].data = \
                                round(float(self.server_data[node]['67'][1]), 1)
                        elif self.server_data[node]['67'][1] > 100:
                            self.network.nodes[node].values[value_id].data = \
                                self.server_data[node]['67'][1] / 100
                        else:
                            self.network.nodes[node].values[value_id].data = \
                                float("{0:.1f}".format(self.server_data[node]['67'][1]))
                            # round(self.server_data[node]['67'][1], 1)
                            # float("{0:.2f}".format(x))

            return

        # values: {CC: (label, data)}
        for node_id, values in message.items():
            self.server_data[node_id] = copy.deepcopy(values)

            for cc, data in values.items():

                try:
                    for value_id in self.network.nodes[int(node_id)].get_values(class_id=int(cc), genre='User'):
                        if isinstance(data[0], list) and self.network.nodes[int(node_id)].manufacturer_id[-4:] == "013c" and self.network.nodes[int(node_id)].product_type[-4:] == "0001" and self.network.nodes[int(node_id)].product_id[-4:] == "0003":  # 2chrel    ais
                            for d in data:
                                self.network.nodes[int(node_id)].set_config_param(3, int(d[0]))
                                self.network.nodes[int(node_id)].values[value_id].data = d[1]
                        elif data[0]:
                            if data[0] == self.network.nodes[int(node_id)].values[value_id].label:
                                if data[0] == 'Heating 1' and self.network.nodes[int(node_id)].product_id[-4:] == "8010":
                                    if isinstance(data[1], str):
                                        self.network.nodes[int(node_id)].values[value_id].data = round(float(data[1]), 1)
                                    elif data[1] > 100:
                                        self.network.nodes[int(node_id)].values[value_id].data = float(data[1]) / 100
                                    else:
                                        self.network.nodes[int(node_id)].values[value_id].data = data[1]
                                else:
                                    self.network.nodes[int(node_id)].values[value_id].data = data[1]
                        else:
                            self.network.nodes[int(node_id)].values[value_id].data = data[1]
                except KeyError:
                    try:
                        requests.post("http://localhost/get/zwave/%s/" % self.network.home_id_str, data={"error": "KeyError in _update_values"})
                    except Exception:
                        pass

    def usb_event_handler(self, action, device):
        if device.device_node == self.device.device_node and action == "remove":
            self.zmqcontext.destroy()  # will send run() into its exception handling

    def zws_awakenodesqueried(self, network):
        pass

    def zws_allnodesqueried(self, network):
        pass

    def zws_resethard(self, network):
        #dispatcher.connect(self.zws_nodesignal, ZWaveNetwork.SIGNAL_NODE)
        #dispatcher.connect(self.gather_values, ZWaveNetwork.SIGNAL_NETWORK_READY)
        #dispatcher.connect(self.zws_nodeadded, ZWaveNetwork.SIGNAL_NODE_ADDED)
        #dispatcher.connect(self.zws_nodenew, ZWaveNetwork.SIGNAL_NODE_NEW)
        dispatcher.connect(self.zws_valuesignal, ZWaveNetwork.SIGNAL_VALUE)

    def _send_data(self, node_id, valuelabel):
        # valuelabel muss der dict key sein, der auf dem server ankommt
        #   (also anstatt 'Heating 1' beim WT muss es 'value' sein)
        self._scrub_update_data(node_id, valuelabel)
        self.outsocket.send_multipart(['zwave', '', json.dumps({self.network.home_id_str: {node_id: self.network_data_update[node_id]}})])

    def _scrub_update_data(self, node_id=0, valuelabel=0):

        # wenn sich ein wert zweimal in einer periode aendert (wiederholtes druecken am thermostat),

        def _scrub_dict(_vals, _tainted):
            for k, v in _vals.items():
                if isinstance(v, dict):
                    _vals[k] = _scrub_dict(v, _tainted)
                else:
                    if k in _tainted:
                        del _vals[k]
            return _vals

        for n_id, vals in self.network_data_update.items():

            if node_id and node_id != n_id:
                continue

            tainted = vals.get('tainted', list())
            if not len(tainted):
                continue
            del vals['tainted']

            if valuelabel in tainted:
                tainted.remove(valuelabel)

            self.network_data_update[n_id] = _scrub_dict(vals, tainted)

            self.network_data_update[n_id].setdefault('tainted', list())
            self.network_data_update[n_id]['tainted'].append(valuelabel)

    def zws_valuesignal(self, network, node, value):
        try:
            # if network.state < 7:
            #     return

            print "%s - %s - hello from node %s (cc %s) with value: %s" \
                  % (datetime.now(), network.state, node.node_id, value.command_class, value)

            if node.node_id not in self.network_data_update:
                self.network_data_update[node.node_id] = dict()
                self.server_data.setdefault(str(node.node_id), dict())

            if value.command_class == 132 and node.product_type[-4:] == "0001" and node.product_id[-4:] == "0001" and \
                    node.product_name == "EUR_STELLAZ Wall Radiator Thermostat Valve Control":  # stella z

                self.network_data_update[node.node_id].setdefault('values', dict())

                for value_id in node.get_values(genre='All', class_id=0x31):  # Multilevel Sensor
                    self.network_data_update[node.node_id]['values'][node.values[value_id].label] \
                        = node.values[value_id].data

                for value_id in node.get_values(genre='All', class_id=0x80):  # Battery
                    self.network_data_update[node.node_id]['battery'] = node.values[value_id].data

            if value.command_class == 0x25:  # binary switch
                self.network_data_update[node.node_id]['Switch'] = value.data
            
            elif value.command_class == 0x30:  # sensor binary
                self.network_data_update[node.node_id].setdefault('values', dict())
                if value.data:
                    self.network_data_update[node.node_id]['values']['Alarm'] = value.data

            elif value.command_class == 0x31:  # sensor multilevel
                if node.product_name == "Multi Sensor":
                    ret = value.enable_poll()
                if value.data != self.network_data_update[node.node_id].get('values', dict()).get(value.label, None) and -50 < value.data < 125:
                    self.network_data_update[node.node_id].setdefault('values', dict())
                    self.network_data_update[node.node_id]['values'][value.label] = value.data
            
            elif value.command_class == 0x43:  # cc thermostat setpoint
                if node.product_name == "EUR_STELLAZ Wall Radiator Thermostat Valve Control":
                    self.network_data_update[node.node_id].setdefault('values', dict())
                    self.network_data_update[node.node_id]['values'][value.label] = value.data
                elif node.manufacturer_id[-4:] == '0059' and node.product_type[-4:] == '0001' and node.product_id[-4:] == '0003':  # srt321 bzw hrt4-zw
                    self.network_data_update[node.node_id].setdefault('values', dict())
                    self.network_data_update[node.node_id]['values'][value.label] = value.data
                else:  # dlc und rs
                    if value.label == 'Heating 1':
                        val = round(value.data, 1)
                        if node.product_id[-4:] == "8010":  # zwavers
                            send_now = False
                            if 'value' in self.network_data_cnt.get(node.node_id, dict()) and (val != self.network_data_cnt[node.node_id]['value'] and val != self.server_data.get(str(node.node_id), dict()).get('67', ['', '0.0'])[1]):
                                self.network_data_update[node.node_id]['value'] = val
                                send_now = True
                            elif 'value' not in self.network_data_cnt.get(node.node_id, dict()) and (not len(self.network_data_cnt.get(node.node_id, dict())) or val != self.server_data.get(str(node.node_id), dict()).get('67', ['', '0.0'])[1]):
                                self.network_data_update.setdefault(node.node_id, dict())
                                self.network_data_update[node.node_id]['value'] = val
                                send_now = True
                            elif 'value' not in self.network_data_cnt.get(node.node_id, dict()) and 'value' in self.network_data_update.get(node.node_id, dict()):
                                # wir sind grade neu gestartet, haben aber schon einen
                                # wert bekommen (wahrscheinlich direkt vom stick beim initialisieren)

                                self.network_data_update.setdefault(node.node_id, dict())
                                if val != self.server_data.get(str(node.node_id), dict()).get('67', ['', '0.0'])[1]:
                                    # noch ein wert kommt und unterscheidet sich vom
                                    # server wert oder wir haben noch keinen server wert
                                    self.network_data_update[node.node_id]['value'] = val
                                    send_now = True
                                else:
                                    # das wt hat von sich aus den korrekten wert nachgeschickt,
                                    # nachdem der stick einen falschen geschickt hat. oder benutzeraenderung.
                                    del self.network_data_update[node.node_id]['value']
                                    self.network_data_update[node.node_id]['updval'] = val
                            else:
                                self.network_data_update.setdefault(node.node_id, dict())
                                self.network_data_update[node.node_id]['updval'] = val

                            if send_now:
                                self._send_data(node.node_id, 'value')

                        else:
                            self.network_data_update[node.node_id]['value'] = val

            elif value.command_class == 0x70 or value.command_class == 0x84 or value.command_class == 0x86:  # cc configuration ... 0x84 ist WakeUp
                if node.product_type[-4:] == "0800" and node.product_id[-4:] == "1001" and node.manufacturer_id[-4:] == "010f":  # fgms
                    if value.label == "Wake-up Interval":
                        node.set_config_param(1, 64)
                        node.set_config_param(60, 1 if self.timer_intervals.get(32, 300) < 1800 else 5)
                        node.set_config_param(62, 300)
                        node.set_config_param(64, self.timer_intervals.get(32, 300))
                        node.set_config_param(80, 0)
                        node.set_config_param(88, 0)
                        node.set_config_param(81, 1)
                if node.product_type[-4:] == "0001" and node.product_id[-4:] == "0003" and node.manufacturer_id[-4:] == "0059":  # srt321
                    if value.command_class == 0x86:
                        node.set_config_param(1, 0xff)
                        node.set_config_param(2, 0x00)
                        node.set_config_param(3, 1)
                if node.product_id[-4:] == "8010":  # zwavers
                    if value.label == "Wake-up Interval":
                        node.set_config_param(3, 4)
                        node.set_config_param(4, 35)
                        node.set_config_param(2, 5)
                if node.product_type[-4:] == "0002" and node.product_id[-4:] == "0064" and node.manufacturer_id[-4:] == "0086":
                    if value.label == "Wake-up Interval":
                        node.request_config_param(4)
                        node.request_config_param(40)
                        node.request_config_param(41)
                    elif value.command_class == 112 and value.label == "Parameter #4" and value.data != 5:
                        node.set_config_param(4, 5)
                    elif value.command_class == 112 and value.label == "Parameter #40" and value.data != 1:
                        node.set_config_param(40, 1)
                    elif value.command_class == 112 and value.label == "Parameter #41" and value.data != 1:
                        node.set_config_param(41, 1)

                self.network_data_update[node.node_id].setdefault('values', dict())
                self.network_data_update[node.node_id]['values']['wakeup'] = True
            
            elif value.command_class == 0x71:  # cc alarm
                if node.product_type[-4:] == "0002" and node.product_id[-4:] == "0064" and node.manufacturer_id[-4:] == "0086":
                    if value.label == "Burglar":
                        self.network_data_update[node.node_id].setdefault('values', dict())
                        if value.data == 0:
                            self.network_data_update[node.node_id]['values']['Alarm'] = False
                        elif value.data == 8:
                            self.network_data_update[node.node_id]['values']['Alarm'] = True

            elif value.command_class == 0x80:  # cc battery level
                self.network_data_update[node.node_id]['battery'] = value.data

        except Exception as e:
            exc_type, exc_obj, exc_tb = sys.exc_info()
            err = "exception %s / %s in zws_valuesignal:%s: %s" % (exc_type, exc_obj, exc_tb.tb_lineno, e)
            print err
            try:
                requests.post("%s/get/zwave/%s/" % (self.error_server, self.network.home_id_str), data=err, timeout=5)
            except Exception:
                try:
                    requests.post("http://localhost/get/zwave/%s/" % self.network.home_id_str, data={"error": "Error transmitting valuesignal error to remote"}, timeout=5)
                except Exception:
                    pass


def main():
    zwdaemon = ZWaveHandler()
    try:
        zwdaemon.run()
    except Exception as e:
        with open('/sys/class/net/eth0/address') as f:
            macaddr = f.read().strip().replace(':', '-')
        try:
            exc_type, exc_obj, exc_tb = sys.exc_info()
            err = "exception %s / %s l%s: %s" % (exc_type, exc_obj, exc_tb.tb_lineno, str(e))
            requests.post("http://controme-main.appspot.com/get/zwave/%s/" % macaddr, data=err, timeout=5)
        except:
            requests.post("http://controme-main.appspot.com/get/zwave/%s/" % macaddr, data={"error": "exception obtaining exception"}, timeout=5)


if __name__ == "__main__":
    main()
