import sys, os, django

# Add django settings to run this script seperately!
try:
    sys.path.append('/home/pi/rpi/')
    os.environ.setdefault("DJANGO_SETTINGS_MODULE", "rpi.settings")
    django.setup()
except:
    print('Error in initialize django settings!')
    sys.exit()
    
from fabric.api import local, settings
from time import sleep
import subprocess
import logging

p_logger = logging.getLogger("servicemonitorlog")
logger = logging.getLogger("logmonitor")

def check_supervisor():
    result = local("sudo supervisorctl -c /etc/supervisor/supervisord.conf status  | sed  -r -e 's/\S+//3' -e 's/\S+//3' | cut -d ' ' -f 1,16-", capture=True)
    for line in result.split('\n'):
        if 'STOPPED' in line or 'EXITED' in line or 'FATAL' in line:
            if any(keyword in line for keyword in ['celery', 'celerybeat', 'uwsgi']):
                try:
                    local("rm /home/pi/rpi/celerybeat.pid")
                except:
                    pass
                try:
                    logger.warning("%s|%s" % ('Systemstatus', u'Celery-Healing-Prozess aktiv.|1b'))
                    local("sudo supervisorctl -c /etc/supervisor/supervisord.conf restart all")
                except:
                    pass
                sleep(10)
                result = local("sudo supervisorctl -c /etc/supervisor/supervisord.conf status  | sed  -r -e 's/\S+//3' -e 's/\S+//3' | cut -d ' ' -f 1,16-", capture=True)
                for line in result.split('\n'):
                    if 'STOPPED' in line or 'EXITED' in line or 'FATAL' in line:
                        if any(keyword in line for keyword in ['celery', 'celerybeat', 'uwsgi']):
                            log_text = "%s|%s" % ('Systemstatus',
                                                  'Automatischer Neustart durch Celery-Healing-Prozess.|2b')
                            logger.warning(log_text)
                            p_logger.warning(log_text)
                            local('sudo reboot now')
                break
                

if __name__ == '__main__':
    try: 
        with open('/proc/uptime', 'r') as f:
            uptime_seconds = float(f.readline().split()[0])
    except:
        sleep(20)
        uptime_seconds = 3600
    if uptime_seconds >= 3600:
        check_supervisor()