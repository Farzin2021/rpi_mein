from django.apps import AppConfig


class RFConfig(AppConfig):
    name = 'rf'
    verbose_name = 'Funk'

    def ready(self):
        from heizmanager.models import sig_solltemp_change
        from signals import rf_solltemp_change
        sig_solltemp_change.connect(rf_solltemp_change)