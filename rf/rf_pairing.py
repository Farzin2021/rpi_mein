from django.views.decorators.csrf import csrf_exempt
from heizmanager.render import render_redirect, render_response
from heizmanager.models import Haus, Raum
from models import RFAktor, RFController
import sys
import time
import struct
from django.http import HttpResponse
import json
from time import sleep
import heizmanager.network_helper as nh
import requests
import json
from config.Controme_ETRV.helper import _local_handler
try:
    from bluepy import btle
except ImportError:
    pass


def device_situation():
    try:
        reload(rpi.server)
    except:
        pass
    try:
        from rpi.server import use_localhost
    except ImportError:
        use_localhost = False
    try:
        from rpi.server import use_host
    except ImportError:
        use_host = 'localhost' if use_localhost else None
    return use_localhost, use_host


def get_secret(pair_id):

    bt_addrs = str(pair_id)

    def read_char1(sA, tp_sensor):
        try:
            secret = struct.unpack('<16c', sA.getCharacteristics(forUUID="1002000B-2749-0001-0000-00805F9B042F")[0].read())
        except:
            tp_sensor.disconnect()
        return secret

    def data():
        for _ in range(3):
            try:
                sleep(0.5)
                tp_sensor = btle.Peripheral(bt_addrs)
                sleep(1)
                tp_serviceA = tp_sensor.getServiceByUUID("10020000-2749-0001-0000-00805f9b042f")
                sleep(1)
                break
            except:
                sleep(2)
                try:
                    from bluepy import btle
                except ImportError:
                    pass

        return read_char1(tp_serviceA, tp_sensor), tp_sensor
    
    for _ in range(2):
        try:
            sec_data, tp_sensor = data()
            if sec_data:
                try:
                    uni_secret = [unichr(ord(u)) for u in list(sec_data)]
                    uni_secret_json = json.dumps(uni_secret)
                    try:
                        use_localhost, use_host = device_situation()
                        r = requests.post("http://localhost/set/%s/" % ('btlesecret'),data={'pair_id': pair_id, 'sec_data': uni_secret_json}, timeout=8)
                        if use_host and use_host != 'localhost':
                            dev = RFAktor.objects.get(name= str(pair_id))
                            rpimac = nh.get_mac()
                            dev_data = json.dumps({'name': dev.name, 'description': dev.description, 'type': dev.type, 'parameters': dev.parameters, 'protocol': dev.protocol, 'haus_id': dev.haus_id, 'controller_id': dev.controller_id})
                            r = requests.post("http://%s/set/%s/" % (use_host, 'btlesecret'),data={'pair_id': pair_id, 'sec_data': uni_secret_json, 'dev': dev_data, 'rpimac': rpimac}, timeout=8)
                    except:
                        pass
                    tp_sensor.disconnect()
                except:
                    pass
                return 'success'
        except:
            sleep(1)
    return 'error'


@csrf_exempt
def pairing(request, hausid):
    if request.method == 'GET':
        haus = Haus.objects.first()
        hparams = haus.get_module_parameters()
        pairing_status = hparams.get('rf_log_params', dict()).get('rf_pairing_status','log-on')
        if pairing_status != 'log-on':
            return HttpResponse('Access denied!')
        try:
            handler_version = _local_handler("[ -f /etc/supervisor/conf.d/btle.conf ] && echo yes || echo no")
            if handler_version == 'yes':
                handler_version = 'Aus'
            else:
                handler_version = 'Ein'
        except:
            handler_version = 'Ein'
        try:
            ctrl_id = RFController.objects.filter(name=nh.get_mac())[0]
            devices = list(RFAktor.objects.filter(type__startswith="hkteTRV", controller_id=ctrl_id))
        except:
            return render_response(request, "m_install_rfs.html",{'handler_version': handler_version, 'hausid': hausid})
        all_etrv = []
        for dev in devices:
            if dev.type == 'hkteTRV':
                try:
                    secret_key = dev.get_parameters().get('secret')
                    try:
                        use_localhost, use_host = device_situation()
                        if use_host and use_host != 'localhost':
                            r = requests.get("http://%s/get/%s/%s/" % (use_host, 'btlesecret', str(dev.name)), timeout=10)
                            dev.text_drop = json.loads(r.text)
                        else:
                            dev.text_drop = 'Device is in master mode.'
                    except:
                        dev.text = 'Request error'
                    try:
                        r = requests.get("http://%s/get/%s/%s/" % ('localhost', 'btlesecret', str(dev.name)), timeout=10)
                        dev.text_master = json.loads(r.text)
                    except:
                        dev.text = 'Request error'
                    if not secret_key or secret_key == '':
                        dev.status = 'red'
                    else:
                        dev.status = 'green'
                except:
                    dev.status = 'red'
                all_etrv.append(dev)
        return render_response(request, "m_install_rfs.html", {'all_etrv': all_etrv, 'handler_version': handler_version, 'hausid': hausid})

    if request.method == 'POST':
        if 'pair_id' in request.POST.keys():
            pair_id = request.POST.get('pair_id')
            secret_key = get_secret(pair_id)
            return HttpResponse(json.dumps({'msg': secret_key}), content_type="application/json")

        elif 'delete_id' in request.POST.keys():
            pair_id = request.POST.get('delete_id')
            try:
                use_localhost, use_host = device_situation()
                r = requests.post("http://localhost/set/%s/" % ('btlesecret_delete'),data={'pair_id': pair_id}, timeout=10)
                if use_host and use_host != 'localhost':
                    r = requests.post("http://%s/set/%s/" % (use_host, 'btlesecret_delete'),data={'pair_id': pair_id}, timeout=10)
            except:
                pass
            return HttpResponse(json.dumps({'msg': pair_id}), content_type="application/json")
  
        elif 'ble' in request.POST.keys():
            if request.POST.get('ble') == 'handler_old':
                _local_handler('sudo sed -i "/#\[program:btlehandler\]/c\\\[program:btlehandler\]" /etc/supervisor/conf.d/messaging.conf')
                _local_handler('sudo sed -i "/# command = sudo \/usr\/bin\/python -u \/home\/pi\/rpi\/config\/btle_handler.py/c\\command = sudo \/usr\/bin\/python -u \/home\/pi\/rpi\/config\/btle_handler.py" /etc/supervisor/conf.d/messaging.conf')
                _local_handler('sudo sed -i "/# command = sudo \/usr\/bin\/python \/home\/pi\/rpi\/config\/btle_handler.py/c\\command = sudo \/usr\/bin\/python \/home\/pi\/rpi\/config\/btle_handler.py" /etc/supervisor/conf.d/messaging.conf')
                _local_handler('sudo supervisorctl -c /etc/supervisor/supervisord.conf stop newbtle')
                _local_handler('sudo systemctl stop etrv_handler.service')
                _local_handler('sudo systemctl disable etrv_handler.service')
                _local_handler('sudo rm /etc/supervisor/conf.d/btle.conf')
                _local_handler("sudo supervisorctl -c /etc/supervisor/supervisord.conf reread")
                _local_handler("sudo supervisorctl -c /etc/supervisor/supervisord.conf update")
                _local_handler('sudo supervisorctl -c /etc/supervisor/supervisord.conf restart btlehandler')
                _local_handler('sudo supervisorctl -c /etc/supervisor/supervisord.conf restart newbtle')
                _local_handler("sudo sed -i '/-l 0.0.0.0/c\-l 127.0.0.1' /etc/memcached.conf ; sudo systemctl restart memcached.service")
            elif request.POST.get('ble') == 'handler_new':
                _local_handler('sudo cp /home/pi/rpi/config/supervisor-newbtle.conf /etc/supervisor/conf.d/btle.conf')
                _local_handler('echo $(date) > /var/log/uwsgi/tempexec_v1')
                _local_handler('sudo supervisorctl -c /etc/supervisor/supervisord.conf stop btlehandler')
                _local_handler('sudo sed -i "/\[program:btlehandler\]/c\\#\[program:btlehandler\]" /etc/supervisor/conf.d/messaging.conf')
                _local_handler('sudo sed -i "/command = sudo \/usr\/bin\/python -u \/home\/pi\/rpi\/config\/btle_handler.py/c\\# command = sudo \/usr\/bin\/python -u \/home\/pi\/rpi\/config\/btle_handler.py" /etc/supervisor/conf.d/messaging.conf')
                _local_handler('sudo sed -i "/command = sudo \/usr\/bin\/python \/home\/pi\/rpi\/config\/btle_handler.py/c\\# command = sudo \/usr\/bin\/python \/home\/pi\/rpi\/config\/btle_handler.py" /etc/supervisor/conf.d/messaging.conf')
                _local_handler("sudo supervisorctl -c /etc/supervisor/supervisord.conf reread")
                _local_handler("sudo supervisorctl -c /etc/supervisor/supervisord.conf update")
                _local_handler('sudo supervisorctl -c /etc/supervisor/supervisord.conf restart btlehandler')
                _local_handler('sudo supervisorctl -c /etc/supervisor/supervisord.conf restart newbtle')
                _local_handler('sudo systemctl stop etrv_handler.service')
                _local_handler('sudo systemctl disable etrv_handler.service')
                _local_handler("sudo sed -i '/-l 127.0.0.1/c\-l 0.0.0.0' /etc/memcached.conf ; sudo systemctl restart memcached.service")
            return render_redirect(request, "/m_setup/%s/rf/pairing" % hausid)