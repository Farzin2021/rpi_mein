# -*- coding: utf-8 -*-

from heizmanager.render import render_redirect, render_response
from models import RFDevice, RFController, RFAktor, RFSensor, RFAusgang
from heizmanager.models import Raum, AbstractSensor, RPi, Haus
import logging
import heizmanager.cache_helper as ch
import heizmanager.network_helper as nh
from django.http import HttpResponse
from itertools import chain
from fabric.api import local
from django.views.decorators.csrf import csrf_exempt
from datetime import datetime, timedelta
import json, os
import ast
from heizmanager.mobile.m_temp import get_module_offsets
from heizmanager.models import sig_new_temp_value, sig_new_output_value, sig_get_outputs_from_output
from benutzerverwaltung.decorators import is_admin
from heizmanager.abstracts.base_mode import BaseMode
try:
    import rpi.server as rpiserver
except ImportError:
    from rpi.settings_test import rpiserver
from wifi.views import get_wlan_interface
import requests

logger = logging.getLogger("logmonitor")


def get_global_settings_link(request, haus):
    return "<a href='/m_setup/%s/rf/'>Funkeinrichtung</a>" % haus.id


def get_global_description_link():
    desc = None
    desc_link = None
    return desc, desc_link


@is_admin
def get_global_settings_page(request, haus):

    if request.method == "GET":
        if 'dev' in request.GET and 'type' in request.GET and 'val' in request.GET:
            ret = {}
            dev = request.GET['dev']
            typ = request.GET['type']
            val = request.GET['val']

            if '-' in dev:
                devicetype = dev.split('-')[1]
                dev = dev.split('-')[0]
                device = RFController.objects.get(pk=long(dev))
            else:
                device = AbstractSensor.get_sensor(dev)
                if device is None:
                    return render_redirect(request, "/m_setup/%s/rf/" % haus.id)

            if typ == "newmac":
                new_mac = val.lower().strip()
                try:
                    _ = RFAktor.objects.get(name=new_mac)
                    ret['error'] = "Gerät bereits angelegt."
                    return HttpResponse(json.dumps(ret), content_type='application/json')  # already exits do nothing
                except RFAktor.DoesNotExist:
                    pass
                except Exception:
                    pass
                try:
                    device = RFController.objects.get(pk=long(dev))
                    newDev = RFAktor(name=new_mac, description='', type=devicetype, parameters='{}', protocol='btle', haus=device.haus, controller=device)
                    newDev.save()
                except Exception:
                    ret['error'] = "Es ist ein Fehler aufgetreten."
                    return HttpResponse(json.dumps(ret), content_type='application/json')
                return HttpResponse(json.dumps(ret), content_type='application/json')

            elif typ == "raum":
                alter_raum = device.raum
                try:
                    raum = Raum.objects.get(pk=long(val))
                    if device.type.startswith('hkt') and device.type != "hktControme" and raum.regelung.regelung != 'funksollregelung' and raum.regelung.get_ausgang():
                        ret['error'] = u'Operation nicht gestattet. Durch die Zuordnung zu diesem Raum w&uuml;rden Ausg&auml;nge verloren.'

                    else:
                        if raum and device.raum != raum:  # umordnung
                            alter_raum = device.raum
                            if device.type in ["wp", "relais", "wpd2010b"] and alter_raum:
                                alte_reg = alter_raum.get_regelung()
                                try:
                                    rfausgang = RFAusgang.objects.get(regelung=alte_reg)
                                    rfausgang.aktor.remove(device.id)
                                    if not len(rfausgang.aktor):
                                        rfausgang.delete()
                                except RFAusgang.DoesNotExist:
                                    pass
                            # wenn der Raum eine RLR mit ignorems=True hat, dann muessen wir eigentlich nicht loeschen
                            if device.mainsensor and alter_raum:
                                ausgang = alter_raum.get_regelung().get_ausgang()
                                if ausgang:
                                    ausgang.delete()
                                alte_reg = alter_raum.get_regelung()
                                try:
                                    rfausgang = RFAusgang.objects.get(regelung=alte_reg)
                                    rfausgang.delete()
                                except RFAusgang.DoesNotExist:
                                    pass
                            else:
                                ms = raum.get_mainsensor()
                                if not ms and device.type in ('wt', 'multisensor', 'sensor'):
                                    device.mainsensor = True  # gesetzt wird unten
                        device.raum = raum
                except ValueError:
                    if device.raum_id and device.mainsensor:
                        device.mainsensor = False
                        device.raum.set_mainsensor(None)
                    device.raum = None
                except Raum.DoesNotExist:  # wurde keinem Raum zugeordnet
                    device.raum = None
                    if device.mainsensor:
                        alter_raum.set_mainsensor(None)
                        device.mainsensor = False
                device.save()
                if device.mainsensor and device.raum_id:
                    device.raum.set_mainsensor(device)
                    if alter_raum and alter_raum != raum and alter_raum.get_mainsensor() == device:    
                        alter_raum.set_mainsensor(None)

                if isinstance(device, RFAktor) and alter_raum and alter_raum.id != device.raum_id \
                        and device.type.startswith("hkt") and device.type != "hktControme":  # umordnung
                    hkts = RFAktor.objects.filter(raum=alter_raum, type__startswith="hkt")
                    if not len(hkts):  # kein HKT mehr im Raum
                        reg = alter_raum.get_regelung()
                        reg.regelung = "zweipunktregelung"
                        reg.set_parameters({'delta': 0.0, 'active': True})
                        reg.save()
                if device.mainsensor:
                    ret['mainsensor'] = True

            elif typ == "type" and val in [t[0] for t in RFDevice.TYPE_CHOICES]:
                if val in {'hkt', 'hktGenius','hktControme', 'hkta52001', 'hkteTRV'} and device.raum and device.raum.regelung.regelung != 'funksollregelung' and device.raum.regelung.get_ausgang():
                    ret['error'] = u'Operation nicht gestattet. Durch die Zuordnung zu diesem Raum w&uuml;rden Ausg&auml;nge verloren.'
                else:
                    device.type = val
                    device.save()

                    aktoren = ["hkt", "hktGenius", "hktControme", "relais", "wp", "wt", "2chrelais", "wpd2010b", "hkta52001", "hkteTRV"]
                    sensoren = ["sensor", "multisensor", "sensora50401", "sensora50205", "cntct"]
                    logging.warning("%s %s" % (type(device), val))
                    if isinstance(device, RFAktor) and val in sensoren:
                        ndevice = RFSensor(name=device.name, description=device.description, protocol=device.protocol,
                                           type=val, haus=device.haus, parameters=device.parameters, raum=device.raum,
                                           controller=device.controller)
                        ndevice.save()
                        device.delete()
                        device = ndevice

                    elif isinstance(device, RFSensor) and val in aktoren:
                        ndevice = RFAktor(name=device.name, description=device.description, protocol=device.protocol,
                                          type=val, haus=device.haus, parameters=device.parameters, raum=device.raum,
                                          controller=device.controller)
                        ndevice.save()
                        device.delete()
                        device = ndevice

                    if val in {"hkt", "hktGenius","hktControme", "hkta52001", "hkteTRV"}:
                        modules = haus.get_modules()
                        if 'funksollregelung' not in modules:
                            modules += ['funksollregelung']
                            haus.set_modules(modules)

                        # das muss dann halt bei aenderungen immer konsistent gehalten werden
                        aktoren = RFAktor.objects.filter(haus=haus, type__startswith="hkt")
                        if len(aktoren):
                            params = device.get_parameters()
                            oparams = aktoren[0].get_parameters()
                            params['wakeup'] = oparams.get('wakeup', 900)
                            device.set_parameters(params)
                        else:
                            params = device.get_parameters()
                            params['wakeup'] = 900
                            device.set_parameters(params)

                    elif val == "multisensor":
                        sensoren = RFSensor.objects.filter(haus=haus, type="multisensor")
                        if len(sensoren):
                            params = device.get_parameters()
                            oparams = sensoren[0].get_parameters()
                            params['wakeup'] = oparams.get('wakeup', 900)
                            device.set_parameters(params)
                        else:
                            params = device.get_parameters()
                            params['wakeup'] = 900
                            device.set_parameters(params)

            elif typ == "desc":
                device.description = val
                device.save()

            elif typ == "interval":
                if device.protocol == 'zwave':
                    params = device.get_parameters()
                    val = int(val)
                    params.setdefault('wakeup_intervals', dict())
                    params['wakeup_intervals'].setdefault(0x84, dict())
                    params['wakeup_intervals'][0x84][int(devicetype)] = int(val)
                    if int(devicetype) == 32:
                        params['wakeup_intervals'][0x84][0x07] = int(val)
                    device.set_parameters(params)
                elif device.protocol == 'btle':
                    params = device.get_parameters()
                    val = int(val)
                    params.setdefault('wakeup_intervals', dict())
                    params['wakeup_intervals']['intervall'] = val
                    device.set_parameters(params)

            elif typ == "lock":
                if device.locked:
                    device.locked = False
                else:
                    device.locked = True
                device.save()

            elif typ == "mainsensor":
                if device.mainsensor:
                    device.mainsensor = False
                    device.raum.set_mainsensor(None)
                else:
                    device.mainsensor = True
                    try:
                        device.raum.set_mainsensor(device)
                    except AttributeError:
                        device.mainsensor = False
                device.save()

            elif typ == "twentyonelocked" and (device.type == 'wt' or (device.protocol == "btle" and device.type.startswith("hkt"))):
                params = device.get_parameters()
                if not params.get('twentyonelocked', False):
                    params['twentyonelocked'] = True
                else:
                    params['twentyonelocked'] = False
                device.set_parameters(params)

            elif typ == "switchmode" and ((device.type == 'wt' and device.protocol == "zwave") or (device.protocol == "btle" and device.type.startswith("hkt"))):
                params = device.get_parameters()
                if not params.get('switchmode', False):
                    params['switchmode'] = True
                else:
                    params['switchmode'] = False
                device.set_parameters(params)

            elif typ == "autark":

                if val == "mycontromecom":
                    autark = False
                    datadest = 'mycontromecom'
                elif val == "localhost":
                    autark = True
                    datadest = 'localhost'
                else:
                    autark = False
                    rpi = RPi.objects.get(name=val)
                    datadest = rpi.local_ip

                try:
                    if autark:
                        local("""echo "use_localhost = True; use_host = '%s';" > /home/pi/rpi/rpi/server.py""" % datadest)
                    else:
                        local("""echo "use_localhost = False; use_host = '%s';" > /home/pi/rpi/rpi/server.py""" % datadest)
                    local("sudo supervisorctl -c /etc/supervisor/supervisord.conf restart messageserver &")
                except:
                    pass

                for rfc in RFController.objects.all():
                    params = rfc.get_parameters()
                    params['autark'] = autark
                    rfc.set_parameters(params)

            elif typ == "discovery":
                params = device.get_parameters()
                if params.get('discovery', True):
                    params['discovery'] = False
                else:
                    params['discovery'] = True
                device.set_parameters(params)

            elif typ == "atsensor":
                hparams = haus.get_module_parameters()
                hparams.setdefault('ruecklaufregelung', dict())
                if device.get_identifier() == hparams.get('ruecklaufregelung', dict()).get('atsensor', -1):
                    del hparams['ruecklaufregelung']['atsensor']
                else:
                    hparams['ruecklaufregelung']['atsensor'] = device.get_identifier()
                haus.set_module_parameters(hparams)

            elif typ == "sensoroffset":                
                if len(val):
                    val = float(val)
                    params = device.get_parameters()
                    params['offset'] = val
                    device.set_parameters(params)
                    if device.mainsensor:
                        device.raum.set_mainsensor(device)  # mainsensor caching ueberschreiben

            elif typ == "dispBright":                
                if len(val):
                    val = int(val)
                    params = device.get_parameters()
                    params['tb_show_ist'] = val
                    device.set_parameters(params)
                    if device.mainsensor:
                        device.raum.set_mainsensor(device)  # mainsensor caching ueberschreiben
     
            elif typ == "deviation":                
                if len(val):
                    val = float(val)
                    params = device.get_parameters()
                    params['tdeviation'] = val
                    device.set_parameters(params)
                    if device.mainsensor:
                        device.raum.set_mainsensor(device)  # mainsensor caching ueberschreiben

            elif typ == "forceWlan":                
                if len(val):
                    val = int(val)
                    params = device.get_parameters()
                    params['tforce_send_wlan'] = val
                    device.set_parameters(params)
                    if device.mainsensor:
                        device.raum.set_mainsensor(device)  # mainsensor caching ueberschreiben

            elif typ == "deepSleep":
                if len(val):
                    val = int( bool(val))
                    params = device.get_parameters()
                    if params.get('tdeep_sleep', 0):
                        params['tdeep_sleep'] = 0
                    else:
                        params['tdeep_sleep'] = 1
                    device.set_parameters(params)
                    if device.mainsensor:
                        device.raum.set_mainsensor(device)  # mainsensor caching ueberschreiben

            elif typ == "dynamicBatSavingRc":
                params = device.get_parameters()
                params['tdynamic_bat_saving_rc'] = params.get('tdynamic_bat_saving_rc', False)

                if not params.get('tdynamic_bat_saving_rc', False):
                    params['tdynamic_bat_saving_rc'] = True
                else:
                    params['tdynamic_bat_saving_rc'] = False
                device.set_parameters(params)
                if device.mainsensor:
                    device.raum.set_mainsensor(device)  # mainsensor caching ueberschreiben

            elif typ == "sendInterval":                
                if len(val):
                    val = int(val)
                    params = device.get_parameters()
                    params['tsendeintervall'] = val
                    device.set_parameters(params)
                    if device.mainsensor:
                        device.raum.set_mainsensor(device)  # mainsensor caching ueberschreiben


            elif typ == "delete" and (device.protocol in {"btle", "enocean"} or device.controller.protocol in {"btle", "enocean"}):
                protocol = device.protocol
                cparams = device.controller.get_parameters()
                last_data = cparams.get('last_data', dict())
                if device.name in last_data:
                    del last_data[device.name]
                    cparams['last_data'] = last_data
                    device.controller.set_parameters(cparams)
                try:
                    device.controller.get_aktoren().remove(device)
                except ValueError:
                    pass
                try:
                    device.controller.get_sensoren().remove(device)
                except ValueError:
                    pass
                device.delete()
                if protocol == 'btle':
                    local("sudo supervisorctl -c /etc/supervisor/supervisord.conf restart btlehandler &")
                
                return HttpResponse(json.dumps({}), content_type='application/json')

            elif typ == "configtemp" and device.protocol == "btle":
                devparams = device.get_parameters()
                try:
                    devparams['configtemp'] = float(val)
                except:
                    devparams['configtemp'] = 21.0
                device.set_parameters(devparams)

            if isinstance(device, RFAktor) and device.raum and device.type.startswith("hkt") and device.type != "hktControme" and not ret.get('error'):
                regelung = device.raum.get_regelung()
                if regelung.regelung != "funksollregelung":
                    regelung.regelung = "funksollregelung"
                    params = {'active': True, 'rsfaktor': 0.0, 'rsactive': True}
                    regelung.set_parameters(params)
                    regelung.save()

            ret['id'] = device.get_identifier()
            return HttpResponse(json.dumps(ret), content_type='application/json')

        elif 'rfcreset' in request.GET:
            rfcid = request.GET['rfcreset']
            try:
                pk = long(rfcid)
                rfc = RFController.objects.get(pk=pk)
                aktoren = RFAktor.objects.filter(controller=rfc)
                sensoren = RFSensor.objects.filter(controller=rfc)
                devices = list(chain(aktoren, sensoren))
                for device in devices:                   
                    ch.delete('getping_%s' % device.controller.name)  # delete last seen
                    ch.delete('getping_%s_%s' % (device.controller.name, device.name))  # delete last seen
                    ch.delete("%s_%s_val" % (device.controller.name, device.name))  # delete last values
            except (ValueError, RFController.DoesNotExist):
                pass
            return render_redirect(request, "/m_setup/%s/rf/" % haus.id)

        elif 'rfcdel' in request.GET:
            rfcid = request.GET['rfcdel']
            try:
                pk = long(rfcid)
                rfc = RFController.objects.get(pk=pk)
                RFAktor.objects.filter(controller=rfc).delete()
                RFSensor.objects.filter(controller=rfc).delete()
                rfc.delete()
            except (ValueError, RFController.DoesNotExist):
                pass
            return render_redirect(request, "/m_setup/%s/rf/" % haus.id)

        elif 'd' in request.GET:
            device = AbstractSensor.get_sensor(request.GET['d'])
            if not device:
                return HttpResponse("")
            try:
                last = ch.get_gw_ping("%s_%s" % (device.controller.name, device.name))
            except RFController.DoesNotExist:
                device.delete()
                return HttpResponse()

            # Initialize default values for rssi_data
            rssi_data = {'rssi_data': {'time': None, 'rssi': None}}
            etrv_setting = '-'
            # Check if the device is BLE and starts with "hkt"
            if device.protocol == 'btle' and device.type.startswith("hkt"):
                # Try to fetch the BLE RSSI value from the channel
                rssi = ch.get('ble_rssi_%s' % (device.name.replace(':', '_')))
                if rssi and 'rssi' in rssi:
                    # Update rssi_data with the fetched RSSI value
                    rssi_data['rssi_data']['rssi'] = rssi['rssi']
                    # Check if the 'time' key is present in the fetched data and update rssi_data with the value
                    if 'time' in rssi:
                        rssi_data['rssi_data']['time'] = rssi['time']
                    else:
                        rssi_data['rssi_data']['time'] = None
                try:
                    get_etrv_setting = ch.get('etrv_settings_%s' % (device.name.replace(':', '_')))
                    mode, mounted, battery_percentage = get_etrv_setting.split(',')
                    etrv_setting = "Montiert: {}; Modus: {}; Batterie: {}%".format(mounted, mode, battery_percentage)
                except:
                    pass

            marker = device.get_marker(use_last_data=False)
            neighbors = "1"
            if last:
                last = last[0].strftime(""" %d.%m.%Y - %H:%M""")
                if device.protocol == 'zwave':
                    params = device.controller.get_parameters()
                    last_data = params.get('last_data')
                    if last_data:
                        neighbors = ''', '''.join([str(n) for n in last_data.get(device.name, dict()).get('neighbors', [1])])

            if device.type in ["2chrelais", "wp", "wpd2010b"]:
                shortest_wakeup = device.controller.get_shortest_wakeup()
                marker = RFController.get_marker(device.controller.name, shortest_wakeup)

            vals = ch.get_last_vals("%s_%s_val" % (device.controller.name, device.name))
            not_configured = False
            if isinstance(device, RFSensor):  # todo
                dparams = device.get_parameters()
                mid = dparams.get('manufacturer_id', '')
                if not len(mid):
                    not_configured = True
            elif isinstance(device, RFAktor) and device.protocol == 'btle':
                if vals and not isinstance(vals, tuple) and vals[0] and not isinstance(vals[0], tuple) and vals[0].get('datetime', [0, 0, 0, 0, 0]) == [0, 0, 0, 0, 0]:
                    not_configured = True

            has_outs = False
            if device.mainsensor and device.raum:  # todo
                cb2outs = sig_get_outputs_from_output.send(
                    sender='rf',
                    raum=device.raum,
                    regelung=device.raum.regelung
                )
                outs = [o for out in [_o[1] for _o in cb2outs] for o in out]
                if len(outs):
                    has_outs = True
            elif device.type in ['relais', 'wp', "wpd2010b"] and device.raum:
                rfas = RFAusgang.objects.filter(controller_id=device.controller_id)
                for rfa in rfas:
                    if device.id in rfa.aktor:
                        has_outs = True
            battery_check = vals[0].get('battery', False) if vals and vals[0] else False
            connect_type = 'Festanschluss'
            if battery_check:
                connect_type = 'Batterie'
            try:
                rh_percent = int(device.raum.get_humidity()[0])
                if rh_percent < 0:
                    rh_percent = '-'
            except:
                rh_percent = '-'
            if device.protocol == 'btle' and device.type.startswith("hkt") and last:
                if device.type == "hkteTRV":
                    last += u"; Solltemperatur: %s°C; Isttemperatur: %s°C; " % ((vals[0]['soll'] if vals and vals[0].get('soll') else "-"), (vals[0]['ist'] if vals and vals[0].get('ist') else "-"))
                else:
                    last += u"; Solltemperatur: %s°C; Offset: %s°C; Isttemperatur: %s°C; Luftfeuchtigkeit: %s%%. RC Version: %s; Typ: %s." % ((vals[0]['soll'] if vals and vals[0].get('soll') else "-"), (vals[0]['offset'] if vals and vals[0].get('offset') else "-"), (vals[0]['ist'] if vals and vals[0].get('ist') else "-"), rh_percent, vals[0].get('version', ''), connect_type)
            elif last and device.type in ['hkta52001', 'sensora50401', 'sensora50205', 'sensora50213', 'multisensor', 'sensor', 'wt']:
                last += u" mit %.2f°C" % vals[0]['Temperature'] if vals and vals[0] and vals[0].get('Temperature') else ""
                if device.type == "hkta52001" and vals and vals[0]:
                    last += "<br/>Batterie geladen: "
                    if 'sufficiently_charged' in vals[0]:
                        if vals[0]['sufficiently_charged'] == 'true':
                            last += 'ja'
                        else:
                            last += 'nein'
                    else:
                        last += '-'
                    last += "<br/>Energy Harvesting aktiv: "
                    if 'currently_charging' in vals[0]:
                        if vals[0]['currently_charging'] == 'true':
                            last += 'ja'
                        else:
                            last += 'nein'
                    else:
                        last += '-'

            hparams = haus.get_module_parameters()
            atsensor = hparams.get('ruecklaufregelung', dict()).get('atsensor', -1)
            twentyonelocked = False
            if device.type == "wt" or (device.protocol == "btle" and device.type.startswith("hkt")):
                dparams = device.get_parameters()
                twentyonelocked = dparams.get('twentyonelocked', False)

            switchmode = True
            if (device.type == "wt" and device.protocol == "zwave") or (device.protocol == "btle" and device.type.startswith("hkt")):
                switchmode = dparams.get('switchmode', False)

            if isinstance(device, RFSensor) and device.type not in ["cntct"] and device.get_wert():
                try:
                    lastsoll = float(device.get_wert()[0])
                except:
                    lastsoll = "-"
            elif vals and vals[0] and vals[0].get('soll'):
                try:
                    lastsoll = float(vals[0]['soll'])
                except:
                    lastsoll = "-"
            else:
                lastsoll = "-"
            isWLAN = False
            if vals and vals[0] and vals[0].get('WLAN'):
                isWLAN = vals[0].get('WLAN')
            html = _get_device_collapsible(device, lastsoll, last, isWLAN, marker, neighbors, vals[0].get('battery', -1) if vals and vals[0] else -1, not_configured, atsensor, has_outs, twentyonelocked, switchmode, rssi_data.get('rssi_data'), etrv_setting)
            return HttpResponse(html)

        else:
            rfaktoren = RFAktor.objects.filter(haus=haus)
            rfsensoren = RFSensor.objects.filter(haus=haus)
            devices = [d for d in chain(rfaktoren, rfsensoren)]  # if not d.hidden]
            for d in devices:
                if ':' in d.name and d.protocol != 'btle' and d.name.count(':') == '5':
                    d.protocol = 'btle'
                    d.save()
                elif ':' in d.name and d.protocol != 'enocean' and d.name.count(':') == '3':
                    d.protocol = 'enocean'
                    d.save()

            try:
                reload(rpiserver)
                ulh = rpiserver.use_localhost
            except ImportError:
                ulh = False
            try:
                reload(rpiserver)
                uh = rpiserver.use_host
            except ImportError:
                uh = 'localhost' if ulh else 'mycontromecom'

            rfcontroller = RFController.objects.filter(haus=haus).values_list('id', 'name', 'parameters', 'rpi__name', 'rpi__local_ip', 'protocol')
            rfcontroller = [(id, name, ast.literal_eval(parameters), rpiname, localip, protocol) for (id, name, parameters, rpiname, localip, protocol) in rfcontroller]
            rfcontroller = [(id, name, params, rpiname, params.get('autark', False), 'RFController', uh, localip, protocol, params.get('discovery', False))
                            for (id, name, params, rpiname, localip, protocol) in rfcontroller]

            last_seen = dict()
            smallest_wakeup = dict()
            last_seen_rfc = dict()
            for rfc in rfcontroller:
                if rfc[4] != ulh:
                    _rfc = RFController.objects.get(pk=rfc[0])
                    params = _rfc.get_parameters()
                    params['autark'] = ulh
                    _rfc.set_parameters(params)
                    rfc = (rfc[0], rfc[1], rfc[2], rfc[3], ulh, rfc[5], rfc[6], rfc[7], rfc[8], rfc[9])
                smallest_wakeup[rfc[1]] = min(rfc[2].get("wakeup_intervals", dict()).get(0x84, dict()).values()) if 0x84 in rfc[2].get("wakeup_intervals", dict()) else max(min(rfc[2].get("wakeup_intervals", {'intervall': 10}).values()), 60)
                params = rfc[2]
                last_data = params.get('last_data')
                if last_data:
                    if rfc[7] == "zwave":
                        set_zwave(None, rfc[3], rfc[1], last_data)
                    elif rfc[7] == "btle":
                        set_btle(None, rfc[3], rfc[1], last_data)
                last = ch.get_gw_ping(rfc[1])

                if last:
                    ls = last[0].strftime("am %d.%m. um %H:%M")
                else:
                    ls = None

                marker = RFController.get_marker(rfc[1], smallest_wakeup[rfc[1]])
                last_seen_rfc[rfc[1]] = (ls, marker)

            battery_devices = dict()
            for rfc in rfcontroller:
                battery_devices[rfc[1]] = list()
                params = rfc[2]
                if rfc[8] == 'zwave':
                    types = [0x08, 0x20]  # das ist behindert.
                    intervals = {0x08: [300, 900, 1800], 0x20: [300, 900, 1800]}
                    for t in types:
                        gtstring = RFController.get_type_from_generic_type(t)
                        interval = params.get('wakeup_intervals', dict()).get(0x84, dict()).get(t, 0)
                        battery_devices[rfc[1]].append((t, gtstring, gtstring, interval, intervals[t]))
                elif rfc[8] == 'btle':
                    types = [("intervall", "Abfrageintervall für Gerätetyp „Heizkörperthermostat eTRV“")]
                    intervals = {"intervall": [10, 300]}
                    for t in types:
                        interval = params.get('wakeup_intervals', dict()).get(t[0], 10)
                        battery_devices[rfc[1]].append((t[0], t[1], t[1], interval, intervals[t[0]]))

            rpis = RPi.objects.all()
            macaddr = nh.get_mac()
            rpis = [r for r in rpis if r.name != macaddr]
            hparams = haus.get_module_parameters()
            pairing_status = hparams.get('rf_log_params', dict()).get('rf_pairing_status','log-on')
            wifi_on = get_wlan_interface().get('wlan0', None)
            # devices kann man auch als values_list machen ... wenn get_type irgendwie hergeht

            return render_response(request, "m_install_rf.html", {'haus': haus, 'last_seen': last_seen,
                                                                  'devices': devices, 'rfcontroller': rfcontroller,
                                                                  'last_seen_rfc': last_seen_rfc, 'rpis': rpis,
                                                                  'battery_devices': battery_devices, 'macaddr': macaddr,
                                                                  'pairing_status': pairing_status, 'wifi_on': wifi_on})

    return render_redirect(request, "/m_setup/%s/rf/" % haus.id)


def get_global_settings_page_help(request, haus):
    return render_response(request, "m_help_setup_rf.html", {'haus': haus})


def _get_device_collapsible(device, lastsoll, last, isWLAN, marker, neighbors, battery, not_configured, atsensor, has_outs, twentyonelocked, switchmode, rssi_data, etrv_setting):
    # device.name, device.id, if desc "- device.description", markings
    # device.id, device.id, device.id, device.description
    # device.id, device.id, device.id, device.type, device.type_str
    # device.id, device.id, device.id, raum-><option>
    # sensor/aktor specific
    # last_seen_string, batterie or "-", nachbarn, incomplete_data_str

    html = u'''<div data-role="collapsible" class="collapse" id="{identifier}" name="{devicename}"><h3 style="padding-left:1em">
    <div style="float:left">
        ID {devicename}{lastsoll}<span id="{identifier}_descshow">{devicedescription}</span>
    </div>

    <div style="float: right">
        {imarkings}
    </div>
</h3>

<p>
<label for="{identifier}_desc" class="chg">Beschreibung</label>
<input type="text" name="{identifier}_desc" id="{identifier}_desc" class="chg" value="{devicedescriptioninput}"/>

<label for="{identifier}_type" class="chg">Typ</label>
<select name="{identifier}_type" id="{identifier}_type" class="chg" {disabledifhasouts}>
    {typeoptions}
</select>

<label for="{identifier}_raum" class="chg">Raum</label>
<select name="{identifier}_raum" id="{identifier}_raum" class="chg" {disabledifhasouts}>
    <option value="undef">---</option>
    {raumoptions}
</select>

{dev_specific}
'''
    if device.protocol == 'btle' and device.type.startswith("hkteTRV"):
        if rssi_data and rssi_data.get('time') and rssi_data.get('rssi'):
            html += u'''
                    <p>
                        Zuletzt gesehen: {rssi_time}; Verbindungsstaerke: {rssi_power}db;
                        <a href="https://support.controme.com/smartes-heizkoerperthermostat-df/#Pruefen_der_Reichweite_und_Einrichtung_von_Dropservern" target="_blank">(Info)</a>.
                    </p>'''

        else:
            html += u'''
                    <p>
                        Zuletzt gesehen: keine Daten 
                        <a href="https://support.controme.com/smartes-heizkoerperthermostat-df/#Pruefen_der_Reichweite_und_Einrichtung_von_Dropservern" target="_blank">(Info)</a>.
                    </p>'''
        html+= u'''
        <p>
            Letzte &Uuml;bertragung: {last_ts} {etrv_setting}</span>
        </p>

            {btry}

            {neighbors}

            {incomplete_data}

            {out_lock}

            {dellink}

            </p>
            </div>
        '''
    else:
        html+= u'''
        <p>
            Letzte &Uuml;bertragung: {last_ts}
        </p>

            {btry}

            {neighbors}

            {incomplete_data}

            {out_lock}

            {dellink}

            </p>
            </div>
        '''

    # device.id, color->type, kreuz/haken, device.id, color->raum, kreuz/haken, markercolor
    markings = u'''<span id="%s_battery">%s</span>
<span id="%s_typec" style="color: %s">
    %s
</span>
<span id="%s_raumc" style="color: %s">
    %s
</span>
<span style='color: %s'>&nbsp;&#8226;</span>'''

    incomplete_data = ""
    if not_configured:
        if device.protocol == 'zwave':
            incomplete_data = '''<p>Es wurden unvollst&auml;ndige Daten &uuml;bertragen. <a target='_blank' href='http://www.controme.com/installation-und-inbetriebnahme/funkkomponenten-einbinden/#multisensor-inbetriebnahme'>Ist der Sensor korrekt konfiguriert?</a></p>'''
        elif device.protocol == 'btle':
            if last and last.startswith("am "):
                lst = last.split(' mit')[0]
                last = None
            else:
                lst = None
            incomplete_data = u'''<p>Lese-/Schreibfehler beim Zugriff auf das Gerät. %s</p>''' % (u"Gerät zuletzt in Reichweite %s." % lst if lst else "")

    out_lock = '''<p style="color: #cb0963">Um &Auml;nderungen am Ger&auml;t vorzunehmen, entfernen Sie bitte zuerst die zugeordneten Ausg&auml;nge.</p>'''

    battery_marker = ""
    if isWLAN:
        battery_marker = "<img src='/static/icons/wifi_icon.png' height='16px'/>"
    elif 100 >= battery > 80:
        battery_marker = "<img src='/static/icons/battery_100.png' height='16px'/>"
    elif 80 >= battery > 60:
        battery_marker = "<img src='/static/icons/battery_80.png' height='16px'/>"
    elif 60 >= battery > 40:
        battery_marker = "<img src='/static/icons/battery_60.png' height='16px'/>"
    elif 40 >= battery > 20:
        battery_marker = "<img src='/static/icons/battery_40.png' height='16px'/>"
    elif 20 >= battery > 0:
        battery_marker = "<img src='/static/icons/battery_20.png' height='16px'/>"
    elif battery == 0:
        battery_marker = "<img src='/static/icons/battery_00.png' height='16px'/>"
    tcolor = "green" if device.type != "undef" else "red"
    tmarker = "&#10003;" if device.type != "undef" else "&#x2717;"
    rcolor = "green" if device.raum_id else "red"
    rmarker = "&#10003;" if device.raum_id else "&#x2717;"
    if device.type == 'repeater':
        tcolor = rcolor = "black"
        tmarker = rmarker = "-"
    imarkings = markings % (device.get_identifier(), battery_marker, device.get_identifier(), tcolor, tmarker, device.get_identifier(), rcolor, rmarker, marker)

    typeoptions = ""
    typeoptions += "<option value='undef'>---</option>"
    if device.protocol == 'zwave':
        for t in RFDevice.TYPE_CHOICES:
            if t[0] == 'undef' or '(' in t[1] or (t[0].startswith('hkt') and len(t[0]) > 3):
                continue
            if t[0] == "hkt":
                typeoptions += "<option value='%s'%s>%s</option>" % (t[0], " selected='selected'" if device.type == t[0] else "", u"Heizkörperthermostat")
            else:
                typeoptions += "<option value='%s'%s>%s</option>" % (t[0], " selected='selected'" if device.type == t[0] else "", t[1])
    elif device.protocol == 'btle':
        typeoptions += u"<option value='%s'%s>%s</options>" % ("hktGenius", " selected='selected'" if device.type == "hktGenius" else "", u"Heizkörperthermostat Genius")
        typeoptions += u"<option value='%s'%s>%s</options>" % ("hkt", " selected='selected'" if device.type == "hkt" else "", u"Heizkörperthermostat Comet")
        typeoptions += u"<option value='%s'%s>%s</options>" % ("hktControme", " selected='selected'" if device.type == "hktControme" else "", u"Controme Raumcontroller PRO")
        typeoptions += u"<option value='%s'%s>%s</options>" % ("hkteTRV", " selected='selected'" if device.type == "hkteTRV" else "", u"Heizkörperthermostat eTRV")
    elif device.protocol == 'enocean':
        typeoptions += "<option value='cntct'%s>Fensterkontakt (D5-00-01)</option>" % (" selected='selected'" if device.type == 'cntct' else "")
        typeoptions += "<option value='relais'%s>Relais (D2-01-01)</option>" % (" selected='selected'" if device.type == 'relais' else "")
        typeoptions += "<option value='wp'%s>Funksteckdose (D2-01-0A)</option>" % (" selected='selected'" if device.type == 'wp' else "")
        for t in RFDevice.TYPE_CHOICES:
            if '(' in t[1]:
                typeoptions += "<option value='%s'%s>%s</option>" % (t[0], " selected='selected'" if device.type == t[0] else "", t[1])

    raumoptions = ""
    if device.type != 'repeater':
        for etage in device.controller.haus.etagen.all().order_by('name'):
            for raum in Raum.objects.filter_for_user(etage.haus.eigentuemer, etage_id=etage.id).order_by('name').values_list('id', 'name'):
                raumoptions += "<option value='%s'%s>%s</option>" % (raum[0], "selected='selected'" if device.raum_id == raum[0] else "", "%s / %s" % (etage.name, raum[1]))

    dev_specific = """"""
    if device.type != 'undef':

        if isinstance(device, RFAktor) and device.protocol == "btle" and (device.type.startswith('hkt') and not device.type.startswith("hktControme")) and not device.raum_id:
            last_vals = ch.get_last_vals("%s_%s_val" % (device.controller.name, device.name))
            dev_specific += u'''<p id="%s_configtemp_p">
    <label for="%s_configtemp">Temperatur für Konfiguration</label>
    <input type="number" name="%s_configtemp" id="%s_configtemp" value="%s" data-mini="true" class="chg" step="1"/>
</p>''' % (device.get_identifier(), device.get_identifier(), device.get_identifier(), device.get_identifier(), device.get_parameters().get('configtemp', 21))

        if isinstance(device, RFAktor):
            if device.type == "wt" or (device.protocol in ['btle', 'enocean'] and device.type.startswith('hkt')):
                dev_specific += u'''<p>
    <label for="%s_lock">&Auml;nderungen am Aktor gesperrt</label>
    <input type="checkbox" name="%s_lock" id="%s_lock" %s class="chg"/>
    <small>Diese Option bewirkt, dass manuell am Gerät vorgenommene Einstellungen nicht ins System übernommen werden.</small>
</p>''' % (device.get_identifier(), device.get_identifier(), device.get_identifier(), " checked " if device.locked else "")
                dev_specific += u'''<p>
    <label for="%s_mainsensor">Raumtemperatursensor</label>
    <input type="checkbox" name="%s_mainsensor" id="%s_mainsensor" %s class="chg" %s/>
    <small>Der Raumtemperatursensor liefert die für die jeweilige Regelung maßgebliche Temperatur für diesen Raum. Es kann pro Raum nur einen Raumtemperatursensor geben.</small>
</p>''' % (device.get_identifier(), device.get_identifier(), device.get_identifier(), " checked " if device.mainsensor else "", "disabled='true'" if has_outs else "")
                if device.type == "wt":
                    dev_specific += u'''<p>
        <label for="%s_twentyonelocked">21° sperren</label>
        <input type="checkbox" name="%s_twentyonelocked" id="%s_twentyonelocked" %s class="chg"/>
        <small>
        Bei Systemen mit stark ausgelastetem Funkstick (viele Komponenten) oder beim Erreichen der Reichweitengrenze kann es
        vorkommen, dass die Übertragung der Funk-Pakete unzuverlässig wird. In diesen Fällen setzen sich die Wandthermostate
        immer wieder auf den Standard-Wert 21.0°. Ein Setzen dieses Hakens bewirkt, dass eine eingestellte Temperatur von
        21.0° vom Wandthermostat generell nicht ins System übernommen wird. Dies gilt auch für manuell gesetzte 21.0°.
        </small>
    </p>''' % (device.get_identifier(), device.get_identifier(), device.get_identifier(), " checked " if twentyonelocked else "")

                if device.protocol == 'zwave' or device.protocol == 'btle':
                    dev_specific += u'''<p>
                           <label for="%s_switchmode">
                           Temperatureinstellungen am Thermostat nur temporär anwenden</a></label>
                           <input type="checkbox" name="%s_switchmode" id="%s_switchmode" %s class="chg"/>
                           <small>Die Temperatureinstellung am Thermostat kann temporär oder dauerhaft übernommen werden <a href="https://support.controme.com/raumcontroller/#Temperatureinstellungen_nur_temporaer_anwenden" target="_blank">(weitere Infos)</a></small>
                       </p>''' % (device.get_identifier(), device.get_identifier(), device.get_identifier(),
                                  " checked " if switchmode else "")
            if device.type == 'hktControme':
                dev_specific += u'''<p>
                       <label for="%s_korrekturoffset">Korrekturoffset (<a href="https://support.controme.com/raumcontroller/#Korrekturoffset_ab_RC-V134" target="_blank">weitere Infos</a>)</label>
                       <input type="number" name="%s_sensoroffset" id="%s_sensoroffset" value="%s"
                        data-mini="true" step="0.1" class="chg"/>
                       <small>Manche Sensoren zeigen aufgrund äußerer Einflüsse
                       (z.B. Zugluft oder Montage an kühler Außenwand)
                       konstant zu hohe oder niedrige Werte.
                       Ein Korrekturoffset wird automatisch auf jeden übertragenen Sensorwert aufgeschlagen.</small>
                   </p>''' % (device.get_identifier(), device.get_identifier(),
                              device.get_identifier(), device.get_temperature_offset())
                if battery and battery != -1:
                    dev_specific += u'''<p>
                        <label for="%s_PermaIst">Helligkeit permanente Isttemperaturanzeige (<a href="https://support.controme.com/raumcontroller/#Helligkeit_permanente_Isttemperaturanzeige" target="_blank">weitere Infos</a>)</label>
                        <input type="number" name="%s_dispBright" id="%s_dispBright" value="%s" min="0" max="0" maxlength="1"
                            oninput="javascript: if (this.value.length > this.maxLength) this.value = this.value.slice(0, this.maxLength); (!validity.rangeOverflow||(value=0)) && (!validity.rangeUnderflow||(value=0)) && 
                            (!validity.stepMismatch||(value=parseInt(this.value)));" data-mini="true" step="1" class="chg"/>
                        <small>Helligkeit der dauerhaften IST-Temperatur-Anzeige. Einstellung von  0-30 möglich.  Wenn > 0 wird die IST-Temperatur dauerhaft angezeigt. <font color="#cb0963">Bei batteriebetriebenen Raumcontrollern bitte diesen Wert auf 0 lassen.</font></small>
                    </p>''' % (device.get_identifier(), device.get_identifier(),
                                device.get_identifier(), 0)
                    dev_specific += u'''<p>Aufwach- / Sende-Intervall in Sekunden (<a href="https://support.controme.com/raumcontroller/#Aufwach-_Sende-Intervall_in_Sekunden" target="_blank">weitere Infos</a>)</br>
                        <label for="%s_SendInterval"></label>
                        <input type="number" name="%s_sendInterval" id="%s_sendInterval" value="%s"  onfocusout="if(this.value<900)this.value=900;"
                            data-mini="true" step="1" class="chg"/>
                        <small style="color:#cb0963;">Bei batteriebetriebenen Raumcontrollern muss das Intervall auf > 15 Minuten (900) gestellt werden.</small>
                    </p>''' % (device.get_identifier(), device.get_identifier(),
                                device.get_identifier(), device.get_send_interval())
                else:
                    dev_specific += u'''<p>
                        <label for="%s_PermaIst">Helligkeit permanente Isttemperaturanzeige (<a href="https://support.controme.com/raumcontroller/#Helligkeit_permanente_Isttemperaturanzeige" target="_blank">weitere Infos</a>)</label>
                        <input type="number" name="%s_dispBright" id="%s_dispBright" value="%s" min="0" max="30" maxlength="2"
                            oninput="javascript: if (this.value.length > this.maxLength) this.value = this.value.slice(0, this.maxLength); (!validity.rangeOverflow||(value=30)) && (!validity.rangeUnderflow||(value=0)) && 
                            (!validity.stepMismatch||(value=parseInt(this.value)));" data-mini="true" step="1" class="chg"/>
                        <small>Helligkeit der dauerhaften IST-Temperatur-Anzeige. Einstellung von  0-30 möglich.  Wenn > 0 wird die IST-Temperatur dauerhaft angezeigt. <font color="#cb0963">Bei batteriebetriebenen Raumcontrollern bitte diesen Wert auf 0 lassen.</font></small>
                    </p>''' % (device.get_identifier(), device.get_identifier(),
                                device.get_identifier(), device.get_display_brightness())
                    dev_specific += u'''<p>Aufwach- / Sende-Intervall in Sekunden (<a href="https://support.controme.com/raumcontroller/#Aufwach-_Sende-Intervall_in_Sekunden" target="_blank">weitere Infos</a>)</br>
                        <label for="%s_SendInterval"></label>
                        <input type="number" name="%s_sendInterval" id="%s_sendInterval" value="%s"  min="0" max="1200" maxlength="4" onfocusout="if(this.value<60)this.value=60;"
                            oninput="javascript: if (this.value.length > this.maxLength) this.value = this.value.slice(0, this.maxLength); (!validity.rangeOverflow||(value=1200)) && (!validity.rangeUnderflow||(value=60)) && 
                            (!validity.stepMismatch||(value=parseInt(this.value)));" data-mini="true" step="1" class="chg"/>
                        <small style="color:#cb0963;">Einstellung von 60 - 1200 möglich. Bei batteriebetriebenen Raumcontrollern muss das Intervall auf > 15 Minuten (900) gestellt werden.</small>
                    </p>''' % (device.get_identifier(), device.get_identifier(),
                                device.get_identifier(), device.get_send_interval())
                dev_specific += u'''<p>Batterie-Sparfunktion: Temperaturabweichung für &Uuml;bertragung neuer Werte (ab Version 13.5, <a href="https://support.controme.com/raumcontroller/#Temperaturabweichung_fuer_Uebertragung_neuer_Werte" target="_blank">weitere Infos</a>)</br>
                       <label for="%s_Deviation"></label>
                       <input type="number" name="%s_deviation" id="%s_deviation" value="%s" min="0.0" max="0.5" maxlength="3"
                        oninput="javascript: if (this.value.length > this.maxLength) this.value = this.value.slice(0, this.maxLength); (!validity.rangeOverflow||(value=0.0)) && (!validity.rangeUnderflow||(value=0.5)) && 
                        (!validity.stepMismatch||(value=parseInt(this.value)));"
                        data-mini="true" step="0.1" class="chg"/>
                        <small style="color:#cb0963;">Einstellung von 0 - 0,5 möglich.</small>
                   </p>''' % (device.get_identifier(), device.get_identifier(),
                              device.get_identifier(), device.get_deviation())

                dev_specific += u'''<p>Batterie-Sparfunktion: Senden nach x Temperaturmessungen erzwingen (ab Version 13.5, <a href="https://support.controme.com/raumcontroller/#Senden_nach_x_Temperaturmessungen_erzwingen" target="_blank">weitere Infos</a>)</br>
                       <label for="%s_ForceWlan"></label>
                       <input type="number" name="%s_forceWlan" id="%s_forceWlan" value="%s" min="0" max="10" maxlength="2" onfocusout="if(this.value<1)this.value=1;"
                        oninput="javascript: if (this.value.length > this.maxLength) this.value = this.value.slice(0, this.maxLength); (!validity.rangeOverflow||(value=1)) && (!validity.rangeUnderflow||(value=10)) && 
                        (!validity.stepMismatch||(value=parseInt(this.value)));"
                        data-mini="true" step="1" class="chg"/>
                        <small style="color:#cb0963;">Einstellung von 1 - 10 möglich.</small>
                   </p>''' % (device.get_identifier(), device.get_identifier(),
                              device.get_identifier(), device.get_force_wlan())

                dev_specific += u'''<p>Deep Sleep (ab Version 13.7, <a href="https://support.controme.com/raumcontroller/#Deep-Sleep_EIN-AUS" target="_blank">weitere Infos</a>)</br>
                       <label for="%s_DeepSleep"></label>
                       <input type="checkbox" name="%s_deepSleep" id="%s_deepSleep" %s
                       class="chg"/>
                        <label for="%s_deepSleep">Deep Sleep</label>
                        <small>Bei eingeschaltetem Deep-Sleep geht der Raumcontroller zwischen den Senderintervallen in den Deep-Sleep-Modus. Bei batteriebetriebenen Raumcontrollern wird diese Einstellung ignoriert und ist fix auf EIN.</small>
                   </p>
                   ''' % (device.get_identifier(), device.get_identifier(),
                              device.get_identifier(), " checked " if device.get_deep_sleep() == 1 else "",device.get_identifier())

                dev_specific += u'''<p><label for="%s_Dynamic_bat_Saving_rc"></label>
                       <input type="checkbox" name="%s_dynamicBatSavingRc" id="%s_dynamicBatSavingRc" %s
                       class="chg"/>
                        <label for="%s_dynamicBatSavingRc">Energiesparmodus Batterie-Raumcontroller</label>
                        <small>Bei eingeschalteter Anpassung werden die Parameter Aufwachintervall, Temperaturabweichung und Sendeerzwingung intelligent der Betriebssituation angepasst um die Batterielaufzeit zu erhöhen (<a href="https://support.controme.com/raumcontroller/#Intelligente_Parameteranpassung_fuer_batteriebetriebene_Raumcontroller" target="_blank">weitere Infos</a>).</small>
                   </p>
                   ''' % (device.get_identifier(), device.get_identifier(),
                              device.get_identifier(), " checked " if device.get_bat_saving() == True else "", device.get_identifier())


        elif (isinstance(device, RFSensor) and device.type != 'cntct' and not device.type.startswith("pir")) or (isinstance(device, RFAktor) and device.type == "wt"):
            dev_specific += u'''<p>
    <label for="%s_mainsensor">Raumtemperatursensor</label>
    <input type="checkbox" name="%s_mainsensor" id="%s_mainsensor" class="chg" %s%s/>
    <small>Der Raumtemperatursensor liefert die für die jeweilige Regelung maßgebliche Temperatur für diesen Raum. Es kann pro Raum nur einen Raumtemperatursensor geben.</small>
</p>
<p>
    <label for="%s_atsensor">Aussentemperatursensor</label>
    <input type="checkbox" name="%s_atsensor" id="%s_atsensor" %s class="chg"/>
</p>''' % (device.get_identifier(), device.get_identifier(), device.get_identifier(), " checked " if device.mainsensor else "",
           " disabled='true' " if not device.raum_id or has_outs else "", device.get_identifier(), device.get_identifier(), device.get_identifier(), " checked " if device.get_identifier() == atsensor else "")

            if device.type in ['sensora50401', 'sensora50205', 'sensora50213', 'multisensor', 'sensor', 'wt']\
                    and device.get_temperature_offset() is not None:

                dev_specific += u'''<p>
                                   <label for="%s_korrekturoffset">Korrekturoffset</label>
                                   <input type="number" name="%s_sensoroffset" id="%s_sensoroffset" value="%s"
                                    data-mini="true" step="0.1" class="chg"/>
                                   <small>Manche Sensoren zeigen aufgrund äußerer Einflüsse
                                   (z.B. Zugluft oder Montage an kühler Außenwand)
                                   konstant zu hohe oder niedrige Werte.
                                   Ein Korrekturoffset wird automatisch auf jeden übertragenen Sensorwert aufgeschlagen.</small>
                               </p>''' % (device.get_identifier(), device.get_identifier(), device.get_identifier(),
                                          device.get_temperature_offset())
    if isWLAN:
        btry = ''
    else:
        btry = "<p>Batterie: %s&#37;</p>" % battery

    relaisstate = ""
    try:
        if device.type in ['relais', 'wp', 'wpd2010b']:
            if device.protocol == 'zwave':
                last_vals = ch.get_last_vals("%s_%s_val" % (device.controller.name, device.name))
                if last_vals and last_vals[0] and 'Switch' in last_vals[0]:
                    relaisstate = "<p>Schaltzustand: %s</p>" % ("Ein" if last_vals[0]['Switch'] else "Aus")
                else:
                    relaisstate = "<p>Schaltzustand: unbekannt</p>"
            elif device.protocol == 'enocean':
                last_vals = ch.get("enocean_relais_%s" % device.name.replace(':', '').lower())
                if last_vals is None or last_vals[0] is None:
                    pass
                else:
                    relaisstate = "<p>Schaltzustand: %s</p>" % ("Ein" if last_vals[0] > 0 else "Aus")
    except Exception as e:
        logging.warning("relaisstate exception: %s" % str(e))
        pass

    dellink = u'''<p><a href="#" onclick="sendChanges('%s', '%s_delete', this);">Gerät aus Liste entfernen</a></p>''' % (device.controller_id, device.get_identifier())
    
    teach_in = ""

    ihtml = html.format(identifier=device.get_identifier(), devicename=device.name, devicedescription="- %s" % device.description if device.description else "",
                        imarkings=imarkings, devicedescriptioninput=device.description, disabledifhasouts="disabled='true'" if has_outs else "",
                        typeoptions=typeoptions, raumoptions=raumoptions, dev_specific=dev_specific, last_ts=last if last else "-", rssi_time = rssi_data.get('time'), rssi_power = rssi_data.get('rssi'), etrv_setting = etrv_setting,
                        btry=btry if battery > -1 else relaisstate, neighbors=u"%s" % (("<p>Nachbarn: %s</p>" % neighbors) if device.protocol == 'zwave' else ""),
                        incomplete_data=incomplete_data, out_lock=out_lock if has_outs else "", dellink=dellink if (device.protocol == 'btle' or device.protocol == 'enocean' or device.controller.protocol == "btle" or device.controller.protocol == "enocean") else "",
                        lastsoll=(u" - %.2f&deg; " % lastsoll) if lastsoll != "-" else "")

    return ihtml


@csrf_exempt
def set_zwave(request, rpimac, controllerid, last_data=None):

    if (request is None and last_data is not None) or request.method == 'POST':

        if not last_data:
            ch.set_gw_ping(controllerid)

        controller = RFController.get_controller(rpimac, controllerid, "zwave")
        if controller is None:
            return HttpResponse()

        if request:
            data = controller.decrypt_data(request.POST.get("data", ""))
        else:
            data = last_data
        if data is None:
            return HttpResponse()

        logging.warning("set_zwave received %s" % data)

        aktoren = RFAktor.objects.filter(controller=controller)
        sensoren = RFSensor.objects.filter(controller=controller)
        devices = list(chain(aktoren, sensoren))

        # nur setzen, wenns nicht intern kam und wenns nicht leer war
        if isinstance(data, dict) and data.values() and len(data.values()[0]):
            controller.update_last_seen(data, last_data=last_data is not None)

        device_names = set()

        for device in devices:

            if device.name in data.keys():
                device_names.add(device.name)

                devparams = device.get_parameters()
                try:
                    if len(data[device.name]['manufacturer_id']) and (not len(devparams.get('manufacturer_id', '')) or device.type == 'undef') and request:
                        _d = _get_device(data[device.name], device.name, controller)
                        _d.raum_id = device.raum_id
                        _d.description = device.description
                        _d.save()
                        device.delete()
                        device = _d
                        devparams['manufacturer_id'] = data[device.name]['manufacturer_id']
                        devparams['product_id'] = data[device.name]['product_id']
                        devparams['product_type'] = data[device.name]['product_type']
                        device.set_parameters(devparams)
                except KeyError:
                    pass

                try:

                    device.update_device(data[device.name], last_data=last_data is not None)

                except KeyError:  # wenn ein Geraet grade nicht uebertragen hat
                    pass

        diffset = set(data.keys()) - device_names - set(["1"])
        for d in diffset:

            device = _get_device(data[d], d, controller)

            try:
                if len(data[device.name]) and not last_data:
                    ch.set_gw_ping(controllerid+'_'+device.name)
            except KeyError:
                logging.warning("keyerror2!")
                pass
            device.save()

    return HttpResponse()


def _get_device(data, d, controller):
    _d = None
    try:
        _d = RFAktor.objects.get(controller=controller, name=d)
        if _d.type != 'undef':
            return _d
    except RFAktor.DoesNotExist:
        pass
    try:
        _d = RFSensor.objects.get(controller=controller, name=d)
        if _d.type != 'undef':
            return _d
    except RFSensor.DoesNotExist:
        pass

    if data.get('manufacturer_id') == "0148" and data.get('product_type') == "0001" and data.get('product_id') == "0001":
        # stella z
        device = RFAktor(name=d, protocol='zwave', type='hkt', haus=controller.haus, controller=controller, parameters='{}')
    elif data.get('manufacturer_id') == "010f" and data.get('product_type') == "0600" and data.get('product_id') == "1000":
        # fibaro fgwpe
        device = RFAktor(name=d, protocol='zwave', type='wp', haus=controller.haus, controller=controller, parameters='{}')
    elif data.get('manufacturer_id') == "010f" and data.get('product_type') == "0800" and data.get('product_id') == "1001":
        # fibaro fgms
        device = RFSensor(name=d, protocol='zwave', type='multisensor', haus=controller.haus, controller=controller, parameters='{}')
    elif data.get('manufacturer_id') == "013c" and data.get('product_type') == "0002" and data.get('product_id') == "0002":
        # philio psm02 ms
        device = RFSensor(name=d, protocol='zwave', type='multisensor', haus=controller.haus, controller=controller, parameters='{}')
    elif data.get('manufacturer_id') == "013c" and data.get('product_type') == "0001" and data.get('product_id') == "0003":
        # philio relais pan04-1
        device = RFAktor(name=d, protocol='zwave', type='relais', haus=controller.haus, controller=controller, parameters='{}')
    elif data.get('manufacturer_id') == "013c" and data.get('product_type') == "0001" and data.get('product_id') == "0013":
        # philio relais pan06-1
        device = RFAktor(name=d, protocol='zwave', type='relais', haus=controller.haus, controller=controller, parameters='{}')
    elif data.get('manufacturer_id') == "0086" and data.get('product_type') == "0002" and data.get('product_id') == "0005":
        # aeon labs ms
        device = RFSensor(name=d, protocol='zwave', type='multisensor', haus=controller.haus, controller=controller, parameters='{}')
    elif data.get('manufacturer_id') == "0086" and data.get('product_type') == "0002" and data.get('product_id') == "004a":
        # aeon labs ms nextgen
        device = RFSensor(name=d, protocol='zwave', type='multisensor', haus=controller.haus, controller=controller, parameters='{}')
    elif data.get('manufacturer_id') == "0086" and data.get('product_type') == "0002" and data.get('product_id') == "0064":
        # aeon labs ms nextgen
        device = RFSensor(name=d, protocol='zwave', type='multisensor', haus=controller.haus, controller=controller, parameters='{}')
    elif data.get('manufacturer_id') == "010f" and data.get('product_type') == "0c00" and data.get('product_id') == "1000":
        # fibaro rauchmelder
        device = RFSensor(name=d, protocol='zwave', type='multisensor', haus=controller.haus, controller=controller, parameters='{}')
    elif data.get('manufacturer_id') == "0002" and data.get('product_type') == "0005" and data.get('product_id') == "0004":
        # dlc
        device = RFAktor(name=d, protocol='zwave', type='hkt', haus=controller.haus, controller=controller, parameters='{}')
    elif data.get('manufacturer_id') == "0002" and data.get('product_type') == "0005" and data.get('product_id') == "0003":
        # dlc
        device = RFAktor(name=d, protocol='zwave', type='hkt', haus=controller.haus, controller=controller, parameters='{}')
    elif data.get('manufacturer_id') == "0086" and data.get('product_type') == "0004" and data.get('product_id') == "0025":
        # aeon labs range extender
        device = RFAktor(name=d, protocol='zwave', type='repeater', haus=controller.haus, controller=controller, parameters='{}')
    elif data.get('manufacturer_id') == "0002" and data.get('product_type') == "0003" and data.get('product_id') == "8010":
        # danfoss zwavers
        device = RFAktor(name=d, protocol='zwave', type='wt', haus=controller.haus, controller=controller, parameters='{}')

    elif 'values' in data:
        device = RFSensor(name=d, protocol='zwave', type='undef', haus=controller.haus,
                          controller=controller, parameters='{}')
    else:
        device = RFAktor(name=d, protocol='zwave', type='undef', haus=controller.haus,
                         controller=controller, parameters='{}')

    if not ('values' in data or 'value' in data or 'battery' in data or 'Switch' in data):
        device.hidden = True

    if _d:
        device.id = _d.id

    if device.type == "hkt":
        modules = controller.haus.get_modules()
        if 'funksollregelung' not in modules:
            modules += ['funksollregelung']
            controller.haus.set_modules(modules)

        aktoren = RFAktor.objects.filter(haus=controller.haus, type__startswith="hkt")
        if len(aktoren):
            params = device.get_parameters()
            oparams = aktoren[0].get_parameters()
            params['wakeup'] = oparams.get('wakeup', 900)
            device.set_parameters(params)
        else:
            params = device.get_parameters()
            params['wakeup'] = 900
            device.set_parameters(params)

        if device.raum_id:
            regelung = device.raum.get_regelung()
            if regelung.regelung != "funksollregelung":
                if regelung.get_ausgang():
                    device.raum = None
                    device.save()
                else:
                    regelung.regelung = "funksollregelung"
                    params = {'active': True, 'rsfaktor': 0.0, 'rsactive': True}
                    regelung.set_parameters(params)

                    # falls das vorher noch eine normale Regelung mit AAusgaengen war
                    ausgang = regelung.get_ausgang()
                    if ausgang:
                        ausgang.regelung = None
                        ausgang.save()

                    regelung.save()

    elif device.type == "multisensor":
        sensoren = RFSensor.objects.filter(haus=controller.haus, type="multisensor")
        if len(sensoren):
            params = device.get_parameters()
            oparams = sensoren[0].get_parameters()
            params['wakeup'] = oparams.get('wakeup', 900)
            device.set_parameters(params)
        else:
            params = device.get_parameters()
            params['wakeup'] = 900
            device.set_parameters(params)

    device.save()
    return device


@csrf_exempt
def get_zwave(request, controllerid):

    if request.method == "GET":

        controller = RFController.get_controller(None, controllerid, "zwave")
        if controller is None:
            return HttpResponse()

        haus = controller.haus
        ret = dict()

        for dev in list(chain(RFAktor.objects.filter(type__startswith="hkt", controller_id=controller.id), RFAktor.objects.filter(haus=haus, type='wt', controller_id=controller.id))):
            ret[dev.name] = dev.get_device_settings()

        for rfausgang in RFAusgang.objects.filter(controller_id=controller.id):
            ret.update(rfausgang.get_output_settings())

        for relais in RFAktor.objects.filter(controller=controller, type='relais'):
            if relais.name not in ret.keys():
                ret.setdefault(relais.name, dict())
                ret[relais.name][0x25] = ("", 0)

        settings = controller.get_controller_settings()
        ret.update(settings)

        logging.warning("get_zwave returning %s" % str(ret))

        try:
            return HttpResponse(controller.rpi.crypt_keys.encrypt(json.dumps(ret)))
        except AttributeError:
            return HttpResponse(json.dumps(ret))

    elif request.method == "POST":
        logging.critical(request.POST)
        return HttpResponse("")


@csrf_exempt
def set_btle(request, rpimac, controllerid, last_data=None):

    if (request is None and last_data is not None) or request.method == "POST":

        if not last_data:
            ch.set_gw_ping(controllerid)

        controller = RFController.get_controller(rpimac, controllerid, "btle")
        if controller is None:
            return HttpResponse()

        if request:
            data = controller.decrypt_data(request.POST.get("data", ""))
        else:
            data = last_data
        if data is None:
            return HttpResponse()

        logging.warning("set_btle received %s as POST" % str(data))

        if isinstance(data, dict) and len(data.values()[0]) and request is not None:
            controller.update_last_seen(data)

        aktoren = RFAktor.objects.filter(controller=controller)
        sensoren = RFSensor.objects.filter(controller=controller)
        devices = list(chain(aktoren, sensoren))

        device_names = set()

        for device in devices:

            if device.name not in data.keys():
                pass

            else:
                device_names.add(device.name)
                cparams = controller.get_parameters()
                prevls = cparams['last_data'].get(device.name, dict()).get('prevls')
                ctrl_prevls = cparams.get('prevls')

                if device.type.startswith('hkt'):
                    device.update_device(data[device.name], prevlseq=ctrl_prevls == prevls, last_data=last_data is not None)

                else:
                    # vorerst mal. falls ein hkt nicht gleich erkannt wird oder hkt abgewaehlt wird.
                    last_vals = {'datetime': data[device.name].get('datetime', [0, 0, 0, 0, 0])}
                    ch.set_new_vals("%s_%s_val" % (controllerid, device.name), last_vals, controller.haus_id)

        diffset = set(data.keys()) - device_names
        for d in diffset:
            devices = list(chain(RFAktor.objects.filter(name=d), RFSensor.objects.filter(name=d)))
            if len(devices):
                # exists for a different controller
                continue
            device = _get_btle_device(data[d], d, controller)  # legt an
            if device and data[d].get('datetime', [0, 0, 0, 0, 0]) != [0, 0, 0, 0, 0]:
                ch.set_gw_ping("%s_%s" % (controllerid, d))

    return HttpResponse()


def _get_btle_device(data, d, controller):

    if not d.count(':') == 5:
        return None
    dev = None

    try:
        dev = RFAktor.objects.get(name=d)
        return dev
    except RFAktor.DoesNotExist:
        pass

    try:
        dev = RFSensor.objects.get(name=d)
        return dev
    except RFSensor.DoesNotExist:
        pass

    if data.get('manufacturer') == "EUROtronic GmbH" and data.get('device') == "Comet Blue":
        dev = RFAktor(name=d, description='', type='hktGenius', parameters='{}', protocol='btle', haus=controller.haus, controller=controller)
        dev.save()
    elif data.get('manufacturer') == "Controme GmbH" and data.get('device') == "Controme RBDG10":
        dev = RFAktor(name=d, description='', type='hktControme', parameters='{}', protocol='btle', haus=controller.haus, controller=controller)
        dev.save()
    elif data.get('manufacturer') == "Eurotronic GmbH" and data.get('device') == "Comet Blue":
        dev = RFAktor(name=d, description='', type='hkt', parameters='{}', protocol='btle', haus=controller.haus, controller=controller)
        dev.save()
    elif data.get('manufacturer') == "Danfoss" and data.get('device') == "eTRV":
        if 'secret' in data and data['secret'] is not None and len(data['secret']):
            dev = RFAktor(name=d, description='', type='hkteTRV', parameters='{"secret": %s}' % data['secret'], protocol='btle', haus=controller.haus, controller=controller)
            dev.save()
        else:
            return None

    return dev


def get_btle(request, controllerid):
    if request.method == "GET":

        controller = RFController.get_controller(controllerid, controllerid, "btle")
        if controller is None:
            return HttpResponse()

        ret = {'data': {}}

        aktoren = RFAktor.objects.filter(controller=controller)
        for aktor in aktoren:
            ret['data'][aktor.name] = aktor.get_device_settings()

        settings = controller.get_controller_settings()
        ret.update(settings)

        logging.warning("get_btle returning %s" % ret)

        try:
            return HttpResponse(controller.rpi.crypt_keys.encrypt(json.dumps(ret)))
        except AttributeError:
            return HttpResponse(json.dumps(ret))

def get_hci(request):
    res = os.popen('hcitool dev')
    devices = res.readlines()
    connected_devices = '\n'.join(str(p.strip()) for p in devices[1:])
    if len(connected_devices) != 0:
        marker = 'green'
        return HttpResponse('green')
    return HttpResponse('red')

@csrf_exempt
def set_enocean(request, rpimac, controllerid):

    if request.method == "POST":
        ch.set_gw_ping(controllerid)

        controller = RFController.get_controller(rpimac, controllerid, "enocean")
        if controller is None:
            return HttpResponse()

        data = controller.decrypt_data(request.POST.get("data", ""))
        if data is None:
            return HttpResponse()

        logging.warning("set_enocean received %s as POST" % str(data))
        modules = controller.haus.get_modules()

        for deviceid, values in data.items():

            if len(RFController.objects.filter(name=deviceid.lower())):
                continue

            device = list(chain(RFAktor.objects.filter(name=deviceid.lower()),
                                RFSensor.objects.filter(name=deviceid.lower())))
            
            if len(device):
                device = device[0]
            else:
                
                rf_params = controller.get_parameters()
                if not rf_params.get('discovery', True):
                    return HttpResponse()

                params = {'teach_in': values.get('teach_in', False)}
                if values.get('description') == 'Contact' or values.get('description') == 'Sensor' or values.get('rorg') == 165:
                    device = RFSensor(name=deviceid.lower(), controller=controller, type='undef', protocol='enocean', haus=controller.haus)
                    device.save()
                elif values.get('description') == 'Output value':
                    device = RFAktor(name=deviceid.lower(), controller=controller, type='undef', protocol='enocean', haus=controller.haus, parameters=str(params))  # nicht optimal, weil es die rorg ignoriert. ist aber via enocean_handler.py im moment eh nur 0xd2
                    device.save()
                else:
                    continue

            ch.set_gw_ping("%s_%s" % (controllerid.lower(), deviceid.lower()))

            if device.type == 'cntct':
                ch.set("enocean_cntct_%s" % deviceid.replace(':', '').lower(), (values.get('value'), datetime.utcnow()))
                if device.raum_id:
                    ch.delete("%s_roffsets_dict" % device.raum_id)
                if values.get('value') == 'open':
                    try:
                        local("/usr/bin/python /home/pi/rpi/telegrambot/bot.py %s %s" % (device.controller.haus_id, device.raum_id))
                    except:
                        pass
            elif device.type == 'relais':
                ch.set("enocean_relais_%s" % deviceid.replace(':', '').lower(), (values.get('raw_value'), datetime.utcnow()))
                params = device.get_parameters()
                if (isinstance(values.get('raw_value'), int) and min(1, values.get('raw_value')) == params.get('last_val')) or values.get('ack'):
                    params['last_ack'] = True
                    params['last_ack_val'] = min(1, values.get('raw_value'))
                    # hier koennte man noch den ts speichern
                    device.set_parameters(params)
                    logger.warning("0|%s|%s,%s" % (controllerid.lower(), device.name, params['last_ack_val']))
            elif device.type.startswith('sensor'):
                offset = device.get_temperature_offset()
                if values.get("Temperature") is not None:
                    ch.set_new_vals("%s_%s_val" % (controller.name, deviceid), values, controller.haus_id)  # rfdevice.get_wert() zaehlt den offset selber dazu
                    sig_new_temp_value.send(sender="tset", name=device.get_identifier(), value=values["Temperature"] + offset, modules=modules, timestamp=datetime.now())
                    logger.warning("0|%s|%s,%s" % (controllerid.lower(), device.name, values["Temperature"] + offset))
                else:
                    vals = ch.get_last_vals("%s_%s_val" % (controller.name, deviceid))
                    ch.set_new_vals("%s_%s_val" % (controller.name, deviceid), vals[0] if vals is not None else None, controller.haus_id)
            elif device.type.startswith("hkt"):
                t, cv = values.get('Temperature'), values.get('CV')
                if t is not None and cv is not None:
                    last = u"VOM GERÄT: Isttemperatur %.2f°C; Ventilstellung: %s%%;" % (t, cv)
                    if device.type == "hkta52001":
                        last += " Batterie geladen: "
                        if 'sufficiently_charged' in values:
                            if values['sufficiently_charged'] == 'true':
                                last += 'ja;'
                            else:
                                last += 'nein;'
                        else:
                            last += '-'
                        last += " Energy Harvesting aktiv: "
                        if 'currently_charging' in values:
                            if values['currently_charging'] == 'true':
                                last += 'ja'
                            else:
                                last += 'nein'
                        else:
                            last += '-'
                    offset = device.get_temperature_offset()
                    sig_new_temp_value.send(sender="tset", name=device.get_identifier(), value=values["Temperature"] + offset, modules=modules, timestamp=datetime.now())
                    logger.warning(u"0|%s|%s,%s" % (device.raum_id if device.raum else "0", device.name, last))
                    ch.set_new_vals("%s_%s_val" % (controllerid.lower(), deviceid), values, controller.haus_id)

            elif device.type.startswith("pir"):
                logger.warning(u"%s|%s Bewegungsmelder %s" % (device.raum_id if device.raum else "0", device.name, values.get('Alarm', 0)))
                if device.raum_id and 'praesenzregelung' in device.raum.get_modules():
                    ch.delete("%s_roffsets_dict" % device.raum_id)
                if values.get('Alarm') == 1:
                    try:
                        local("/usr/bin/python /home/pi/rpi/telegrambot/bot.py %s %s" % (device.controller.haus_id, device.raum_id))
                    except:
                        pass
                ch.set_new_vals("%s_%s_val" % (controllerid.lower(), deviceid), values, controller.haus_id)

    return HttpResponse()


def get_enocean(request, controllerid):

    if request.method == "GET":
        try:
            ctrl = RFController.objects.get(name=controllerid.lower())
        except RFController.DoesNotExist:
            return HttpResponse("", status=404)

        aktoren = RFAktor.objects.filter(controller_id=ctrl.id)
        ausgaenge = RFAusgang.objects.filter(controller_id=ctrl.id)
        ret = {'data': {}}
        now = datetime.now()
        mods = ctrl.rpi.haus.get_modules()

        for ausgang in ausgaenge:

            try:
                val = ausgang.regelung.do_rfausgang(ctrl.id)  # todo das geht aktuell von zpr oder rlr aus
            except Exception as e:
                logging.exception("error getting do_rfausgang out for reg.id==%s" % ausgang.regelung_id)
                val = 0

            for a_id in ausgang.aktor:

                try:
                    aktor = aktoren.get(pk=a_id)
                except RFAktor.DoesNotExist:
                    continue

                raum = ausgang.regelung.get_raum()

                if raum is not None:
                    clear_offsets = get_module_offsets(raum, only_clear_offsets=True)
                    ziel = raum.solltemp + clear_offsets
                    params = ausgang.regelung.get_parameters()
                    params.update({'regelung': ausgang.regelung.regelung})
                    sig_new_output_value.send(sender='get_enocean', name='%s_%s' % (controllerid.lower(), aktor.name), value=val, ziel=ziel, parameters=params, timestamp=now, modules=mods)

                if aktor.type == 'relais':
                    ret['data'][aktor.name] = {'val': val, 'rorg': 0xd2, 'rorg_func': 0x01, 'rorg_type': 0x01}
                    params = aktor.get_parameters()
                    val = min(val, 1)
                    if params.get('last_val') != val:
                        params['last_val'] = val
                        params['last_ack'] = False
                        params['last_ack_val'] = None
                        aktor.set_parameters(params)

                elif aktor.type == 'wp':
                    ret['data'][aktor.name] = {'val': val, 'rorg': 0xd2, 'rorg_func': 0x01, 'rorg_type': 0x01}

                elif aktor.type == 'wpd2010b':
                    ret['data'][aktor.name] = {'val': val, 'rorg': 0xd2, 'rorg_func': 0x01, 'rorg_type': 0x01}

        for aktor in aktoren:
            if aktor.type == "hkta52001" and aktor.raum_id:

                # todo können wir stattdessen fsr.do_rfausgang aufrufen?

                soll = aktor.raum.solltemp
                offset = get_module_offsets(aktor.raum)
                ziel = soll + offset
                ms = aktor.raum.get_mainsensor()
                msval = 0x00
                if ms and ms != aktor:
                    msval = ms.get_wert()
                    marker = ms.get_marker()
                    if msval and marker == "green":
                        msval = msval[0]
                        last_vals = ch.get_last_vals("%s_%s_val" % (controllerid.lower(), aktor.name))
                        if msval - ziel > 2 and isinstance(last_vals, tuple) and int(last_vals[0].get('CV', 0)) > 0:
                            ziel = -99
                    else:
                        msval = 0x00  # wegen der green bedingung
                ret['data'][aktor.name] = {'SP': ziel, 'TMP': msval, 'rorg': 0xa5, 'rorg_func': 0x20, 'rorg_type': 0x01}
                params = aktor.raum.get_regelung().get_parameters()
                params.update({'regelung': 'funksollregelung'})
                sig_new_output_value.send(sender='get_enocean', name='%s_%s' % (controllerid.lower(), aktor.name), value=ret['data'][aktor.name]['SP'], ziel=soll+offset, parameters=params, timestamp=now, modules=mods)

        ret['devicedef'] = {}
        for sensor in RFSensor.objects.filter(controller_id=ctrl.id):
            if sensor.type == 'cntct':
                continue
            if sensor.type.startswith('sensor'):
                ret['devicedef'][sensor.name] = {'rorg_func': int(sensor.type[8:10], 16), 'rorg_type': int(sensor.type[10:], 16)}
            if sensor.type.startswith('pir'):
                ret['devicedef'][sensor.name] = {'rorg_func': 0x07, 'rorg_type': 0x01}

        logging.warning("get_enocean returning %s" % ret)
        
        try:
            return HttpResponse(ctrl.rpi.crypt_keys.encrypt(json.dumps(ret)))
        except AttributeError:
            return HttpResponse(json.dumps(ret))


@csrf_exempt
def set_rc(request, rcid):

    if request.method == "POST":

        try:
            rc = RFAktor.objects.get(name=rcid.lower().replace('-', ':'), type="hktControme")
        except RFAktor.DoesNotExist:
            return HttpResponse()
        try:
            body = json.loads(request.body)['data']
        except Exception:
            return HttpResponse(status=403)
        logger.warning("set_rc received %s" % str(body))

        haus = Haus.objects.first()
        haus_params = haus.get_module_parameters()
        max_allowed_rc = haus_params.get('max_allowed_rc_soll')
        if body.get('soll') and max_allowed_rc:
            if float(body.get('soll')) <= float(max_allowed_rc):
                logger.warning("set_rc received and it is less than maximum.")
            else:
                logger.warning("set_rc received but it is greater than maximum.")
                return HttpResponse()

        ist = body.get('ist')
        offset = body.get('offset','0')
        tdata = rc.get_device_settings()
        # correct ist because controm rc is already handling it.
        ist = float(ist) - float(offset);
        bat_saving_status = rc.get_device_settings().get('dynamic_bat_saving_rc')
        global_test_mode = haus_params.get('global_tests', {}).get('rc_global_test_mode', False)
        if bat_saving_status or global_test_mode:
            try:
                room_id = rc.raum_id
                raum = Raum.objects.get(id=int(room_id))
                ms = raum.get_mainsensor()
                mstemp = ms.get_wert()
                mstemp = mstemp[0]
                offset = get_module_offsets(raum)
                temp_solltemp = raum.solltemp
                deviation = mstemp - offset - temp_solltemp
                deviation = abs(deviation) - 1
                textfield_deviation = rc.get_parameters().get('tdeviation',0)
                final_deviation = deviation - textfield_deviation
                if final_deviation <= 0:
                    final_deviation = textfield_deviation
                else:
                    final_deviation = max(deviation, textfield_deviation)
                logger.warning("set_rc dyn_deviation %s-%s-%s." % (deviation, textfield_deviation, final_deviation))
            except:
                final_deviation = tdata.get('deviation','0')
                temp_solltemp = 'kein Daten'

            try:
                textfield_interval = rc.get_parameters().get('tsendeintervall',60)
                deviation = round(deviation)
                if deviation == 0:
                    interval = 0.25
                elif deviation <= 1:
                    interval = 0.5
                else:
                    interval = deviation
                interval_seconds = interval * 3600
                if interval_seconds < textfield_interval:
                    interval_seconds = textfield_interval
                elif interval_seconds > 21600:
                    interval_seconds = 21600
                url = "http://127.0.0.1/get/json/v1/1/tempforecast/"
                response = requests.get(url)
                data = response.json()
                target_room_number = room_id
                next_due_time_rel_list = [int(room["next_due_time_rel"].replace("+", "")) for scene in data for room in scene["raeume"] if room["room-number"] == target_room_number and room["next_due_time_rel"] is not None]
                if next_due_time_rel_list:
                    min_next_due_time_rel = min(next_due_time_rel_list)
                    next_due_time_rel = min_next_due_time_rel * 3600
                    if next_due_time_rel < interval_seconds:
                        final_interval = next_due_time_rel
                    else:
                        final_interval = interval_seconds
                else:
                    final_interval = interval_seconds
                    min_next_due_time_rel = "aktuell aktiv"
                logger.warning("set_rc dyn_interval %s-%s-%s-%s-%s." % (textfield_interval, deviation, interval_seconds, next_due_time_rel, final_interval))
            except:
                final_interval = tdata.get('sendeintervall')
                min_next_due_time_rel = "kein Daten"

            try:
                max_value = 21600
                result = max_value / final_interval
                rounded_result = round(result)
                if rounded_result >= 1:
                    final_send =  rounded_result
                else:
                    final_send = 1
                logger.warning("set_rc force_send_wlan %s-%s-%s." % (result, rounded_result, final_send))
            except:
                final_send = tdata.get('force_send_wlan',1)

        data = {'ist': str(ist), 'soll': body.get('soll'), 'WLAN': True, 'force_send_wlan': tdata.get('force_send_wlan',1),'deep_sleep': tdata.get('deepSleep',0),'deviation': tdata.get('deviation','0'), 'battery': body.get('battery'), 'userinput': body.get('userinput', False), 'sleep_delay': tdata.get('sleep_delay'), 'version': body.get('version'), 'feuchte': body.get('feuchte')}
        # only take offset from rc if we don't have set it before. otherwise rc could override our offset before we are able to send it to rc (rc first sends data and after that tries to get new data)

        if tdata.get('offset','0') == '0':
            data['offset'] = body.get('offset','0')
        else:
            data['offset'] = tdata.get('offset','0')
        logging.warning("set_rc received %s" % data)
        rc.update_device(data, last_data=False)

        if bat_saving_status:
            try:
                existing_params = rc.get_parameters()
                existing_params['dyn_deviation'] = final_deviation
                existing_params['dyn_sendeintervall'] = final_interval
                existing_params['dyn_force_send_wlan'] = final_send
                rc.set_parameters(existing_params)
                rc.save()
                try:
                    mode = BaseMode.get_active_mode_raum(raum)
                    active_mode_name = mode['activated_mode_text']
                except:
                    active_mode_name = "kein Modus"
                last = u"Dynamische Batteriespar-Parameter ZUM GERÄT: Temperaturabweichung: %s° --> %s°; Aufwachintervall: %ss --> %ss; Senderzwingung: %s --> %s; Maßgebliche Solltemperatur: %s / %s / +%sh." %(existing_params.get('tdeviation'), 
                round(final_deviation, 2), existing_params.get('tsendeintervall'), int(final_interval) if final_interval == int(final_interval) else round(final_interval, 2), existing_params.get('tforce_send_wlan',1), int(final_send) if final_send == int(final_send) else round(final_send, 2), temp_solltemp, active_mode_name, min_next_due_time_rel)
                logger.warning("0|%s|%s,%s" % (RFController.objects.get(id=rc.controller_id).name, rc.name, last))
            except:
                logging.warning("error in get_rc_new")

        elif global_test_mode:
            try:
                existing_params = rc.get_parameters()
                try:
                    mode = BaseMode.get_active_mode_raum(raum)
                    active_mode_name = mode['activated_mode_text']
                except:
                    active_mode_name = "kein Modus"
                last = u"Dynamische Batteriespar-Parameter ZUM GERÄT (Test): Temperaturabweichung: %s° --> %s°; Aufwachintervall: %ss --> %ss; Senderzwingung: %s --> %s; Maßgebliche Solltemperatur: %s / %s / +%sh." %(existing_params.get('tdeviation'), 
                round(final_deviation, 2), existing_params.get('tsendeintervall'), int(final_interval) if final_interval == int(final_interval) else round(final_interval, 2), existing_params.get('tforce_send_wlan',1), int(final_send) if final_send == int(final_send) else round(final_send, 2), temp_solltemp, active_mode_name, min_next_due_time_rel)
                logger.warning("0|%s|%s,%s" % (RFController.objects.get(id=rc.controller_id).name, rc.name, last))
            except:
                logging.warning("error in global test get_rc_new")

        return HttpResponse()


def get_rc(request, rcid):
    
    if request.method == "GET":

        try:
            rc = RFAktor.objects.get(name=rcid.lower().replace('-', ':'), type="hktControme")
        except RFAktor.DoesNotExist:
            return HttpResponse()

        ret = {'data': {}}
        ret['data'][rc.name.replace(':', '-')] = rc.get_device_settings()
        logging.warning("get_rc returning %s" % ret)
        return HttpResponse(json.dumps(ret))

@csrf_exempt
def set_secret(request):
    try:
        dev = RFAktor.objects.get(name=str(request.POST.get("pair_id")))
        params = dev.get_parameters()
        params['secret'] = json.loads(request.POST.get("sec_data"))
        dev.set_parameters(params)
    except:
        dev = json.loads(request.POST.get("dev"))
        controllerid = str(request.POST.get("rpimac"))
        controller_device = RFController.get_controller(controllerid, controllerid, "btle")
        dev2 = RFAktor(name=dev.get('name'), description=dev.get('description'), type=dev.get('type'), parameters= dev.get('parameters'), protocol=dev.get('protocol'), haus_id=controller_device.haus.id, controller_id=controller_device.id)
        dev2.save()
    local('sudo supervisorctl -c /etc/supervisor/supervisord.conf restart newbtle')
    return HttpResponse()


def get_secret(request, pairid):
    try:
        dev = RFAktor.objects.get(name=str(pairid))
        params = dev.get_parameters()
        secret = params.get('secret', 'No secret code')
        return HttpResponse(json.dumps(secret))
    except:
        return HttpResponse('Error in get secret request!')

@csrf_exempt
def del_secret(request):
    dev = RFAktor.objects.get(name=str(request.POST.get("pair_id")))
    params = dev.get_parameters()
    params['secret'] = {}
    dev.set_parameters(params)
    local('sudo supervisorctl -c /etc/supervisor/supervisord.conf restart newbtle')
    return HttpResponse()

@csrf_exempt
def get_settings(request):
    pair_id = str(request.GET.get('pair_id')).replace(':', '_')  # Get the pair_id from the request's query parameters
    tupple = request.GET.get('tupple')  # Get the tupple from the request's query parameters
    ch.set('etrv_settings_%s' % (pair_id), str(tupple))
    return HttpResponse(pair_id)

@csrf_exempt
def get_rssi(request):
    pair_id = str(request.GET.get('pair_id')).replace(':', '_')
    current_time = request.GET.get('current_time')
    rssi = request.GET.get('rssi')
    ch.set('ble_rssi_%s' % (pair_id), {'time': current_time, 'rssi': rssi})
    return HttpResponse(pair_id)