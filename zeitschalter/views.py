# -*- coding: utf-8 -*-
from heizmanager.render import render_response, render_redirect
import pytz
from datetime import datetime, timedelta
import heizmanager.cache_helper as ch
from pytz.tzinfo import StaticTzInfo
from heizmanager.models import Haus, Raum

'''
Zeitschalter funktioniert so:
+ es gibt einen hausweiten Default, der in den Settings eingestellt wird.
+ das Modul steuert aber raumweise, d.h. wir koennen pro Raum schauen, ob
    'abwesenheit' in raum.modules steht und dann ggf. den zeitoffset, solloffset
    und startzeit aus raum.module_parameters holen. Alles natuerlich auch cachen.
'''

def get_name():
    return u'Zeitschalter'


def get_cache_ttl():
    return 1800


def is_togglable():
    return True


def is_hidden_offset():
    return False


def get_offset(haus, raum=None):

    if raum:
        params = raum.get_module_parameters()
    else:
        params = haus.get_module_parameters()

    offset = params.get('zeitschalter', dict()).get('offset', 0.0)
    ttl = get_cache_ttl()*10
    if offset != 0.0 and 'end' in params['zeitschalter']:
        berlin = pytz.timezone('Europe/Berlin')
        if datetime.now(berlin) > load_datetime(params['zeitschalter']['end']):
            params['zeitschalter']['offset'] = 0.0
            params['zeitschalter']['end'] = ''
            offset = 0.0
            if raum:
                raum.set_module_parameters(params)
            else:
                haus.set_module_parameters(params)

        else:
            delta = (load_datetime(params['zeitschalter']['end']) - datetime.now(berlin))
            ttl = delta.days*86400 + delta.seconds  # setzen bis zum ende des zeitschalters

    if raum:
        ch.set_module_offset_raum('zeitschalter', haus.id, raum.id, offset, ttl=ttl)
    else:
        ch.set_module_offset_haus('zeitschalter', haus.id, offset, ttl=ttl)

    return offset


def get_global_settings_link(request, haus):
    return "<a href='/m_setup/%s/zeitschalter/'>Zeitschalter</a>" % haus.id


def get_global_description_link():
    desc = None
    desc_link = None
    return desc, desc_link


def get_global_settings_page(request, haus):
    params = haus.get_module_parameters()
    if request.method == "GET":
        return render_response(request, "m_settings_zeitschalter.html",
                               {"haus": haus, "default": params.get('zeitschalter', dict()).get('default', 0.0)})
    
    if request.method == "POST":
        default = request.POST['defaultoffset']
        if not len(default):
            error = "Bitte Standard Offset eingeben."
            return render_response(request, 'm_settings_zeitschalter.html', {"haus": haus, "error": error})
        params['zeitschalter']['default'] = float(default)
        haus.set_module_parameters(params)
        return render_redirect(request, '/m_setup/%s/zeitschalter/' % haus.id)


def get_global_settings_page_help(request, haus):
    return render_response(request, "m_help_zeitschalter.html", {'haus': haus})


def get_local_settings_link_haus(request, haus):
    params = haus.get_module_parameters()
    a = '<span class="ui-li-aside" style="margin-top:0px;">inaktiv</span>'
    if params.get('zeitschalter', dict()).get('offset', 0.0) > 0.0:
        a = '<span class="ui-li-aside" style="margin-top:0px;color:#cb0963">aktiv</span>'
    if params.get('zeitschalter', dict()).get('offset', 0.0) > 0.0:
        a = '<span class="ui-li-aside" style="margin-top:0px;color:blue">aktiv</span>'
    return ["<li data-role='list-divider'>Abwesenheitsschalter</li>",
            "<li style='max-height:43px'>"
            "<a style='max-height:20px' href='/m_zeitschalter/%s/'>Abwesenheitsschalter %s</a></li>" % (haus.id, a)]


def get_local_settings_page_haus(request, haus):
    if request.method == "GET":
        if 'asdel' in request.GET and request.user.userprofile.get().role != 'K':
            _remove_zeitschalter(haus)
            ch.set_module_offset_haus('zeitschalter', haus.id, 0.0, ttl=get_cache_ttl())
            return render_redirect(request, '/')
        
        else:
            default, deltastr = _get_duration_string(haus)
            if len(deltastr):
                deltastr += "<a href='/m_zeitschalter/%s/?asdel'>l&ouml;schen</a></li>" % haus.id
            else:
                params = haus.get_module_parameters()
                default = params['zeitschalter']['default']
            zeitd, zeith = _get_zeit_select()
            return render_response(request, 'm_zeitschalter.html',
                                   {"haus": haus, "zeitd": zeitd, "zeith": zeith,
                                    'delta': deltastr, 'default': default})
    
    elif request.method == "POST" and request.user.userprofile.get().role != 'K':
        d = int(request.POST['zeitd'])
        h = float(request.POST['zeith'])
        o = request.POST['offset'].replace(',', '.')
        
        error = ''
        if d == 0 and h == 0.0:
            error += "Bitte Zeit ausw&auml;hlen.<br/>"
        if not len(o):
            error += "Bitte Offset eingeben."
        else:
            o = float(o)
        
        if error:
            zeitd, zeith = _get_zeit_select()
            return render_response(request, 'm_zeitschalter.html',
                                   {'haus': haus, 'zeitd': zeitd, 'zeith': zeith,
                                    'error': error, 'o': o, 'hh': h, 'dd': d})
        
        _set_zeitschalter(haus, o, d, h)
        ch.set_module_offset_haus('zeitschalter', haus.id, o, ttl=get_cache_ttl())

        for e in haus.etagen.all():
            for rid in Raum.objects.filter_for_user(request.user, etage_id=e.id).values_list('id'):
                ch.delete("%s_roffsets_dict" % rid[0])
        
        return render_redirect(request, '/')


def get_local_settings_link(request, raum):
    rparams = raum.get_module_parameters()
    hparams = raum.etage.haus.get_module_parameters()
    offset = rparams['zeitschalter']['offset'] + hparams['zeitschalter']['offset']
    if offset != 0.0:
        return "Zeitschalter", "%.2f" % offset, 75, "/m_raum/%s/zeitschalter/" % raum.id
    else:
        return "Zeitschalter", "deaktiviert", 75, "/m_raum/%s/zeitschalter/" % raum.id


def get_local_settings_page(request, raum):
    if request.method == "GET":
        
        if 'zsdel' in request.GET and request.user.userprofile.get().role != 'K':
            _remove_zeitschalter(raum)
            ch.set_module_offset_raum('zeitschalter', raum.etage.haus.id, raum.id, 0.0, ttl=get_cache_ttl())
            ch.delete("%s_roffsets_dict" % raum.id)
            return render_redirect(request, '/m_raum/%s/' % raum.id)
        
        else:
            deltastr = []
            default, raumstr = _get_duration_string(raum)
            if len(raumstr):
                raumstr += "<a href='/m_raum/%s/zeitschalter/?zsdel'>l&ouml;schen</a></li>" % raum.id
            else:
                params = raum.etage.haus.get_module_parameters()
                default = params['zeitschalter']['default']
            deltastr.append(raumstr)
            throwaway, hausstr = _get_duration_string(raum.etage.haus)
            hausstr = hausstr.replace('Zeitschalter', 'Abwesenheitsschalter')
            hausstr += '</li>'
            deltastr.append(hausstr)
            
            zeitd, zeith = _get_zeit_select()
            return render_response(request, "m_raum_zeitschalter.html",
                                   {"raum": raum, "zeitd": zeitd, "zeith": zeith,
                                    'delta': deltastr, 'default': default})
    
    if request.method == "POST" and request.user.userprofile.get().role != 'K':
        d = int(request.POST['zeitd'])
        h = float(request.POST['zeith'])
        o = request.POST['offset']
        
        error = ''
        if d == 0 and h == 0.0:
            error += "Bitte Zeit ausw&auml;hlen.<br/>"
        if not len(o):
            error += "Bitte Offset eingeben."
        else:
            o = float(o)
        
        if error:
            zeitd, zeith = _get_zeit_select()
            return render_response(request, 'm_raum_zeitschalter.html',
                                   {"raum": raum, 'zeitd': zeitd, 'zeith': zeith,
                                    'error': error, 'o': o, 'hh': h, 'dd': d})
        
        _set_zeitschalter(raum, o, d, h)
        ch.set_module_offset_raum('zeitschalter', raum.etage.haus.id, raum.id, o, ttl=get_cache_ttl())
        ch.delete("%s_roffsets_dict" % raum.id)
        
        return render_redirect(request, '/m_raum/%s/' % raum.id)


def _get_duration_string(obj):
    deltastr = ''
    params = obj.get_module_parameters()
    offset = params['zeitschalter']['offset']
    if offset != 0.0:
        berlin = pytz.timezone('Europe/Berlin')
        delta = (load_datetime(params['zeitschalter']['end']) - datetime.now(berlin))
        days = delta.days
        hours, remainder = divmod(delta.seconds, 3600)
        minutes, seconds = divmod(remainder, 60)
        deltastr = "<li><a><h2>%s &deg;C bis</h2><p>%s Tage, %s Stunden, %s Minuten</p></a>" \
                   % (offset, days, hours, minutes)
    return offset, deltastr


def _remove_zeitschalter(hausoderraum):
    params = hausoderraum.get_module_parameters()
    params['zeitschalter']['offset'] = 0.0
    params['zeitschalter']['end'] = ''
    hausoderraum.set_module_parameters(params)
    return


def _set_zeitschalter(raum, offset, days, hours):
    params = raum.get_module_parameters()
    params['zeitschalter']['offset'] = offset
    params['zeitschalter']['end'] = _parse_duration(days, hours)
    raum.set_module_parameters(params)
    return


def _parse_duration(days, hours):
    if str(hours).endswith(".5"):
        hours = int(str(hours).split('.')[0])
        minutes = 30
    else:
        hours = int(hours)
        minutes = 0
    berlin = pytz.timezone('Europe/Berlin')
    end = datetime.now(berlin) + timedelta(days=days, hours=hours, minutes=minutes)
    return dump_datetime(end)


def _get_zeit_select():
    zeitd = list()
    zeitd.append((1, "1 Tag"))
    for i in range(2, 32):
        zeitd.append((i, "%s Tage" % i))
    zeith = list()
    zeith.append((1.0, "1 Stunde"))
    zeith.append((1.5, "1,5 Stunden"))
    for i in range(2, 24):
        zeith.append((i, "%s Stunden" % i))
        zeith.append((float(str(i)+".5"), "%s.5 Stunden" % i))
    return zeitd, zeith


class OffsetTime(StaticTzInfo):
    def __init__(self, offset):
        """A dumb timezone based on offset such as +0530, -0600, etc."""
        hours = int(offset[:3])
        minutes = int(offset[0] + offset[3:])
        self._utcoffset = timedelta(hours=hours, minutes=minutes)


def load_datetime(value):
    fmt = '%Y-%m-%d %H:%M:%S %z'
    fmt = fmt[:-2]
    offset = value[-5:]
    value = value[:-5]
    return OffsetTime(offset).localize(datetime.strptime(value, fmt))


def dump_datetime(value):
    fmt = '%Y-%m-%d %H:%M:%S %z'
    return value.strftime(fmt)