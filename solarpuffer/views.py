# -*- coding: utf-8 -*-
from heizmanager.render import render_response, render_redirect
from heizmanager.models import Sensor, GatewayAusgang, Gateway, Regelung, AbstractSensor
import logging
import heizmanager.cache_helper as ch
from django.http import HttpResponse
import wetter.views as wetter
from operator import itemgetter
from heizmanager.getandset import get_gwout_by_no


def get_name():
    return u"Solarpuffer"


def managesoutputs():
    return True


def is_togglable():
    return True


def is_hidden_offset():
    return False


def get_cache_ttl():
    return 900


def get_offset(haus, raum=None):
    offset = 0.0
    if raum and 'solarpuffer' in raum.get_modules() and _is_active(haus):
        params = raum.get_module_parameters()
        offset = params.get('solarpuffer', dict()).get('offset', 0.0)

        ch.set_module_offset_raum('solarpuffer', haus.id, raum.id, offset, ttl=get_cache_ttl())

    return offset


def do_ausgang(regelung, ausgang):
    # solarpuffer ist ein hybrid: offset fuer die raeume, gleichzeitig wird auch ein ausgang geschalten.

    gw = regelung.gatewayausgang.gateway
    try:
        haus = gw.haus
    except AttributeError:
        return HttpResponse('')

    if _is_active(haus):
        return HttpResponse('<1>')
    else:
        return HttpResponse('<0>')


def _is_active(haus):

    # helfer fuer get_offset() und do_ausgang().
    #  checkt, ob die solarpuffersteuerung allgemein aktiv ist, d.h.
    #  ob die deaktivierungsschwelle nicht unterschritten ist und ob
    #  die maximaltemperatur in den naechsten tagen nicht hoeher ist
    #  als der aktivitaetsgrenzenparameter.

    if 'solarpuffer' in haus.get_modules():
        params = haus.get_module_parameters()

        try:
            s = AbstractSensor.get_sensor(params['solarpuffer']['puffersensor'])
            if s:
                last = s.get_wert()
            else:
                last = None

            if last and last[0] > params['solarpuffer']['lower_threshold']:
                lookahead = min(72, 12*params['solarpuffer']['lookahead'])
                forecast = wetter.get_10dayforecast(haus, lookahead)
                if forecast and float(max(map(itemgetter(1), forecast))) < params['solarpuffer']['max_forecast_temp']:
                    return True

        except (AttributeError, KeyError, TypeError):
            logging.warning('solarpuffer::_is_active() in haus %s' % haus.id)
            pass

    return False


def get_outputs(haus, raum, regelung):

    belegte_ausgaenge = dict()
    if 'solarpuffer' not in haus.get_modules():
        return belegte_ausgaenge

    params = haus.get_module_parameters()
    if 'solarpuffer' not in params:
        return belegte_ausgaenge

    sensor = Sensor()
    if params['solarpuffer']['puffersensor']:
        identifier = params['solarpuffer']['puffersensor']
        s = AbstractSensor.get_sensor(identifier)
        sensor.id = s.id
    sensor.name = "Solarpuffer"
    sensor.raum = None

    try:
        if params['solarpuffer']['ausgang'] is None:
            raise AttributeError
        ausgang = GatewayAusgang.objects.get(pk=params['solarpuffer']['ausgang'])
    except (GatewayAusgang.DoesNotExist, AttributeError):
        ausgang = None

    if ausgang:
        belegte_ausgaenge[ausgang.gateway] = [(int(ausgang.ausgang), ausgang, sensor, False)]

    return belegte_ausgaenge


def get_global_settings_link(request, haus):
    return "<a href='/m_setup/"+str(haus.id)+"/solarpuffer/'>Solarpuffer</a>"


def get_global_description_link():
    desc = u"Gebäudemasse als Speicher für kostenlose Energie der Solaranlage nutzen."
    desc_link = "http://support.controme.com/auswahl-und-aktivierung-von-modulen/modul-solarpuffer/"
    return desc, desc_link


def get_global_settings_page(request, haus):
    if request.method == "GET":
        active = False
        if 'solarpuffer' in haus.get_modules():
            active = True

        params = haus.get_module_parameters()
        if 'solarpuffer' in params:
            lower_threshold = params['solarpuffer']['lower_threshold']
            lookahead = params['solarpuffer']['lookahead']
            max_forecast_temp = params['solarpuffer']['max_forecast_temp']
            try:
                if params['solarpuffer']['ausgang'] is None:
                    raise AttributeError
                ausgang = GatewayAusgang.objects.get(pk=params['solarpuffer']['ausgang'])
            except (GatewayAusgang.DoesNotExist, AttributeError):
                ausgang = None
            identifier = params['solarpuffer']['puffersensor']
            sensor = AbstractSensor.get_sensor(identifier)
        else:
            lower_threshold = 75
            lookahead = 1
            max_forecast_temp = 10
            ausgang = None
            sensor = None

        sensors_info = AbstractSensor.get_all_sensors_info(exclude=['RFAktor', 'RFSensor', 'VSensor', 'KNXSensor'])

        return render_response(request, 'm_settings_solarpuffer.html',
                                {'haus': haus, 'sensors_info': sensors_info, 'active': active, 'lower_threshold': lower_threshold,
                                 'lookahead': lookahead, 'max_forecast_temp': max_forecast_temp, 'ausgang': ausgang,
                                 'sensor': sensor})

    elif request.method == "POST":

        params = haus.get_module_parameters()

        try:
            lower_threshold = float(request.POST['lower_threshold'])
        except ValueError:
            lower_threshold = 75
        lookahead = int(request.POST['lookahead'])
        try:
            max_forecast_temp = float(request.POST['max_forecast_temp'])
        except ValueError:
            max_forecast_temp = 10

        identifier = request.POST.get('puffersensor', '')
        sensor = AbstractSensor.get_sensor(identifier)

        a = request.POST['ausgang']
        ausgang = None
        alter_ausgang = params.get('solarpuffer', dict()).get('ausgang', None)
        if a != '0':
            gwid = a.split('_')[0]
            a = a.split('_')[1]
            gw = Gateway.objects.get(pk=long(gwid))
            ausgang = get_gwout_by_no(gw.name, a)
            if ausgang is None:
                reg = Regelung(regelung='solarpuffer', parameter=str({}))
                reg.save()
                ausgang = GatewayAusgang(gateway=gw, ausgang=a, stellantrieb='NC', regelung=reg)
                ausgang.save()
            else:
                ausgang.gateway = gw
                ausgang.ausgang = a
                if not ausgang.regelung:  # kann passieren beim loeschen
                    reg = Regelung(regelung='solarpuffer', parameter=str({}))
                    reg.save()
                    ausgang.regelung = reg
                ausgang.save()

            if alter_ausgang != ausgang.id:  # aufraeumen
                try:
                    alter_ausgang = GatewayAusgang.objects.get(pk=alter_ausgang)
                    alter_ausgang.regelung.delete()
                    alter_ausgang.delete()
                except (GatewayAusgang.DoesNotExist, Exception):  # da kommt manchmal ein DatabaseError?!
                    pass

        else:  # aufraeumen, wenn der zu aktivierende Ausgang weggenommen wurde
            if alter_ausgang:
                a_a = GatewayAusgang.objects.get(pk=long(alter_ausgang))
                a_a.get_regelung().delete()
                a_a.delete()

        if 'solarpuffer' not in haus.get_modules():
            haus.set_modules(haus.get_modules() + ['solarpuffer'])

        params.setdefault('solarpuffer', dict())
        params['solarpuffer']['lower_threshold'] = lower_threshold
        params['solarpuffer']['lookahead'] = lookahead
        params['solarpuffer']['max_forecast_temp'] = max_forecast_temp
        params['solarpuffer']['puffersensor'] = identifier if sensor else None
        params['solarpuffer']['ausgang'] = ausgang.id if ausgang is not None else None
        haus.set_module_parameters(params)

        return render_redirect(request, '/m_setup/%s/solarpuffer/' % haus.id)


def get_global_settings_page_help(request, haus):
    return render_response(request, "m_help_solarpuffer.html", {'haus': haus})


# die global_settings_page soll noch einen aktivierungsbutton a la zeitschalter bekommen
# deswegen auch ggf die aktuelle puffertemperatur auf der einstellungsseite anzeigen.
def get_local_settings_link_haus(request, haus):
    pass


def get_local_settings_page_haus(request, haus):
    pass


def get_local_settings_link(request, raum):

    # das zeigt einfach den voreingestellten absenkungswert an, wenn aktiv.

    haus = raum.etage.haus
    if 'solarpuffer' not in haus.get_modules():
        return ''

    offset = 'nicht aktiv'

    if _is_active(haus):
        offset = raum.get_module_parameters().get('solarpuffer', dict()).get('offset', 0.0)

    return "Solarpuffer", offset, 75, "/m_raum/%s/solarpuffer/" % raum.id


def get_local_settings_page(request, raum):
    if request.method == "GET":
        active = False
        if 'solarpuffer' in raum.get_modules():
            active = True
        offset = raum.get_module_parameters().get('solarpuffer', dict()).get('offset', 0.0)
        return render_response(request, 'm_raum_solarpuffer.html',
                               {'raum': raum, 'offset': offset, 'active': active})

    elif request.method == "POST":
        if 'solarpuffer' in request.POST and request.POST['solarpuffer'] == '1':
            params = raum.get_module_parameters()
            params.setdefault('solarpuffer', dict())
            try:
                offset = float(request.POST['offset'])
            except ValueError:
                offset = 0.0
            params['solarpuffer']['offset'] = offset
            raum.set_module_parameters(params)

            if 'solarpuffer' not in raum.get_modules():
                raum.set_modules(raum.get_modules() + ['solarpuffer'])

        else:
            if 'solarpuffer' in raum.get_modules():
                mods = raum.get_modules()
                mods.remove('solarpuffer')
                raum.set_modules(mods)

        return render_redirect(request, '/m_raum/%s/' % raum.id)