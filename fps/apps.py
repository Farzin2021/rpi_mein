from django.apps import AppConfig


class FPSConfig(AppConfig):
    name = 'fps'
    verbose_name = 'FPS'

    def ready(self):
        from . import signals
