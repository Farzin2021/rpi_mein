from django.apps import AppConfig


class FensterOffenConfig(AppConfig):
    name = 'fenster_offen'
    verbose_name = 'Fensteroffen'

    def ready(self):
        from . import signals
