# -*- coding: utf-8 -*-

from heizmanager.render import render_response, render_redirect
from django.contrib.auth.models import User
from heizmanager.models import Haus, RPi, CryptKeys
from django.contrib.auth.decorators import login_required
from django.contrib.auth import authenticate, login
from models import UserProfile
from django.contrib.auth import logout
import heizmanager.network_helper as nh


def loginview(request):

    if request.method == "GET":
        users = User.objects.all()
        registr = False
        if not len(users):  # noch nichts in der db
            registr = True
        return render_response(request, "m_login.html", {'register': registr})

    elif request.method == "POST":
        email = request.POST['mail'].decode('utf-8').lower().strip()
        passwort = request.POST['pw'].strip() if type(request.POST['pw']) == unicode else request.POST['pw'].decode('utf-8').strip()

        usr = authenticate(username=email, password=passwort)
        if usr is not None:
            login(request, usr)
            return render_redirect(request, "/")
        else:
            return render_response(request, 'm_login.html', {'error': "Benutzername und/oder Passwort ungültig.",
                                                             'register': False})

    else:
        return render_redirect(request, "/accounts/m_login/")


def index(request):
    return render_redirect(request, "/")


def logout_view(request):
    logout(request)
    return render_redirect(request, "/")


#@login_required(login_url='/accounts/m_login/')
def get_global_settings_page(request, haus):
    usr = request.user
    profile = UserProfile.objects.get(user=usr)
    if request.method == "GET":
        return render_response(request, "m_user.html", {"haus": haus, "usr": usr, "profile": profile})

    elif request.method == "POST" and not usr.email == 'test@controme.com':
        user = User.objects.get(username=usr.username)
        if 'che' in request.POST:
            pw = request.POST['pw'].decode('utf-8')
            mail = request.POST['mail'].decode('utf-8').lower()
            if len(mail) > 30:
                cheerror = 'Emailadresse darf maximal 30 Zeichen lang sein.'
                return render_response(request, 'm_user.html', {"haus": haus, "usr": usr,
                                                                "profile": profile, "mail": mail,
                                                                "cheerror": cheerror})
            if not usr.check_password(pw):
                cheerror = "Falsches Passwort."
                return render_response(request, "m_user.html", {"haus": haus, "usr": usr,
                                                                "profile": profile, "mail": mail,
                                                                "cheerror": cheerror})
            if len(User.objects.filter(email=mail)) or len(User.objects.filter(username=mail)):
                cheerror = "Emailadresse wird bereits von einem anderen Nutzer verwendet."
                return render_response(request, "m_user.html", {"haus": haus, "usr": usr,
                                                                "profile": profile, "mail": mail,
                                                                "cheerror": cheerror})
            user.email = mail
            user.username = mail
            user.save()
            request.user = user
        elif 'chp' in request.POST:
            oldpw = request.POST['oldpw']
            newpw1 = request.POST['newpw1']
            newpw2 = request.POST['newpw2']
            if not usr.check_password(oldpw):
                chperror = "Altes Passwort falsch."
                return render_response(request, "m_user.html", {"haus": haus, "usr": usr,
                                                                "profile": profile, "chperror": chperror})
            if newpw1 != newpw2:
                chperror = "Neue Passwörter stimmen nicht überein."
                return render_response(request, "m_user.html", {"haus": haus, "usr": usr,
                                                                "profile": profile, "chperror": chperror})
            usr.set_password(newpw1)
            usr.save()
        return render_redirect(request, '/m_setup/%s/users/' % haus.id)


def reset_password(request):
    if request.method == "GET":
        return render_response(request, 'm_login_recoverpw.html')

    elif request.method == "POST":

        verificationcode = request.POST['ver_code'].decode('utf-8')
        if not len(RPi.objects.all()):
            macaddr = nh.get_mac()
            from rpi.aeskey import key
            from rpi.verification_code import verification_code
            ck = CryptKeys()
            ck.aeskey = key
            ck.save()
            haus = Haus.objects.filter()
            if len(haus):
                haus = haus[0]
            else:
                haus = None
            rpi = RPi(crypt_keys=ck, verification_code=verification_code, name=macaddr, upgrade_version="",
                      local_address="", global_address="", haus=haus)
            rpi.save()
        rpi = RPi.objects.all()[0]
        if not verificationcode or verificationcode != rpi.verification_code:
            return render_response(request, "m_login_recoverpw.html", {'reseterror': 'Ungültiger Verifikationscode.'})

        lostmail = request.POST['recmail'].decode('utf-8').lower()

        reset_usr = None
        for usr in User.objects.all():
            if usr.username != usr.username.lower():
                usr.username = usr.username.lower()
                usr.save()
            if usr.email != usr.email.lower():
                usr.email = usr.email.lower()
                usr.save()

            if usr.username == lostmail:
                reset_usr = usr
                break
        else:
            return render_response(request, "m_login_recoverpw.html", {'reseterror': 'Unbekannter Benutzer.'})

        pw = request.POST['password'].decode('utf-8')
        pw2 = request.POST['pw2'].decode('utf-8')
        if pw != pw2:
            return render_response(request, "m_login_recoverpw.html",
                                   {'reseterror': 'Passwörter stimmen nicht überein.'})

        reset_usr.set_password(pw)
        reset_usr.save()

        return render_redirect(request, "/accounts/m_login/")


def register(request):

    if request.method == "GET":
        if len(User.objects.all()):
            return render_redirect(request, "/accounts/m_login/")
        return render_response(request, "m_register.html")

    elif request.method == "POST":
        try:
            usr = User.objects.get(username=request.POST['email'].decode('utf-8').lower())
            return render_response(request, "m_register.html", {'error': 'Emailadresse ist bereits registriert.',
                                                                'register': True, 'e': request.POST['email']})
        except User.DoesNotExist:
            error = None
            if not len(request.POST['email']):
                error = 'Bitte Emailadresse eingeben.'

            if len(request.POST['email']) > 30:  # django beschraenkung auf benutzernamenlaenge
                error = 'Emailadresse darf nur 30 Zeichen lang sein.'

            if not len(request.POST['password']) or request.POST['password'] != request.POST['pw2']:
                error = 'Passwort falsch eingegeben.'

            if error:
                return render_response(request, "m_register.html",
                                       {'error': error, 'register': True, 'e': request.POST.get('email', '')})

            email = request.POST['email'].strip().lower()
            email = email.decode('utf-8') if type(email) != unicode else email
            pw = request.POST['password']
            pw = pw.decode('utf-8') if type(pw) != unicode else pw

            usr = User.objects.create_user(email, email, pw)

            usr.is_active = True
            usr.save()  # legt UserProfile an

            if len(User.objects.all()) == 1:
                usr.userprofile.get().role = 'A'
                usr.userprofile.get().save()

            return render_redirect(request, '/accounts/m_login/')