# -*- coding: utf-8 -*-

import logging
from heizmanager.render import render_response, render_redirect
from heizmanager.models import Haus, RPi
import heizmanager.cache_helper as ch
from django.http import HttpResponse
import json
import re
from heizmanager.network_helper import get_mac
from django.http import JsonResponse
import requests


def get_name():
    return u'ApiQuery'


def get_global_settings_link(request, haus):
    return "<a href='/m_setup/%s/api_query/'>API-Query</a>" % str(haus.id)


def get_global_description_link():
    desc = u"Abfrage von APIs auf anderen Miniservern"
    desc_link = ""
    return desc, desc_link


def get_providing_api_modules_dict():
    return {
        'fps': 'FPS',
        'pumpenlogik': 'Raumanforderung',
        'vorlauftemperaturkorrektur': 'Vorlauftemperaturkorrektur',
    }


def prepare_request_to_execute(request_data, post_data=None):

    host = request_data.get('host')
    request_type = request_data.get('request_type')
    logging.warning(request_data)

    if host != 'custom' and request_type == 'get':
        data_module = request_data.get('data_module')
        url = 'https://%s/get/json/v1/1/%s/' % (host, data_module)
        # url = 'http://%s:8000/get/json/v1/1/%s/' % (host, data_module)
        if int(request_data.get('data_object', -1)) != -1:
            url = url + "%s/" % request_data.get('data_object')
        return url, 'get', dict()

    elif host != 'custom' and request_type == 'post':
        return

    elif host == 'custom' and request_type == 'get':
        return

    elif host == 'custom' and request_type == 'post':
        return

    return


def execute_request(url, method, data):
    if method == 'get':
        try:
            r = requests.get(url=url, timeout=10)
        except Exception:  # todo error handling & reporting
            logging.exception("e_r")
            return
        else:
            if r.status_code == 404 and "fwd.controme.com" in url:
                url.replace("/v1/1/", "/v1/2/")
                try:
                    r = requests.get(url=url, timeout=10)
                except Exception:  # todo error handling & reporting
                    logging.exception("e_r")
                    return
    else:
        try:
            r = requests.post(url=url, data=data, timeout=10)
        except:
            return
    return r.json()


def get_global_settings_page(request, haus, action=None, entityid=None):

    if not isinstance(haus, Haus):
        try:
            haus = Haus.objects.get(pk=long(haus))
        except Haus.DoesNotExist:
            return HttpResponse("Error", status=400)

    if request.method == "GET":
        context = dict()
        context['haus'] = haus
        api_params = haus.get_spec_module_params('api_query')
        api_param_items = api_params.get('items', dict())

        if action == 'edit':
            mac = get_mac()
            hosts = ["%s%s%s.fwd.controme.com" % (r.name[-8:-6], r.name[-5:-3], r.name[-2:]) for r in RPi.objects.all() if r.name != mac]
            context['hosts'] = hosts
            # context['hosts'] = ['localhost']

            modules = get_providing_api_modules_dict().items()
            context['data_modules'] = modules

            if entityid:
                api_param = api_param_items.get(entityid)
                context['api_param'] = api_param
                context['api_id'] = entityid
                # load objects by executing the saved query
                ####
                d_o_swp = api_param['data_object']
                api_param['data_object'] = -1
                data_to_send = prepare_request_to_execute(api_param, api_param.get('post_parameters', dict()))
                if data_to_send is not None:
                    url, method, data = data_to_send
                    ret = execute_request(url, method, data)
                    if ret is not None:
                        context['data_objects'] = ret
                        logging.warning(ret)
                api_param['data_object'] = d_o_swp
                ####
                post_parameters = api_param.get('post_parameters', dict())
                if post_parameters:
                    context['post_parameters'] = json.loads(post_parameters).items()
            return render_response(request, "m_settings_api_query_edit.html", context)

        elif action == 'delete':
            del api_param_items[entityid]
            api_params['items'] = api_param_items
            haus.set_spec_module_params('api_query', api_params)
            return render_redirect(request, '/m_setup/%s/api_query/' % haus.id)

        else:
            context['api_param_items'] = api_param_items.items()
            context['api_params'] = api_params

        return render_response(request, "m_settings_api_query.html", context)

    elif request.method == "POST":
        if action == "edit":

            #  preparing data
            data_d = dict()
            data_d['name'] = request.POST.get('name', '')
            data_d['data_type'] = request.POST.get('data_type', '')
            host = request.POST.get('host', '')
            data_d['host'] = host
            request_type = request.POST.get('request_type', '')
            data_d['request_type'] = request_type
            if host == 'custom':
                data_d['custom_host'] = request.POST.get('custom_host')
                if request_type == 'get':
                    query_param = request.POST.get('query_params')
                    if re.match("^\\?([\\w-]+(=[\\w-]*)?(&[\\w-]+(=[\\w-]*)?)*)?$", query_param):
                        data_d['query_params'] = request.POST.get('query_params')
                    else:
                        return render_response(request, "m_settings_api_query_edit.html",
                                               {'error': 'Query parameter format error '})

            if request_type == 'post':
                data_type = request.POST.get('data_type', '')
                data_d['data_type'] = data_type
                if data_type == 'parametric':
                    data_d['parameters_number'] = request.POST.get('parameters_number', '')
                    data_d['post_parameters'] = request.POST.get('post_parameters', '')
                else:
                    data_d['post_json'] = request.POST.get('post_json', '')

            data_d['data_module'] = request.POST.get('data_module')
            data_d['data_object'] = request.POST.get('data_object')
            interval = int(request.POST.get('interval', 900))
            if not 60 <= interval <= 86400:
                interval = 900
            data_d['interval'] = interval

            # get existing data or define new id
            api_params = haus.get_spec_module_params('api_query')
            api_param_items = api_params.get('items', dict())

            # get a new id
            if entityid is None:
                ids = [0] + [int(k) for k in api_param_items.keys()]
                entityid = str(max(ids) + 1)
                api_param_items[entityid] = dict()

            # saving data
            api_param_items[entityid].update(data_d)
            api_params['items'] = api_param_items
            haus.set_spec_module_params('api_query', api_params)

            cache_key = 'api_query_%s_%s' % (entityid, haus.id)
            data_to_send = prepare_request_to_execute(data_d)
            if data_to_send is not None:
                url, method, data = data_to_send
                ret = execute_request(url, method, data)
                if ret:
                    ch.set(cache_key, ret, time=interval)

            return render_redirect(request, "/m_setup/%s/api_query/" % haus.id)

        elif action == 'execute_query':
            # cleaning data
            parameters_list = json.loads(request.POST['api_query_data'])
            frm_data = parameters_list.get('frm_data')
            post_data = parameters_list.get('post_params')
            frm_data = dict([(item['name'], item['value']) for item in frm_data])

            data_to_send = prepare_request_to_execute(frm_data, post_data)
            if data_to_send is not None:
                url, method, data = data_to_send
                ret = execute_request(url, method, data)
                if ret:
                    return JsonResponse(ret)
            return JsonResponse({}, status=400)

        return render_redirect(request, '/config')


def get_module_variables(haus):
    api_params = haus.get_spec_module_params('api_query')
    api_param_items = api_params.get('items', dict())

    module_variables = list()
    for item_id, item_d in api_param_items.items():
        cache_key = 'api_query_%s_%s' % (item_id, haus.id)
        query_name = item_d.get('name', '')
        value = None
        cached_query = ch.get(cache_key)
        if cached_query is not None:
            object_id = item_d.get('data_object', '')
            value = cached_query.get(object_id, dict()).get('value')
        module_variables.append((item_id, query_name, value))

    return [{'module_name': 'api_query', 'variables': module_variables,
                            'verbose_module_name': get_name()}]


def get_global_settings_page_help(request, haus):
    pass


def get_local_settings_page_haus(request, haus):
    pass


def get_local_settings_link(request, raum):
    pass


def get_local_settings_page(request, raum):
    pass


def deactivate(hausoderraum):
    mods = hausoderraum.get_modules()
    try:
        mods.remove('api_query')
        hausoderraum.set_modules(mods)
    except ValueError:  # nicht in modules
        pass


def activate(hausoderraum):
    if not 'api_query' in hausoderraum.get_modules():
        hausoderraum.set_modules(hausoderraum.get_modules() + ['api_query'])
