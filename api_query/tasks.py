from celery import shared_task
from heizmanager.models import Haus
import heizmanager.cache_helper as ch
from views import execute_request, prepare_request_to_execute


@shared_task()
def periodic_query_execution():
    for haus in Haus.objects.all():
        # module activation check
        if 'api_query' in haus.get_modules():
            api_params = haus.get_spec_module_params('api_query')
            api_param_items = api_params.get('items', dict())
            for key, item_d in api_param_items.items():
                interval = int(item_d.get('interval', 900))
                cache_key = 'api_query_%s_%s' % (key, haus.id)
                if ch.get(cache_key) is not None:
                    continue

                data_to_send = prepare_request_to_execute(item_d)
                if data_to_send is not None:
                    url, method, data = data_to_send
                    ret = execute_request(url, method, data)
                    if ret:
                        ch.set(cache_key, ret, time=interval)


