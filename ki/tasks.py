# -*- coding: utf-8 -*-
import os
from celery import shared_task
from datetime import datetime, timedelta
from heizmanager.models import Haus
from .views import calc_datapoint, linear_regression
import pytz
import csv
import ast
from fabric.api import local
from heizmanager.mobile.m_error import CeleryErrorLoggingTask
from django.http import HttpResponse
try:
    import pandas as pd
except ImportError:
    pd = None
import heizmanager.cache_helper as ch


base_addr = "/home/pi/rpi/usbmount/"


class DataNotFoundError(Exception):
    pass


def save_to_csv(file_addr, cols, rows_lst, append=True):

    try:
        local("ls %s| grep db_log" % base_addr, capture=True)
    except:
        raise DataNotFoundError('no data found on usb')

    try:
        open(file_addr)
    except IOError:
        append = False

    write_mode = 'a' if append else 'w'
    with open(file_addr, write_mode) as csvout:
        writer = csv.writer(csvout)
        if not append:
            writer.writerow(cols)
        for row in rows_lst:
            writer.writerow(row)
    return True


def calc_data_point_process(hausid, raumid):
    berlin = pytz.timezone('Europe/Berlin')
    berlin_now = datetime.now(berlin)
    ret_tup = calc_datapoint(hausid, raumid, 1)
    rows_lst = []
    if ret_tup is None:
        return None

    # calculate ranking
    at = ret_tup[0]["AT"]
    it = ret_tup[0]["IT"]
    out = ret_tup[0]["OUT"]
    ranking = out * ((it - at) / 10000)

    # prepare a list to write as a row in csv
    row_lst = [berlin_now.strftime('%Y-%m-%d'), hausid, raumid, ranking]
    row_lst = row_lst + list(ret_tup)[0:-1]
    try:
        row_lst[4] = dict((k, round(v, 2)) for k, v in row_lst[4].items())
    except TypeError:
        pass
    rows_lst.append(row_lst)
    return rows_lst


def delete_oldest_row_per_room(file_addr):
    try:
        old_time = datetime.today() - timedelta(days=13)  # Die room_ranking.csv hat eine Länge von 14 Tagen
        old_time = old_time.strftime('%Y-%m-%d')
        df = pd.read_csv(file_addr)
        df = df[df.time != old_time]
        df.to_csv(file_addr, index=False)
    except:
        return


@shared_task()
def periodic_calc_data_point():
    '''
    This method calculates the room_ranking.csv with 14 1-day values of all rooms.
    '''
    rows_list = list()
    for haus in Haus.objects.all():
        hparams = haus.get_module_parameters()
        wp_active = hparams.get('wetter_pro', {}).get('is_active', False)
        for etage in haus.etagen.all():
            for raum in etage.raeume.all():
                hmods = haus.get_modules()
                mods = raum.get_modules()
                if "ki" in hmods and wp_active and "ki" not in mods:
                    raum.set_modules(mods + ['ki'])
                elif ("ki" not in hmods or not wp_active) and "ki" in mods:
                    mods.remove("ki")
                    raum.set_modules(mods)
                    continue

                data_point = calc_data_point_process(haus.id, raum.id)
                if data_point is not None:
                    rows_list += data_point

    file_addr = '%sroom_ranking.csv' % (base_addr)
    cols = ['time', 'hausid', 'raumid', 'Ranking', 'Ergebnis', 'IT-Sensor', 'RLT-Sensor', 'Ausgang']
    delete_oldest_row_per_room(file_addr)
    save_to_csv(file_addr, cols, rows_list)
    return True


def save_slopes():
    '''
    This method saves for every room the slope of the straight.
    '''

    for haus in Haus.objects.all():
        for etage in haus.etagen.all():
            for raum in etage.raeume.all().only("id"):
                m = linear_regression(haus.id, raum.id)
                params = raum.get_module_parameters()
                r_mod_params = params.get('heizflaechenoptimierung', {})  # statt hfo ki?
                r_mod_params['slope'] = m
                raum.set_module_parameters(params)


###### Erstellung der kilog_rated_final.csv ####

def load_data_room():
    file_addr = '%s/room_ranking.csv' % base_addr
    try:
        open(file_addr)
    except IOError:
        return None

    rows = list()
    with open(file_addr, 'r') as csvfile:
        # creating a csv reader object
        csvreader = csv.reader(csvfile)
        fields = next(csvreader)
        for row in csvreader:
            if row:
                rows.append(row[0:3] + [float(row[3])] + row[4:])
    return fields, rows


def delete_from_csv(file_addr, worst_ranking, raumid):
    '''
    If a value with a ranking is better, then the worst value must be deleted from kilog_rated_new.csv.
    This is implemented through this function.
    '''
    try:
        df = pd.read_csv(file_addr)
        indexName = df[(df['raumid'] == raumid) & (df['ranking'] == worst_ranking)].index[0]
        df.drop(indexName, inplace = True)
        df.to_csv(file_addr, index=False)
    except:
        return


def delete_from_kilog_rated(day, blocker_ranking, blocker_time, key):
    '''
    This function clears a blocker that is worse than the interval with the last key.
    '''
    try:
        file_addr = '%s/kilog_rated_new.csv' % base_addr
        df = pd.read_csv(file_addr)
        indexName = df[(df['t_intervall'] == day) & (df['ranking'] == blocker_ranking) & (df['time'] == blocker_time)].index[0]
        df = df.drop(df.index[indexName])
        df.to_csv(file_addr, index=False)
    except:
        return


def edit_kilog_rated(room_rows, key, max_key, day, worst_ranking, best_ranking, haus, raum, etage, anzahl, to_delete):
    '''
    This function saves the respective value in the kilog_rated_new.csv
    '''
    file_addr_to_save = '%s/kilog_rated_new.csv' % base_addr
    cols_to_write = ['time', 'hausid', 'etage', 'raumid', 'source_raum', 't_intervall', 'Ergebnis', 'ranking']

    it_array = list()
    at_array = list()
    rlt_array = list()
    out_array = list()
    ranking_array = list()
    right_rows = room_rows[int(key):int(key) + day]
    for row in right_rows:
        ergebnis_dict = ast.literal_eval(row[4])
        it_array.append(ergebnis_dict['IT'])
        at_array.append(ergebnis_dict['AT'])
        rlt_array.append(ergebnis_dict['RLT'])
        out_array.append(ergebnis_dict['OUT'])
        ranking_array.append(row[3])

    diff = int(max_key) - int(key)
    time = datetime.today() - timedelta(days=diff)
    ergebnis = {"RLT": sum(rlt_array)/len(rlt_array), "AT": sum(at_array)/len(at_array), "IT": sum(it_array)/len(it_array), "OUT" : sum(out_array)/len(out_array)}

    # prüfe die Aufnahmekriterien, wenn diese nicht erfüllt sind, dann wird es nicht in die kilog_rated geschrieben
    if (ergebnis['IT'] - ergebnis['AT']) < 15:
        return
    elif ergebnis['OUT'] < 40.0:
        return

    row_to_write = [time, haus.id, etage.id, raum.id, raum.id, day, ergebnis, sum(ranking_array)]
    rows_to_write = list()
    rows_to_write.append(row_to_write)
    save_to_csv(file_addr_to_save, cols_to_write, rows_to_write, append=True)
    if anzahl >= 20 and to_delete:
        delete_from_csv(file_addr_to_save, worst_ranking, raum.id)


def calc_worst_ranking(raumid):
    '''
    This function finds out the worst existing ranking in kilog_rated_new.csv for the room.
    '''
    rankings_of_ratedcsv = list()
    file_addr = '%s/kilog_rated_new.csv' % base_addr
    anzahl = 0
    row_list = list()
    try:
        open(file_addr)
    except IOError:
        return None, None

    with open(file_addr, 'r') as csvfile:
        csvreader = csv.reader(csvfile)
        for row in csvreader:
            if row[3] == str(raumid):
                row_list.append(row)
                rankings_of_ratedcsv.append(float(row[7]))
                anzahl = anzahl + 1
    sorted_rankings = sorted(rankings_of_ratedcsv)
    if len(sorted_rankings) > 0:
         return sorted_rankings[0], anzahl
    else:
        return 0.0, anzahl


def prove_intersection(day, key, max_key, raumid, hausid):
    '''
    This function checks the overlap with other points of the same time interval and space. The overlap may not exceed t_intervall / 2.
    '''
    diff = int(max_key) - int(key)
    end_date = datetime.today() - timedelta(days=diff)  # Die room_ranking.csv hat eine Länge von 14 Tagen

    intersection_scope = day / 2.0
    forbidden_dates_back = end_date - timedelta(days=intersection_scope)
    forbidden_dates_fore = end_date + timedelta(days=intersection_scope)
    forbidden_dates_back_str = forbidden_dates_back.strftime('%Y-%m-%d %H:%M')
    forbidden_dates_fore_str = forbidden_dates_fore.strftime('%Y-%m-%d %H:%M')

    try:
        file_addr = '%s/kilog_rated_new.csv' % base_addr
        df = pd.read_csv(file_addr)
    except:
        return True, None, None
    df_query = df[(df['hausid'] == hausid) & (df['raumid'] == raumid) & (df['t_intervall'] == day) & (df['time'] <= forbidden_dates_fore_str) & (df['time'] >= forbidden_dates_back_str)]
    length = df_query.size
    if length == 0:
        return True, None, None
    else:
        df_query = df_query.sort_values(by=['time'])
        blocker_index = df_query.index[0]
        blocker_ranking = df_query['ranking'][blocker_index]
        blocker_time = df_query['time'][blocker_index]
        return False, blocker_ranking, blocker_time


def build_subsets(t_intervall, ranking_list):
    '''
    This function creates connected subsets of the size t_intervall and adds their rankings.
    The rankings are then sorted in descending order.
    '''
    intervall_rankings = dict()
    for i in range(len(ranking_list)-t_intervall + 1):
        ende = i + t_intervall
        subset = ranking_list[i:ende]
        subset_ranking = sum(subset)
        intervall_rankings.update({str(i): subset_ranking})
    sorted_rankings = sorted(intervall_rankings.items(), key=lambda x: x[1], reverse=True)
    return sorted_rankings


@shared_task
def periodic_evaluation_of_kilog(request=None):
    '''
    This is the main function for calculating the kilog_rated_new.csv (per room) from the room_rated.csv.
    '''

    #Lade die Daten aus room_ranking.csv
    fields, rows = load_data_room()

    for haus in Haus.objects.all():
        for etage in haus.etagen.all():
            for raum in etage.raeume.all().only("id"):

                # Step 1: Lade alle Rankings in ein Array
                ranking_list = list()
                room_rows = list()
                for row in rows:
                    if row[2] == str(raum.id) and row[1] == str(haus.id):
                        room_rows.append(row)
                        ranking_list.append(row[3])

                # Step 2: Bilde sortierte Untermengen für jedes Intervall
                days = [1, 2, 4, 6, 8, 10, 12, 14]
                for day in days:
                    sorted_rankings = build_subsets(day, ranking_list)

                    # Step 3: Füge die entsprechenden Untermengen zur kilog_rated.csv hinzu
                    max_key = len(sorted_rankings) - 1
                    for x in range(len(sorted_rankings)):
                        key = sorted_rankings[x][0]
                        best_ranking = sorted_rankings[x][1]
                        intersection, blocker_ranking, blocker_time = prove_intersection(day, key, max_key, raum.id, haus.id)
                        worst_ranking, anzahl = calc_worst_ranking(raum.id)
                        if (best_ranking > worst_ranking or anzahl < 20) and intersection: #wenn es zu den Top 20 gehört und keine Überschneidung vorliegt
                            edit_kilog_rated(room_rows, key, max_key, day, worst_ranking, best_ranking, haus, raum, etage, anzahl, True)
                        elif (best_ranking > worst_ranking or anzahl < 20) and int(key) == max_key:  # es liegt eine intersection beim letzten key vor
                            if sorted_rankings[x][1] > blocker_ranking:  # Falsche Abfrage
                                delete_from_kilog_rated(day, blocker_ranking, blocker_time, key) # lösche den Wert aus der kilog_rated_new... funktioniert nicht
                                edit_kilog_rated(room_rows, key, max_key, day, worst_ranking, best_ranking, haus, raum, etage, anzahl, False)  # füge den neuen Wert ein
    combine_with_other_rooms()
    save_slopes()
    return HttpResponse('Done')


def combine_with_other_rooms():
    '''
    This function adds the data points of other rooms to the considered room, provided that these other data points are 1.5 times better.
    The kilog_rated_final.csv is created here.
    '''
    if pd is None:
        return
    df_csv = pd.DataFrame()
    file_addr = '%s/kilog_rated_new.csv' % base_addr
    df = pd.read_csv(file_addr)
    for haus in Haus.objects.all():
        for etage in haus.etagen.all():
            for raum in etage.raeume.all().only("id"):
                raumid = raum.id
                etageid = etage.id
                df_others = df[(df['raumid'] != raumid) & (df['etage'] == etageid)] # selbe Etage anderer Raum
                df_room = df[(df['raumid'] == raumid)]
                df_others.loc[:, 'ranking'] *= 1 / 1.5
                df_all = pd.concat([df_room, df_others])
                df_all = df_all.sort_values(by=['ranking'], ascending=False)
                df_all = df_all.reset_index(drop=True)
                df_all = df_all.loc[0:19]
                df_all.loc[:, 'raumid'] = raumid
                df_csv = pd.concat([df_csv, df_all])

                from views import valuation_ki
                ch.set_usage_val('ki_score', {raum.id: round(sum(valuation_ki(haus.id, raum.id)), 2)})
                ch.set_usage_val("active_ki", {raum.id: raum.get_hfo_params().get('ki_mode', 'proki')})
    file_addr_new = '%s/kilog_rated_final.csv' % base_addr
    df_csv.to_csv(file_addr_new, index=False)


@shared_task(base=CeleryErrorLoggingTask)
def analyze_logs():
    from ki.ahk import analyze_logs
    analyze_logs()


@shared_task(base=CeleryErrorLoggingTask)
def adjust_curves():
    from ki.ahk import adjust_curves
    adjust_curves()
