from django.conf.urls import url
from . import views
urlpatterns = [
    url(r'^m_setup/(?P<haus>\d+)/alexa_interface/(?P<action>[a-z]+)/$', views.get_global_settings_page),
    ]
