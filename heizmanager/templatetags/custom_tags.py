from django import template

from benutzerverwaltung.decorators import has_access_pro
register = template.Library()


@register.filter(name='is_standard_user_with_smartui')
def is_standard_user_with_smartui(value, arg):
    if has_access_pro(value):
        return arg
    else:
        return ''
