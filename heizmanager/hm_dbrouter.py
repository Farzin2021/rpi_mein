class HeizmanagerRouter(object):

    
    def db_for_read(self, model, **hints):
        return 'default'

    def db_for_write(self, model, **hints):
        return 'default'

    def allow_relation(self, obj1, obj2, **hints):
        db_list = ('heizmanager', 'auth', 'users', 'vsensoren', 'raumgruppen')
        if obj1._state.db in db_list and obj2._state_db in db_list:
            return True
        return None

    def allow_migrate(self, db, app_label, model_name=None, **hints):
        apps = ['benutzerverwaltung', 'geolocation', 'heizmanager', 'raumgruppen', 'users', 'vsensoren']
        if app_label in apps:
            return db == "default"
        return None
