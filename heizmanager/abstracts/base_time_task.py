from abc import ABCMeta, abstractmethod
from datetime import datetime
import pytz
import importlib
import heizmanager.helpers as helpers
import json
import heizmanager.cache_helper as ch


class TimeTask:
    """
    this class is for implementing in the modules that are providing time based tasks,
    NOTE 1: priorities(MID_HIGH, MID, LOW) are responsible for running tasks in an order,BUT
    HIGH has most prio, such that task with high prio would be executed first then lists will be refilled,
    this is useful when a task needs repopulating with entries like heizprogramm changing,
    in other cases developer can use Mid_high, Mid, Low
    Note 2: if a task has end time it's like a duration so since time to end time every minute will be running
    useful for fixed task, like jahreskalender
    """

    HIGH = 3
    MID_HIGH = 2
    MID = 1
    LOW = 0

    method_factory = None

    __metaclass__ = ABCMeta

    def __init__(self, module_name, task_name, requested_module, tm, end_tm=None, p=MID, *args, **kwargs):
        """
        :param module_name: module that is providing the operation
        :param task_name: function according to factory of the module
        :param tm: action time
        :param end_tm: if it's not none the operation is a duration
        :param args: the args that function needs for the operation
        :param kwargs: keyword args that function needs for the operation
        """
        self.module_name = module_name
        self.task_name = task_name
        self.tm = helpers.get_str_formatted_time(tm) if isinstance(tm, datetime) else tm
        self.ts = helpers.get_ts_from_str_time(self.tm)
        self.args = list(args)
        self.p = p
        self.kwargs = kwargs
        self.end_tm = end_tm
        self.requested_module = requested_module
        if end_tm is not None:
            self.end_tm = helpers.get_str_formatted_time(end_tm) if isinstance(end_tm, datetime) else end_tm
            self.end_ts = helpers.get_ts_from_str_time(self.end_tm)

    def __repr__(self):
        return json.dumps(self.__dict__)

    @classmethod
    def get_task_cls(cls, mod):
        """
        :param mod: module name
        :return: Task class or None if couldn't import
        """
        try:
            module = importlib.import_module(mod + '.views')
            # load task class
            taskCls = getattr(module, mod.capitalize() + 'Task')
        except (ImportError, AttributeError):
            taskCls = None

        return taskCls

    @staticmethod
    def get_all_modules_tasks(haus):
        berlin = pytz.timezone("Europe/Berlin")
        now = datetime.now(berlin)
        mods = helpers.get_active_modules(haus)
        tasks_list = []
        for mod in mods:
            # check tasks existing in cache or not
            module_tasks = ch.get_module_tasks(haus.id, mod)
            if module_tasks:
                tasks_list += module_tasks
                continue
            taskCls = TimeTask.get_task_cls(mod)
            if taskCls:
                module_tasks = taskCls.get_module_tasks(haus)
                if module_tasks is not None:
                    ch.set_module_tasks(haus.id, mod, module_tasks, tm=60*60*24-60*60*now.hour)
                    tasks_list += module_tasks
        return tasks_list

    @staticmethod
    @abstractmethod
    def get_module_tasks(haus):
        """
        :param haus:
        :return: a list of TimeTask instance
        """
        raise NotImplementedError

    def execute(self):
        return self.method_factory[self.task_name](**self.kwargs)
