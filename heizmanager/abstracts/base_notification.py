from abc import ABCMeta, abstractmethod


class BaseNotification:
    """
        this class is for implementing notification classes that are providing any kind of notification
        every class that wants to implement this class show follow a naming convention
        the name MUST be like this: Nameofthemodule(capital)+Notification
        they are in heizmanager/notifaction.py
    """

    __metaclass__ = ABCMeta

    EMAIL = 'email'

    @staticmethod
    def get_notification_class(typ):
        from benachrichtigung.notifications import EmailNotification
        notification_factory = {
            'email': EmailNotification
        }
        return notification_factory[typ]

    @staticmethod
    def notify(typ, message, to=[], *args, **kwargs):

        try:
            notification_cls = BaseNotification.get_notification_class(typ)
            ret = notification_cls().send(message, to, *args, **kwargs)
            return ret
        except KeyError:
            raise ValueError('there is no implemented method/class for given notification type')

    @abstractmethod
    def send(self, message, to=[], *args, **kwargs):
        """
        :param message: text message that should be sent
        :param to: a list of receivers id or email address, like email, telegram id,.....
        :param args: other params like subject base on notification type needs
        :param kwargs: other params like subject base on notification type needs
        :return:
        """
        raise NotImplementedError




