# -*- coding: utf-8 -*-
import time

from heizmanager.models import RPi, Haus
from heizmanager.render import render_redirect, render_response
from datetime import datetime, timedelta
import pytz
from fabric.api import local
from django.http import HttpResponse
import requests
import re
from benutzerverwaltung.decorators import is_admin
import logging
import heizmanager.cache_helper as ch
import heizmanager.network_helper as nh
from heizmanager.network_helper import get_ip
import os.path

p_logger = logging.getLogger('servicemonitorlog')
logger = logging.getLogger("logmonitor")


def get_global_settings_link(request, haus):
    return "<a href='/m_setup/%s/setup_rpi/'>Miniserver</a>" % haus.id


@is_admin
def get_global_settings_page(request, hausid):

    if isinstance(hausid, Haus):
        haus = hausid
    else:
        haus = Haus.objects.get(pk=long(hausid))

    macaddr = nh.get_mac()

    if request.method == "GET":
        if 'create' in request.GET:
            rpiid = request.GET.get('create')
            is_self = False
            lu = None
            branch = None
            failed_update = False
            uptime = None
            updating = False
            if rpiid:
                try:
                    rpi = RPi.objects.get(pk=long(rpiid))
                except RPi.DoesNotExist:
                    return render_redirect(request, "/m_setup/%s/hardware/setup_rpi/" % haus.id)
                if rpi.name == macaddr.lower():
                    is_self = True
                    res = local("git log -1 --date=raw", capture=True)
                    luiso = [r[8:-6] for r in res.split('\n') if r.startswith("Date: ")][0]
                    lu = datetime.utcfromtimestamp(long(luiso)).strftime('%d.%m.%Y %H:%M:%S')
                    branch = local("git rev-parse --abbrev-ref HEAD", capture=True)
                    if branch == 'master':
                        branch = 'Stable'
                    elif branch == 'beta':
                        branch = 'Lab'
                    elif branch == 'development':
                        branch = 'dev'

                    try:
                        local("ls /home/pi/rpi/failed_pull")
                        failed_update = True
                        local("rm /home/pi/rpi/failed_pull")
                    except:
                        pass
                    try:
                        uptime = local("cat /proc/uptime", capture=True)
                        uptime = uptime.split(' ')[0]
                        uptime = datetime.now(pytz.timezone("Europe/Berlin")) - timedelta(seconds=float(uptime))
                        uptime = uptime.strftime("%H:%M %d.%m.%Y")
                    except:
                        uptime = None
                    try:
                        r = local("ps aux | grep 'fab_update'", capture=True)
                        if "/fab_update.py update:True" in r:
                            updating = True
                    except:
                        updating = False

                hparams = haus.get_module_parameters()
            else:
                rpi = None
            
            try:
                ip = get_ip()
            except:
                ip = None

            try:
                model = local("cat /sys/firmware/devicetree/base/model", capture= True)
                model = model.replace('Raspberry Pi', '').strip()
                if model == '':
                    raise ValueError('empty string')
                rpi_model = 'Miniserver-Version: %s' % model
            # command is not executable (Fetal Error) or output is empty
            except (SystemExit, ValueError):
                rpi_model = 'Miniserver-Version: Unbekannt'

            try:                    
                revision = local("grep -i 'Revision' /proc/cpuinfo | awk '{print $3}' | tail -1", capture= True)
                if revision == '':
                    raise ValueError('empty string')
                rpi_revision = 'Revision: %s' % revision
            # command is not executable (Fetal Error) or output is empty
            except (SystemExit, ValueError):
                rpi_revision = 'Revision: Unbekannt'

            # get test mode status
            test_mode = local('[ -f /var/log/uwsgi/.test_mode ] && echo on || echo off', capture=True)

            system_time = local("date '+%Y-%m-%d %H:%M:%S'", capture=True)
            supervisorctl_status = local(
                "sudo supervisorctl -c /etc/supervisor/supervisord.conf status"
                " | sed  -r -e 's/\S+//3' -e 's/\S+//3' | cut -d ' ' -f 1,16-", capture=True)

            fbi = None
            old_sd_warning = None
            if (os.path.exists('/home/pi/fbi.pid')):
                fbi = time.ctime(os.stat('/home/pi/fbi.pid').st_mtime)
                fbi_date_time = datetime.strptime(fbi, "%a %b %d %H:%M:%S %Y")
                if fbi_date_time.year <= 2019:
                    old_sd_warning = True

            network_type = local("ip route get 8.8.8.8 | cut -d' ' -f5", capture=True)
            network_type = 'LAN' if 'eth' in network_type else 'WLAN'
            return render_response(request, "m_install_rpic.html", {'haus': haus, 'rpi': rpi,
                                                                    'is_self': is_self, 'lu': lu, 'branch': branch,
                                                                    'failed_update': failed_update,
                                                                    'ip': ip, 'uptime': uptime,
                                                                    'updating': updating,
                                                                    'rpi_model': rpi_model,
                                                                    'rpi_revision': rpi_revision,
                                                                    'test_mode': test_mode,
                                                                    'system_time': system_time,
                                                                    "supervisorctl_status": supervisorctl_status,
                                                                    "network_type": network_type,
                                                                    "fbi": fbi,
                                                                    "old_sd_warning": old_sd_warning
                                                                    })

        elif 'switchto' in request.GET:
            targetversion = request.GET.get('switchto')
            ms = request.GET.get('ms')
            if targetversion and ms:
                try:
                    rpi = RPi.objects.get(pk=long(ms))
                except RPi.DoesNotExist:
                    return render_redirect(request, "/m_setup/%s/hardware/setup_rpi/" % haus.id)
                if rpi.name == macaddr.lower():
                    if targetversion in {'stable', 'lab'}:
                        trans = {'stable': 'master', 'lab': 'beta'}
                        r = local("git checkout %s" % trans[targetversion], capture=True)
                        updated_date = local("git log -1 --pretty=format:'%ad'", capture=True)
                        log_message = "%s|%s" % ("Systemstatus",
                                                 u"Installierte Softwareversion: %s;  Softwarestand von: %s|2a" % (trans[targetversion],updated_date))
                        p_logger.warning(log_message)
                        logger.warning(log_message)

                        local('find /home/pi/rpi -name "*.pyc" -exec rm -f {} \;')
                        if not os.path.isfile('/home/pi/rpi/wetter_pro/cred.yml'):
                            ch.delete('weatherpro_updating_forecast_%s' % haus.id)
                        local("sudo supervisorctl -c /etc/supervisor/supervisord.conf restart uwsgi celery celerybeat")
                        # todo db migrations ggf beachten!
            return render_redirect(request, "/m_setup/%s/hardware/setup_rpi/?create=%s" % (haus.id, ms))

        elif 'rpidel' in request.GET:
            rpiid = request.GET['rpidel']
            pk = long(rpiid)
            rpi = RPi.objects.get(pk=pk)
            hparams = haus.get_module_parameters()
            if rpi.name in hparams.get('rpi', dict()):
                del hparams['rpi'][rpi.name]
                haus.set_module_parameters(hparams)
            if rpi.name != macaddr:
                rpi.delete()
            return render_redirect(request, "/m_setup/%s/setup_rpi/" % haus.id)

        elif 'update' in request.GET:
            res = local("git log -1 --date=raw", capture=True)
            luiso = [r[8:-6] for r in res.split('\n') if r.startswith("Date: ")][0]
            branch = local("git rev-parse --abbrev-ref HEAD", capture=True)
            r = requests.get("http://controme-main.appspot.com/get/update/%s/?verdate=%s&ver=%s" % (macaddr, luiso, branch), timeout=20)
            if r.status_code == 200:
                local("fab -f ~/rpi/config/fab_update.py update:True")
            return render_redirect(request, "/m_setup/%s/hardware/setup_rpi/?create=%s" % (haus.id, request.GET.get('update', 1)))

        elif 'verdate' in request.GET:
            try:
                rpi = RPi.objects.get(pk=long(request.GET['verdate']))
            except RPi.DoesNotExist:
                return HttpResponse(status=404)
            else:
                if macaddr.lower() == rpi.name:
                    res = local("git log -1 --date=raw", capture=True)
                    luiso = [r[8:-6] for r in res.split('\n') if r.startswith("Date: ")][0]
                    branch = local("git rev-parse --abbrev-ref HEAD", capture=True)
                    r = requests.get("http://controme-main.appspot.com/get/update/%s/?verdate=%s&branch=%s" % (macaddr, luiso, branch), timeout=20)
                    return HttpResponse(status=r.status_code)
                else:
                    return HttpResponse(status=404)

        elif 'vertarget' in request.GET:
            branch = request.GET.get('vertarget')
            ms = request.GET.get('ms')
            if branch and ms:
                try:
                    rpi = RPi.objects.get(pk=long(ms))
                except RPi.DoesNotExist:
                    return HttpResponse(status=404)
                if rpi.name == macaddr.lower():
                    r = requests.get("http://controme-main.appspot.com/get/update/%s/?vertarget=%s" % (macaddr, branch), timeout=20)
                    return HttpResponse(r.content)
            return HttpResponse()

        elif 'reboot' in request.GET:
            try:
                requests.get("http://controme-main.appspot.com/get/update/%s/?reboot" % macaddr, timeout=10)
            except Exception as e:
                logging.exception("fehler in reboot request an gae")

            log_message = "%s|%s" % ("Systemstatus", u"Manueller Neustart ausgelöst.|2a")
            p_logger.warning(log_message)
            logger.warning(log_message)
            local("sudo shutdown -r now")
            return HttpResponse()

        elif 'shutdown' in request.GET:
            try:
                requests.get("http://controme-main.appspot.com/get/update/%s/?shutdown" % macaddr, timeout=10)
            except Exception as e:
                logging.exception("fehler in shutdown request an gae")
            logger.warning("%s|%s" % ("Systemstatus",
                                      u"Manuelles Herunterfahren ausgelöst.|1a"))
            local("sudo shutdown -h now")
            return HttpResponse()

        else:
            rpis = RPi.objects.filter(haus=haus)
            try:  # gibts in production immer
                rpi = RPi.objects.get(name=macaddr)
            except RPi.DoesNotExist:
                rpi = None
            rpiList = list()
            for r in rpis:
                last = ch.get_gw_ping(r.name)
                marker = r.get_marker()
                rpiList.append({'rpi': r, 'last': last[0].strftime("am %d.%m. um %H:%M") if last != None else '-', 'marker': marker})
            sorted(list(rpiList), reverse=True)
            return render_response(request, 'm_install_rpi.html', locals())

    elif request.method == "POST":
        if request.POST.get("name") is None:
            from heizmanager.network_helper import get_mac
            name = get_mac() 
        else:
            name = request.POST.get("name").lower()
        description = request.POST.get("description")
        local_ip = request.POST.get("local_ip")

        rpierror = ""
        if not name or not len(name) or not len(name.strip()) or not re.match(r'^([0-9A-Fa-f]{2}[-]){5}([0-9A-Fa-f]{2})$', name.strip()):
            rpierror = "Bitte g&uuml;ltige MAC Adresse eingeben."

        if local_ip is not None and local_ip != '' and re.match(r'^([0-9]{1,3}[\.]){3}[0-9]{1,3}$', local_ip) is None:
            rpierror = "Bitte g&uuml;ltige oder keine IP Adresse eingeben."

        if len(rpierror):
            return render_response(request, "m_install_rpic.html", {'haus': haus, 'rpierror': rpierror, 'rpi': RPi(name=name, local_ip=local_ip)})

        try:
            rpi = RPi.objects.get(name=name.strip())

            rpi.haus = haus
            rpi.description = description
            rpi.local_ip = local_ip
            rpi.save()

            hparams = haus.get_module_parameters()
            hparams.setdefault('rpi', dict())
            hparams['rpi'].setdefault(rpi.name, dict())
            haus.set_module_parameters(hparams)

            return render_redirect(request, "/m_setup/%s/hardware/setup_rpi/?create=%s" % (haus.id, rpi.id))

        except RPi.DoesNotExist:
            rpi = RPi(name=name.strip(), haus=haus, description=description, local_ip=local_ip.strip())
            rpi.save()

            hparams = haus.get_module_parameters()
            hparams.setdefault('rpi', dict())
            hparams['rpi'].setdefault(rpi.name, dict())
            haus.set_module_parameters(hparams)

            return render_redirect(request, "/m_setup/%s/hardware/setup_rpi/?create=%s" % (haus.id, rpi.id))

