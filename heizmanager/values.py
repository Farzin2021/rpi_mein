import json
from django.views.decorators.csrf import csrf_exempt
from heizmanager.render import render_redirect, render_response
from heizmanager.models import Haus
from django.http import JsonResponse, HttpResponseRedirect
from django.contrib import messages


@csrf_exempt
def values(request, ):
    haus = Haus.objects.first()
    haus_params = haus.get_module_parameters()

    default_thermostat_values = {
        'hkt': {'red': 2400, 'yellow': 1200, 'green': 0},
        'hktControme': {'red': 2400, 'yellow': 1200, 'green': 0},
        'hktGenius': {'red': 2400, 'yellow': 1200, 'green': 0},
        'hkteTRV': {'red': 2400, 'yellow': 1200, 'green': 0},
        'sensora50401': {'red': 3600, 'yellow': 1800, 'green': 0},
        'sensora50205': {'red': 3600, 'yellow': 1800, 'green': 0},
        'sensora50213': {'red': 3600, 'yellow': 1800, 'green': 0},
        'hkta52001': {'red': 3600, 'yellow': 1800, 'green': 0},
        'pira50703': {'red': 3600, 'yellow': 1800, 'green': 0},
        'cntct': {'red': 3600, 'yellow': 1800, 'green': 0},
        'sensors': {'red': 86400, 'yellow': 43200, 'green': 0},
        'gateways': {'red': 86400, 'yellow': 43200, 'green': 0},
    }

    if 'thermostat_data' not in haus_params:

        haus_params['thermostat_data'] = json.dumps(default_thermostat_values)
        haus.set_module_parameters(haus_params)

    thermostat_values = json.loads(haus_params['thermostat_data'])
    protocol_multiplication = haus_params.get('protocols_multiplication', {})
    btle_value = protocol_multiplication.get('btle', 1)
    enocean_value = protocol_multiplication.get('enocean', 1)
    sensors_value = protocol_multiplication.get('sensors', 1)
    gateways_value = protocol_multiplication.get('gateways', 1)
    max_allowed_rc = haus_params.get('max_allowed_rc_soll')
    max_allowed_etrv = haus_params.get('max_allowed_etrv_soll', 30)

    if request.method == 'POST':
        if 'btle_input' in request.POST:
            data = {
                'btle': int(request.POST.get('btle_input')),
                'enocean': int(request.POST.get('enocean_input')),
                'sensors': int(request.POST.get('sensors_input')),
                'gateways': int(request.POST.get('gateways_input')),
                }
            haus_params['protocols_multiplication'] = data
            haus.set_module_parameters(haus_params)
            # Add a success message to inform the user
            messages.success(request, 'Werte erfolgreich gespeichert.')

            # Reload the current page
            return HttpResponseRedirect(request.path_info)

        if 'max_allowed_rc_soll' in request.POST:
            haus_params['max_allowed_rc_soll'] = float(request.POST.get('max_allowed_rc_soll'))
            haus.set_module_parameters(haus_params)
            success_message = "Die Maximal erlaubte Solltemp wurden erfolgreich gespeichert."
            return JsonResponse({'success_message': success_message})

        if 'max_allowed_etrv_soll' in request.POST:
            haus_params['max_allowed_etrv_soll'] = float(request.POST.get('max_allowed_etrv_soll'))
            haus.set_module_parameters(haus_params)
            success_message = "Die Maximal erlaubte Solltemp wurden erfolgreich gespeichert."
            return JsonResponse({'success_message': success_message})

        thermostat_name = request.POST.get('name')
        red = int(request.POST.get('red'))
        yellow = int(request.POST.get('yellow'))
        green = int(request.POST.get('green'))
        custom_function = request.POST.get('custom_function') == 'true'

        data = {
            'name': thermostat_name,
            'red': red,
            'yellow': yellow,
            'green': green,
            'custom_function': custom_function,
        }

        thermostat_values[thermostat_name] = data
        haus_params['thermostat_data'] = json.dumps(thermostat_values)
        haus.set_module_parameters(haus_params)


        success_message = "Die Thermostatwerte wurden erfolgreich gespeichert."
        return JsonResponse({'success_message': success_message})
    else:
        return render_response(request, "m_values.html", {'thermostat_values': json.dumps(thermostat_values),
                                                          'default_thermostat_values': default_thermostat_values,
                                                          'btle_value': btle_value, 'enocean_value': enocean_value,
                                                          'sensors_value': sensors_value, 'gateways_value': gateways_value,
                                                          'max_allowed_rc': max_allowed_rc, 'max_allowed_etrv': max_allowed_etrv})
