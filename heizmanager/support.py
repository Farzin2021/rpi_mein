# -*- coding: utf-8 -*-
from django.conf import settings
from django.shortcuts import redirect
from heizmanager.render import render_response, render_redirect
from django.http import HttpResponse
import json
import time
from django.views.decorators.csrf import csrf_exempt
import os
from fabric.api import local
from heizmanager.network_helper import get_mac
import shutil
from zipfile import ZipFile
import re, ast
import base64
from heizmanager.models import Haus
import datetime
from heizmanager.modules.module import get_mods_list
from heizmanager.modules.module import get_all_modules_list, get_default_hidden_modules
from heizmanager.models import RPi, CryptKeys
try:
    import pyudev
except ImportError:
    pass
import socket
import fcntl
import struct
import logging
import pytz


p_logger = logging.getLogger('servicemonitorlog')
logger = logging.getLogger('logmonitor')

# Handle local commands
def _local_handler(cmd, state='capture'):
    try:
        local_run = local(cmd, capture=True)
        if state == 'status':
            status = 'yes'
        else:
            status = local_run
    except:
        status = 'no'
    return status


# processors before GET and POST
def _pre_processing(request):
    if request.user is None or not request.user.is_authenticated:
        try:
            if len(User.objects.all()) > 0:
                return redirect('%s?next=%s' % (settings.LOGIN_URL, request.path))
        except Exception as er:
            return redirect('%s?next=%s' % (settings.LOGIN_URL, request.path))
    # Check if there is a copy of default NTP config file
    if _local_handler('[ -f /etc/ntp_default.conf ] && echo yes || echo no') == 'no':
        _local_handler('sudo cp /etc/ntp.conf /etc/ntp_default.conf')


# support GET method
def _support_get(request):
    # Resource monitoring function
    def resource_status(source):

        def get_interface_ip_addresses():
            net_str = ','
            max_possible = 128
            sock = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
            for i in range(max_possible):
                for x in {'eth', 'wlan', 'tun'}:
                    try:
                        name = "{}{}".format(x,i)
                        ip_address = socket.inet_ntoa(fcntl.ioctl(
                            sock.fileno(),
                            0x8915,
                            struct.pack('256s', name[:15])
                        )[20:24])
                        if name == 'eth0':
                            net_str += ' Lan: %s,' % ip_address
                        elif name == 'wlan0':
                            net_str += ' Wifi: %s,' % ip_address
                        elif name == 'tun0':
                            net_str += ' FWD: %s,' % ip_address
                        else:
                            net_str += '  %s: %s,' %(name, ip_address)
                    except:
                        pass
            return net_str[1:-1]

        def bluetooth_status():
            res = os.popen('sudo systemctl status bluetooth.service')
            for x in res.readlines():
                if 'Active: ' in x:
                    return x.strip()
            return 'No info about status'

        def bluetooth_version():
            res = os.popen('bluetoothctl -v')
            devices = res.readlines()
            devices = '\n'.join(str(p.strip()) for p in devices)
            try:
                if len(devices) != 0:
                    return devices
                else:
                    return 'No connected adaptors!'
            except:
                return 'No info about connected adaptors'

        def bluetooth_devices():
            res = os.popen('hcitool dev')
            devices = res.readlines()
            connected_devices = '\n'.join(str(p.strip()) for p in devices[1:])
            try:
                if len(connected_devices) != 0:
                    return connected_devices
                else:
                    return 'No connected adaptors!'
            except:
                return 'No info about connected adaptors'

        def bluetooth_connections():
            res = os.popen('hcitool con')
            devices = res.readlines()
            connections = '\n'.join(str(p.strip()) for p in devices[1:])
            try:
                if len(connections) != 0:
                    return connections
                else:
                    return 'No connection available!'
            except:
                return 'No info about connected devices'

        def get_sd_info():
            try:
                df = os.popen("df -h /")
                i = 0
                while True:
                    i = i + 1
                    line = df.readline()
                    if i == 2:
                        sd = line.split()[0:6]
                        sd = 'Filesystem: %s | Total: %s | Used: %s | Available: %s | Used_percent: %s' % (
                            sd[0], sd[1], sd[2], sd[3], sd[4])
                        return sd
            except:
                return 'Error!'

        def get_mem_info():
            ram_list = []
            strings = ['Mem:', 'Swap:', 'Total:']
            try:
                proc = os.popen('free -ht')
                for index, line in enumerate(proc):
                    if index == 0:
                        ram_list.append(line)
                    add_info = [line for string in strings if string in line]
                    if len(add_info) != 0:
                        ram_list.append(add_info[0])
                return ram_list
            except:
                return 'Error!'

        def get_cpu_info():
            usage = _local_handler(
                """grep 'cpu ' /proc/stat | awk '{usage=($2+$4)*100/($2+$4+$5)} END {print usage "%"}'""")
            freq = _local_handler("cat /sys/devices/system/cpu/cpu0/cpufreq/scaling_cur_freq")
            try:
                freq = ('%sMHz' % (int(freq) / 100))
            except:
                pass
            cores = _local_handler("grep -c ^processor /proc/cpuinfo")
            temp = _local_handler(
                "cat /sys/class/thermal/thermal_zone0/temp | { read message; message=$(($message/1000)); echo "
                "$message°C;}")
            cpu_output = 'Usage_percent: %s | Frequency: %s | Cores: %s | Temperature: %s' % (usage, freq, cores, temp)
            return cpu_output

        resource_list = {'ble_status': bluetooth_status,
                         'ble_version': bluetooth_version,
                         'ble_devices': bluetooth_devices,
                         'bluetooth_connections': bluetooth_connections,
                         'sd_info': get_sd_info,
                         'mem_info': get_mem_info,
                         'cpu_info': get_cpu_info,
                         'network_info': get_interface_ip_addresses
                         }

        return resource_list[source]

    # main GET function
    def main(request):
        directory_addr = "/home/pi/rpi"
        current_branch = local("cd %s && echo $(git rev-parse --abbrev-ref HEAD)" % directory_addr, capture=True)
        updated_date = local("git log -1 --pretty=format:'%ad'", capture=True)
        git_branches = local("cd %s && git branch -a" % directory_addr, capture=True)
        branches_lst = []
        for branch in git_branches.splitlines():
            branches_lst.append(branch.strip())
        # Get state of services
        ntp_status = local("echo $(sudo systemctl is-active ntp.service)", capture=True)
        timesyncd_status = local("echo $(sudo systemctl is-active systemd-timesyncd.service)", capture=True)
        # Get ble systemd timer
        ble_repeat_service = local(
            'cat /etc/systemd/system/ble_restart.timer | grep OnUnitActiveSec | cut -d "=" -f2 | cut -d " " -f1',
            capture=True)
        if not ble_repeat_service.isdigit():
            ble_repeat_service = ''
        # Get ble systemd status
        ble_service_status = local('echo $(sudo systemctl is-active ble_restart.timer)', capture=True)
        ble_active_time = _local_handler("echo $(sudo systemctl show ble_restart -p ExecMainStartTimestamp --value)")
        ble_left_time = _local_handler("echo $(sudo systemctl list-timers ble_restart.timer | grep -i 'ble_restart.timer'"
                                       " | grep -o -P '.*(?= left)' | cut -d' ' -f 5-)")
        try:
            ble_active_time = datetime.datetime.strptime(ble_active_time, '%a %Y-%m-%d %H:%M:%S %Z').ctime()
        except:
            pass
        if ble_left_time == '':
            ble_left_time = 'Warten Sie mal'
        # Get RPI restart systemd timer
        rpi_repeat_service = local(
            'cat /etc/systemd/system/rpi_restart.timer | grep OnUnitActiveSec | cut -d "=" -f2 | cut -d " " -f1',
            capture=True)
        if not rpi_repeat_service.isdigit():
            rpi_repeat_service = ''
        # Get RPI restart systemd status
        rpi_service_status = local('echo $(sudo systemctl is-active rpi_restart.timer)', capture=True)
        rpi_active_time = _local_handler("echo $(sudo systemctl show rpi_restart -p ExecMainStartTimestamp --value)")
        rpi_left_time = _local_handler("echo $(sudo systemctl list-timers rpi_restart.timer | grep -i 'rpi_restart.timer'"
                                      " | grep -o -P '.*(?= left)' |  cut -d' ' -f 5-)")
        try:
            rpi_active_time = datetime.datetime.strptime(rpi_active_time, '%a %Y-%m-%d %H:%M:%S %Z').ctime()
        except:
            pass
        if rpi_left_time == '':
            rpi_left_time = 'Warten Sie mal'
        system_uptime = local('uptime -p', capture=True)
        # Get status of Bluetooth logs
        btmon_status = _local_handler("sudo /bin/pidof btmon")
        dbus_status = _local_handler("sudo /bin/pidof dbus-monitor")
        # Get current datetime in different formats
        current_time_date = local("date '+%Y-%m-%d %H:%M:%S'", capture=True)
        current_time_date_format = local("date '+%Y-%m-%dT%H:%M'", capture=True)
        # Compare default NTP config with the previous copy
        ntp_default = _local_handler('sudo cmp -s /etc/ntp.conf /etc/ntp_default.conf', 'status')
        # Check if a custom NTP-server has been set
        ntp_custom = _local_handler('sudo grep -q "#manually_customized NTP" /etc/ntp.conf', 'status')
        # Get the IP of custom NTP-server from config file
        ntp_server = _local_handler("sudo grep -o -P '(?<=server ).*(?= prefer)' /etc/ntp.conf")
        # Get last uploaded time
        offline_date = _local_handler("stat -c '%y' /home/pi/rpi/heizmanager/scripts/update/update.zip")
        # Get supervisorctl status
        supervisorctl_status = _local_handler(
            "sudo supervisorctl -c /etc/supervisor/supervisord.conf status"
            " | sed  -r -e 's/\S+//3' -e 's/\S+//3' | cut -d ' ' -f 1,16-")
        # Get os info
        os_info = _local_handler("uname -a")
        # Get os version
        os_version = _local_handler(
            """cat /etc/os-release | grep PRETTY_NAME= |  cut -d "=" -f2 | sed -e 's/^"//' -e 's/"$//'""")
        # Check if Btle_handler log is on
        check_btle_log = _local_handler(
            'grep -zoPq "stdout_logfile = /home/pi/rpi/heizmanager/scripts/btle_handler.log\\nstdout_logfile_maxbytes '
            '= 2MB\\nstdout_logfile_backups = 1"  /etc/supervisor/conf.d/messaging.conf')

        firstboot = 'Nein'
        if (os.path.exists('/home/pi/firstbootinit.py')):
            firstboot = 'Ja'

      
        fbi = 'Datei nicht vorhanden'
        if (os.path.exists('/home/pi/fbi.pid')):
            fbi = time.ctime(os.stat('/home/pi/fbi.pid').st_mtime)

        log_device_hw = 'No device found!'
        hw_cmd = _local_handler('mount | grep usbmount')
        if hw_cmd == 'no':
            hw_cmd = _local_handler('ls -la /home/pi/rpi/usbmount | grep "\.usbmount"')
            if hw_cmd != 'no':
                log_device_hw = 'SD-Karte'
        else:
            log_device_hw = 'USB-Stick'

        haus = Haus.objects.first()
        haus_params = haus.get_module_parameters().get('rf_log_params', dict())
        rf_exec_logmonitor = haus_params.get('rf_exec_logmonitor',  15)
        rf_exec_duration = int(rf_exec_logmonitor) - int(haus_params.get('rf_exec_duration',  0))
        if rf_exec_duration < 0:
             rf_exec_duration = 0

        model = _local_handler("cat /sys/firmware/devicetree/base/model")
        rf_log_default = 'log-off'
        if 'Pi 4' in model:
            rf_log_default = 'log-on'
        rf_log_status = haus_params.get('rf_log_status',  rf_log_default)

        rf_pairing_status = haus_params.get('rf_pairing_status',  'log-on')

        # get test mode status
        test_mode = _local_handler('[ -f /var/log/uwsgi/.test_mode ] && echo on || echo off')

        # global test mode
        global_test_mode = haus.get_module_parameters().get('global_tests', {}).get('rc_global_test_mode', False)

        # get zwave status
        single_zwave = _local_handler('[ -f /var/log/uwsgi/.zwave_single ] && echo on || echo off')

        # get sh_reset status
        sh_reset_status = _local_handler('[ -f /var/log/uwsgi/.ble_reset_status ] && echo on || echo off')

        # get zlib encryption status
        handler_zlib = haus.get_module_parameters().get('encryption', {}).get('handler_zlib', 'off')

        # get handler status
        try:
            handler_version = local("[ -f /etc/supervisor/conf.d/btle.conf ] && echo yes || echo no", capture=True)
            if handler_version == 'yes':
                handler_version = 'Aus'
            else:
                handler_version = 'Ein'
        except:
            handler_version = 'Ein'

        # get handler status
        try:
            if all(word in supervisorctl_status for word in ['newbtle', 'btlehandler']):
                handler_version_number = 'Warnung! Beide btle-Handler sind aktiv!'
            elif 'newbtle' in supervisorctl_status and 'btlehandler' not in supervisorctl_status:
                handler_version_number = 'Neuer btle-Handler ist aktiv!'
            elif 'newbtle' not in supervisorctl_status and 'btlehandler' in supervisorctl_status:
                handler_version_number = 'Alter btle-Handler ist aktiv!'
            else:
                handler_version_number = 'Achtung! Keiner der btle-Handler ist aktiv!'
        except:
            handler_version_number = 'Achtung! Keiner der btle-Handler ist aktiv!!'
        
        try:
            res = local("git log -1 --date=raw", capture=True)
            luiso = [r[8:-6] for r in res.split('\n') if r.startswith("Date: ")][0]
            lu = datetime.datetime.utcfromtimestamp(long(luiso)).strftime('%d.%m.%Y %H:%M:%S')
        except:
            lu = 'error'

        try:
            branch_c = local("git rev-parse --abbrev-ref HEAD", capture=True)
            if branch_c == 'master':
                branch_c = 'Stable'
            elif branch_c == 'beta':
                branch_c = 'Lab'
            elif branch_c == 'development':
                branch_c = 'dev'
        except:
            branch_c = 'error'
        try:
            rpi = RPi.objects.all()[0]
        except:
            rpi = None

        def read_secret_files(secrets):
            def read_file(file_path):
                try:
                    with open(file_path, 'r') as f:
                        return f.read().strip().replace(':', '-')
                except IOError:
                    return ''
            
            secret_file_contents = []
            for name, path in secrets.items():
                content = read_file(path)
                secret_file_contents.append({'names': name, 'content': content})
            return secret_file_contents

        secrets = {
            'MAC': '/sys/class/net/eth0/address',
            'AES': '/home/pi/rpi/rpi/aeskey.py',
            'VAC': '/home/pi/rpi/rpi/verification_code.py',
            'SECRET': '/home/pi/rpi/rpi/secret_key.py',
        }
        secret_file_contents = read_secret_files(secrets)

        def get_attr_or_empty(obj, attr_name):
            return getattr(obj, attr_name, '') if obj else ''

        # Retrieve values from the database
        mac_address_obj = RPi.objects.first()
        aes_key_obj = CryptKeys.objects.first()

        secret_dbs = [
            get_attr_or_empty(mac_address_obj, 'name'),
            get_attr_or_empty(aes_key_obj, 'aeskey'),
            get_attr_or_empty(mac_address_obj, 'verification_code')
        ]

        context = {
            'branches': branches_lst,
            'current_branch': current_branch,
            'updated_date': updated_date,
            'status': resource_status('ble_status'),
            'version': resource_status('ble_version'),
            'devices': resource_status('ble_devices'),
            'connections': resource_status('bluetooth_connections'),
            'ntp_status': ntp_status,
            'timesyncd_status': timesyncd_status,
            'current_time_date': current_time_date,
            'current_time_date_format': current_time_date_format,
            'ntp_default': ntp_default,
            'ntp_custom': ntp_custom,
            'ntp_server': ntp_server,
            'btmon_status': btmon_status,
            'dbus_status': dbus_status,
            'ble_repeat_service': ble_repeat_service,
            'ble_service_status': ble_service_status,
            'ble_active_time': ble_active_time,
            'ble_left_time': ble_left_time,
            'rpi_repeat_service': rpi_repeat_service,
            'rpi_service_status': rpi_service_status,
            'rpi_active_time': rpi_active_time,
            'rpi_left_time': rpi_left_time,
            'system_uptime': system_uptime,
            'offline_date': offline_date,
            'supervisorctl_status': supervisorctl_status,
            'sd_info': resource_status('sd_info'),
            'ram_t': resource_status('mem_info'),
            'cpu_t': resource_status('cpu_info'),
            'os_info': os_info,
            'os_version': os_version,
            'check_btle_log': check_btle_log,
            'firstboot': firstboot,
            'fbi': fbi,
            'rf_exec_logmonitor': rf_exec_logmonitor,
            'rf_exec_duration': rf_exec_duration,
            'rf_log_status':rf_log_status,
            'rf_pairing_status': rf_pairing_status,
            'log_device_hw': log_device_hw,
            'haus': haus,
            'test_mode': test_mode,
            'global_test_mode': global_test_mode,
            'single_zwave':single_zwave,
            'sh_reset_status':sh_reset_status,
            'handler_version': handler_version,
            'branch': branch_c,
            'lu': lu,
            'rpi': rpi,
            'handler_zlib': handler_zlib,
            'network_info': resource_status('network_info'),
            'secret_file_contents': secret_file_contents,
            'secret_dbs': secret_dbs,
            'handler_version_number': handler_version_number,
        }

        return render_response(request, "m_support.html", context)

    return main(request)


def _delete_helper(request):
    local("sudo python /home/pi/rpi/fernzugriff/delete_helper.py")
    return render_redirect(request, '/support/')


def _change_branch(request):
    branch = request.POST.get('branch')
    directory_addr = "/home/pi/rpi"
    local("cd %s && git checkout %s" % (directory_addr, branch))
    updated_date = local("git log -1 --pretty=format:'%ad'", capture=True)
    log_message = "%s|%s" % ("Systemstatus",
                             u"Installierte Softwareversion: %s;  Softwarestand von: %s|2a" % (branch,
                                                                                               updated_date))
    p_logger.warning(log_message)
    logger.warning(log_message)
    local('find /home/pi/rpi -name "*.pyc" -exec rm -f {} \;')
    local("sudo supervisorctl -c /etc/supervisor/supervisord.conf restart all")
    return HttpResponse("done")


# change || update || fetch ==> master2020 branch
def _change_to_stable2020(request, target_branch="stable_2020"):

    branch_name = local('git rev-parse --symbolic-full-name --abbrev-ref HEAD', capture= True)
    if branch_name != target_branch:
        master2020 = ('Z2l0IGZldGNoIC1mIGh0dHBzOi8vc2hhaGFia2FtYWxpMDE6VG04cGtKc0Z4amhjU2FoWTRFcWdAYml'
                      '0YnVja2V0Lm9yZy9oY2VybnkvcnBpLmdpdCBzdGFibGVfMjAyMDpzdGFibGVfMjAyMA==')
        local(base64.b64decode(master2020).decode("utf-8"))
    directory_addr = "/home/pi/rpi"
    local("cd %s && git checkout %s" % (directory_addr, target_branch))
    if branch_name == target_branch:
        master2020 = ('Z2l0IHB1bGwgLWYgIGh0dHBzOi8vc2hhaGFia2FtYWxpMDE6VG04cGtKc0Z4amhjU2FoWTRFcWdAYml'
                      '0YnVja2V0Lm9yZy9oY2VybnkvcnBpLmdpdCBzdGFibGVfMjAyMDpzdGFibGVfMjAyMA==')
        local(base64.b64decode(master2020).decode("utf-8"))

    updated_date = local("git log -1 --pretty=format:'%ad'", capture=True)
    log_message = "%s|%s" % ("Systemstatus", u"Installierte Softwareversion: %s;  Softwarestand von: %s|2a" % (target_branch, updated_date))
    p_logger.warning(log_message)
    logger.warning(log_message)
    local("sudo supervisorctl -c /etc/supervisor/supervisord.conf restart all")
    return HttpResponse("done")


def _change_to_stable2021(request):
    _change_to_stable2020(request, target_branch="stable_2021")


def _change_to_stable2022(request):
    _change_to_stable2020(request, target_branch="stable_2022")


# change || update || fetch ==> development branch
def _change_to_development(request):
    branch_name = local('git rev-parse --symbolic-full-name --abbrev-ref HEAD', capture= True)
    if branch_name != 'development':
        dev = ('Z2l0IGZldGNoIC1mIGh0dHBzOi8vc2hhaGFia2FtYWxpMDE6VG04cGtKc0Z4amhjU2FoWTRFcWdAYm'
               'l0YnVja2V0Lm9yZy9oY2VybnkvcnBpLmdpdCBkZXZlbG9wbWVudDpkZXZlbG9wbWVudA==')
        local(base64.b64decode(dev).decode("utf-8"))
    directory_addr = "/home/pi/rpi"
    local("cd %s && git checkout development" % (directory_addr))
    if branch_name == 'development':
        dev = ('Z2l0IHB1bGwgLWYgIGh0dHBzOi8vc2hhaGFia2FtYWxpMDE6VG04cGtKc0Z4amhjU2FoWTRFcWdAYml'
               '0YnVja2V0Lm9yZy9oY2VybnkvcnBpLmdpdCBkZXZlbG9wbWVudDpkZXZlbG9wbWVudA==')
        local(base64.b64decode(dev).decode("utf-8"))

    updated_date = local("git log -1 --pretty=format:'%ad'", capture=True)
    log_message = "%s|%s" % ("Systemstatus",
                             u"Installierte Softwareversion: %s;  Softwarestand von: %s|2a" % (branch_name,
                                                                                               updated_date))
    p_logger.warning(log_message)
    logger.warning(log_message)
    local("sudo supervisorctl -c /etc/supervisor/supervisord.conf restart all &")
    return redirect('/')

# change || update || fetch ==> revive branch
def _change_to_revive(request):
    branch_name = local('git rev-parse --symbolic-full-name --abbrev-ref HEAD', capture= True)
    if branch_name != 'revive':
        dev = ('Z2l0IGZldGNoIC1mIGh0dHBzOi8vc2hhaGFia2FtYWxpMDE6VG04cGtKc0Z4amhjU2FoWTRFcWdAYml0Y'
               'nVja2V0Lm9yZy9oY2VybnkvcnBpLmdpdCByZXZpdmU6cmV2aXZl')
        local(base64.b64decode(dev).decode("utf-8"))
    directory_addr = "/home/pi/rpi"
    local("cd %s && git checkout revive" % (directory_addr))
    if branch_name == 'revive':
        dev = ('Z2l0IHB1bGwgLWYgIGh0dHBzOi8vc2hhaGFia2FtYWxpMDE6VG04cGtKc0Z4amhjU2FoWTRFcWdAYml0YnV'
               'ja2V0Lm9yZy9oY2VybnkvcnBpLmdpdCByZXZpdmU6cmV2aXZl')
        local(base64.b64decode(dev).decode("utf-8"))
    local("sudo supervisorctl -c /etc/supervisor/supervisord.conf restart all &")
    return redirect('/')

# sometimes deleting uwsgi logs will solve log-monitor issue
def _log_functions(request):
    haus = Haus.objects.first()
    haus_params = haus.get_module_parameters().get('rf_log_params', dict())
    if 'uwsgi_delete' in request.POST.get('log'):
        _local_handler('sudo rm /var/log/uwsgi/uwsgi.log*')
        _local_handler('sudo echo "start logging:" > /var/log/uwsgi/uwsgi.log')
        _local_handler('sudo supervisorctl -c /etc/supervisor/supervisord.conf restart uwsgi')
    if 'rf_delete' in request.POST.get('log'):
        _local_handler('sudo echo > /var/log/uwsgi/service_monitor.log1')
        _local_handler('sudo echo > /var/log/uwsgi/service_monitor.log2')
        _local_handler('sudo supervisorctl -c /etc/supervisor/supervisord.conf restart uwsgi')
    if 'rf_hide' in request.POST.get('log'):
        haus_params['rf_log_status'] = 'log-off'
    if 'rf_shown' in request.POST.get('log'):
        haus_params['rf_log_status'] = 'log-on'
    if 'pairing_hide' in request.POST.get('log'):
        haus_params['rf_pairing_status'] = 'log-off'
    if 'pairing_shown' in request.POST.get('log'):
        haus_params['rf_pairing_status'] = 'log-on'
    if 'uwsgi_improve_20220520' in request.POST.get('log'):
        _local_handler('sudo cp /home/pi/rpi/config/logrotate-uwsgi-improves /etc/logrotate.d/uwsgi')
        _local_handler('sudo supervisorctl -c /etc/supervisor/supervisord.conf restart all')
    if 'uwsgi_default' in request.POST.get('log'):
        _local_handler('sudo cp /home/pi/rpi/config/logrotate-uwsgi /etc/logrotate.d/uwsgi')
        _local_handler('sudo supervisorctl -c /etc/supervisor/supervisord.conf restart all')

    haus.set_spec_module_params('rf_log_params', haus_params)
    return render_redirect(request, '/support/')


# log-out from /support
def _log_out(request):
    if 'support_auth' in request.session:
        del request.session['support_auth']
    return render_redirect(request, '/support/')


# set rf timer in logmonitor
def _set_rf_value(request):
        haus = Haus.objects.first()
        haus_params = haus.get_module_parameters().get('rf_log_params', dict())
        haus_params['rf_exec_logmonitor'] = request.POST.get('rf_log_value')
        haus.set_spec_module_params('rf_log_params', haus_params)
        return render_redirect(request, '/support/')


# add switch function for log
def _hw_log_device(request):

    if request.POST.get('hw_log') == 'usbdisk':
        _local_handler("sudo rm -r /home/pi/rpi/usbmount; sudo umount -l /home/pi/rpi/usbmount")
        try:
            context = pyudev.Context()
        except:
            return HttpResponse('It seems pydev is not installed on this device. Please Contact developer team!')
        for device in context.list_devices(subsystem='block', DEVTYPE='partition'):
            if 'mmcblk0p' not in device.device_node:
                try:
                    local("mkdir /home/pi/rpi/usbmount/")
                except:
                    pass
                try:
                    ret = local("mount | grep %s" % device.device_node, capture=True)
                except:
                    ret = ''
                try:
                    ret_sd = local('ls -la /home/pi/rpi/usbmount | grep "\.usbmount"', capture=True)
                except:
                    ret_sd = ''
                if not len(ret) and not len(ret_sd):
                    _local_handler("sudo mount -t vfat -ouser,umask=0000 %s /home/pi/rpi/usbmount/" % device.device_node)
                elif len(ret) and not len(ret_sd) and '/home/pi/rpi/usbmount' not in ret:
                    _local_handler("sudo umount %s" % device.device_node)
                    _local_handler("sudo mount -t vfat %s /home/pi/rpi/usbmount/" % device.device_node)

    if request.POST.get('hw_log') == 'sdcard':
        _local_handler("""sudo umount -l /home/pi/rpi/usbmount;
                          sudo rm -r /home/pi/rpi/usbmount; 
                          mkdir /home/pi/.usbmount;
                          cp -n /home/pi/rpi/heizmanager/scripts/db_log.db /home/pi/.usbmount;
                          sudo ln -s /home/pi/.usbmount /home/pi/rpi/usbmount; 
                          sudo chmod -R 777 /home/pi/rpi/usbmount""")
    
    if request.POST.get('hw_log') == 'sdcard_reset':
        _local_handler("""sudo rm -r /home/pi/.usbmount/*;
                          cp /home/pi/rpi/heizmanager/scripts/db_log.db /home/pi/.usbmount;
                          sudo chmod -R 777 /home/pi/rpi/usbmount""")

    _local_handler('sudo supervisorctl -c /etc/supervisor/supervisord.conf restart all &')
    return render_redirect(request, '/support/')


# Test mode for customers
def _test_mode(request):
    if request.POST.get('test_mode') == 'on':
        _local_handler('touch /var/log/uwsgi/.test_mode')
    if request.POST.get('test_mode') == 'off':
        _local_handler('rm /var/log/uwsgi/.test_mode')
    return render_redirect(request, '/support/')

# Global test mode for customers
def _global_test_mode(request):
    haus = Haus.objects.first()
    global_tests_params = haus.get_module_parameters().get('global_tests', {})
    if request.POST.get('rc_global_test_mode') == 'on':
        global_tests_params['rc_global_test_mode'] = True
    if request.POST.get('rc_global_test_mode') == 'off':
        global_tests_params['rc_global_test_mode'] = False
    haus.set_spec_module_params('global_tests', global_tests_params)
    return render_redirect(request, '/support/')

# Handler encryption
def _encryption(request):
    haus = Haus.objects.first()
    haus_params = haus.get_module_parameters().get('encryption', {})
    haus_params['handler_zlib'] = str(request.POST.get('zlib_handler'))
    haus.set_spec_module_params('encryption', haus_params)
    return render_redirect(request, '/support/')

# Zwave sending satus
def _zwave_status(request):
    if request.POST.get('single_zwave') == 'on':
        _local_handler('touch /var/log/uwsgi/.zwave_single')
    if request.POST.get('single_zwave') == 'off':
        _local_handler('rm /var/log/uwsgi/.zwave_single')
    return render_redirect(request, '/support/')


# system functions
def _system(request):
    if request.POST.get('system') == 'supervisor_restart':
        _local_handler('sudo supervisorctl -c /etc/supervisor/supervisord.conf restart all &')
    return render_redirect(request, '/support/')


# all BLE functions
def _ble_helper(request):
    path = os.path.dirname(os.path.abspath(__file__))
    script_path = os.path.join(path, "scripts")

    def update_ble_driver(request):
        if request.POST.get('ble_version') == '1':
            local('sudo chmod 777 %s/bluetooth_driver_update_5_54.sh' % os.path.abspath(script_path))
            local('cd %s && bash bluetooth_driver_update_5_54.sh' % os.path.abspath(script_path))
        elif request.POST.get('ble_version') == '2':
            local('sudo chmod 777 %s/bluetooth_driver_update_5_50.sh' % os.path.abspath(script_path))
            local('cd %s && bash bluetooth_driver_update_5_50.sh' % os.path.abspath(script_path))
        return render_redirect(request, '/support/')

    def ble_restart(request):
        local('sleep 0.5 && sudo systemctl restart bluetooth.service')
        local('sleep 2 && sudo supervisorctl -c /etc/supervisor/supervisord.conf restart btlehandler')
        return render_redirect(request, '/support/')

    def ble_restart_hw(request):
        _local_handler('sudo bash /home/pi/rpi/config/Controme_ETRV/utils/hard_ble_reset.sh')
        return render_redirect(request, '/support/')

    def ble_restart_new_handler(request):
        _local_handler('sudo supervisorctl -c /etc/supervisor/supervisord.conf restart newbtle')
        return render_redirect(request, '/support/')

    def ble_start_service(request):
        local('cd %s && sudo bash ble_service_create.sh ble_restart "/bin/systemctl restart bluetooth.service" %s'
              % (os.path.abspath(script_path), request.POST.get('ble_start_value')))
        return render_redirect(request, '/support/')

    def ble_scan(request):
        local('sudo python /home/pi/rpi/config/Controme_ETRV/scan_btle.py')
        return render_redirect(request, '/support/')

    def ble_stop_service(request):
        local('sudo systemctl stop ble_restart.timer && sudo systemctl disable ble_restart.timer')
        return render_redirect(request, '/support/')

    def rpi_start_service(request):
        local("cd %s && sudo bash ble_service_create.sh rpi_restart  '/bin/bash /home/pi/rpi/heizmanager/scripts/reboot.sh' %s" % (
            os.path.abspath(script_path), request.POST.get('rpi_start_value')))
        return render_redirect(request, '/support/')

    def rpi_stop_service(request):
        local('sudo systemctl stop rpi_restart.timer && sudo systemctl disable rpi_restart.timer')
        return render_redirect(request, '/support/')

    def btmon_start(request):
        _local_handler('sudo pkill dbus-monitor')
        local('sudo timeout 60m btmon -T > %s/btmon.log' % os.path.abspath(script_path))
        return render_redirect(request, '/support/')

    def btmon_stop(request):
        _local_handler('sudo pkill btmon')
        return render_redirect(request, '/support/')

    def btmon_log(request):
        filename = "%s/btmon.log" % os.path.abspath(script_path)
        with open(filename, 'r') as data_file:
            data = data_file.read()
        response = HttpResponse(data, content_type='text/plain')
        response['Content-Disposition'] = 'attachment; filename=btmon_ble.log'
        return response

    def dbus_start(request):
        _local_handler('sudo pkill btmon')
        local('''sudo timeout 60m dbus-monitor --system "type='signal',sender='org.bluez'" > %s/dbus.log'''
              % os.path.abspath(script_path))
        return render_redirect(request, '/support/')

    def dbus_stop(request):
        _local_handler('sudo pkill dbus-monitor')
        return render_redirect(request, '/support/')

    def dbus_log(request):
        filename = "%s/dbus.log" % os.path.abspath(script_path)
        with open(filename, 'r') as data_file:
            data = data_file.read()
        response = HttpResponse(data, content_type='text/plain')
        response['Content-Disposition'] = 'attachment; filename=dbus_ble.log'
        return response

    def rf_log(request):
        with open('/var/log/uwsgi/rf_logs.log', 'r') as data_file:
            data = data_file.read()
        response = HttpResponse(data, content_type='text/plain')
        response['Content-Disposition'] = 'attachment; filename=rf_devices.log'
        return response

    def ble_log(request):
        with open('/var/log/uwsgi/scan_log', 'r') as data_file:
            data = data_file.read()
        response = HttpResponse(data, content_type='text/plain')
        response['Content-Disposition'] = 'attachment; filename=ble_devices.log'
        return response

    def handler_old(request):
        _local_handler('sudo sed -i "/#\[program:btlehandler\]/c\\\[program:btlehandler\]" /etc/supervisor/conf.d/messaging.conf')
        _local_handler('sudo sed -i "/# command = sudo \/usr\/bin\/python -u \/home\/pi\/rpi\/config\/btle_handler.py/c\\command = sudo \/usr\/bin\/python -u \/home\/pi\/rpi\/config\/btle_handler.py" /etc/supervisor/conf.d/messaging.conf')
        _local_handler('sudo sed -i "/# command = sudo \/usr\/bin\/python \/home\/pi\/rpi\/config\/btle_handler.py/c\\command = sudo \/usr\/bin\/python \/home\/pi\/rpi\/config\/btle_handler.py" /etc/supervisor/conf.d/messaging.conf')
        _local_handler('sudo supervisorctl -c /etc/supervisor/supervisord.conf stop newbtle')
        _local_handler('sudo systemctl stop etrv_handler.service')
        _local_handler('sudo systemctl disable etrv_handler.service')
        _local_handler('sudo rm /etc/supervisor/conf.d/btle.conf')
        _local_handler("sudo supervisorctl -c /etc/supervisor/supervisord.conf reread")
        _local_handler("sudo supervisorctl -c /etc/supervisor/supervisord.conf update")
        _local_handler('sudo supervisorctl -c /etc/supervisor/supervisord.conf restart btlehandler')
        _local_handler('sudo supervisorctl -c /etc/supervisor/supervisord.conf restart newbtle')
        _local_handler("sudo sed -i '/-l 0.0.0.0/c\-l 127.0.0.1' /etc/memcached.conf ; sudo systemctl restart memcached.service")
        return render_redirect(request, '/support/')

    def handler_new(request):
        _local_handler('sudo cp /home/pi/rpi/config/supervisor-newbtle.conf /etc/supervisor/conf.d/btle.conf')
        _local_handler('echo $(date) > /var/log/uwsgi/tempexec_v1')
        _local_handler('sudo supervisorctl -c /etc/supervisor/supervisord.conf stop btlehandler')
        _local_handler('sudo sed -i "/\[program:btlehandler\]/c\\#\[program:btlehandler\]" /etc/supervisor/conf.d/messaging.conf')
        _local_handler('sudo sed -i "/command = sudo \/usr\/bin\/python -u \/home\/pi\/rpi\/config\/btle_handler.py/c\\# command = sudo \/usr\/bin\/python -u \/home\/pi\/rpi\/config\/btle_handler.py" /etc/supervisor/conf.d/messaging.conf')
        _local_handler('sudo sed -i "/command = sudo \/usr\/bin\/python \/home\/pi\/rpi\/config\/btle_handler.py/c\\# command = sudo \/usr\/bin\/python \/home\/pi\/rpi\/config\/btle_handler.py" /etc/supervisor/conf.d/messaging.conf')
        _local_handler("sudo supervisorctl -c /etc/supervisor/supervisord.conf reread")
        _local_handler("sudo supervisorctl -c /etc/supervisor/supervisord.conf update")
        _local_handler('sudo supervisorctl -c /etc/supervisor/supervisord.conf restart btlehandler')
        _local_handler('sudo supervisorctl -c /etc/supervisor/supervisord.conf restart newbtle')
        _local_handler('sudo systemctl stop etrv_handler.service')
        _local_handler('sudo systemctl disable etrv_handler.service')
        _local_handler("sudo sed -i '/-l 127.0.0.1/c\-l 0.0.0.0' /etc/memcached.conf ; sudo systemctl restart memcached.service")
        return render_redirect(request, '/support/')

    def btle_init(request):

        replace_str = (
            "\\ncommand = sudo \/usr\/bin\/python -u \/home\/pi\/rpi\/config\/btle_handler.py\\n" \
            "directory = \/home\/pi\/rpi\/\\n" \
            "user = pi\\n" \
            "autostart = true\\n" \
            "autorestart = true\\n" \
            "startretries=3\\n" \
            "redirect_stderr = true\\n" \
            "stdout_logfile = \/home\/pi\/rpi\/heizmanager\/scripts\/btle_handler.log\\n" \
            "stdout_logfile_maxbytes = 2MB\\n" \
            "stdout_logfile_backups = 1\\n\\n" \
        )
        _local_handler("sudo sed -i '/\[program:btlehandler\]/,/\[program:telegrambot\]/c\\[program:btlehandler\]%s\["
                       "program:telegrambot\]'  /etc/supervisor/conf.d/messaging.conf " % (replace_str))
        _local_handler("sudo supervisorctl -c /etc/supervisor/supervisord.conf reread")
        _local_handler("sudo supervisorctl -c /etc/supervisor/supervisord.conf update")
        return render_redirect(request, '/support/')

    def btle_log(request):
        filename = "%s/btle_handler.log" % os.path.abspath(script_path)
        with open(filename, 'r') as data_file:
            data = data_file.read()
        response = HttpResponse(data, content_type='text/plain')
        response['Content-Disposition'] = 'attachment; filename=btle_handler.log'
        return response

    def ble_hs_enable(request):
        _local_handler('rm /var/log/uwsgi/.ble_reset_status')
        return render_redirect(request, '/support/')

    def ble_hs_disable(request):
        _local_handler('touch /var/log/uwsgi/.ble_reset_status')
        return render_redirect(request, '/support/')

    ble_list = {'update_ble_driver': update_ble_driver,
                'ble_restart': ble_restart,
                'ble_restart_hw': ble_restart_hw,
                'ble_restart_new_handler': ble_restart_new_handler,
                'ble_start_service': ble_start_service,
                'ble_stop_service': ble_stop_service,
                'ble_scan': ble_scan,
                'rpi_start_service': rpi_start_service,
                'rpi_stop_service': rpi_stop_service,
                'btmon_start': btmon_start,
                'btmon_stop': btmon_stop,
                'btmon_log': btmon_log,
                'dbus_start': dbus_start,
                'dbus_stop': dbus_stop,
                'dbus_log': dbus_log,
                'btle_init': btle_init,
                'btle_log': btle_log,
                'rf_log': rf_log,
                'ble_log': ble_log,
                'handler_old': handler_old,
                'handler_new': handler_new,
                'ble_hs_enable': ble_hs_enable,
                'ble_hs_disable': ble_hs_disable,
                }

    return ble_list[request.POST.get('ble')](request)


# all NTP functions
def _ntp_helper(request):
    # Get state of services
    ntp_status = local("echo $(sudo systemctl is-active ntp.service)", capture=True)
    timesyncd_status = local("echo $(sudo systemctl is-active systemd-timesyncd.service)", capture=True)

    def ntp_start(request):
        if ntp_status != 'active':
            local("sudo sed -i 's/#ConditionFileIsExecutable=!\/usr\/sbin\/ntpd/ConditionFileIsExecutable=!\/usr"
                  "\/sbin\/ntpd/g' /lib/systemd/system/systemd-timesyncd.service.d/disable-with-time-daemon.conf")
        local('sudo systemctl disable  systemd-timesyncd.service ; sudo systemctl stop  systemd-timesyncd.service')
        local('sudo systemctl daemon-reload')
        local('sudo systemctl enable ntp.service ; sudo systemctl start ntp.service')
        return render_redirect(request, '/support/')

    def syncd_start(request):
        if timesyncd_status != 'active':
            local("sudo sed -i 's/ConditionFileIsExecutable=!\/usr\/sbin\/ntpd/#ConditionFileIsExecutable=!\/usr"
                  "\/sbin\/ntpd/g' /lib/systemd/system/systemd-timesyncd.service.d/disable-with-time-daemon.conf")
        local('sudo systemctl disable ntp.service ; sudo systemctl stop ntp.service')
        local('sudo systemctl daemon-reload')
        local('sudo systemctl enable systemd-timesyncd.service ; sudo systemctl start systemd-timesyncd.service')
        return render_redirect(request, '/support/')

    def date_time(request):
        local('sudo date -s "%s"' % request.POST.get('date_time_value'))
        return render_redirect(request, '/support/')

    def ntp_conf_default(request):
        _local_handler('sudo killall ntpd')
        local('sudo cp /etc/ntp_default.conf /etc/ntp.conf ')
        local('sudo service ntp stop ; sudo ntpd -gq ; sudo service ntp start')
        return render_redirect(request, '/support/')

    def ntp_conf_custom(request):
        _local_handler('sudo killall ntpd')
        local(
            '''sudo bash -c "echo -e '#manually_customized NTP\nserver %s prefer iburst' > /etc/ntp.conf"'''
            % request.POST.get('ntp_conf_custom_value'))
        local('sudo service ntp stop ; sudo ntpd -gq ; sudo service ntp start')
        return render_redirect(request, '/support/')

    ntp_list = {'NTP_start': ntp_start,
                'syncd_start': syncd_start,
                'date_time': date_time,
                'ntp_conf_default': ntp_conf_default,
                'ntp_conf_custom': ntp_conf_custom,
                }

    return ntp_list[request.POST.get('NTP')](request)


# all offline update functions
def _offline_update(request):
    path = os.path.dirname(os.path.abspath(__file__))
    script_path = os.path.join(path, "scripts")

    def nginx(request):
        local("grep -qxF '    client_max_body_size 100M;' /etc/nginx/nginx.conf || sudo sed -i '/\http {/a\    "
              "client_max_body_size 100M;' /etc/nginx/nginx.conf")
        local("sudo systemctl restart nginx")
        return render_redirect(request, '/support/')

    def nginx_502(request):
        local("sudo supervisorctl -c /etc/supervisor/supervisord.conf stop uwsgi")
        return render_redirect(request, '/support/')

    def updates_log(request):
        filename = "%s/offline.logs" % os.path.abspath(script_path)
        with open(filename, 'r') as data_file:
            data = data_file.read()
        response = HttpResponse(data, content_type='text/plain')
        response['Content-Disposition'] = 'attachment; filename=offline_update.log'
        return response

    def git_update(request):
        mac = get_mac()
        mac_dec = [str(int(sep, 16)) for sep in mac.split('-')]
        mac_dec_bare = "".join(mac_dec)
        if mac_dec_bare == request.POST.get('password'):
            local('cd %s && sudo bash offline_update.sh %s/update > offline.logs' % (
            os.path.abspath(script_path), os.path.abspath(script_path)))
            return render_redirect(request, '/support/')
        local('cd %s && echo "Error: Password for installation is not correct!" > offline.logs' % (
            os.path.abspath(script_path)))
        return HttpResponse('Wrong password!')

    def upload_zip(request):
        mac = get_mac()
        mac_dec = [str(int(sep, 16)) for sep in mac.split('-')]
        mac_dec_bare = "".join(mac_dec)
        crossum_bare = "".join(
            [str(sum(int(digit) for digit in str(sum(int(digit) for digit in str(sep))))) for sep in mac_dec])
        if mac_dec_bare == request.POST.get('pwd'):
            db = request.FILES.get('db', None)
            if db is None:
                return HttpResponse('Error: No zip file selected!')
            if not os.path.exists(script_path):
                os.makedirs('%s/update' % script_path)
            local('sudo chmod 777 %s/update' % script_path)
            dest_path = "%s/update/update.zip" % script_path
            _local_handler("sudo rm -r %s/update/rpi" % script_path)
            with open(dest_path, 'wb') as dest:
                shutil.copyfileobj(db, dest)
            zf = ZipFile(dest_path)
            try:
                zf.testzip()
                _local_handler("sudo rm -r %s/update/rpi" % script_path)
                return HttpResponse('Error: Your Zip file is not encrypted!')
            except RuntimeError as e:
                if 'encrypted' in str(e):
                    try:
                        local('cd %s/update && unzip -P %s -d rpi update.zip' % (script_path, crossum_bare))
                    except:
                        return HttpResponse('Error: Cannot unzip the file please contact the developer team!')
                return render_redirect(request, '/support/')
        return HttpResponse('Error: Please enter the correct password for uploading!')

    def upload_csv_files(request):
        error_files = []
        allowed_csv = ['room_ranking.csv', 'kilog_rated_new.csv', 'kilog_rated_final.csv']
        db = request.FILES.getlist('csv_logs', None)
        if db is None:
            return HttpResponse('Error: No csv file selected!')
        for csvfile in db:
            if str(csvfile.name) in allowed_csv:
                dest_path = "/home/pi/rpi/usbmount/%s" % str(csvfile.name)
                with open(dest_path, 'wb') as dest:
                    shutil.copyfileobj(csvfile, dest)
            else:
                error_files.append(str(csvfile.name))
        if len(error_files):
            return HttpResponse('Error: You can not import these files ==> %s' % error_files)
        return render_redirect(request, '/support/')

    update_list = {'nginx': nginx,
                   'nginx_502': nginx_502,
                   'updates_log': updates_log,
                   'git_update': git_update,
                   'upload_zip': upload_zip,
                   'upload_csv_files': upload_csv_files,
                   }

    return update_list[request.POST.get('update')](request)


# authenticate support
@csrf_exempt
def support_login(request):
    if request.method == 'GET':
        auth_check = request.session.get('support_auth')
        if auth_check:
            return redirect('support')
        return render_response(request, "m_authenticate.html", {'':''})
    if request.method == 'POST':
        user_name = request.POST.get('name')
        user_password = request.POST.get('pws')
        encode_password = base64.b64decode('cXdlcjEyMzQ=').decode("utf-8")
        if user_name == 'support' and user_password == encode_password:
            request.session['support_auth'] = True
            return redirect('support')
        return render_response(request, "m_authenticate.html", {'valid':'no'})


# main support function
@csrf_exempt
def support(request):
    _pre_processing(request)
    if request.method == 'GET':
        auth_check = request.session.get('support_auth')
        if not auth_check:
            return redirect('support_login')
        return _support_get(request)
    if request.method == 'POST':
        post_list = {'del': _delete_helper,
                     'branch': _change_branch,
                     'ble': _ble_helper,
                     'NTP': _ntp_helper,
                     'update': _offline_update,
                     'dev': _change_to_development,
                     'revive': _change_to_revive,
                     'stable2020': _change_to_stable2020,
                     'stable2021': _change_to_stable2021,
                     'stable2022': _change_to_stable2022,
                     'log': _log_functions,
                     'auth': _log_out,
                     'rf_log_value': _set_rf_value,
                     'hw_log': _hw_log_device,
                     'test_mode': _test_mode,
                     'rc_global_test_mode': _global_test_mode,
                     'single_zwave':_zwave_status,
                     'system': _system,
                     'zlib_handler': _encryption,
                     }
        key_list = post_list.keys()
        request_keys = list(request.POST.keys())
        post_key = [e for e in key_list if e in request_keys][0]
        return post_list[post_key](request)


@csrf_exempt
def support_table(request):

    def regex_btle():
        csv_list = []
        log_files = ['/home/pi/rpi/heizmanager/scripts/btle_handler.log.1', 
                     '/home/pi/rpi/heizmanager/scripts/btle_handler.log',]

        def regex_finder(btle_line):
            for btle_line in btle_log:
                try:
                    data_time = btle_line.split(' -')[0]
                    substring = re.search(" - (.+?):", btle_line).group(1)
                    if substring == 'received message':
                        convertedDict = ast.literal_eval(re.search("(?<=received message: )(.*)", btle_line).group(1))
                        for key in convertedDict['data']:
                            if re.match("^([0-9A-Fa-f]{2}[:-]){5}([0-9A-Fa-f]{2})$", str(key)):
                                csv_list.append([data_time, substring+' (Daten zum Gerät)', key, convertedDict['data'][key]])
                    elif substring == 'sending':
                        convertedDict = ast.literal_eval(re.search("(?<=sending: )(.*)", btle_line).group(1))
                        for key in convertedDict:
                            if re.match("^([0-9A-Fa-f]{2}[:-]){5}([0-9A-Fa-f]{2})$", str(key)):
                                csv_list.append([data_time, substring+' (Daten vom Gerät)', key, convertedDict[key]])
                except:
                    pass

        for log in log_files:
            if os.path.isfile(log):
                with open(log) as btle_log:
                    regex_finder(btle_log)

        if len(csv_list) != 0:
            return csv_list
        return [['Click on "Download Btle log" to activate logging!']]

    return render_response(request, "m_btle_log.html", {'btle_table':regex_btle})


def activation_interface(request):
    haus = Haus.objects.first()
    h_params = haus.get_module_parameters()
    if request.method == 'GET':
        # auth_check = request.session.get('support_auth')
        # if auth_check is None:
        #     return redirect('/')

        swlinks, category_dict = get_mods_list(haus)
        mods = list((mod, modename) for mod, modename, desc, desc_link, link in swlinks)
        all_mods_list = list(set(get_all_modules_list(haus)) - set(get_default_hidden_modules()))

        module_activation_params = h_params.get('module_activation', {})
        mods_activation = module_activation_params.get('mods_activation', all_mods_list)
        mods_configuration = module_activation_params.get('mods_configuration', all_mods_list)
        interface = module_activation_params.get('interface', 'proui')
        return render_response(request, "m_support_module_activation.html", {
            'mods': mods,
            'mods_activation': mods_activation,
            'mods_configuration': mods_configuration,
            'interface': interface,
        })
    else:
        mods_activation = request.POST.getlist('mods_activation')
        mods_configuration = request.POST.getlist('mods_configuration')
        interface = request.POST.get('interface', 'proui')
        if interface == 'smartui':
            h_params['only_smart'] = True
        else:
            h_params['only_smart'] = False
        module_activation_d = {
            'mods_activation': mods_activation,
            'mods_configuration': mods_configuration,
            'interface': interface
        }
        h_params['module_activation'] = module_activation_d
        haus.set_module_parameters(h_params)
        return redirect('/support/activation/')
