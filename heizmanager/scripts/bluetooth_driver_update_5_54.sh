#!/bin/bash

# Log output to file
exec 3>&1 4>&2
trap 'exec 2>&4 1>&3' 0 1 2 3
exec 1>~/.b5_54.log 2>&1
echo $(date)
echo

# Change dir to home
cd ~

# Install dependencies
sudo apt-get update -y
sudo apt-get update --allow-releaseinfo-change -y
sudo apt-get install libdbus-1-dev libglib2.0-dev libudev-dev libical-dev libreadline-dev autoconf -y

# Install json-c-0.13
wget --no-check-certificate https://s3.amazonaws.com/json-c_releases/releases/json-c-0.13.tar.gz
tar -xvf json-c-0.13.tar.gz
cd json-c-0.13/
./configure --prefix=/usr --disable-static && make
sudo make install

# Change dir to home
cd ~

# Install driver 5.54
wget --no-check-certificate http://www.kernel.org/pub/linux/bluetooth/bluez-5.54.tar.xz
tar -xvf bluez-5.54.tar.xz
cd bluez-5.54/
./configure --enable-mesh --enable-testing --enable-tools --prefix=/usr --mandir=/usr/share/man --sysconfdir=/etc --localstatedir=/var
sudo make
sudo make install

# Remove unneeded files
sleep 2
cd ~
sudo rm -r bluez-5.54 bluez-5.54.tar.xz json-c-0.13 json-c-0.13.tar.gz

# Reboot system
sudo reboot now