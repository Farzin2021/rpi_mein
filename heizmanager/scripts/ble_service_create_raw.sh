#!/usr/bin/bash
SERVICE_NAME=$1
DESCRIPTION=$1
SERVICE_PATH=$2

DESCRIPTION=${DESCRIPTION//'"'/}
SERVICE_NAME=${SERVICE_NAME//'"'/}
SERVICE_PATH=${SERVICE_PATH//'"'/}

sudo systemctl stop $SERVICE_NAME'.service'
sudo systemctl disable $SERVICE_NAME'.service'

sudo cat > /etc/systemd/system/${SERVICE_NAME//'"'/}.service << EOF
[Install]
WantedBy=multi-user.target
[Unit]
Description=$DESCRIPTION
[Service]
Type=simple
Restart=always
ExecStart=$SERVICE_PATH
EOF

echo "Reloading daemon and enabling service"
sudo systemctl daemon-reload
sudo systemctl enable $SERVICE_NAME
sudo systemctl start $SERVICE_NAME
echo "Service Started"

exit 0
