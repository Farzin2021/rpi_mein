#!/bin/bash

# Log output to file
exec 3>&1 4>&2
trap 'exec 2>&4 1>&3' 0 1 2 3
exec 1>~/.b5_50.log 2>&1
echo $(date)
echo

# change dir to home
cd ~ 

# install dependencies
sudo apt-get update -y
sudo apt-get update --allow-releaseinfo-change -y
sudo apt-get install libdbus-1-dev libglib2.0-dev libudev-dev libical-dev libreadline-dev -y

# install driver 5.50
wget --no-check-certificate www.kernel.org/pub/linux/bluetooth/bluez-5.50.tar.xz
tar xvf bluez-5.50.tar.xz && cd bluez-5.50
./configure --prefix=/usr --mandir=/usr/share/man --sysconfdir=/etc --localstatedir=/var --enable-experimental
make -j4
sudo make install

# change policies
sudo bash -c 'echo -e "[Policy]\nAutoEnable=true" >> /etc/bluetooth/main.conf'

# Remove unneeded files
sleep 2
cd ~
sudo rm -r bluez-5.50 bluez-5.50.tar.xz

# Reboot system
sudo reboot now