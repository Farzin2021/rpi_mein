#!/usr/bin/bash
# Version_File=V1

sudo bash /home/pi/rpi/heizmanager/scripts/ble_service_create_raw.sh 'etrv_handler' "sudo /usr/bin/python /home/pi/rpi/config/Controme_ETRV/main.py"
echo $(date) > /var/log/uwsgi/tempexec_V1
sudo sed -i "/\[program:btlehandler\]/c\\#\[program:btlehandler\]" /etc/supervisor/conf.d/messaging.conf
sudo supervisorctl -c /etc/supervisor/supervisord.conf reread
sudo supervisorctl -c /etc/supervisor/supervisord.conf update
sudo supervisorctl -c /etc/supervisor/supervisord.conf restart btlehandler