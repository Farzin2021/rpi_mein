#!/usr/bin/bash
SERVICE_NAME=$1
DESCRIPTION=$1
SERVICE_PATH=$2
TIMMER=$3

# remove the double quotes
DESCRIPTION=${DESCRIPTION//'"'/}
SERVICE_NAME=${SERVICE_NAME//'"'/}
SERVICE_PATH=${SERVICE_PATH//'"'/}

sudo systemctl stop $SERVICE_NAME'.service'
sudo systemctl disable $SERVICE_NAME'.service'
sudo systemctl stop $SERVICE_NAME'.timer'
sudo systemctl disable $SERVICE_NAME'.timer'

sudo cat > /etc/systemd/system/${SERVICE_NAME//'"'/}.service << EOF
[Install]
WantedBy=multi-user.target
[Unit]
Description=$DESCRIPTION
[Service]
Type=oneshot
ExecStartPre=/bin/sleep 2
ExecStart=/bin/sh -c 'sudo systemctl status $SERVICE_NAME.timer | grep -Po ".*; \K(.*)(?= ago)" | sed "s/[0-9]*//g" | grep -Ec "(^s$|^ms$)" | { read message; if [ "\$message"  -eq "0" ] ; then $SERVICE_PATH; else echo "skip_first"; fi }'
EOF

sudo cat > /etc/systemd/system/${SERVICE_NAME//'"'/}.timer << EOF
[Unit]
Description=$DESCRIPTION

[Timer]
OnBootSec=0
OnUnitActiveSec=$TIMMER min
AccuracySec=1s

[Install]
WantedBy=timers.target
EOF

# restart daemon, enable and start service
echo "Reloading daemon and enabling service"
sudo systemctl daemon-reload 
sudo systemctl enable $SERVICE_NAME'.timer'
sudo systemctl start $SERVICE_NAME'.timer'
echo "Service Started"

exit 0
