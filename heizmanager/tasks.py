# -*- coding: utf-8 -*-
from celery import shared_task
import logging
from abstracts.base_time_task import TimeTask
from heizmanager.models import Haus
import pytz
from datetime import datetime, timedelta
import helpers
from heizmanager.mobile.m_error import CeleryErrorLoggingTask
import requests
from fabric.api import local
from heizmanager import cache_helper as ch

logger = logging.getLogger('logmonitor')
p_logger = logging.getLogger('servicemonitorlog')


@shared_task(base=CeleryErrorLoggingTask)
def every_minute_task_manager():
    berlin = pytz.timezone('Europe/Berlin')
    berlin_now = datetime.now(berlin)
    berlin_now = berlin_now - timedelta(seconds=berlin_now.second, microseconds=berlin_now.microsecond)
    next_min = berlin_now + timedelta(minutes=1)
    now_ts = helpers.get_ts_from_str_time(helpers.get_str_formatted_time(berlin_now))
    ts_next_min = helpers.get_ts_from_str_time(helpers.get_str_formatted_time(next_min))

    for haus in Haus.objects.all():
        tasks = TimeTask.get_all_modules_tasks(haus)
        # high priority tasks are for tasks that need a repopulating, like heizprogramm changing
        high_prio_tasks = filter(lambda x:  (x.ts <= now_ts < x.end_ts if x.end_tm else now_ts <= x.ts < ts_next_min)
                                 and x.p == TimeTask.HIGH, tasks)

        re_populate = False
        # tasks with high priority should be run first
        for task in high_prio_tasks:
            re_populate = True
            task.execute()

        # if there was any task with high priority we need to populate tasks again
        if re_populate:
            tasks = TimeTask.get_all_modules_tasks(haus)

        tasks = filter(lambda x: (x.ts <= now_ts < x.end_ts if x.end_tm else now_ts <= x.ts < ts_next_min)
                       and x.p != TimeTask.HIGH, tasks)
        tasks.sort(key=lambda x: x.p, reverse=True)
        for task in tasks:
            task.execute()


# get request the page
@shared_task
def page_request():

    # process on error codes
    def nginx_status(status):
        if status == 502:
            if ch.get('nginx_error') == '502':
                reboot_system()
            logger.warning(u"%s|%s" % ('Systemstatus', u'502-Healing-Prozess aktiv.|1b'))
            local('sudo supervisorctl -c /etc/supervisor/supervisord.conf restart uwsgi')
            ch.set('nginx_error', '502')

    # reboot whole system
    def reboot_system():
        log_text = u"%s|%s" % ('Systemstatus', u'Automatischer Neustart durch 502-Healing-Prozess.|2b')
        p_logger.warning(log_text)
        logger.warning(log_text)
        local('sudo /sbin/reboot &')

    status = requests.get('http://127.0.0.1:80').status_code
    nginx_status(status)
