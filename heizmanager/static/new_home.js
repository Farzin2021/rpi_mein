var sliderSaving = 0,
    startAjaxSlider = function(){sliderSaving++;},
    endAjaxSlider = function(){sliderSaving--;},
    doneSlider = function(id ,number) {
        if(window.location.pathname == "/") {
            var url = '/m_raum/' + id + '/?rl';
        } else {
            var url = '/m_raum/' + id + '/';
        }
        // post to show_raum
        $.ajax({
            url: url,
            data: {
               'slidernumber' : number
            },
            type: 'post',
            async: true,
            beforeSend: function() {
                startAjaxSlider();
            },
            complete: function() {
                endAjaxSlider();
            }
        });
    },
    ajax_quickui = function(data,callback){
        $.ajax({
                url: '/quick-change/',
                data: data,
                type: 'POST',
                success: callback,
                beforeSend: function() {
                    startAjaxSlider();
                },
                complete: function() {
                    endAjaxSlider();
                }
            });
    }, ajax_smartui = function(data,callback){
        $.ajax({
                url: '/raumregelung-smart/',
                data: data,
                type: 'POST',
                success: callback,
            });
    },ajax_temperaturszenen = function(url,headers, data, callback,errorCallback){
            $.ajax({
                url: url,
                data: data,
                async: true,
                headers: headers,
                dataType: "json",
                type: 'POST',
                beforeSend: function() {
                    startAjaxSlider();
                },
                success: callback,
                error: errorCallback,
                complete: function() {
                    endAjaxSlider();
                }
           });
    };
window.addEventListener("beforeunload", function (e) {
    if (sliderSaving>0) {
        var confirmationMessage = 'Der Speichervorgang ist noch nicht beendet. Möchten Sie diese Seite wirklich verlassen?';
        (e || window.event).returnValue = confirmationMessage; //Gecko + IE
        return confirmationMessage; //Gecko + Webkit, Safari, Chrome etc.
    }
});
$(document).on('pageshow', function() {

    // load content with ajax to be faster ( multi trading )
    //$('.home_rome_list .ui-collapsible-content ul,.room_list').each(function(){
    $('.home_rome_list .ui-collapsible-content ul, #roomlistview').each(function(){
        var $self = $(this);
       $.get( "/m_raum_temp_html/" + $(this).data('id')+'/', function( data ) {
            $self.find('.roomli').html( data );
            var num = $self.find('.right_ui .details_left .soll').text();
            $self.find('li:last').append('<input  type="range"  name="slider" class="slider" value="'+num+'" min="18" max="26" step=".1" data-mini="true" data-highlight="true" data-popup-enabled="true" />').trigger('create');
            var $module_icon = $self.find('.right_ui .details_leaf .module-iconn img');
            if($module_icon!==undefined&&$module_icon!==null&&$module_icon.length>0){
                var image_src = $module_icon.attr("src");
                if(image_src!==undefined&&image_src!==null&&image_src.includes("icon_heizflaechenoptimierung")){
                    $module_icon.attr("src", image_src.replace(".png",".gif"));
                }
            }
            // check hand mode
           let mod_icon_src = $self.find('.details_left img').attr('src');
           if(mod_icon_src && mod_icon_src.endsWith('icon-hand.png')){
               $('.delete-all-hand').removeClass('hide');
               $self.find('.ui-slider .ui-btn-active').attr('style', function(i,s) { return (s || '')
                    + 'background-color: #779A45 !important;' });
           }
       });
    })


//    $( ".ui-slider" ).change(function(){
//        alert("hello");
//     });
    $('.floor_name').each(function(){
        $(this).css({'margin-left': ($(this).width()/2) * -1});
    })
    $('.room .module-temp').each(function(){
        if(parseFloat($(this).html()) == 0){
            $(this).parentsUntil('ul','li').addClass('gray-text');
        }
    })

    // room list
    $.get( "/get_main_menus_ajax", screen.width + "x" + screen.height, function( data ) {
        $("#nav-room-list ul").html(data.room_html).trigger('create');
        $("#nav-panel ul.module-list").html(data.module_html).trigger('create');
        $("#nav-panel ul.system-list").html(data.system_html).trigger('create');
    });
    // soll proui click
    $( document).on( "click", ".right_ui .soll",function(){
        $parent = $(this).parentsUntil('.ui-collapsible-content','ul');
        let room_id = $parent.data('id');
        $('#ziel_popup_roomid').val(room_id);
        $('.popupTempName').text( $parent.find('.room_name').text() );
        $('.popupTempIcon img').attr('src',$parent.find('.room_icon img').attr('src'));
        let mod_icon_src = $parent.find('.details_left img').attr('src');
        if(mod_icon_src && mod_icon_src.endsWith('icon-hand.png')){
            $('#popupZielInput').val($parent.find('.right_ui .ziel').text());
            $("#popupZielSave").data('id',  $parent.data('id'));
            $( "#popupZielTemp" ).popup("open");
        }else {
            $('#popupSollInput').val($parent.find('.right_ui .soll').text());
            $("#popupSollSave").data('id', $parent.data('id'));
            $(".popupSollTemp").popup("open");
        }

        return false;
    })

    // ziel proui click
    $( document).on( "click", ".right_ui .ziel",function(){
        $parent = $(this).parentsUntil('.ui-collapsible-content','ul');
        let room_id = $parent.data('id');
        $('#ziel_popup_roomid').val(room_id);
        $('.popupTempName').text( $parent.find('.room_name').text() );
        $('.popupTempIcon img').attr('src',$parent.find('.room_icon img').attr('src'));
        $('#popupZielInput').val($parent.find('.right_ui .ziel').text());
        $("#popupZielSave").data('id',  $parent.data('id'));
        $( "#popupZielTemp" ).popup("open");

        return false;
    })

    // select all checkbox in temperaturszenen
    $( document).on( "change", ".temperaturszenen .select-all-checkbox",function(){
        $('input:checkbox.select-checkbox').not(':hidden').not(this).prop('checked', this.checked).checkboxradio('refresh');
    });
    //
    $( document).on( "change", ".heizprogramm #heizprogramm-en-dis",function(){
        var haus_id = $(this).data('hausid');
        url = "/m_heizprogramm/" + haus_id + "/";
        var headers = {"X-CSRFToken": $("[name=csrfmiddlewaretoken]").val()};

        if ($(this).is(":checked")){
            data = {'enabled': true};
        }else{
            data = {'enabled': false};
        }
        ajax_temperaturszenen(url,headers, data, function(result){
        }, {});

    });
    $( document).on( "change", ".heizprogramm .select-heizprogramm input:radio[name='radio']",function(){
        if ($(this).is(':checked')) {
            var heizp_id = $(this).data('heizpid');
            var haus_id = $(this).data('hausid');
            url = "/m_heizprogramm/" + haus_id + "/";
            var headers = {"X-CSRFToken": $("[name=csrfmiddlewaretoken]").val()};
            var data = {'activated': heizp_id};
            ajax_temperaturszenen(url,headers, data, function(result){
            }, {});
        }
    });
    $(document).on( "change", ".heizflaechenoptimierung #form-heizflaechenoptimierung-regelstrategie input:radio[name='radio']",function(){
        if ($(this).is(':checked')) {
            var strategy_mode = $(this).data('mode');
            var ftype = $(this).data('chtype');
            url = $('.heizflaechenoptimierung #form-heizflaechenoptimierung-regelstrategie').attr("action");
            var headers = {"X-CSRFToken": $("[name=csrfmiddlewaretoken]").val()};
            var data = {'selected_mode': strategy_mode, 'ftype': ftype};
            ajax_temperaturszenen(url,headers, data, function(result){
            }, {});
        }
    });
    //
    $( document).on( "click", ".heizprogramm .heiz-reset-btn",function(){
        if (confirm('Möchten Sie wirklich alle Schaltpunkte löschen?')) {

            var haus_id = $(this).data('hausid');
            var heizp_id = $(this).data('heizpid');
            url = "/m_heizprogramm/" + haus_id + "/?edit=" + heizp_id;
            var headers = {"X-CSRFToken": $("[name=csrfmiddlewaretoken]").val()};

            data = {'reset_all': true};
            ajax_temperaturszenen(url,headers, data, function(){
               $('.li-entry').remove();
               $("#popupReset").popup("close");
             }, {});
        }
    });
    ///
    var clearUl = function(){
        $('ul.mon li:not(:first)').remove();
        $('ul.dien li:not(:first)').remove();
        $('ul.mit li:not(:first)').remove();
        $('ul.don li:not(:first)').remove();
        $('ul.freit li:not(:first)').remove();
        $('ul.sam li:not(:first)').remove();
        $('ul.son li:not(:first)').remove();
    }
    $('.heizprogramm .addpoint').click(function(e){

        e.preventDefault();
        var haus_id = $('#haus-id').val();
        var heizp_id = $('#heizp_id').val();
        var url = '/m_heizprogramm/' + haus_id + '/?edit=' + heizp_id;
        var data = $('#frm-addpoint').serialize();
        var headers = {"X-CSRFToken": $("[name=csrfmiddlewaretoken]").val()};
        if($('#time').val() == "" || !$('#select-mode').val()){
            $("#popupInfo").popup("open");
            return;
        }

          ajax_temperaturszenen(url,headers, data, function(data){
          clearUl();
          for (item in data){
                $('ul[id="'+data[item].day+'"]').append('<li data-id='+ data[item].id +' class="li-entry">'+
                '<span class="mod">'+data[item].mode_name+'</span> <span class="timeli">' + data[item].time +' <span class="arrow"></span> </span></li>');
          }
            $("#switchingPopup").popup("close");
            $('.dayUl').listview('refresh');
        }, function(request, textStatus, errorThrown){
            $("#switchingPopup").popup("close");
            $("#popupInfo").popup("open");
        });


    });

    $(document).on('click','.heizprogramm li.li-entry',function(){
        var haus_id = $('#haus-id').val();
        var heizp_id = $('#heizp_id').val();
        var url = '/m_heizprogramm/' + haus_id + '/?edit=' + heizp_id;
        var headers = {"X-CSRFToken": $("[name=csrfmiddlewaretoken]").val()};
        var data_id = $(this).attr('data-id');
        var data = {'data_id': data_id, 'action':'fill_popup'};
        $("#switchingEditPopup #data_id").val(data_id);

        ajax_temperaturszenen(url,headers, data, function(data){
           $('#switchingEditPopup #edit-time').val(data.time);
           $('#switchingEditPopup #select-edit-mode').val(data.mode);
           $('#switchingEditPopup #select-edit-mode').selectmenu('refresh');
           $("#switchingEditPopup").popup("open");
        }, {});

    });

    $('.heizprogramm .deletePoint').click(function(){
        var haus_id = $('#haus-id').val();
        var heizp_id = $('#heizp_id').val();
        var url = '/m_heizprogramm/' + haus_id + '/?edit=' + heizp_id;
        var data = {'data_id': $("#switchingEditPopup #data_id").val(), 'action':'delete'};
        var headers = {"X-CSRFToken": $("[name=csrfmiddlewaretoken]").val()};
        ajax_temperaturszenen(url,headers, data, function(data){
           $('li[data-id="'+data.data_id+'"]').remove();
           $('.dayUl').listview('refresh');
           $("#switchingEditPopup").popup("close");
        }, {});
    });

    $('.heizprogramm .editPoint').click(function(){
        var haus_id = $('#haus-id').val();
        var heizp_id = $('#heizp_id').val();
        var url = '/m_heizprogramm/' + haus_id + '/?edit=' + heizp_id;
        var data_id = $("#switchingEditPopup #data_id").val();
        var time = $('#switchingEditPopup #edit-time').val();
        var mode_id = $('#switchingEditPopup #select-edit-mode').val();
        var headers = {"X-CSRFToken": $("[name=csrfmiddlewaretoken]").val()};
        var data = {'data_id': data_id, 'action':'edit', 'mode_id': mode_id, 'time': time};
        clearUl();


        ajax_temperaturszenen(url,headers, data, function(data){
          if (data.error){
              $("#popupInfo").popup("open");
              return
          }
          for (item in data){
                $('ul[id="'+data[item].day+'"]').append('<li data-id='+ data[item].id +' class="li-entry">'+
                '<span class="mod">'+data[item].mode_name+'</span> <span class="timeli">' + data[item].time +' <span class="arrow"></span> </span></li>');
          }
            $("#switchingEditPopup").popup("close");
            $('.dayUl').listview('refresh');
        }, {});
    });
    /// temperature umschalten quick, smart ui
    $( document).on( "click", ".quick_change .hand_time, .raumregelung-smart .hand_time",function(){
        let img_mode_pic = $(this).find('img').attr('src');

        if(img_mode_pic.endsWith('icon-clock.png') || img_mode_pic.endsWith('icon-clock-gray.png')){
            $parent = $(this).parentsUntil('li');
            var room_id = $parent.data('id')
            var room_name = $parent.find('.raum_name').text();
            var src = $parent.find('.room_icon img').attr('src');
            var room_id = $parent.find('#first_row_roomid').val();
            $('#popup-change-mode .popupTempIcon img').attr('src',src);
            $('#popup-change-mode .popupTempName').html(room_name);
            $('#popup-change-mode #changemode_popup_roomid').val(room_id);
            $( ".raumregelung-smart #popup-change-mode" ).popup("open");
            let option_vals = [];
            $("#popup-change-mode #mode-select option").each(function()
            {
                if($(this).val() != '0'){
                    option_vals.push($(this).val());
                }
            });
            let data = {
                'get_enabled_mode_room': true,
                'haus_id': $('#haus_id').val(),
                'raum_id': room_id,
                'mode_id_lst': JSON.stringify(option_vals)
            }
            ajax_smartui(data, function (result){
                $("#popup-change-mode #mode-select option").show();
                for(let mode_id in result){
                    $("#popup-change-mode #mode-select option[value='" + result[mode_id] + "']").hide();
                }
            })
            return 0;
        }

        if(img_mode_pic.endsWith('icon-hand-grey.png')){
            $( ".raumregelung-smart #all_popupTime" ).popup("open");
            return 0;
        }
        if(!img_mode_pic.endsWith('icon-hand.png')){
            return 0;
        }
        $parent = $(this).parentsUntil('li');
        $('.popupTimeContent .popup_raum_name').text( $parent.find('.raum_name').text() );
        $('.popupTimeContent img').attr('src',$parent.find('.room_icon img').attr('src'));
        $("#popupTime #room_id").val($parent.find('#raum_id').val());
        room_id = $('#room_id').val();
        let data = {
               'get_room_slider': true,
               'room_id':room_id,
               'smart_ui': true
        }
        // check is sending via smartui or not to handle permission
        let smartui = $('#smartui_page').val();
        if (smartui){
            data['smartui'] = true;
        }
        if(room_id > 0){
            ajax_quickui(data,function(result){
                $('#popupTime .time_inner_room').html(result.room_time);
                $('#popupTime #time').val(result.slider_val);
                $('#popupTime #time').slider("refresh");
                $( "#popupTime" ).popup("open");
            });
        }

        return false;
    })

    /// temperature umschalten home, room
    $( document).on( "click", ".home .hand_time, .room .hand_time",function(e){
        e.preventDefault();
        $parent = $(this).closest('ul');
        let img_mode_pic = $parent.find('.hand img').attr('src');

        if(img_mode_pic.endsWith('icon-clock.png') || img_mode_pic.endsWith('icon-clock-gray.png')){
            let room_id = $parent.data('id');
            let room_name = $parent.find('.room_name').text();
            let src = $parent.find('.room_icon img').attr('src');
            $('#popup-change-mode .popupTempIcon img').attr('src',src);
            $('#popup-change-mode .popupTempName').html(room_name);
            $('#popup-change-mode #changemode_popup_roomid').val(room_id);
            $( "#popup-change-mode" ).popup("open");
            let option_vals = [];
            $("#popup-change-mode #mode-select option").each(function() {
                if($(this).val() != '0'){
                    option_vals.push($(this).val());
                }
            });
            let data = {
                'get_enabled_mode_room': true,
                'haus_id': $('#haus-id').val(),
                'raum_id': room_id,
                'mode_id_lst': JSON.stringify(option_vals)
            }
            ajax_smartui(data, function (result){
                $("#popup-change-mode #mode-select option").show();
                for(let mode_id in result){
                    $("#popup-change-mode #mode-select option[value='" + result[mode_id] + "']").hide();
                }

            })
        }
    })

    $( document).on( "click", ".top_header",function(){
        $parent = $(this).parent("ui");
        $( "#all_popupTime" ).popup("open");
        return false;
    })

    $( document).on( "click", ".raumregelung-smart .duration-for-all",function(){
        $parent = $(this).parent("ui");
        $( ".raumregelung-smart #all_popupTime" ).popup("open");
        return false;
    })
    //
    $( document).on( "click", ".top_header",function(){
        $( "#all_popupTime" ).popup("open");
        return false;
    })

    // popup save function
    $(document).on( "click", ".home #popupSollSave,.room #popupSollSave",function(){
        let num;
        let id  = $(this).data('id') ,
        ul = $('ul[data-id='+ id +']');
        let icon = ul.find('.details_left img').attr('src')
        if( icon == "/static/icons/icon-hand.png"){
            num = parseFloat($('#popupZielInput').val().replace(',', '.'))
        } else{
            num = parseFloat($('#popupSollInput').val().replace(',', '.'))
        }
        ul.find('.slider').val(num).slider("refresh");
        ul.find('.right_ui .soll').html(num.toFixed(2));
        var mode_id = ul.find('.right_ui .hand_time').data('id');
        doneSlider(id,num.toString(), mode_id);

        var soll = num.toFixed(2);
        var soll_converted = ((soll-10) * 100) / 25
        var sign = ul.find('.right_ui .signed').text();
        var offset = parseFloat(ul.find('.right_ui .heat5_room_offset').text()).toFixed(2);
        var offset_converted = (offset * 100) / 25;
        ul.find('.right_ui .soll').html(soll);
        ul.find('#vbar_soll_s2').css('left',soll_converted+"%");
        var hand_time = ul.find('.right_ui .hand_time').data('id');

        if (sign=="+"){
            var ziel = parseFloat(soll) + parseFloat(offset);
        }
        else{
            var ziel = parseFloat(soll) - parseFloat(offset);
        }

        var ziel_converted = ((ziel-10) * 100) / 25;
        ul.find('#vbar_ziel_s2').css('left',ziel_converted+"%");

        if(sign == "-")
            offset = offset * -1;

        if (parseFloat(offset) > 0){
            ul.find('.arrow-bar-right').css({'left':soll_converted+"%", 'width': offset_converted+"%"});
            ul.find('.arrow-right').css({'left':ziel_converted+"%"});
        }
        if (parseFloat(offset) < 0){
            ul.find('.arrow-bar-left').css({'left':ziel_converted+"%", 'width': offset_converted+"%"});
            ul.find('.arrow-left').css({'left':ziel_converted+"%"});
        }

        updatetemps(ul, parseFloat(num));
        $( "#popupTemp" ).popup("close");

    });

    //  ziel save popup proui
    $(document).on('click', ".home #popupZielSave,.room #popupZielSave", function(){
        let num = parseFloat($('#popupZielInput').val()
            .replace(',', '.')).toFixed(2);
        let room_id  = $('#ziel_popup_roomid').val() ,
        ul = $('ul[data-id='+ room_id +']');
        let icon = ul.find('.details_left img').attr('src')
        let data = {}
        if(icon == "/static/icons/icon-hand.png"){
            doneSlider(room_id,num.toString());
            ul.find('.slider').val(num).slider("refresh");
            ul.find('.right_ui .soll').html(num);
            ul.find('.right_ui .ziel').html(num);
        }else {
            data = {
                'set_ziel_slider': true,
                'room_id': room_id,
                'slidernumber': num,
                'all_time': 180
            };
            ajax_quickui(data, function (result) {
                ul.find('.slider').val(num).slider("refresh");
                ul.find('.right_ui .soll').html(num);
                ul.find('.right_ui .ziel').html(num);
                ul.find('.details_left .soll').attr('style', "color: grey;");
                ul.find('.details_left img').attr('src', '/static/icons/icon-hand.png');
                ul.find('.ui_details .hand_time').html(result.time);
                $('.delete-all-hand').removeClass('hide');
            });
        }

    });

    // temp popup open
    $( document).on( "click", ".wochenkalender .temp_offset,.jahreskalender .temp_offset",function(){

        $('.popupTempName').text( $('.room_wrapper').text() );
        $('.popupTempIcon img').attr('src',$('.room_wrapper img').attr('src'));
        $('#popupTempInput').val($('.temp_offset').text());
        $( "#popupTemp" ).popup("open");
        return false;
    });
    // save popup timer
    $('.timer .temp_offset').click(function(){
        $('#popupTemp').popup('open');
    });

    $('.timer #popupTempSave').click(function(){
        url = $('form').attr( "action" );
        console.log(url);
        $.ajax({
            url: url,
            data: {
                'offset': $('#popupTempInput').val(),
                'starttime': $('#starttime').val(),
                'endtime': $('#endtime').val(),
                'csrfmiddlewaretoken': $("input[name=csrfmiddlewaretoken]").val(),
            },
            type: 'post',
            async: true
        });
    });

    // popup save function wochenkalender
    $(document).on( "click", ".wochenkalender #popupTempSave",function(){
        var num = parseFloat($('#popupTempInput').val());

        $('.ui-slider-input').val(num).slider("refresh");
        $('.temp_offset').html( num.toFixed(2) );
        $( "#popupTemp" ).popup("close");
        $('.wochenkalender  input[name=offset]').val( num.toFixed(2)  );
    });

    // popup save function jahreskalender
    $(document).on( "click", ".jahreskalender #popupTempSave",function(){
        var num = parseFloat($('#popupTempInput').val());

        $('.ui-slider-input').val(num).slider("refresh");
        $('.temp_offset').html( num.toFixed(2) );
        $( "#popupTemp" ).popup("close");
        $('.jahreskalender  input[name=offset]').val( num.toFixed(2)  );
    });


    // don't scroll to top
    $( document).on( "click", ".ui-collapsible-heading a",function(){
        return false;
    });


    // plus and minus temp in differenzregelung
    $(document).on( "click", ".differenzregelung .plus-tempOne,.differenzregelung .minus-tempOne",function(){
        var minus = false;
        if($(this).hasClass('minus-tempOne'))
            minus = true;
        var $slider = $(this).parent().find('.ui-slider-input');
        if($slider.size() == 0)
            return;
        var number = parseFloat($slider.val());
        if(minus)
            number -= 1;
        else
            number += 1;
        $slider.val(number.toFixed(2));
        $slider.trigger('change');
        $slider.slider("refresh");
    });

    $(document).on( "click", ".home .plus-tempOne,.home .minus-tempOne,.room .plus-tempOne,.room .minus-tempOne,.jahreskalender .plus-tempOne,.jahreskalender .minus-tempOne,.wochenkalender .plus-tempOne,.wochenkalender .minus-tempOne",function(){
        var minus = false;
        if($(this).hasClass('minus-tempOne'))
            minus = true;
        var $slider = $(this).parent().find('.ui-slider-input');
        if($slider.size() == 0)
            return;
        var number = parseFloat($slider.val());
        if(minus)
            number -= 0.1;
        else
            number += 0.1;
        $slider.val(number.toFixed(2));
        $slider.trigger('change');
        $slider.trigger('slidestop');
        $slider.slider("refresh");
    });

    // slider diffrenzegelung change
    $( document).on( "change", ".differenzregelung #slider_s2",function(){

        var soll_Val = $('.differenzregelung .soll-val').html();
        var newNumber = parseFloat($(this).val());
        var offset = $('.differenzregelung .s-row .right .offset-val').html();
        ziel_val = newNumber + parseFloat(offset)
        ziel_val_convert = (ziel_val * 100) /120;
        vbar_val = (newNumber * 100) /120;
        if(vbar_val > 100){
            vbar_val = 100;
        }
        if(vbar_val < 0){
            vbar_val = 0;
        }
        if(ziel_val_convert > 100){
            ziel_val_convert = 100;
        }
        if(ziel_val_convert < 0 ){
            ziel_val_convert = 0;
        }

        //move slider

        $('.differenzregelung .arrow-left').css("left",ziel_val_convert+"%");


        $('.differenzregelung .arrow-bar-right').css("left",vbar_val+"%");
        $('.differenzregelung .arrow-bar-left').css("left",ziel_val_convert+"%");
        $('.differenzregelung #vbar_soll_s2').css("left",vbar_val+"%");
//        console.log(newNumber);
        if(newNumber.toFixed(2) <0 ){
            newNumber = 0.0;
        }
        $('.differenzregelung .soll-val').html(newNumber.toFixed(2));
        $('.differenzregelung .ziel-val').html(ziel_val.toFixed(2));
        $('.differenzregelung .arrow-bar-right').css("right","");
        try{
            var arrow_bar_right_right = $('.differenzregelung .arrow-bar-right').css("right");
            arrow_bar_right_right = arrow_bar_right_right.replace('px','');

            if (arrow_bar_right_right < 0){
                $('.differenzregelung .arrow-bar-right').css("right","-1");
                $('.differenzregelung .arrow-bar-right').css("left","");
            }
            var arrow_bar_left_right = $('.differenzregelung .arrow-bar-left').css("right");
            arrow_bar_left_right = arrow_bar_left_right.replace('px','');
            if (arrow_bar_left_right < 0){
                $('.differenzregelung .arrow-bar-left').css("right","-1");
                $('.differenzregelung .arrow-bar-left').css("left","");
            }
        }catch(err){}

        // vbar_ziel_s2 (square) controll right
        var move = false;
        if (parseFloat(soll_Val) > newNumber){
            move = true;
        }

        var at_end = false;
        try{
        var ziel_right = $('.differenzregelung #vbar_ziel_s2').css("right");
        ziel_right = ziel_right.replace('px','');
        }catch(err){}
        if(ziel_right <= 0){
            //vbar_val = 100;
            at_end = true;
        }
        else{
            at_end = false;
        }
        if(!at_end ||move){
            $('.differenzregelung #vbar_ziel_s2').css("left",ziel_val_convert+"%");
        }
        $('.differenzregelung .vbar_ziel_s2').css('right','');
        //vbar_ziel_s2_center
        if(ziel_val_convert<0){
            ziel_val_convert = 0;
        }
        if(ziel_val_convert> 100){
            ziel_val_convert = 100;
        }
        $('.differenzregelung #vbar_ziel_s2_center').css("left",ziel_val_convert+"%");
        try{
            var arrow_bar_right_left = $('.differenzregelung .arrow-bar-right').css("left");
            arrow_bar_right_left = arrow_bar_right_left.replace('px','');
            }catch(err){}
            if(arrow_bar_right_left > 0){
                $('.differenzregelung .arrow-right').css("left",ziel_val_convert+"%");
            }
        //// control  bar left
        try{
            var box_margin_left = $('.differenzregelung #vbar_ziel_s2').css("margin-left");
            var box_left = $('.differenzregelung #vbar_ziel_s2').css("left");
            box_margin_left = box_margin_left.replace("px","");
            box_left = box_left.replace("px","");
            var box_left_converted = Math.abs(box_margin_left) + box_left;
            if(parseFloat(box_left) < parseFloat(box_left_converted)){
                $('.differenzregelung #vbar_ziel_s2').css("left",parseFloat(box_left_converted));
                console.log("biron");
                console.log("box_left"+box_left);
                console.log("box_left_converted"+parseFloat(box_left_converted));
                console.log("box_margin_left"+box_margin_left);
            }
        }catch(err){console.log(err);}
        var hausid = $('.differenzregelung #hausid').val();
        var objid = $('.differenzregelung #dregid').val();
        $.ajax({
            url: '/m_differenzregelung/'+hausid+'/show/' + objid + '/',
            data: {
                'slidernumber' : newNumber,
                'csrfmiddlewaretoken': $("input[name='csrfmiddlewaretoken']").val(),
            },
            type: 'POST',
            async: true,
            success: function (result) {
            },
        });
//        $('.wochenkalender  .temp_offset').html( newNumber.toFixed(2)  );
//        $('.wochenkalender  input[name=offset]').val( newNumber.toFixed(2)  );
    });
    /// checkbox check for active inactive mods
    $(document).on( "change", ".temperaturszenen .mode-active",function(){
        var checked = "";
        var mode_id = $(this).val();
        var raum_id = $(this).attr('data-id');

        if($(this).is(":checked")){
            checked = true;
            $('.temperaturszenen div[data-id="'+mode_id+'"] #solltemp-'+raum_id+'-'+mode_id).slider('enable');
        }else{
            $('.temperaturszenen div[data-id="'+mode_id+'"] #solltemp-'+raum_id+'-'+mode_id).slider('disable');
        }
        data ={
         'mode-active':true,
          mode_id: mode_id,
          raum_id:raum_id,
          is_checked:checked
        };
        var url = "/m_raum/"+raum_id+"/temperaturszenen/";
        var headers = {"X-CSRFToken": $("[name=csrfmiddlewaretoken]").val()};
        ajax_temperaturszenen(url, headers, data, function(data){}, {});
    });
    // change slider in ruam temperatur modi
    $(document).on( "change", ".temperaturszenen .solltemp-slider",function(){
        var mode_id = $(this).attr('data-modeid');
        var raum_id = $(this).attr('data-id');
        $('.temperaturszenen li[data-id="'+raum_id+'"] div[data-id="'+mode_id+'"] .solltemp').html(parseFloat($(this).val()).toFixed(2));

    });

      // change slider in ruam temperatur modi
    $(document).on( "slidestop", ".temperaturszenen .solltemp-slider",function(){
        var mode_id = $(this).attr('data-modeid');
        var raum_id = $(this).attr('data-id');

        var url = "/m_raum/"+raum_id+"/temperaturszenen/";
        headers = {"X-CSRFToken": $("[name=csrfmiddlewaretoken]").val()};
        data = {
            'set_slider':true,
            'slider_val':parseFloat($(this).val()),
            'raum_id': raum_id,
            'mode_id': mode_id
        };
        ajax_temperaturszenen(url,headers, data, function(data){}, {});

    });
    // active mode in room page
    $(document).on( "change", ".room #mode-select",function(){
        var mode_id = $(this).val();
        var raum_id = $(this).attr('data-id');
        $('.temperaturszenen li[data-id="'+raum_id+'"] div[data-id="'+mode_id+'"] .solltemp').html(parseFloat($(this).val()).toFixed(2));
        var url = "/m_raum/"+raum_id+"/temperaturszenen/";
        headers = {"X-CSRFToken": $("[name=csrfmiddlewaretoken]").val()};
        data = {'mode_activate':true, 'mode_id': mode_id};
        ajax_temperaturszenen(url,headers, data, function(data){
            if(!data.success){
                $('#mode-select').val(data.activated_mode);
                alert("selected mode for this room is not enabled");
            }else{
                $('.activated-mode').html($('#mode-select :selected').text());
                if (parseInt($('.right_ui .hand_time').data('id')) == -1)
                    return;
                $('.right_ui .ui_details.hand_click .details_sign').html("");
                $('.right_ui .details_left.hand img').attr("src","/static/icons/icon-clock.png");
                $('.right_ui .details_right.hand_time').html(data.activated_mode_name);
                $('.right_ui .soll').html(parseFloat(data.solltemp).toFixed(2));
                var offset = $('.right_ui .heat5_room_offset').text();
                $('.ui_details.temperature .soll').removeClass('gray');
                $('.ui_details.temperature .heat5_room_offset').removeClass('gray');
                $(".right_ui .details_right.hand_time").data('id', data.activated_mode);

                // move temp-bar
                /////
                var soll_converted = (((parseFloat(data.solltemp)) - 10) * 100) / 25;
                var sign = $('.right_ui .signed').text();
                offset_converted = (Math.abs(offset) * 100) / 25;
                $('#vbar_soll_s2').css('left',soll_converted+"%");
                let ziel = 0;
                if (sign=="+"){
                    ziel = parseFloat(data.solltemp) + parseFloat(offset);
                }
                else{
                    ziel = parseFloat(data.solltemp) - parseFloat(offset);
                }
                $('#sollslider').val(data.solltemp);
                $('#sollslider').slider('refresh');
                $('#zielslider').val(ziel);
                $('#zielslider').slider('refresh');
                ////
                $('.right_ui .ziel').html(ziel.toFixed(2));
                ////
                var ziel_converted = (((parseFloat(ziel)) - 10) * 100) / 25;
                $('#vbar_ziel_s2').css('left',ziel_converted+"%");

                if(sign == "-")
                    offset = offset * -1;

                if (parseFloat(offset) > 0){
                    $('.arrow-bar-right').css({'left':soll_converted+"%", 'width': offset_converted+"%"});
                    $('.arrow-right').css({'left':ziel_converted+"%"});
                }

                if (parseFloat(offset) < 0){
                    $('.arrow-bar-left').css({'left':ziel_converted+"%", 'width': offset_converted+"%"});
                    $('.arrow-left').css({'left':ziel_converted+"%"});
                }
                $( "#popup-change-mode" ).popup("close");


            }
        }, {});
    });
    // active mode in home page
    $(document).on( "change", ".home #mode-select",function(){
        var mode_id = $(this).val();
        $("ul[class='room_list']").each(function() {
            var raum_id = $(this).attr('data-id');
            var url = "/m_raum/"+raum_id+"/temperaturszenen/";
            headers = {"X-CSRFToken": $("[name=csrfmiddlewaretoken]").val()};
            data = {'mode_activate':true, 'mode_id': mode_id};
            ajax_temperaturszenen(url,headers, data, function(data){
                ul = $('ul[data-id='+data.raum_id+']');
                var activated_mode_id = ul.find('.right_ui .hand_time').data('id');
                if (parseInt(activated_mode_id) == -1)
                    return;
                $('.activated-mode').html($('#mode-select :selected').text());
                ul.find('.right_ui .ui_details.hand_click .details_sign').html("");
                ul.find('.right_ui .details_left.hand img').attr("src","/static/icons/icon-clock.png")
                ul.find('.slider').val(data.solltemp);
                ul.find('.slider').slider("refresh");
                ul.find('.right_ui .details_right.hand_time').html(data.activated_mode_name);
                ul.find('.right_ui .soll').html(parseFloat(data.solltemp).toFixed(2));
                var offset = ul.find('.right_ui .heat5_room_offset').text();

                ul.find('.ui_details.temperature .soll').removeClass('gray');
                ul.find('.ui_details.temperature .heat5_room_offset').removeClass('gray');
                ul.find(".right_ui .details_right.hand_time").data('id', data.activated_mode);

                // move temp-bar
                /////
                var soll_converted = ( (parseFloat(data.solltemp).toFixed(2) -10) * 100) / 25
                var sign = ul.find('.right_ui .signed').text();
                offset_converted = (offset * 100) / 25;
                ul.find('#vbar_soll_s2').css('left',soll_converted+"%");

                if (sign=="+"){
                    ziel = parseFloat(data.solltemp) + parseFloat(offset);
                }
                else{
                    ziel = parseFloat(data.solltemp) - parseFloat(offset);
                }
                        ////
                ul.find('.right_ui .ziel').html(ziel.toFixed(2));
                var ziel_converted = ((ziel-10) * 100) / 25;
                ul.find('#vbar_ziel_s2').css('left',ziel_converted+"%");

                if(sign == "-")
                    offset = offset * -1;

                if (parseFloat(offset) > 0){
                    ul.find('.arrow-bar-right').css({'left':soll_converted+"%", 'width': offset_converted+"%"});
                    ul.find('.arrow-right').css({'left':ziel_converted+"%"});
                }
                if (parseFloat(offset) < 0){
                    ul.find('.arrow-bar-left').css({'left':ziel_converted+"%", 'width': offset_converted+"%"});
                    ul.find('.arrow-left').css({'left':ziel_converted+"%"});
                }
            }, {});
        });

    });
    ///
    $(document).on( "change", ".raumregelung-smart #two #mode-select",function(){
        var mode_id = $(this).val();
        $("#two .room-row").each(function() {
            var raum_id = $(this).attr('data-id');
            var url = "/m_raum/"+raum_id+"/temperaturszenen/";
            headers = {"X-CSRFToken": $("[name=csrfmiddlewaretoken]").val()};
            data = {'mode_activate':true, 'mode_id': mode_id};
            ajax_temperaturszenen(url,headers, data, function(data){
                let li = $('#two li[data-id='+data.raum_id+']');
                li.find('.soll-val').html(parseFloat(data.solltemp).toFixed(1));
                li.find('#sollslider').val(data.solltemp);
                li.find('#sollslider').slider('refresh');
                li.find('.stat-text').html(data.activated_mode_name);
                li.find('#dauerhafte_room_modeid').html(data.activated_mode);

            }, {});
        });

    });

    // Temperaturszene umschalten, smartui
    $(document).on( "change", ".raumregelung-smart #popup-change-mode #mode-select",function(){
        let mode_id = $(this).val();
        let raum_id = $('#changemode_popup_roomid').val()
        let url = "/m_raum/"+raum_id+"/temperaturszenen/";
        let headers = {"X-CSRFToken": $("[name=csrfmiddlewaretoken]").val()};
        let data = {'mode_activate':true, 'mode_id': mode_id};
        $( ".raumregelung-smart #popup-change-mode" ).popup("close");
        ajax_temperaturszenen(url,headers, data, function(data){
            let li = $('#two li[data-id='+data.raum_id+']');
            li.find('.soll-val').html(parseFloat(data.solltemp).toFixed(1));
            li.find('#sollslider').val(data.solltemp);
            li.find('#sollslider').slider('refresh');
            li.find('.stat-text').html(data.activated_mode_name);
            li.find('#dauerhafte_room_modeid').html(data.activated_mode);

        }, {});
    });
    ///
    $(document).on( "change", ".home #popup-change-mode #mode-select",function(){
        let mode_id = $(this).val();
        let raum_id = $('#changemode_popup_roomid').val()
        let url = "/m_raum/"+raum_id+"/temperaturszenen/";
        let headers = {"X-CSRFToken": $("[name=csrfmiddlewaretoken]").val()};
        let data = {'mode_activate':true, 'mode_id': mode_id};
        $( ".home #popup-change-mode" ).popup("close");
        ajax_temperaturszenen(url,headers, data, function(data){
            console.log(data);
            // let li = $('#two li[data-id='+data.raum_id+']');
            // li.find('.soll-val').html(parseFloat(data.solltemp).toFixed(1));
            // li.find('#sollslider').val(data.solltemp);
            // li.find('#sollslider').slider('refresh');
            // li.find('.stat-text').html(data.activated_mode_name);
            // li.find('#dauerhafte_room_modeid').html(data.activated_mode);

        }, {});

    });
    ///
    $(document).on( "click", ".m_temperaturszenen .first-row .solltemp",function(){
       var room_name = $(this).parent().find('.second-part').html();
       var room_id = $(this).parent().parent().parent().data('raumid');
       var mode_id = $(this).parent().parent().parent().data('id');
       $('.popupTempName').html(room_name);
       $('#popupTempInput').val($(this).html());

       $('#popupTempSave').data('raumid', room_id);
       $('#popupTempSave').data('modeid', mode_id);
       $('#popupTemp').popup('open');
    });

    $(document).on( "click", ".m_temperaturszenen #popupTempSave",function(){
       var solltemp = $(this).parent().find('#popupTempInput').val();
       var mode_id = $(this).data('modeid');
       var raum_id = $(this).data('raumid');
       $("#modes-list li[data-id='"+ raum_id +"'] div[data-id='"+ mode_id +"']").find('.solltemp-slider').val(parseFloat(solltemp).toFixed(2)).slider('refresh');
       $("#modes-list li[data-id='"+ raum_id +"'] div[data-id='"+ mode_id +"']").find('.solltemp').html(parseFloat(solltemp).toFixed(2));
       var headers= {"X-CSRFToken": $("[name=csrfmiddlewaretoken]").val()};
       var url = "/m_raum/"+raum_id+"/temperaturszenen/";
       data = {'set_slider':true, 'slider_val': solltemp, 'raum_id': raum_id,'mode_id': mode_id};
       ajax_temperaturszenen(url,headers, data, () => {}, {});

    });
    // filter by modes in temperaturszenen
    $(document).on( "change", ".temperaturszenen #select-mode",function(){
        var mode_id = $(this).val();
        var hausid = $(this).attr('data-id');
        var raum_id = $('#select-raum').val();
        headers = {"X-CSRFToken": $("[name=csrfmiddlewaretoken]").val()};
        data = {'filter':'mode', 'mode_id': mode_id, 'raum_id': raum_id};
        url =  "/m_temperaturszenen/"+hausid+"/";
        ajax_temperaturszenen(url,headers, data, (data) => {
            $('.temperaturszenen .li-modes').hide()
            for(var item in data){
                var str_id = ".temperaturszenen #li-"+ data[item].raum_id+ "-" + data[item].mode_id;
                $(str_id).show();
            }
        }, {});
    });

    // filter by raums in temperaturszenen
    $(document).on( "change", ".temperaturszenen #select-raum",function(){
        var mode_id = $('#select-mode').val();
        var hausid = $(this).attr('data-id');
        var raum_id = $(this).val();
        var headers = {"X-CSRFToken": $("[name=csrfmiddlewaretoken]").val()};
        var data = {'filter':'raum', 'mode_id': mode_id, 'raum_id': raum_id};
        var url = "/m_temperaturszenen/"+hausid+"/";
        ajax_temperaturszenen(url,headers, data, (data) => {
            $('.temperaturszenen .li-modes').hide()
            for(var item in data){
                var str_id = ".temperaturszenen #li-"+ data[item].raum_id+ "-" + data[item].mode_id;
                $(str_id).show();
            }
        }, {});
    });

    // wochenkalender slider change
    $( document).on( "change", ".wochenkalender .ui-slider-input",function(){
        var newNumber = parseFloat($(this).val());
        $('.wochenkalender  .temp_offset').html( newNumber.toFixed(2)  );
        $('.wochenkalender  input[name=offset]').val( newNumber.toFixed(2)  );
    });

    // jahreskalender slider change
    $( document).on( "change", ".jahreskalender .ui-slider-input",function(){
        var newNumber = parseFloat($(this).val());
        $('.jahreskalender  .temp_offset').html( newNumber.toFixed(2)  );
        $('.jahreskalender  input[name=offset]').val( newNumber.toFixed(2)  );
    });

    // home page sliders change
    $( document).on( "change", ".home .slider,.room #sollslider",function() {
        let room_id = $('#room-id').val()
        let ul;
        if(room_id){
            ul = $('ul[data-id='+ room_id +']')
        } else{
            ul = $(this).parentsUntil('.ui-collapsible-content', 'ul')
        }
        let newNumber = parseFloat($(this).val());
        let soll = newNumber.toFixed(2);
        let soll_converted = ((soll-10) * 100) / 25
        let sign = ul.find('.right_ui .signed').text();
        let offset = parseFloat(ul.find('.right_ui .heat5_room_offset').text()).toFixed(2);
        let offset_converted = (offset * 100) / 25;
        ul.find('.right_ui .soll').html(soll);
        ul.find('#vbar_soll_s2').css('left',soll_converted+"%");

        if (sign=="+"){
            var ziel = parseFloat(soll) + parseFloat(offset);
        }
        else{
            var ziel = parseFloat(soll) - parseFloat(offset);
        }

        var ziel_converted = ((ziel-10) * 100) / 25;
        ul.find('#vbar_ziel_s2').css('left',ziel_converted+"%");

        if(sign == "-")
            offset = offset * -1;

        if (parseFloat(offset) > 0){
            ul.find('.arrow-bar-right').css({'left':soll_converted+"%", 'width': offset_converted+"%"});
            ul.find('.arrow-right').css({'left':ziel_converted+"%"});
        }
        if (parseFloat(offset) < 0){
            ul.find('.arrow-bar-left').css({'left':ziel_converted+"%", 'width': offset_converted+"%"});
            ul.find('.arrow-left').css({'left':ziel_converted+"%"});
        }
        updatetemps(ul, newNumber);
    });

    function updatetemps(ul, solltemp) {
        slidertemp = parseFloat(solltemp);
        offsetsign = ul.find('.right_ui .signed').html();
        offset = parseFloat(ul.find('.right_ui .heat5_room_offset').html());
        temp= parseFloat(ul.find('.right_ui .temp').html());
        if(offsetsign == "-"){
           targettemp = slidertemp - offset
        } else{
           targettemp = slidertemp + offset
        }
        var hand_time = ul.find('.right_ui .hand_time').data('id');
        if (hand_time == -1){
            ul.find('.right_ui .ziel').text(solltemp.toFixed(2));
        }
        else{
            ul.find('.right_ui .ziel').text(targettemp.toFixed(2));
        }
        ul.find('.right_ui .details_leaf .leaf-icon').removeClass("ui-icon-leafgreen");
        ul.find('.right_ui .details_leaf .leaf-icon').removeClass("ui-icon-leafgray");
        if (targettemp >= temp){
            ul.find('.right_ui .details_leaf .leaf-icon').addClass("ui-icon-leafgray");
            ul.find('.right_ui  .details_sign f').text(">");
        } else {
            ul.find('.right_ui .details_leaf .leaf-icon').addClass("ui-icon-leafgreen");
            ul.find('.right_ui  .details_sign f').text("<");
        }
    }

    // room slider
    $( document).on( "slidestop", ".home .slider,.room #sollslider",function(){
        let room_id = $('#room-id').val();
        let ul = $('ul[data-id='+ room_id +']');
        let newNumber = parseFloat($(this).val());
        let num = $(this).val().replace('.',',');
        doneSlider(ul.data('id'), num);
        updatetemps(ul, newNumber);
    });

    // home sliders
    $( document).on( "slidestop", ".home .slider",function(){
        let ul = $(this).parentsUntil('.ui-collapsible-content', 'ul')
        let newNumber = parseFloat($(this).val());
        let temp_type = $('.temp-type').hasClass('temporaryCls');
        if(temp_type){
            let data = {
               'set_ziel_slider': true,
               'room_id': ul.data('id'),
               'slidernumber' : newNumber,
               'all_time': 180 //# default
            };
            ajax_quickui(data,function(result){
                ul.find('.right_ui .hand img').attr('src', '/static/icons/icon-hand.png');
                ul.find('.right_ui .module-iconn img').attr('src', '/static/icons/icon-hand.png');
                ul.find('.right_ui .hand_time').html(result.time);
                ul.find('.right_ui .ziel').css('color', '#d79a2d');
                ul.find('.right_ui .soll').css('color', 'gray');
                $('.delete-all-hand').removeClass('hide');
                ul.find('.ui-slider .ui-btn-active').attr('style', function(i,s) { return (s || '')
                    + 'background-color: #779A45 !important;' });
            });
        }else{
            let num = $(this).val().replace('.',',');
            doneSlider(ul.data('id'), num);
            updatetemps(ul, newNumber);
        }
    });

    // home temp type change
    $(document).on("click", ".home .temp-type", function (){
        console.log($('#func_access').val())
        if($('#func_access').val() == 'full_access') {
            $(this).toggleClass('temporaryCls');
        }
    });

    $( document).on( "change", ".room #zielslider",function(){
        $('.delete-all-hand').removeClass('hide');
        let room_id = $('#room-id').val()
        let ul;
        ul = $('ul[data-id='+ room_id +']')
        let val = parseFloat($(this).val());
        ul.find('.details_left .ziel').html(val.toFixed(2));
        ul.find('.details_left .soll').attr('style', "color: grey;");
        ul.find('.details_left img').attr('src','/static/icons/icon-hand.png');
    });

    $( document).on( "slidestop", ".room #zielslider",function(){
        let duration_val = $('#duration_time').val();
        let room_id = $('#room-id').val()
        let val = parseFloat($(this).val());
        let data = {
            'set_ziel_slider': true,
            'slidernumber' : val,
            'room_id': room_id,
            'all_time': duration_val,
        }
        ajax_quickui(data, function(result){
            $('.hand_time').html(result.time);
        })
    });



    //// slider hand time
    ///
    $( document).on( "slidestop",
        ".quick_change #all_time, .raumregelung-smart #all_time",function() {
        let room_id = $('#room-id').val();
        let slider_val;
        if(room_id){
            slider_val = $('#duration_time').val()
        } else {
            slider_val = $('#all_time').val();
        }
        let data = {
               'set_all_time': true,
               'slider_time_val': slider_val
        };
        let smartui = $('#smartui_page').val();
        if (smartui){
            data['smartui'] = true;
        }
        ajax_quickui(data,function(result){
            $('.top_header .time, .top_header_smart .time').html(result.all_time);
            $('#all_popupTime .time_inner_popup').html(result.all_time);

        });
    });

    ////
    $( document).on( "change", ".quick_change #all_time, .raumregelung-smart  #all_time",function() {
        var slider_val = $('#all_time').val();
        var time = new Date().toLocaleString("en-US", {timeZone:'Europe/Berlin'});
        time = new Date(time);
        time.setMinutes(time.getMinutes() + parseInt(slider_val));
        var hours = time.getHours();
        var minutes = time.getMinutes();
        if (minutes >= 55 && minutes <=59) {
            minutes = '00';
            hours += 1;
        }
        else {
            minutes = Math.round(minutes / 10) * 10;
            minutes = minutes < 5 ? '0'+minutes : minutes;
            }
        if (hours === 24 || hours === 0){
            hours = '00';
        }
        hours = String(hours).padStart(2, '0');
        var room_time = hours + ':' + minutes;
        $('.top_header .time, .top_header_smart .time').html(room_time);
        $('#all_popupTime .time_inner_popup').html(room_time);
    });
    ////
    $( document).on( "slidestop", ".quick_change #time,.raumregelung-smart #time",function() {
        var slider_val = $('#time').val();
        var room_id = $('#popupTime #room_id').val();
        let data = {
           'room_id': room_id,
           'set_room_time': true,
           'slider_time_val': slider_val,
        };
        // check is sending via smartui or not to handle permission
        let smartui = $('#smartui_page').val();
        if (smartui){
            data['smartui'] = true;
        }
        if (room_id > 0){
            ajax_quickui(data,function(result){
                $('#popupTime .time_inner_room').html(result.room_time);
                $('li[data-id='+result.room_id+']').find('.in_rooms').html(result.room_time);
            });
        }
    });
    ////
    $( document).on( "change", ".quick_change #time, .raumregelung-smart #time",function() {
        var slider_val = $('#time').val();
        var room_id = $('#popupTime #room_id').val();
        var time = new Date().toLocaleString("en-US", {timeZone:'Europe/Berlin'});
        time = new Date(time);
        time.setMinutes(time.getMinutes() + parseInt(slider_val));
        var hours = time.getHours();
        var minutes = time.getMinutes();
        if (minutes >= 55 && minutes <=59) {
            minutes = '00';
            hours += 1;
        }
        else {
            minutes = Math.round(minutes / 10) * 10;
            minutes = minutes < 5 ? '0'+minutes : minutes;
            }
        if (hours === 24 || hours === 0){
            hours = '00';
        }
        hours = String(hours).padStart(2, '0');
        var room_time = hours + ':' + minutes;
        $('#popupTime .time_inner_room').html(room_time);
        $('li[data-id='+room_id+']').find('.in_rooms').html(room_time);
    });
    ////
    $( document).on( "click", ".quick_change #all_popupTimeDelete",function() {
        data = {
               'set_all_time': true,
               'slider_time_val': 0,
               'delete_all':true
        };
        ajax_quickui(data,function(result){
            $('.top_header .time, .top_header_smart .time').html(result.all_time);
            for(var i = 0; i < $(result.ziel_list).length; i++) {
                var li = $('li[data-id='+result.id_list[i]+']');
                li.find('.ziel_val').html(Math.abs(result.ziel_list[i]).toFixed(1));
                li.find('#zielslider').val(result.ziel_list[i]);
                li.find('#zielslider').slider('refresh');
                // image
                li.find('.hand_time .hand img').attr('src', '/static/icons/icon-hand-grey.png');
                li.find('.hand_time .hand .in_rooms').html('');
            }
        });
    });
    ////
    $(document).on( "click", ".raumregelung-smart #delete-all-handmode-popup #delete-btn",function() {
        hide_crossed_hand();
        $('.hand_time .hand img').attr('src', '/static/icons/icon-hand-grey.png');
        $('.hand_time .hand .in_rooms').html('');
        let data = {};
        $("#room_list li").each(function() {
            data = {
               'del_hand_mode': true,
               'haus_id': $('#haus_id').val(),
               'raum_id': $(this).data('id')
            };
            if($(this).data('id')) {
                ajax_smartui(data,function(result){
                    let rooms_obj = result.rooms;
                    for(let room_id in rooms_obj) {
                        let first_tab = $('#one li[data-id=' + room_id + ']');
                        let room_obj = rooms_obj[room_id];
                        first_tab.find('.ziel_val').html(room_obj.ziel);
                        first_tab.find('#zielslider').val(room_obj.ziel);
                        first_tab.find('#zielslider').slider('refresh');
                        first_tab.find('.hand_time .hand .in_rooms').html(room_obj.mode_text);
                        var second_tab = $('#two li[data-id=' + room_id + ']');
                        second_tab.find('.hand_time .hand img').attr('src', '/static/icons/' + room_obj.icon + '.png');
                        second_tab.find('.hand_time .hand .in_rooms').html(room_obj.mode_text);
                        second_tab.find('.soll-val').html(room_obj.soll);
                        second_tab.find('#sollslider').val(room_obj.soll);
                        second_tab.find('#sollslider').slider('refresh');
                        second_tab.find('.soll-val').addClass('light-green');
                        second_tab.find('.soll-val').removeClass('dark-green');
                        let style = second_tab.find('.ui-btn-active').attr('style');
                        style += '; background-color: #9ec552 !important;';
                        second_tab.find('.ui-btn-active').attr("style", style);
                    }
                });
            }
        })
    });

    $(document).on( "click", ".home #delete-all-handmode-popup #delete-btn",function() {
        let data = {};
        let mod_img_src;
        $("ul.room_list").each(function() {
            mod_img_src = $(this).find('.hand img').attr('src');
            if(!(mod_img_src && mod_img_src.endsWith('icon-hand.png'))){
                return
            }
            data = {
               'del_hand_mode': true,
               'haus_id': $('#haus_id').val(),
               'raum_id': $(this).data('id')
            };
            if($(this).data('id')) {
                ajax_smartui(data,function(result){
                    let rooms_obj = result.rooms;
                    for(let room_id in rooms_obj) {
                        let room_obj = rooms_obj[room_id];
                        let ul = $('ul[data-id=' + room_id + ']');
                        ul.find('.right_ui .ziel').html(parseFloat(room_obj.ziel).toFixed(2));
                        ul.find('.right_ui .ziel').attr('style', '');
                        ul.find('.right_ui .soll').attr('style', '');
                        ul.find('.right_ui #mode_name').html(room_obj.mode_text);
                        if(room_obj.icon) {
                            ul.find('.right_ui .hand img').attr('src', '/static/icons/' + room_obj.icon + '.png');
                        }else{
                            ul.find('.right_ui .hand').css('visibility', 'hidden')
                        }
                        ul.find('.right_ui .module-iconn img').css('visibility', 'hidden')
                        ul.find('.right_ui #sign_bis').css('visibility', 'hidden')
                        ul.find('.right_ui .soll').html(parseFloat(room_obj.soll).toFixed(2));
                        ul.find('.right_ui .slider').val(room_obj.soll);
                        ul.find('.right_ui .slider').slider('refresh');
                        ul.find('.ui-slider .ui-btn-active').attr('style', function(i,s) { return (s || '')
                                 + 'background-color: #9ec552 !important;' });
                        $('.delete-all-hand').addClass('hide');
                    }
                });
            }
        })
    });


    // room delete btn inside cross hand clicked
    $(document).on( "click", ".room #delete-all-handmode-popup #delete-btn",function() {
        $('.room #popupTime #popupTimeSave').trigger('click');
    });

    $(document).on( "click", ".room #delete-all-handmode-popup #delete-btn",function() {
        let ul = $('#roomlistview');
        let room_id = ul.data('id');
        let data = {
           'room_id':room_id,
           'set_room_time': true,
           'slider_time_val': 0,
        };
        ajax_quickui(data, function(result){
            let room_obj = result;
            let ul = $('ul[data-id=' + room_id + ']');
            ul.find('.right_ui .ziel').html(parseFloat(room_obj.ziel).toFixed(2));
            ul.find('.right_ui .ziel').attr('style', '');
            ul.find('.right_ui .soll').attr('style', '');
            ul.find('.right_ui #mode_name').html(room_obj.mode_text);
            if(room_obj.mode_icon) {
                ul.find('.right_ui .hand img').attr('src', '/static/icons/' + room_obj.mode_icon + '.png');
            }else{
                ul.find('.right_ui .hand').css('visibility', 'hidden')
            }
            ul.find('.right_ui .module-iconn img').css('visibility', 'hidden')
            ul.find('.right_ui #sign_bis').css('visibility', 'hidden')
            ul.find('.right_ui .soll').html(parseFloat(room_obj.soll).toFixed(2));
            ul.find('.right_ui .slider').val(room_obj.soll);
            ul.find('.right_ui .slider').slider('refresh');
        })

    });

    $( document).on( "click", ".quick_change #popupTimeSave",function() {
        var room_id = $('#popupTime #room_id').val();
        data = {
           'room_id':room_id,
           'set_room_time': true,
           'slider_time_val': 0,
        };
        if(room_id > 0){
            ajax_quickui(data,function(result){
                var li = $('li[data-id='+result.room_id+']');
                var ziel = $('#ziel'+ room_id).attr("data-ziel");
                if ($('li[data-id='+room_id+']').find('.temp').attr("temp-value") != '-') {
                    li.find('.ziel_val').html(Math.abs(result.ziel).toFixed(2));}
                else if($('li[data-id='+room_id+']').find('.temp').attr("temp-value") == '-'){
                    li.find('.ziel_val').html(0);}
                $('#zielslider').val(result.ziel);
                $('li[data-id='+result.room_id+']').find('.hand_time .hand').addClass("hide");
            });

        }
    });
    //
    $( document).on( "click", ".raumregelung-smart #popupTimeSave",function() {
        var room_id = $('#popupTime #room_id').val();
        let data = {
           'room_id':room_id,
           'set_room_time': true,
           'slider_time_val': 0,
           'smartui': 'true'
        };
        if(room_id > 0){
            ajax_quickui(data,function(result){
                var li = $('li[data-id='+result.room_id+']');
                if ($('li[data-id='+room_id+']').find('.temp').attr("temp-value") != '-') {
                    li.find('.ziel-val').html(Math.abs(result.ziel).toFixed(1));}
                else if($('li[data-id='+room_id+']').find('.temp').attr("temp-value") == '-'){
                    li.find('.ziel-val').html(0);}

                let ziel = result.ziel;
                // first tab
                let first_tab = $('#one li[data-id='+result.room_id+']');
                first_tab.find('.hand_time .hand img').attr('src', '/static/icons/icon-hand-grey.png');
                first_tab.find('.hand_time .hand .in_rooms').html(result.mode_name)
                $('#one li[data-id='+result.room_id+']').find('.ziel_val').html(ziel);
                var $slider = first_tab.find('#zielslider');
                $slider.val(ziel);
                $slider.slider('refresh');

                // second tab
                let second_tab = $('#two li[data-id='+result.room_id+']');
                second_tab.find('.soll-val').html(result.soll);
                if(result.tempmode){
                    second_tab.find('.hand img').attr('src',
                    '/static/icons/' + result.mode_icon + '.png');
                }else{
                    second_tab.find('.hand img').attr('src',
                    '/static/icons/icon_buttonMenu.png');
                }
                second_tab.find('.stat-text').html(result.mode_name);
                second_tab.find('.soll-val').addClass('light-green');
                second_tab.find('.soll-val').removeClass('dark-green');

                let style = second_tab.find('.ui-btn-active').attr('style');
                style += '; background-color: #9ec552 !important;';
                second_tab.find('.ui-btn-active').attr("style", style);

                check_hand_mode()
            });

        }
    });

    // change mode in quick time
    $(document).on( "change", ".quick_change #mode-select, .raumregelung-smart #tabs #one #mode-select",function(){
        show_crossed_hand()
        var mode_id = $(this).val();
        headers = {"X-CSRFToken": $("[name=csrfmiddlewaretoken]").val()};
        $("#room_list li").each(function() {
            var raum_id = $(this).data('id');
            if(!raum_id){
                return
            }
            data = {'mode_activate_temporary': true, 'mode_id': mode_id, 'all_time': $('#all_time').val()};
            var url = "/m_raum/"+raum_id+"/temperaturszenen/";
            ajax_temperaturszenen(url,headers, data, function(result){
                var li = $('#room_list li[data-id="'+ result.raum_id +'"]')

                  li.find('.hand_time .hand img').attr("src","/static/icons/icon-hand.png");
                  li.find('.hand_time .hand').removeClass('hide');
                  li.find('.hand_time .hand').css('visibility', 'visible');
                  li.find('.hand_time .hand .in_rooms').html(result.time);
                  li.find('.ziel .ziel_val').html(parseFloat(result.ziel).toFixed(1));
                  li.find('#zielslider').val(parseFloat(result.ziel).toFixed(1));
                  li.find('#zielslider').slider("refresh");

            }, {});
        });

    });
    // change mode in temperatureszenen
    $(document).on( "change", ".temperaturszenen #mode-select",function(){
        var action_id = parseInt($(this).val());
        //$(this).val('-1');
        // Select the relevant option, de-select any others
        $(this).val('-1').attr('selected', true).siblings('option').removeAttr('selected');
        // jQM refresh
        $(this).selectmenu("refresh", true);
        
        if (action_id>0){
            // set temperature for all selected items
            if(action_id == 1)
                $("#first-action").popup("open");
            // adjust temperatur for all selected items
            if(action_id == 2)
                $("#second-action").popup("open");
            // move checkboxes for all selected items
            if(action_id == 3)
                $("#third-action").popup("open");
        }

    });

    // set temperature for selected items
    $(document).on( "click", ".temperaturszenen #first-action .multiple-action-savebtn",function(){
        var headers = {"X-CSRFToken": $("[name=csrfmiddlewaretoken]").val()};
        var hausid = $(this).data('hausid');
        var mode_ids = [];
        var temperature = parseFloat($('#first-action #solltemp').val()).toFixed(2)
        $('.temperaturszenen .select-checkbox').each(function () {
            if($(this).is(':checked')){
                mode_ids.push({raum_id: $(this).data('raumid'), mode_id: $(this).data('modeid'), temp: temperature})
            }
        });
        var data = {
            'multiple_action': true,
            'action': 'set_temperature',
            'value': temperature,
            'mode_ids': JSON.stringify(mode_ids)
        };
        var url = "/m_temperaturszenen/"+hausid+"/";
        $.mobile.loading( "show", {
                text: 'loading',
                textVisible: true,
                theme: 'a',
                html: ''
        });
        ajax_temperaturszenen(url,headers, data, (data) => {
            for (item in data){
                var object = $('#solltemp-'+ data[item].raum_id + '-' + data[item].mode_id);
                object.val(data[item].value);
                object.trigger("change");
                object.slider("refresh");
            }
            $.mobile.loading( "hide" );
        }, {});
    });

    // funksollregelung
    $(".funksollregelung #rsfaktor_checkbox").click(function(){
        if(this.checked) {
            $('#slider_rsfaktor').slider( "option", "disabled", false );
        } else {
            $('#slider_rsfaktor').slider( "option", "disabled", true );
        }
    });

    $(".funksollregelung #schwelle_checkbox").click(function(){
        if(this.checked) {
            $('#slider_schwelle').slider( "option", "disabled", false );
        } else {
            $('#slider_schwelle').slider( "option", "disabled", true );
        }
    });

    $(".funksollregelung #select-type").change(function(){
        $(".funksollregelung #one .slider-wrapper").addClass('hide');
        $(".funksollregelung #one div[data-type='"+$(this).val()+"']").parent().removeClass('hide');
    });

    $(".funksollregelung #one .ui-slider-input").on('slidestop',function(){

            var id = $(this).data('id'),
            number =  $(this).val(),
            name =  $(this).attr('name');

        $.ajax({
            url: '/m_funksollregelung/{{ haus.id }}/',
            data: {
                'number' : number ,
                'id'     : id ,
                'name'   : name,
                'csrfmiddlewaretoken' : $("input[name=csrfmiddlewaretoken]").val(),
                'singleroom': true
            },
            type: 'post',
            async: true,
            beforeSend: function() {
                if(typeof startAjaxSlider === "function"){startAjaxSlider();}
            },
            complete: function() {
                if(typeof endAjaxSlider === "function"){endAjaxSlider();}
            }
        });
    });

    // +- offset selected items
    $(document).on( "click", ".temperaturszenen #second-action .multiple-action-savebtn",function(){
        var headers = {"X-CSRFToken": $("[name=csrfmiddlewaretoken]").val()};
        var hausid = $(this).data('hausid');
        var mode_ids = [];
        var offset = parseFloat($('#second-action #offset').val()).toFixed(2)
        $('.temperaturszenen .select-checkbox').each(function () {
            if($(this).is(':checked')){
                mode_ids.push({raum_id: $(this).data('raumid'), mode_id: $(this).data('modeid'), offset: offset})
            }
        });
        var data = {
            'multiple_action': true,
            'action': 'set_offset',
            'value': offset,
            'mode_ids': JSON.stringify(mode_ids)
        };
        $.mobile.loading( "show", {
                text: 'loading',
                textVisible: true,
                theme: 'a',
                html: ''
        });
        var url = "/m_temperaturszenen/"+hausid+"/";
        ajax_temperaturszenen(url,headers, data, (data) => {
            for (item in data){
                var object = $('#solltemp-'+ data[item].raum_id + '-' + data[item].mode_id);
                object.val(parseFloat(object.val()) + parseFloat(data[item].value));
                object.slider("refresh");
            }
            $.mobile.loading( "hide" );
        }, {});
    });
    //

    // enable disable selected items
    $(document).on( "click", ".temperaturszenen #third-action .multiple-action-savebtn",function(){
        var value= parseInt($(this).data('value'));
        var headers = {"X-CSRFToken": $("[name=csrfmiddlewaretoken]").val()};
        var hausid = $(this).data('hausid');
        var mode_ids = [];
        $('.temperaturszenen .select-checkbox').each(function () {
            if($(this).is(':checked')){
                mode_ids.push({raum_id: $(this).data('raumid'), mode_id: $(this).data('modeid'), value: value})
            }
        });
        var data = {
            'multiple_action': true,
            'action': 'set_enable',
            'value': value,
            'mode_ids': JSON.stringify(mode_ids)
        };
        $.mobile.loading( "show", {
                text: 'loading',
                textVisible: true,
                theme: 'a',
                html: ''
        });
        var url = "/m_temperaturszenen/"+hausid+"/";
        ajax_temperaturszenen(url,headers, data, (data) => {
            for (item in data){
                var object = $('#mode-active-'+ data[item].raum_id + '-' + data[item].mode_id);
                var checked = false;
                if(parseInt(data[item].value) == 1)
                    checked = true
                object.prop('checked', checked);
                object.flipswitch();
                object.flipswitch("refresh");
            }
            $.mobile.loading( "hide" );
        }, {});
    });
    $( document).on("click", ".right_ui .hand",function(e) {
        e.preventDefault();
        var div = $(this).closest('.main');
        room_name = $(div).find(".left_ui .room_name").text();
        room_id = $(div).find(".left_ui #room_row_id").val();
        $('.popup_room_icon img').attr('src',$(div).find(".left_ui .room_icon img").attr('src'));
        $('.popup_raum_name').html(room_name);
        data = {
           'get_room_slider': true,
           'room_id':room_id,
        };

        if(room_id > 0){
            ajax_quickui(data,function(result){
                $('#popupTime .showtime').html(result.room_time);
                $('#popupTime #time').val(result.slider_val);
                $('#popupTime #popup_room_id').val(room_id);
                $('#popupTime #time').slider("refresh");
                $("#popupTime").popup("open");
            });
        }

    });

    $( document).on("click", ".room #duration-time-row",function(e) {
        $("#duration_popupTime").popup("open");
    });

    ////
    $( document).on("click", ".quick_change .first_row .ziel, .raumregelung-smart #one .ziel",function(e) {
        $parent = $(this).parentsUntil('li');
        var room_name = $parent.find('.raum_name').text();
        var src = $parent.find('.room_icon img').attr('src');
        var room_id = $parent.find('#first_row_roomid').val();
        $('#popupTempContent .popupTempIcon img').attr('src',src);
        $('#popupTempContent .popupTempName').html(room_name);
        $("#popupTempContent #popupZielInput").val($parent.find('.ziel_val').text())
        $("#popupTempContent  #ziel_popup_roomid").val(room_id);
        $("#popupTemp").popup("open");
    });
    ///
    $( document).on("click", ".raumregelung-smart #two .soll",function(e) {
        $parent = $(this).parentsUntil('li');
        var room_name = $parent.find('.raum_name').text();
        var src = $parent.find('.room_icon img').attr('src');
        var room_id = $parent.find('#first_row_roomid').val();
        $('#popupTempContent .popupTempIcon img').attr('src',src);
        $('#popupTempContent .popupTempName').html(room_name);
        if($parent.find('.hand img').attr('src') == '/static/icons/icon-hand.png'){
            $("#popupTempContent #popupZielInput").val($parent.find('.soll-val').text())
            $("#popupTempContent  #ziel_popup_roomid").val(room_id);
            $("#popupTemp").popup("open");
        }else {
            $("#popupTempContent #popupSollInput").val($parent.find('.soll-val').text())
            $("#popupTempContent  #soll_popup_roomid").val(room_id);
            $("#popupTempContent  .info-text span").html($parent.find('.hand .in_rooms').text());
            $(".popupSollTemp").popup("open");
        }
    });
    ///
    $( document).on("click", ".raumregelung-smart .ist",function(e) {
        $parent = $(this).parentsUntil('li');
        var room_name = $parent.find('.raum_name').text();
        var src = $parent.find('.room_icon img').attr('src');
        var room_id = $parent.find('#first_row_roomid').val();
        $('#popupTempContent .popupTempIcon img').attr('src',src);
        $('#popupTempContent .popupTempName').html(room_name);
        $('#popupTempContent .popupRoomName').html(room_name);
        $("#popupTempContent .isttemp-val").html($parent.find('.temp-val').text())
        $("#popupTempContent  #soll_popup_roomid").val(room_id);
        $("#popupTempIst").popup("open");
    });
    $( document).on("click", ".home .details_right .temp,.room .details_right .temp",function(e) {
        $parent = $(this).parentsUntil('ul');
        let room_name = $parent.find('.room_name').text();
        let src = $parent.find('.room_icon img').attr('src');
        $('#popupTempContent .popupTempIcon img').attr('src',src);
        $('#popupTempContent .popupTempName').html(room_name);
        $('#popupTempContent .popupRoomName').html(room_name);
        $("#popupTempContent .isttemp-val").html($parent.find('.temp').text())
        $("#popupTempIst").popup("open");
    });
    ///
    $( document).on("click", ".raumregelung-smart #popupTempContent #popupZielSave",function(e) {
        show_crossed_hand()
        var id = $("#popupTempContent #ziel_popup_roomid").val();
        var value = $("#popupTempContent #popupZielInput").val();
        var $slider = $('li[data-id="'+id+'"]').find('#zielslider')
        $slider.val(value);
        $slider.slider("refresh");
        var slider_num = value;
        var li = $('#one input[data-id="'+  id + '"]').closest('li');
        var second_tab_li = $('#two input[data-id="'+  id + '"]').closest('li');
        li.find('.hand_time .align-center').removeClass('hide');
        li.find('.ziel_val').html(parseFloat(slider_num).toFixed(1));
        var all_time = $('#popupTime #time').val();
        data = {
           'set_ziel_slider': true,
           'room_id':id,
           'slidernumber' : slider_num,
           'all_time': all_time
        };
        if(id > 0){
            ajax_quickui(data,function(result){
                li.find('.hand img').attr('src','/static/icons/icon-hand.png');
                li.find('.hand_time .hand .in_rooms').html(result.time);
                if(second_tab_li.find('.soll-val').length) {
                    second_tab_li.find('.soll-val').addClass('dark-green');
                    second_tab_li.find('.soll-val').removeClass('light-green');
                    second_tab_li.find('.soll-val').html(slider_num);

                    let style = second_tab_li.find('.ui-btn-active').attr('style');
                    style += '; background-color: #779A45 !important;';
                    second_tab_li.find('.ui-btn-active').attr("style", style);
                    second_tab_li.find('.hand_time .hand .in_rooms').html(result.time);
                    second_tab_li.find('.hand img').attr('src','/static/icons/icon-hand.png');
                    }
            });
        }
    });
    ///

    $( document).on("click", ".raumregelung-smart #popupTempContent #popupSollSave",function(e) {
        var id = $("#popupTempContent #soll_popup_roomid").val();
        var value = $("#popupTempContent #popupSollInput").val();
        var $slider = $('li[data-id="'+id+'"]').find('#sollslider');
        $slider.val(value);
        $slider.slider("refresh");
        $('li[data-id="'+id+'"]').find('.soll-val').html(parseFloat(value).toFixed(1));
        let data = {
            'set_temperature': true,
            'raum_id': id,
            'temperature': value,
            'csrfmiddlewaretoken': $("input[name='csrfmiddlewaretoken']").val(),
        }
        $.ajax({
            url: '/raumregelung-smart/',
            data: data,
            type: 'post',
            success: function(result){
                let ziel = parseFloat(result.ziel).toFixed(1);
                $('#one li[data-id=' + id +']').find('.ziel_val').html(ziel);
                var $slider = $('#one li[data-id="'+id+'"]').find('#zielslider');
                $slider.val(ziel);
                $slider.slider("refresh");
            }
        })
    });
    ///
    $( document).on( "slidestop", ".home #popupTime #time, .room #popupTime #time",function() {
        var slider_val = $('#popupTime #time').val();
        var room_id = $('#popupTime #popup_room_id').val();
        data = {
           'room_id': room_id,
           'set_room_time': true,
           'slider_time_val': slider_val,
        };
        if(room_id > 0){
            ajax_quickui(data,function(result){
                $('#popupTime .showtime').html(result.room_time);
                $('div[data-id='+result.room_id+']').find('.right_ui .hand_time').data('id', -1);
                $('div[data-id='+result.room_id+']').find('.hand_time').html(result.room_time);
            });
        }
    });
    ////
    $( document).on( "slidestop", ".room #duration_time",function() {
        let slider_val = $('#duration_time').val();
        let room_id = $('#room-id').val();
        let data = {
           'room_id': room_id,
           'set_room_time': true,
           'slider_time_val': slider_val,
        };
        if(room_id > 0){
            ajax_quickui(data,function(result){
                $('#popupTime .showtime').html(result.room_time);
                $('div[data-id='+result.room_id+']').find('.right_ui .hand_time').data('id', -1);
                $('div[data-id='+result.room_id+']').find('.hand_time').html(result.room_time);
                $('#duration-val').html(result.room_time);
                $('#duration_popupTime .time_inner_popup').html(result.room_time);
            });
        }
    });

    $( document).on( "change", ".home #popupTime #time, .room #popupTime #time",function() {
        var slider_val = $('#popupTime #time').val();
        var room_id = $('#popupTime #popup_room_id').val();
        var time = new Date().toLocaleString("en-US", {timeZone:'Europe/Berlin'});
        time = new Date(time);
        time.setMinutes(time.getMinutes() + parseInt(slider_val));
        var hours = time.getHours();
        var minutes = time.getMinutes();
        if (minutes >= 55 && minutes <=59) {
            minutes = '00';
            hours += 1;
        }
        else {
            minutes = Math.round(minutes / 10) * 10;
            minutes = minutes < 5 ? '0'+minutes : minutes;
            }
        if (hours === 24 || hours === 0){
            hours = '00';
        }
        var room_time = hours + ':' + minutes;
        $('#popupTime .showtime').html(room_time);
        $('div[data-id='+room_id+']').find('.right_ui .hand_time').data('id', -1);
        $('div[data-id='+room_id+']').find('.hand_time').html(room_time);
    });
    ////
    $( document).on("click", ".home #popupTime #popupTimeSave,.room #popupTime #popupTimeSave",function(e) {
        let room_id = $('#room-id').val();
        if(room_id){
            $('.delete-all-hand').addClass('hide');
        }
        else{
            room_id = $('#popupTime #popup_room_id').val();
        }
        let data = {
           'room_id':room_id,
           'set_room_time': true,
           'slider_time_val': 0,
        };
        if(room_id > 0){
            ajax_quickui(data,function(result){
                String.prototype.format = function() {
                var str = this;
                for (var i = 0; i < arguments.length; i++) {
                    var reg = new RegExp("\\{" + i + "\\}", "gm");
                    str = str.replace(reg, arguments[i]);
                     }
                return str;
                };
                let li = $('ul[data-id='+room_id+']');
                li.find('#'+ room_id).html(Math.abs(result.soll).toFixed(2));
                li.find('.soll').css('color', '#9EC553');
                $('div[data-id='+room_id+'] .visualbar_slider').find('.ziel').css('background-color', '#779A45');
                li.find('#offset').html(Math.abs(result.offset).toFixed(2));
                if (result.offset > 0 || result.offset === 0) {
                    li.find('#pom_sign').text('+').show();
                    li.find('#offset').css('color', '#cb0963');
                }
                else{
                   li.find('#pom_sign').text('-').show();
                   li.find('#offset').css('color', '#019BB0');
                }
                $('div[data-id='+room_id+'] .details_left').find('.ziel').html(parseFloat(result.recover_ziel).toFixed(2));
                li.find('.ziel').css('color', '#779A45');
                li.find('.module-iconn').hide();
                ul.find('.right_ui #sign_bis').css('visibility', 'hidden')
                li.find('#mode_name').html(result.mode_name);
                li.find('#mode_name').css('margin-left', '15px');

                if (result.mode_icon) {
                    li.find('#mode_icon').html('<img src="/static/icons/{0}.png" width="30" height="30">'.format([result.mode_icon]));
                }else{
                    $li.find('#mode_icon').hide();
                }
                if (result.module_icon) {
                    li.find('.module-iconn').html('<img src="/static/icons/{0}.png"/>'.format([result.module_icon])).show();

                }
                let style = li.find('.ui-slider .ui-btn-active').attr('style');
                console.log(style)
                style += '; background-color: #9ec552 !important';
                li.find('.ui-slider .ui-btn-active').attr("style", style);
                $('#sollslider').val(result.soll);
                $('#sollslider').slider('refresh');
                $('#zielslider').val(result.recover_ziel);
                $('#zielslider').slider('refresh');
            });
        }
    });
    ////
    $( document).on("change", ".quick_change #zielslider, .raumregelung-smart #tabs #one #zielslider",function() {
        let id = $(this).attr("data-id");
        slider_num = $(this).val();
        all_time = $('#all_popupTime #all_time').val();
        var li = $(this).closest('li');
        li.find('.ziel_val').html(parseFloat(slider_num).toFixed(1));
        li.find('.hand_time .hand').removeClass("hide");
    });


    $( document).on("slidestop", ".quick_change #zielslider, .raumregelung-smart #tabs #one  #zielslider",function() {
        if($('body').hasClass('raumregelung-smart')) {
            show_crossed_hand()
        }
        let id = $(this).attr("data-id");
        let slider_num = $(this).val();
        let li = $('li[data-id=' + id +']');
        li.find('.hand_time .align-center').css('visibility','visible');
        li.find('.hand img').attr('src','/static/icons/icon-hand.png');
        li.find('.ziel_val').html(parseFloat(slider_num).toFixed(1));

        let second_tab_li = $('#two li[data-id=' + id +']');

        if(second_tab_li.length) {
            // indication in permanent tab gets dark green
            second_tab_li.find('.soll-val').addClass('dark-green');
            second_tab_li.find('.soll-val').removeClass('light-green');
            second_tab_li.find('.soll-val').html(parseFloat(slider_num).toFixed(1));
            // slider in permanent tab gets dark green
            let style = second_tab_li.find('.ui-btn-active').attr('style');
            style += '; background-color: #779A45 !important;';
            second_tab_li.find('.ui-btn-active').attr("style", style);
        }

        let all_time = $('#all_popupTime #all_time').val();
        let data = {
           'set_ziel_slider': true,
           'room_id':id,
           'slidernumber' : slider_num,
           'all_time': all_time
        };
        // check is sending via smartui or not to handle permission
        let smartui = $('#smartui_page').val();
        if (smartui){
            data['smartui'] = true;
        }
        if(id > 0){
            ajax_quickui(data,function(result){
                li.find('.hand_time .hand .in_rooms').html(result.time);
                li.find('.hand_time .hand').removeClass('hide');
            });
        }
    });

    $( document).on("change", ".raumregelung-smart #tabs #two #sollslider",function() {
        id = $(this).attr("data-id");
        slider_num = $(this).val();
        var li = $(this).closest('li');
        li.find('.soll-val').html(parseFloat(slider_num).toFixed(1));

    });
    $( document).on("slidestop", ".raumregelung-smart #tabs #two #sollslider",function() {
        let id = $(this).attr("data-id");
        let slider_num = $(this).val();
        let data = {
            'set_temperature': true,
            'raum_id': id,
            'temperature': slider_num,
            'csrfmiddlewaretoken': $("input[name='csrfmiddlewaretoken']").val(),
        }
        $.ajax({
            url: '/raumregelung-smart/',
            data: data,
            type: 'post',
            success: function(result){
                let ziel = result.ziel
                $('#one li[data-id=' + id +']').find('.ziel_val').html(ziel);
                var $slider = $('#one li[data-id="'+id+'"]').find('#zielslider');
                $slider.val(ziel);
                $slider.slider("refresh");
            }
        })
    });

    ////
    $( document).on("click", ".raumregelung-smart .delete-all-hand, .home .delete-all-hand, .room .delete-all-hand",function() {
        $("#delete-all-handmode-popup" ).popup("open");
    });
    ////
    $(".open-sub-collapsible").on('click',function(){
        if($(this).hasClass('ui-icon-arrow-d')){
            $(this).removeClass('ui-icon-arrow-d').addClass('ui-icon-arrow-u');
        }else{
            $(this).removeClass('ui-icon-arrow-u').addClass('ui-icon-arrow-d');
        }
        $(this).parent().parent().parent().parent().find('.sub-collapsible').toggle();
        $(".ui-collapsible-heading a").removeClass('ui-btn-active ui-focus ');
        return false;
    })

    //time slider change
    $(".timer #offset-slider").on('change',function(){
        var num = $('.timer #offset-slider').slider().val();
        $('.timer .temp_set span').text(num);
    });
    // wochenkalender
    /*$(".wochenkalender input[type='checkbox']").click(function(){
        if($("input[name=begin-day]:checked").val() != $("input[name=end-day]:checked").val()){
            $(".wochenkalender input[type='checkbox']").removeAttr("checked").checkboxradio("refresh");
            alert('this entry collidates with itselt. Please change the repeat settings.');
        }
    })*/
    // sort it
    var ul = $(".wochenkalender ul.sort");
    var arr = $.makeArray(ul.children("li"));

    arr.sort(function(a, b) {
        //console.log($(a).data('hour'))
        var hourA = $(a).data('hour').split(':'),
        textA =  new Date(2015, 1, $(a).data('day'), hourA[0], hourA[1]).getTime(),
        hourB = $(b).data('hour').split(':'),
        textB = new Date(2015, 1, $(b).data('day'), hourB[0], hourB[1]).getTime();

        if (textA < textB) return -1;
        if (textA > textB) return 1;

        return 0;
    });

    ul.empty();

    $.each(arr, function() {
        ul.append(this);
    });

    $(".wochenkalender .offset,.room .offset,.jahreskalender .offset").each(function(){
        var txt_get = $(this).text().trim();
        if(txt_get.endsWith("berschwingschutz")) {
            $(this).addClass('blue-str');
            $('.room div.collaps-HFO').show();
            $('.room div.collaps-HFO a').addClass('ui-icon-heizflaechenoptimierung-blue');
            $(this).closest('li').hide();
        } else if(txt_get === "Unterschwingschutz") {
            $(this).addClass('red-str');
            $('.room div.collaps-HFO').show();
            $('.room div.collaps-HFO a').addClass('ui-icon-heizflaechenoptimierung-red');
            $(this).closest('li').hide();
        } else if(txt_get === "Normalbetrieb") {
            $('.room div.collaps-HFO').show();
            $('.room div.collaps-HFO a').addClass('ui-icon-heizflaechenoptimierung-grey');
            $(this).closest('li').hide();
        }else {
            var num = parseFloat(txt_get);
            if (num > 0)
                $(this).addClass('red-str');
            else if (num < 0)
                $(this).addClass('blue-str');
        }
    });
    // itereating over module list in room page
    $('#cmods ul  li' ).each(function( index ) {
        var module_name = $(this).find('.module_name').text().trim();
        if (module_name == 'Zieltemperaturanpassung Funkregelung'){
            $(this).closest('li').hide();
            $('.ziel_funk').show();
        }
    });

    // back button in header fix
    // if it is empty remove it
    if(!$.trim($('.second-header').html())){
        $('.second-header').remove();
    }else{
        // add css class to add top
        $('.ui-content,#nav-panel').addClass('have-second-header');
    }

    /*
    * jahreskalender
    *
    */

    (function ( root, doc, factory ) {

         $(".benutzerverwaltung  .select-house").change(function(){
            var elem  = $(".benutzerverwaltung input[data-house="+$(this).val()+"]");
            if($(this).is(':checked')){
                elem.prop('checked', true).checkboxradio("refresh");
            }else{
                elem.prop('checked', false).checkboxradio("refresh");
            }
        });

        // select all for a house
        $(".jahreskalender  .select-house").change(function(){
            var elem  = $(".jahreskalender input[data-house="+$(this).val()+"]");
            if($(this).is(':checked')){
                elem.prop('checked', true).checkboxradio("refresh");
            }else{
                elem.prop('checked', false).checkboxradio("refresh");
            }
        });

        // go to calender record when click on header
        $(".jahreskalender  .record-list .ui-collapsible-heading-toggle").bind("click", function (e) {
            e.stopPropagation();
            var $link = $(this).parent().parent().find('.link-record');
            if ($link.length > 0)
                //console.log($(this).hasClass())
                window.location = $link.attr('href');
            return false;
        });
        //select all room
        $(".benutzerverwaltung  .select-all, .jahreskalender  .select-all").change(function(){
            var elem  = $(" .select-room input[class=room]");
            if($(this).is(':checked')){
                elem.prop('checked', true).checkboxradio("refresh");
            }else{
                elem.prop('checked', false).checkboxradio("refresh");
            }
        });
        //
        //select all diff
        $(".benutzerverwaltung  .select-all-diff,.jahreskalender .select-all-diff").change(function(){
            var elem  = $(".select-room input[class=diff]");
            if($(this).is(':checked')){
                elem.prop('checked', true).checkboxradio("refresh");
            }else{
                elem.prop('checked', false).checkboxradio("refresh");
            }
        });
        // select all vorlauf
        $(".benutzerverwaltung  .select-all-vor,.jahreskalender .select-all-vor").change(function(){
            var elem  = $(".select-room input[class=vor]");
            if($(this).is(':checked')){
                elem.prop('checked', true).checkboxradio("refresh");
            }else{
                elem.prop('checked', false).checkboxradio("refresh");
            }
        });

        $("#select-all-pl").change(function(){
            var elem  = $(".select-room input[class=pl]");
            if($(this).is(':checked')){
                elem.prop('checked', true).checkboxradio("refresh");
            }else{
                elem.prop('checked', false).checkboxradio("refresh");
            }
        });

        $(".jahreskalender .collapsible-open").on('click',function(){
            if($(this).hasClass('ui-icon-arrow-d')){
                $(this).removeClass('ui-icon-arrow-d').addClass('ui-icon-arrow-u');
                $('.record-collapsible  ').collapsible( "option", "collapsed", false );
            }else{
                $(this).removeClass('ui-icon-arrow-u').addClass('ui-icon-arrow-d');
                $('.record-collapsible  ').collapsible( "option", "collapsed", true );
            }
            $('.jahreskalender .ui-content .ui-collapsible-heading a').removeClass('ui-btn-active ui-focus ');
            return false;
        });

        var filter = function(){
            var type = $(this).val();
            // show all first
            $(".jahreskalender .ui-content .ui-collapsible").show();
            switch(type){
                case 'relevant'      :
                    $(".jahreskalender .ui-content .ui-collapsible[data-old=true]").hide();
                    return;
                break;
                case 'currently'      :
                    $(".jahreskalender .ui-content .ui-collapsible:not(.active)").hide();
                    return;
                break;
                case 'all'      :
                    return;
                break;
                case 'past'      :
                    $(".jahreskalender .ui-content .ui-collapsible:not([data-old=true])").hide();
                    return;
                break;
            }

        },
        sort = function(){
            var type = $(this).val();

            var warrper = $(".jahreskalender .record-list");
            var array = $.makeArray(warrper.children(".record-collapsible"));

            var sort_fn;
            switch(type){
                case 'start' :
                    // start sort method
                    sort_fn = function(a, b)
                    {
                        var beginA = parseFloat( $(a).data('begin')  ),
                            endA   = parseFloat( $(a).data('end')  ),
                            activeA = $(a).hasClass('active') ,
                            beginB = parseFloat( $(b).data('begin')  ),
                            endB   = parseFloat( $(b).data('end')  ),
                            activeB = $(b).hasClass('active');

                        if(activeA && !activeB)
                            return -1;
                        else if(!activeA && activeB)
                            return 1;
                        else if ( !activeA && !activeB){
                            if(beginA < beginB)
                                return -1;
                            else if(beginA > beginB)
                                return 1;
                            return 0;
                        }else{
                            console.log(endA , endB)
                            if(endA < endB)
                                return -1;
                            else if(endA > endB)
                                return 1;
                            return 0;
                        }
                    }
                break;
                case 'end' :
                    // created sort method
                    sort_fn = function(a, b)
                    {

                        var A = parseFloat( $(a).data('end')  ),
                            B = parseFloat( $(b).data('end')  );
                        if(A < B)
                            return -1;
                        else if(A > B)
                            return 1;
                        return 0;
                    }
                break;
                case 'name' :
                    sort_fn = function(a, b)
                    {
                        var nA = ( $(a).data('name') + "" ).toLowerCase();
                        var nB = ( $(b).data('name') + "" ).toLowerCase();

                        if(nA < nB)
                            return -1;
                        else if(nA > nB)
                            return 1;
                        return 0;
                    }
                break;
                case 'created' :
                    // created sort method
                    sort_fn = function(a, b)
                    {

                        var A = parseFloat( $(a).data('created')  ),
                            B = parseFloat( $(b).data('created')  );
                        console.log(A,B)
                        if(A < B)
                            return 1;
                        else if(A > B)
                            return -1;
                        return 0;
                    }
                break;
                case 'edited' :
                    // edited sort method
                    sort_fn = function(a, b)
                    {
                        var A = parseFloat( $(a).data('edited')  ),
                            B = parseFloat( $(b).data('edited')  );

                        console.log(A,B)
                        if(A < B)
                            return 1;
                        else if(A > B)
                            return -1;
                        return 0;
                    }
                break;
            }

            array = array.sort(sort_fn);
            warrper.empty();

            $.each(array, function() {
                warrper.append(this).trigger('create');
            });
        }
        // call it for first time to do a relevant filter


        $(".jahreskalender #select-order").on('change',filter);

        $(".jahreskalender #select-sort").on('change',sort);

        $(".jahreskalender #select-order").trigger( "change" );


    }());

    /**
     * Url: /m_raum/1/heizflaechenoptimierung/ -> .heizflaechenoptimierung #one #form-heizflaechenoptimierung .ui-slider-input
     * Url: /m_raum/1/aussentemperaturkorrektur/ -> .heizflaechenoptimierung #one #form-aussentemperaturkorrektur .ui-slider-input
     * Url: /m_raum/1/kontaktabsenkung/ -> .wetter .kontaktabsenkung .ui-slider-input
     * Url: /m_raum/1/luftfeuchte/ -> .luftfeuchte #lfform .ui-slider-input
     * Url: /m_raum/1/wetter/ -> .wetter #wrform input
     */

    $( document).on( "change", ".wetter #wrform input:checkbox[name='wetter']",function(){
        url = $(this).parents("form").first().attr('action');
        if ($(this).is(':checked')) {
            $.ajax({
            url: url,
            data: {
                'csrfmiddlewaretoken' : $("input[name=csrfmiddlewaretoken]").val(),
                'enable_wetter': true
            },
            type: 'post',
            async: true,
            beforeSend: function() {
                startAjaxSlider();
            },
            complete: function() {
                endAjaxSlider();
            }
        });
        }
        else {
            $.ajax({
            url: url,
            data: {
                'csrfmiddlewaretoken' : $("input[name=csrfmiddlewaretoken]").val(),
                'disable_wetter': true
            },
            type: 'post',
            async: true,
            beforeSend: function() {
                startAjaxSlider();
            },
            complete: function() {
                endAjaxSlider();
            }
        });

        }
    });



    $(".heizflaechenoptimierung #one .ui-slider-input, \
        .wetter .kontaktabsenkung .ui-slider-input, \
        .wetter #fsrform .ui-slider-input, \
        .luftfeuchte #lfform .ui-slider-input, \
        .wetter #wrform input").on('slidestop',function(){
        var id = $(this).data('id'),
        number =  $(this).val(),
        name =  $(this).attr('name'),
        url = $(this).parents("form").first().attr('action');
        if(name==='wetter' && $(this).attr('type')==='checkbox'){
            if($(this).is(":checked")){
                number = '1';
            }
            else{
                number = '0';
            }
        }
        $.ajax({
            url: url,
            data: {
                'number' : number ,
                'id'     : id ,
                'name'   : name,
                'csrfmiddlewaretoken' : $("input[name=csrfmiddlewaretoken]").val(),
                'singleroom': true
            },
            type: 'post',
            async: true,
            beforeSend: function() {
                startAjaxSlider();
            },
            complete: function() {
                endAjaxSlider();
            }
        });
    });

    $(".heizflaechenoptimierung #one .ui-slider-input, \
        .wetter .kontaktabsenkung .ui-slider-input, \
        .luftfeuchte #lfform .ui-slider-input, \
        .wetter #wrform input").on('change',function(e){
            if (e.originalEvent === undefined) {
            } else {
                $(this).trigger("slidestop");
            }
    });

    /*********************************************/
    /** Start vorlauftemperatur page javascript **/
    $(".vorlauftemperatur #tabs").on("tabsactivate", function (event, ui) {
        var activeTab = $(this).tabs("option", "active");
        var offset = 0;
        if (activeTab == 2) { // the third tab is selected
            $('html, body').animate({
                scrollTop: $("#listOfLungs").offset().top + offset
            }, 1000);
        }
    });
    $(window).on('load', function () {
        /*if (!location.hash) {
            $('#tabs').tabs('option', 'active', 0); // activate first tab by default
            return;
        }*/
        var selected = false;
        $('.vorlauftemperatur #tabs > div > ul > li > a').each(function (index, a) {
            if ($(a).attr('href') == location.hash) {
                $('.vorlauftemperatur #tabs').tabs('option', 'active', index);
                $(a).trigger('click');
                selected = true;
                var offset = 0;
                if (index == 2) { // the third tab is selected
                    $('html, body').animate({
                        scrollTop: $("#listOfLungs").offset().top + offset
                    }, 1000);
                }
            }
        });
        if (!selected) {
            $('.vorlauftemperatur #tabs > div > ul > li > a').first().addClass("ui-btn-active");
        }
    });
    $(".vorlauftemperatur #absenkung_checkbox").click(function () {
        if (this.checked) {
            // set the slider as disable
            $('.vorlauftemperatur #absenkungs').slider("option", "disabled", false);
        } else {
            // set the slider as enable again when you click to uncheck
            $('.vorlauftemperatur #absenkungs').slider("option", "disabled", true);
        }
    });
    $(".vorlauftemperatur #anhebung_checkbox").click(function () {
        if (this.checked) {
            // set the slider as disable
            $('.vorlauftemperatur #anhebungs').slider("option", "disabled", false);
        } else {
            // set the slider as enable again when you click to uncheck
            $('.vorlauftemperatur #anhebungs').slider("option", "disabled", true);
        }
    });
    $(".vorlauftemperatur #select-type").change(function () {
        $(".vorlauftemperatur #one .slider-wrapper").hide();
        $(".vorlauftemperatur #one div[data-type='" + $(this).val() + "']").show();
    });
    $(".vorlauftemperatur #one .ui-slider-input").on('slidestop', function () {
        var id = $(this).data('id'),
            hausid = $(this).data('hausid'),
            number = $(this).val(),
            name = $(this).attr('name');
        $.ajax({
            url: '/m_vorlauftemperaturkorrektur/' + hausid + '/',
            data: {
                'number': number,
                'id': id,
                'name': name,
                'csrfmiddlewaretoken': $("input[name=csrfmiddlewaretoken]").val(),
                'singleroom': true
            },
            type: 'post',
            async: true,
            beforeSend: function () {
                if (typeof startAjaxSlider === "function") { startAjaxSlider(); }
            },
            complete: function () {
                if (typeof endAjaxSlider === "function") { endAjaxSlider(); }
            }
        });
    });

    $(".vorlauftemperatur #one input").on('change', function (e) {
        if (e.originalEvent === undefined) {
            } else {
                $(this).trigger("slidestop");
            }
    });
    /** End vorlauftemperatur page javascript **/
    /*********************************************/

});
document.addEventListener('gesturestart', function (e) {
    e.preventDefault();
});
document.addEventListener('touchmove', function(event) {
    event = event.originalEvent || event;
    numTouches = event.touches.length;
    if (event.scale !== 1 || numTouches > 1) { event.preventDefault(); }
}, false);
document.addEventListener('touchend', function(event) {
    screen.width = window.innerWidth;
}, false);
window.addEventListener('scroll', function(event) { 
    if(event.touches !== undefined && event.touches.length==2)
    {
        event.preventDefault();
    }
    screen.width = window.innerWidth;
 });
$(document).on( "mousedown touchstart", "a.ui-slider-handle",function(){
    /* Remove slider's link */
    $(this).removeAttr("href");
});
$(function() {
    $(document).on( "mousedown touchstart",function(){
      $("a.ui-slider-handle").each(function( i ) {
        /* Remove slider's link */
        $(this).removeAttr("href");
      });
    });
});

$(document).on('ajaxComplete', function () {
    setContentHeight();
    function setContentHeight () {
        try {
            var windowWidth = document.documentElement.clientWidth;
            document.getElementById('cntnt').style.minHeight  = "auto";
            if (windowWidth > 959) {
                var contentHeight = document.getElementById('cntnt').clientHeight;
                var leftSidebarHeight = document.querySelector('#nav-panel .ui-panel-inner').clientHeight;
                var rightSidebarHeight = document.querySelector('#nav-room-list .ui-panel-inner').clientHeight;
                contentHeight = contentHeight < leftSidebarHeight ? leftSidebarHeight : contentHeight;
                contentHeight = contentHeight < rightSidebarHeight ? rightSidebarHeight : contentHeight;
                document.getElementById('cntnt').style.minHeight = (contentHeight + 25).toString() + 'px';
            }
        } catch (e) {
            // We don't have sidebars on some pages
            console.log(e)
        }
    }
    window.addEventListener("resize", setContentHeight);
})