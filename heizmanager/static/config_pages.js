
let call_ajax = function(url, method, data,success_cb, err_cb){
    $.ajax({
        url: url,
        data: data,
        type: method,
        success: success_cb,
        error: err_cb
    });
}

let change_right_menu_indication = function(){
    let token = $('input[name=csrfmiddlewaretoken]').val();
    let url = window.location.path;

    let right_menu = 0;
    if ($('#right-menu-ch').attr("checked")){
        right_menu = 1;
    }

    let data = {
        'csrfmiddlewaretoken': token,
        'right_menu_ajax': right_menu
    }

    call_ajax(url, 'post', data,function () {}, function () {})

}


// api query host selection field
let displayElement = function(value, element){
    if (value){
        element.show();
    }else{
        element.hide()
    }
}

// host selection
let customHostDisplay = function(value){
    if(value == 'custom'){
        requestTypeDisplay($('.api-query-edit #request-type').val());
        displayElement(true, $('.api-query-edit .host-selection-div'));
        displayElement(false, $('.api-query-edit .data-module-li'));
    }else{
        displayElement(false, $('.api-query-edit .get-div'));
        displayElement(false, $('.api-query-edit .host-selection-div'));
        displayElement(true, $('.api-query-edit .data-module-li'));
    }
}

// request type selection, get or post
let requestTypeDisplay = function(value){
    if ($('.api-query-edit #host-selection').val() == 'custom'){
        displayElement(value == 'get', $('.api-query-edit .get-div'));
    }

    displayElement(value == 'post', $('.api-query-edit .post-div'));
}

let dataTypeDisplay = function(value){
    displayElement(value == 'parametric', $('.api-query-edit .parametric-post-div'));
    displayElement(value == 'json', $('.api-query-edit .json-post-div'));
}

let validateApiQueryFrm = function(fetch=false){
    // post parameters
    let obj = {};
    for(let i=0 ; i < $('#parameters-number').val(); i++){
        obj[$(`#param-name-${i}`).val()] = $(`#param-value-${i}`).val();
    }

    //validations
    let error_messages = [];
    $('.errors-li').html("");
    // query string validation

    let qs_patt = new RegExp("^\\?([\\w-]+(=[\\w-]*)?(&[\\w-]+(=[\\w-]*)?)*)?$")

    if (!$('#host-selection').val()){
        error_messages.push('Bitte Host auswählen.');
    }
    if ($('#host-selection').val() == 'custom'
        && $('#request-type').val() == 'get'
        && $('#query-params').val()
        && !qs_patt.exec($('#query-params').val())){

        error_messages.push('Query Parameter ist nicht korrekt formatiert, ?a=1&b=2.');
    }

    // json format check
    try{
        if(($('#request-type').val() == 'post' && $('#data-type').val() == 'json')) {
            JSON.parse($('#json-input').val())
        }
    }catch{
        error_messages.push('JSON String ist nicht korrekt formatiert.')
    }

    let interval = parseInt($('#interval').val());
    if (! (interval >= 60 &&  interval <= 86400)){
        error_messages.push('Abfrageintervall muss zwischen 60 und 86400s liegen.')
    }

    if($('#data-module').val() == '-1'){
        error_messages.push('Bitte Data Plugins auswählen. ')
    }

    if($('#data-object').val() == '-1' && !fetch){
        error_messages.push('Bitte Data Object auswählen.')
    }

    for (let errmsg of error_messages){
        $('.errors-li').append(`<p>${errmsg}</p>`)
    }

    $('#post-parameters').val(JSON.stringify(obj));

    if (error_messages.length){
        $('.errors-li').show();
    }else {
        return true
    }
}

$(document).ready(function() {
    // show error div in case of error
    if ($('.api-query-edit .errors-li').html())
    {
        $(this).show();
    }

    // api_query edit page
    requestTypeDisplay($('.api-query-edit #request-type').val());
    dataTypeDisplay($('.api-query-edit #data-type').val());
    customHostDisplay($('.api-query-edit #host-selection').val())
    // right menu indication changing by chbox in module config main-pages
    $('.main-config-page #right-menu-ch').change(function(){
        change_right_menu_indication();
    })

    // host selection
    $('.api-query-edit #host-selection').change(function () {
        customHostDisplay($(this).val());
    })

    $('.api-query-edit #request-type').change(function () {
        requestTypeDisplay($(this).val());
    })

    $('.api-query-edit #data-type').change(function () {
        dataTypeDisplay($(this).val());
    })

    // append or remove rows to parameter table
    $('.api-query-edit #parameters-number').change(function () {
        let input_number = $(this).val();
        let tr_str = '';
        let tr_count = $('#parameters-table tr').length
        let append = input_number > tr_count ? true  : false;

        if (append) {
            let rows_to_append = input_number - tr_count;
            for(let i=0; i < rows_to_append; i++){
                tr_str = `<tr>
                    <td class="table-td-th">
                       <input type="text"  id="param-name-${i + tr_count}"  placeholder="name"/>
                    </td>
                    <td class="table-td-th">
                      <input type="text"  id="param-value-${i + tr_count}" placeholder="value"/>
                    </td>
                 </tr>`
                 $('.payload-div table').append(tr_str);
             }

        }else{
            for(let i=0; i < tr_count - input_number; i++){
                $('#parameters-table tr:last'). remove()
            }
        }
        $('.payload-div table').trigger( "create" );
    })

    // post btn
    $('.api-query-edit #save-btn').click(function () {
        if(validateApiQueryFrm()){
            $('#api-form').get(0).submit();
        }
    })

    // api query get ajax module data, call ajax by selectbox of mod names
    $('.api-query-edit #fetch-btn').click(function () {
        if(!validateApiQueryFrm(true)){
            return
        }

        let form_data = $('#api-form').serializeArray();
        // preparing parametric post parameters
        let obj = {};
        for(let i=0 ; i < $('#parameters-number').val(); i++){
            obj[$(`#param-name-${i}`).val()] = $(`#param-value-${i}`).val();
        }
        let token = $('input[name=csrfmiddlewaretoken]').val();
        let hausid = $('#data-module').data('hausid');
        let url = `/m_setup/${hausid}/api_query/execute_query/`;
        $.mobile.loading('show', {
            text: 'loading',
            textVisible: false,
            theme: 'a',
            html: ''
        });
        let posting_data = {
            'csrfmiddlewaretoken': token,
            'api_query_data': JSON.stringify({'frm_data': form_data, 'post_params': obj})
        }
        call_ajax(url, 'post', posting_data,
        function (data) {
            $('#data-object').html('<option value="-1">----</option>')
            let option_text = '';
            for (let item in data) {
                option_text = `<option value="${item}">${data[item].name}</option>`;
                $('#data-object').append(option_text);
            }
            $.mobile.loading("hide");
            $('.errors-li').show();
            }, function () {
                $.mobile.loading("hide");
                $('.errors-li').html('')
                $('.errors-li').append(`<p>Fehler bei Abfrage der API</p>`)
                $('.errors-li').show();
            })

    })
    // homebridge reset
    $('.homebridge #homebridge-reset').click(function () {
        let haus_id = $('#haus-id').val();
        let token = $('input[name=csrfmiddlewaretoken]').val();
        let data = {
            'csrfmiddlewaretoken': token,
            'reset': true
        }
        call_ajax(`/m_setup/${haus_id}/homebridge/`, 'POST', data, function (){
            alert('Reset erfolgreich');
        });
    })


    //
    $('.differenzregelung-edit #submit-btn').click(function () {
        if(!$('#name').val()){
            if($('.error-li').length == 0){
                $('.first-li').after('<li class="error-li" style="color:#cb0963;">bitte Name eingeben</li>');
                $('ul').listview('refresh');
            }
        }else{
            $('#drform').get(0).submit();
        }
    })

    $('.vorlauftemperaturregelung #submit-btn').click(function () {
        if(!$('#name').val()){
            if($('.error-li').length == 0){
                $('.first-li').after('<li class="error-li" style="color:#cb0963;">bitte Name eingeben</li>');
                $('ul').listview('refresh');
            }
        }else{
            $('#mrform').get(0).submit();
        }
    })


   // minute slider in sensor simulator module
   $( document).on( "slidestop", ".sensor-simulator #minute-slider",function(){
       let haus_id = $(this).data('id');
       $.ajax({
            url: `/m_setup/${haus_id}/sensor_simulator/`,
            data: {
                'minute_slider' : $(this).val() ,
                'csrfmiddlewaretoken' : $("input[name=csrfmiddlewaretoken]").val(),
            },
            type: 'post',
            async: true,
            complete: function() {
                //
            }
       });
   })


   // aus ein in sensor simulator module
   $('.sensor-simulator .set-status').click(function (){
      let haus_id = $('#haus_id').val()
      let status = $(this).hasClass('set-off') ? 'off': 'on';
      if(status == 'off'){
          $('.set-on').removeClass('ui-btn-active');
          $('.set-on').addClass('ui-btn-inactive');
          $('.set-off').addClass('ui-btn-active')
          $('.set-off').removeClass('ui-btn-inactive')
      }else{
          $('.set-off').removeClass('ui-btn-active');
          $('.set-off').addClass('ui-btn-inactive');
          $('.set-on').addClass('ui-btn-active')
          $('.set-on').removeClass('ui-btn-inactive')
      }

      $.ajax({
            url: `/m_setup/${haus_id}/sensor_simulator/`,
            data: {
                'set_status' : status,
                'csrfmiddlewaretoken' : $("input[name=csrfmiddlewaretoken]").val(),
            },
            type: 'post',
            async: true,
            complete: function() {
                //
            }
       });
   });


    // hour slider in periodicher reset module
    $( document).on( "slidestop", ".periodischer-reset #hour-slider",function(){
        let haus_id = $(this).data('id');
        $.ajax({
            url: `/m_setup/${haus_id}/periodischer_reset/`,
            data: {
                'hour_slider' : $(this).val() ,
                'csrfmiddlewaretoken' : $("input[name=csrfmiddlewaretoken]").val(),
            },
            type: 'post',
            async: true,
            complete: function() {
                //
            }
        });
    })

   // aus ein in perodischer reset module
   $('.periodischer-reset .set-status').click(function (){
    let haus_id = $('#haus_id').val()
    let status = $(this).hasClass('set-off') ? 'off': 'on';
    if(status == 'off'){
        $('.set-on').removeClass('ui-btn-active');
        $('.set-on').addClass('ui-btn-inactive');
        $('.set-off').addClass('ui-btn-active')
        $('.set-off').removeClass('ui-btn-inactive')
    }else{
        $('.set-off').removeClass('ui-btn-active');
        $('.set-off').addClass('ui-btn-inactive');
        $('.set-on').addClass('ui-btn-active')
        $('.set-on').removeClass('ui-btn-inactive')
    }

    $.ajax({
          url: `/m_setup/${haus_id}/periodischer_reset/`,
          data: {
              'set_status' : status,
              'csrfmiddlewaretoken' : $("input[name=csrfmiddlewaretoken]").val(),
          },
          type: 'post',
          async: true,
          complete: function() {
              //
          }
     });
 });

})