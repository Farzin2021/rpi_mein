from django.test import TestCase, RequestFactory, Client
from heizmanager.models import Haus, HausProfil, Etage, Raum, Regelung, Sensor, Gateway, GatewayAusgang, RPi
from django.core.cache import cache as memcache
import heizmanager.cache_helper as ch
import pytz
from datetime import datetime, timedelta
from django.contrib.auth.models import User
import logging
from django.core.management import call_command
from time import mktime
from heizmanager.abstracts.base_mode import BaseMode
from heizmanager.abstracts.base_time_task import TimeTask
from quickui.views import QuickuiMode
import heizmanager.helpers as helpers
from heizmanager.tasks import every_minute_task_manager


class BaseTestSetup(TestCase):

    def setUp(self):
        # set time-zone
        berlin = pytz.timezone('Europe/Berlin')
        self.berlin_now = datetime.now(berlin)

        # request factory
        self.factory = RequestFactory()
        self.client = Client()
        user = User.objects.create_user("test@test.de", "test@test.de", "test")
        haus = Haus.objects.create(name="Teststr. 1, 12345 Testhausen", eigentuemer=user)
        HausProfil.objects.create(haus=haus,
                                  modules="ruecklaufregelung,zweipunktregelung,funksollregelung",
                                  module_parameters="{}")
        etage = Etage.objects.create(name="testetage", haus=haus, position=0)
        reg1 = Regelung.objects.create(
            regelung="ruecklaufregelung",
            parameter="{'kruemmung': 2.0, 'intervall': 600, 'schaerfe': 0.0, 'rlsensorssn':"
                      " {'Sensor*3': [1]}, 'neigung': 5.5, 'offset': 20, 'active': True}"
        )

        reg2 = Regelung.objects.create(
            regelung="zweipunktregelung",
            parameter="{'active': True, 'delta': 0.0}"
        )

        raum1 = Raum.objects.create(
            name="testraum 1",
            etage=etage,
            solltemp=21.0,
            position=0,
            regelung=reg1,
            modules="",
            module_parameters="{}"
        )
        raum2 = Raum.objects.create(
            name="testraum 2",
            etage=etage,
            solltemp=21.0,
            position=0,
            regelung=reg2,
            modules="",
            module_parameters="{}"
        )

        gateway = Gateway.objects.create(
            haus=haus,
            name="aa-bb-cc-dd-ee-ff",
            version="nix",
            parameters=""
        )

        GatewayAusgang.objects.create(
            ausgang="1",
            stellantrieb="NC",
            gateway=gateway,
            regelung=reg1
        )

        Sensor.objects.create(
            haus=haus,
            name="00_22_33_44_55_66_20_21",
            raum=raum1,
            mainsensor=True,
            gateway=gateway
        )

        Sensor.objects.create(
            haus=haus,
            name="00_22_33_44_55_66_20_23",
            raum=raum2,
            mainsensor=True,
            gateway=gateway
        )

        Sensor.objects.create(
            haus=haus,
            name="00_22_33_44_55_66_20_22",
            raum=raum1,
            mainsensor=False,
            gateway=gateway
        )

        RPi.objects.create(
            haus=haus,
            name="b8-27-eb-e0-e7-5f"
        )

    @staticmethod
    def get_raum_by_id(room_id):
        return Raum.objects.get(id=room_id)

    @staticmethod
    def get_haus_by_id(haus_id):
        return Haus.objects.get(id=1)

    def assert_raum_solltemp_equal(self, room_id, soll):
        raum = Raum.objects.get(id=room_id)
        self.assertEqual(raum.solltemp, float(soll))

    def get_output_state_room(self, room_id):
        raum = Raum.objects.get(id=room_id)
        content = raum.get_regelung().do_ausgang(1).content
        return content

    def assert_room_state_is_on(self, room_id):
        self.assertEqual(self.get_output_state_room(room_id), '<1>')

    def assert_room_state_is_off(self, room_id):
        self.assertEqual(self.get_output_state_room(room_id), '<0>')

    def assert_module_is_activated(self, haus, mod):
        self.assertIn(mod, helpers.get_active_modules(haus))


class BaseModeTest(BaseTestSetup):

    def setUp(self, fixture_path=None):
        super(BaseModeTest, self).setUp()
        call_command('loaddata', 'heizmanager/fixtures/base_mode_fixture.json', verbosity=0)
        self.fixture_path = fixture_path
        if fixture_path:
            call_command('loaddata', self.fixture_path, verbosity=0)

        # login
        from users.models import UserProfile
        user_p = UserProfile.objects.get(user__username='test@test.de')
        user_p.role = "A"
        user_p.save()
        self.client.login(username='test@test.de', password='test')
        # clear the cache
        memcache._cache.flush_all()

    @staticmethod
    def run_auto_switch():
        every_minute_task_manager()

    @staticmethod
    def run_modules_timer():
        every_minute_task_manager()

    @staticmethod
    def get_active_mode_raum(raum):
        return BaseMode.get_active_mode_raum(raum)

    def set_hand_mode_single_room_ui(self, room_id=1, slidernumber=20.5, duration=180):
        # setting hand-mode
        response = self.client.post('/quick-change/', {
            'set_ziel_slider': True,
            'room_id': room_id,
            'slidernumber': slidernumber,
            'all_time': duration
        })
        # test through response
        self.assertEqual(response.status_code, 200)
        self.assertIn('time', response.json())
        return response.json()

    def set_hand_mode_single_room_db(self, room_id=1, slidernumber=20.5, duration=180):
        quick_mode = QuickuiMode(slidernumber, duration)
        raum = self.get_raum_by_id(room_id)
        return quick_mode.set_mode_raum(raum)

    def set_expired_hand_mode_single_room(self, room_id, slidernumber=25.0):
        # setting an expired ziel with trick of adding a negative duration
        raum = self.get_raum_by_id(room_id)
        r_params = raum.get_module_parameters()
        r_params['quickui'] = dict()
        ex_time = helpers.get_str_formatted_time(self.berlin_now)
        r_params['quickui']['room_time'] = (10, ex_time, slidernumber)
        r_params['temp_solltemp'] = raum.solltemp
        raum.set_module_parameters(r_params)
        raum.solltemp = slidernumber
        raum.save()
        return raum

    def set_temperaturszene_in_single_room_ui(self, room_id, mode_id):
        haus = Haus.objects.get(id=1)
        self.assertIn('temperaturszenen', haus.get_modules())
        response = self.client.post('/m_raum/%s/temperaturszenen/' % room_id, {
            'mode_activate': True,
            'mode_id': mode_id
        })
        self.assertIn('success', response.json())
        self.assertTrue(response.json()['success'])
        return response

    def set_temperaturszene_in_single_room_db(self, room_id, mode_id):
        raum = Raum.objects.get(id=room_id)
        r_params = raum.get_module_parameters()
        r_temperaturmodi = r_params.get('temperaturmodi', dict())
        r_temperaturmodi['activated_mode'] = mode_id
        r_params['temperaturmodi'] = r_temperaturmodi
        raum.set_module_parameters(r_params)
        return raum

    def set_heizprogramm_entry_ui(self, mode_id, time_to_set=None, heizp_id=0):
        if time_to_set is None:
            time_to_set = self.berlin_now
        day_of_week = time_to_set.weekday()
        str_now_time = time_to_set.strftime("%H:%M")
        haus = Haus.objects.get(id=1)
        heizp_dict = helpers.get_heizprogramm_dict(haus, heizp_id)
        response = self.client.post('/m_heizprogramm/1/?edit=%s' % heizp_id, {
            'time': str_now_time,
            'repeat': [0, 1, 2, 3, 4, 5, 6],
            'select-mode': mode_id,
            'add_entry': 'add_entry',
            'heizprogramm_name': heizp_dict.get('heizp_name')
        })
        self.assertEqual(response.status_code, 200)
        return response

    def active_heizprogramm_ui(self, heizp_id):
        response = self.client.post('/m_heizprogramm/1/', {
            'activated': heizp_id
        })
        self.assertEqual(response.status_code, 204)
        return response

    def set_jahreskalender_entry_ui(self, typ, id_to_set, begin=None, end=None, repeat=1):
        if begin is None:
            begin = self.berlin_now

        if end is None:
            end = self.berlin_now + timedelta(days=7)

        begin = begin.strftime('%Y-%m-%dT%H:%M')
        end = end.strftime('%Y-%m-%dT%H:%M')
        url = '/m_jahreskalender/1/'
        if typ == 'temperaturszenen':
            params_to_post = {
                'typ': typ,
                'select-mode': id_to_set,
                'next-select-mode': 1,
                'begin': begin,
                'end': end,
                'repeat': repeat,
                'end_repeat': '2099-12-31T23:59',
                'name': ''
            }
            response = self.client.post(url, params_to_post)
            self.assertEqual(response.status_code, 302)
            return typ, id_to_set, begin, end
        elif typ == 'heizprogramm':
            params_to_post = {
                'typ': typ,
                'select-heizpid': id_to_set,
                'repeat': repeat,
                'begin': begin,
                'end_repeat': '2099-12-31T23:59',
                'name': ''
            }
            response = self.client.post(url, params_to_post)
            self.assertEqual(response.status_code, 302)

            return typ, id_to_set, begin, end

    def set_jahreskalender_entry_db(self, typ, id_to_set, begin=None, end=None):
        if begin is None:
            begin = self.berlin_now

        if end is None:
            end = self.berlin_now + timedelta(days=7)

        begin_int = mktime(begin.timetuple())
        end_int = mktime(begin.timetuple())
        begin = begin.strftime('%Y-%m-%dT%H:%M')
        end = end.strftime('%Y-%m-%dT%H:%M')
        created = self.berlin_now.strftime('%Y-%m-%d %H:%M')
        created_int = mktime(self.berlin_now.timetuple())
        haus = Haus.objects.get(id=1)
        h_params = haus.get_module_parameters()
        if typ == 'temperaturszenen':
            mode_name = helpers.get_temperatureszene_mode_name(haus, id_to_set)
            params_to_set = {
                'begin_int': begin_int,
                'end_int': end_int,
                'begin': begin,
                'end': end,
                'created': created,
                'created_int': created_int,
                'edited': created,
                'edited_int': created_int,
                'repeat': [1, 2, 3, 4, 5, 6, 7],
                'mode_name': mode_name,
                'mode_id': id_to_set,
                'next_mode_id': 1,
                'showing_value': id_to_set,
                'showing_text': mode_name,
                'end_repeat': '2099-12-31T23:59',
                'name': '',
                'typ': 'temperaturszenen'
            }
            h_params['jahreskalender'] = dict()
            h_params['jahreskalender']['temperaturszenen'] = list()
            h_params['jahreskalender']['temperaturszenen'].append(params_to_set)
            haus.set_module_parameters(h_params)
            return h_params

        elif typ == 'heizprogramm':
            heizp_record = helpers.get_heizprogramm_dict(haus, id_to_set)
            if heizp_record:
                heizp_name = heizp_record.get('heizp_name')
            else:
                heizp_name = ''
            params_to_set = {
                'begin_int': begin_int,
                'begin': begin,
                'end': None,
                'created': created,
                'created_int': created_int,
                'edited': created,
                'edited_int': created_int,
                'repeat': [1, 2, 3, 4, 5, 6, 7],
                'heizp_name': heizp_name,
                'heizp_id': id_to_set,
                'next_mode_id': 1,
                'showing_value': id_to_set,
                'showing_text': heizp_name,
                'end_repeat': '2099-12-31T23:59',
                'name': '',
                'typ': 'heizprogramm'
            }
            h_params['jahreskalender'] = dict()
            h_params['jahreskalender'][typ] = list()
            h_params['jahreskalender'][typ].append(params_to_set)
            haus.set_module_parameters(h_params)
            return h_params

    def set_heizprogramm_entry_db(self, mode_id, time_to_set=None, heizp_id=0):
        if time_to_set is None:
            time_to_set = self.berlin_now
        day_of_week = time_to_set.weekday()
        str_now_time = time_to_set.strftime("%H:%M")
        time_object = datetime.strptime("2018-1-1-" + str_now_time, '%Y-%m-%d-%H:%M').timetuple()
        ts = int(mktime(time_object))
        # adding a switch with given mode id into db
        haus = Haus.objects.get(id=1)
        haus_params = haus.get_module_parameters()
        mode_name = haus_params['temperaturmodi'].get(mode_id, {}).get('name')
        switchingmode = [{'heizp_id': heizp_id, 'mode_id': mode_id, 'ts': ts, 'day': day_of_week,
                          'time': str_now_time, 'mode_name': mode_name, 'id': 1}]
        haus_params['switchingmode'] = switchingmode
        haus.set_module_parameters(haus_params)
        return switchingmode

    def assert_hand_mode_room_is_set_in_room_ui(self, room_id, temperature):
        response = self.client.get('/m_raum_temp_html/%s/' % room_id)
        self.assertContains(response, str(temperature))
        self.assertContains(response, 'icon-hand.png')

    def assert_temperaturszene_is_set_in_room_ui(self, room_id, mode_id):
        haus = Haus.objects.get(id=1)
        mode_name = helpers.get_temperatureszene_mode_name(haus, mode_id)
        # get mode name
        self.assertIsNotNone(mode_name)
        raum = Raum.objects.get(id=room_id)
        # get temperatur
        temperature = helpers.get_temperatureszene_temperature(raum, mode_id)
        self.assertIsNotNone(temperature)
        response = self.client.get('/m_raum_temp_html/%s/' % room_id)
        self.assertContains(response, mode_name)
        self.assertContains(response, 'icon-clock.png')
        self.assertContains(response, str(temperature))

    def assert_temperatureszene_is_set_in_db(self, room_id, mode_id):
        raum = Raum.objects.get(id=room_id)
        self.assertEqual(helpers.get_temperatureszene_activated_mode_id_raum(raum), mode_id)

    def assert_temperatureszene_is_activated_in_room(self, room_id, mode_id):
        raum = Raum.objects.get(id=room_id)
        haus = raum.etage.haus
        self.assertIn('temperaturszenen', haus.get_modules())
        mode = BaseMode.get_active_mode_raum(raum)
        self.assertIsInstance(mode, dict)
        self.assertEqual(mode['mod'], 'temperaturszenen')
        self.assertEqual(mode['activated_mode_id'], mode_id)
        self.assert_raum_solltemp_equal(room_id, mode.get('soll'))

    def assert_hand_mode_is_activated_in_room(self, room_id, temperature):
        raum = Raum.objects.get(id=room_id)
        mode = BaseMode.get_active_mode_raum(raum)
        self.assertIsInstance(mode, dict)
        self.assertEqual(mode['mod'], 'quickui')
        self.assertEqual(mode['soll'], temperature)

    def assert_hand_mode_is_set_in_db(self, room_id, temperature, duration):
        raum = Raum.objects.get(id=room_id)
        qui_params = raum.get_module_parameters().get('quickui')
        self.assertIsNotNone(qui_params)
        ziel_val = qui_params.get('ziel_val')
        self.assertIsNotNone(ziel_val)
        room_time = qui_params.get('room_time')
        self.assertIsNotNone(room_time)
        self.assertIsInstance(room_time, tuple)
        self.assertEqual(qui_params['room_time'][0], duration)
        self.assertEqual(qui_params['room_time'][2], temperature)

    def assert_heizprogramm_is_set_db(self, mode_id, str_time, day):
        haus = Haus.objects.get(id=1)
        haus_params = haus.get_module_parameters()
        swithching_mode = haus_params.get('switchingmode', [])

        # check there is any record with given info
        assertion = any(sw['mode_id'] == mode_id and sw['time'] == str_time and sw['day'] == day
                        for sw in swithching_mode)
        self.assertTrue(assertion)

    def assert_hand_mode_is_not_set(self, room_id):
        raum = Raum.objects.get(id=room_id)
        mode = BaseMode.get_active_mode_raum(raum)
        if mode is not None:
            self.assertNotEqual(mode['mod'], 'quickui')

    def assert_heizprogramm_activated(self, heizp_id):
        haus = Haus.objects.get(id=1)
        heizp_dict = helpers.get_heizprogramm_dict(haus, heizp_id)
        self.assertIsNotNone(heizp_dict)
        self.assertIn('is_active', heizp_dict)
        self.assertTrue(heizp_dict['is_active'])


class TaskTest(BaseTestSetup):

    def setUp(self, fixture_path=None):
        super(TaskTest, self).setUp()
        # call_command('loaddata', 'heizmanager/fixtures/base_mode_fixture.json', verbosity=0)
        self.fixture_path = fixture_path
        if fixture_path:
            call_command('loaddata', self.fixture_path, verbosity=0)

        # login
        self.client.login(username='test@test.de', password='test')
        # clear the cache
        memcache._cache.flush_all()
        self.ch = ch

    def get_module_tasks(self, module_name):
        haus = self.get_haus_by_id(1)
        return TimeTask.get_module_tasks(haus, module_name)
