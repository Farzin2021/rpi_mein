from heizmanager.render import render_response, render_redirect
from heizmanager.models import Haus, Raum
from heizmanager.mobile.m_temp import delete_room_offset_in_cache
from django.contrib.auth.decorators import login_required
import logging
import importlib
from django.http import HttpResponse
from django.views.decorators.csrf import csrf_exempt
from django.http import Http404
from benutzerverwaltung.decorators import is_admin, multizone_pro_access_page
from users.models import UserProfile
import heizmanager.cache_helper as ch
from heizmanager.modules.module import get_mods_list, get_hidden_modules
import json


@csrf_exempt
def config(request, hausid=None):

    profiles = UserProfile.objects.all().order_by("id")

    try:
        logging.warning([p.id for p in profiles])
    except:
        from fabric.operations import local as lrun
        result = lrun("sqlite3 /home/pi/rpi/db.sqlite3 '.schema users_userprofile'", capture=True)
        if 'parameters' not in result:
            lrun("python /home/pi/rpi/manage.py makemigrations users")
            lrun("python /home/pi/rpi/manage.py migrate users")

    if len(profiles) == 1 and profiles[0].role == "K":
        profiles[0].role = 'A'
        profiles[0].save()

    if hausid:
        haus = Haus.objects.filter(pk=long(hausid))
    else:
        for haus in Haus.objects.order_by('id'):
            if request.user.id in haus.besitzer:
                haus = [haus]
                break
    if len(haus):
        haus = haus[0]
    if request.method == 'GET':

        swlinks, category_dict = get_mods_list(haus, just_activated=True, setting_link=True)

        hidden_modules = get_hidden_modules('configuration')

        return render_response(request, "m_menu.html", {"haus": haus, "swlinks": swlinks,
                                                        "category_dict": category_dict.items(),
                                                        'cat': json.dumps(category_dict),
                                                        'hidden_modules': hidden_modules})
    else:
        params = haus.get_module_parameters()
        params['show_config'] = 1 if request.POST['checked'] == '1' else 0
        haus.set_module_parameters(params)
        return HttpResponse('Done')


@is_admin
def get_module_settings(request, hausid, modulename):
    haus = Haus.objects.get(pk=long(hausid))
    module = None
    try:
        module = importlib.import_module(modulename.strip() + '.views')
    except ImportError:
        if modulename.strip() in ['module', 'setup_rpi']:
            module = importlib.import_module('heizmanager.modules.' + modulename)
    if module is None:
        raise Http404("Plugin nicht gefunden.")
    return module.get_global_settings_page(request, haus)


@is_admin
def get_module_settings_help(request, hausid, modulename):
    haus = Haus.objects.get(pk=long(hausid))
    try:
        module = importlib.import_module(modulename.strip() + '.views')
        return module.get_global_settings_page_help(request, haus)
    except (ImportError, AttributeError):
        return render_redirect(request, "/m_setup/%s/%s/" % (hausid, modulename))


def get_entity(entityid):
    return Haus.objects.get(pk=long(entityid))


def get_regelung_entity(haus, objid, modulename):
    # abhaengig von modulename - also dem namen der regelung -
    # kann auch an Regelung object gespeichert werden
    hparams = haus.get_module_parameters()
    return hparams.get(modulename, dict()).get(objid, dict())


def set_regelung_entity(haus, modulename, objid, params, setting_module):
    hparams = haus.get_module_parameters()
    hparams.setdefault(modulename, dict())
    hparams[modulename][objid].setdefault('module_params', dict())
    hparams[modulename][objid]['module_params'][setting_module] = params
    haus.set_module_parameters(hparams)
    return True


@login_required(login_url='/accounts/m_login/')
@multizone_pro_access_page
def get_module_page_for_entity(request, modulename, entityid, action=None, objid=None, regmodule=None):
    module = importlib.import_module(modulename.strip() + '.views')
    entity = get_entity(entityid)

    if request.method == 'POST' or 'delete' in request.GET or 'zsdel' in request.GET or 'asdel' in request.GET :
        if request.method == 'POST' and request.POST.get('singleroom', False):
            roomid = long(request.POST.get('id', 0))
            try:
                raum = Raum.objects.get(id=roomid)
                delete_room_offset_in_cache(raum, modulename)
            except Raum.DoesNotExist:
                pass
        ch.delete_module_offset_haus(modulename, entityid)

    if action is None:
        ch.set_usage_val('glsph_%s' % ("post" if request.method == 'POST' else "get"), modulename)
        return module.get_local_settings_page_haus(request, entity)
    elif objid is None:
        return module.get_local_settings_page_haus(request, entity, action)
    elif regmodule is None:
        return module.get_local_settings_page_haus(request, entity, action, objid)
    else:
        module = importlib.import_module(regmodule.strip() + '.views')
        params = get_regelung_entity(entity, objid, modulename).get('module_params', dict()).get(regmodule, dict())
        return module.get_local_settings_page_regelung(request, entity, modulename, objid, params)
