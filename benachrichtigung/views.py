# -*- coding: utf-8 -*-
import logging

from heizmanager.render import render_response, render_redirect
import pytz
from heizmanager.models import Haus, Regelung
from users.models import UserProfile
from django.contrib.auth.models import User
from heizmanager.abstracts.base_notification import BaseNotification
from heizmanager.network_helper import get_mac
from datetime import datetime, timedelta
import heizmanager.cache_helper as ch
from .notifications import EmailNotification

from django.http import HttpResponse
from fabric.api import local
from django.views.decorators.csrf import csrf_exempt


def get_cache_ttl():
    return 1800


def is_togglable():
    return True


def is_hidden_offset():
    return False


def calculate_always_anew():
    return False


def get_offset(haus, raum=None):
    pass


def get_name():
    return 'Timer'


def get_global_settings_link(request, haus):
    return "<a href='/m_setup/%s/benachrichtigung/'>Email Benachrichtigungen</a>" % str(haus.id)


def get_global_description_link():
    desc = "Email-Nachrichten bei frei konfigurierbaren Ereignissen.."
    desc_link = "http://support.controme.com/email-benachrichtigungen/"
    return desc, desc_link


def get_service_monitor_logs(filters):
    ret = []

    if 'fps_name' in filters:
        typ = 'fps'
        matching_regex = '\(^/home/pi/rpi/fps\|^./fps\)/views.py'
        try:
            log = local("grep '%s' /var/log/uwsgi/uwsgi.log | sort -r -k2 -t '|'" % matching_regex, capture=True)
        except:
            log = ''
        try:
            log1 = local("grep '%s' /var/log/uwsgi/uwsgi.log.1 | sort -r -k2 -t '|'" % matching_regex, capture=True)
            log += '\n' + log1
        except:
            pass

    else:
        typ = 'device'
        matching_regex = r'^/home/pi/rpi/logmonitor/tasks.py.update_service_monitor\|.*\|(Gateway|Sensor)\|'

        try:
            log = local("grep -a '%s' /var/log/uwsgi/service_monitor.log2 | sort -r -k2 -t '|'" % matching_regex,
                        capture=True)
        except:
            return ret

    if not log:
        return ret

    log = log.split('\n')[:10000]
    try:
        for line in log:
            l = line.split('|')
            if typ == 'fps':
                if l[2] == str(filters['selected_fps']):
                    ret.append([l[1], filters['fps_name'], l[3]])
            else:
                if l[2] == 'Gateway':
                    if 'gateway' not in filters:
                        continue
                    ret.append([l[1], "%s - %s" % (l[2], l[3]), l[4]])
                elif l[2] == 'Sensor':
                    if 'sensor' not in filters:
                        continue
                    ret.append([l[1], "%s - %s" % (l[2], l[3]), l[4]])
    except:
        pass
    return ret


def generate_report(request, filters=None):

    report_data = get_service_monitor_logs(filters)
    # Define the HTML template string
    return render_response(request, 'benachrichtigung_report.html', {'report_data': report_data})


def calculate_next_nearest_n_minutes(n):
    berlin_now = datetime.now(pytz.timezone('Europe/Berlin'))
    berlin_now = berlin_now + timedelta(minutes=n)
    now = berlin_now.now().replace(hour=berlin_now.hour, minute=berlin_now.minute, second=0, microsecond=0)
    next_multiple = (now.minute // 5 + 1) * 5
    next_time = now + timedelta(minutes=next_multiple - now.minute)
    return next_time


def get_global_settings_page(request, haus, action=None, entityid=None):

    if not isinstance(haus, Haus):
        haus = Haus.objects.get(pk=long(haus))

    context = dict()
    context['haus'] = haus
    params = haus.get_spec_module_params('benachrichtigung')
    b_params = params.get('items', dict())
    if request.method == "GET":
        if action == 'edit':

            userlist = UserProfile.objects.filter(user_id__in=haus.besitzer).order_by('id')
            context['userlist'] = userlist
            fps_regs = Regelung.objects.filter(regelung='fps')
            context['fps_lst'] = [(fps.id, fps.get_parameters().get('name', '')) for fps in fps_regs]
            if entityid:
                b_param = b_params.get(entityid)
                context['b_param'] = b_param
                context['b_id'] = entityid
                cache_key = "last_time_email_sent_user_%s" % entityid
                last_time_email_sent = get_from_cache_or_db(haus, cache_key)
                context['last_time_email_sent'] = last_time_email_sent if last_time_email_sent else ''
                send_frequency = b_param.get('send_frequency', 0)
                context['send_frequency'] = send_frequency
                if send_frequency > 0:
                    scheduled_email = get_from_cache_or_db(haus, 'scheduled_email')
                    if scheduled_email is None:
                        scheduled_email = {}
                    if '%s_%s' % (entityid, 'user') in scheduled_email:
                        next_allowed_time = scheduled_email['%s_%s' % (entityid, 'user')]['next_allowed_time']
                        context['next_allowed_time'] = next_allowed_time
            return render_response(request, "m_settings_benachrichtigung_edit.html", context)

        if action == 'delete':
            # invalidate cache of scheduled email for this entry
            scheduled_email = get_from_cache_or_db(haus, 'scheduled_email')
            if scheduled_email and entityid:
                scheduled_email.pop('%s_%s' % (entityid, 'user'), None)
                set_to_cache_and_db(haus, 'scheduled_email', scheduled_email)

            del b_params[entityid]
            params['items'] = b_params
            haus.set_spec_module_params('benachrichtigung', params)
            cache_key = "last_time_email_sent_user_%s" % entityid
            last_time_email_sent = get_from_cache_or_db(haus, cache_key)
            if last_time_email_sent:
                set_to_cache_and_db(haus, cache_key, None)
            return render_redirect(request, '/m_setup/%s/benachrichtigung/' % haus.id)

        if action == 'report':
            b_param = b_params.get(entityid)
            filters = {}
            if b_param:
                if b_param.get('notify_mode') == 'fps':
                    selected_fps = b_param.get('selected_fps', 0)
                    if selected_fps:
                        fps_name = Regelung.objects.get(pk=selected_fps).get_parameters().get('name', '')
                        filters = {
                            'selected_fps': selected_fps,
                            'fps_name': fps_name
                        }
                else:
                    if b_param.get('gateways_on', True):
                        filters['gateway'] = True
                    if b_param.get('sensors_on', True):
                        filters['sensor'] = True

                return generate_report(request, filters=filters)

        context['b_params'] = sorted(b_params.items())
        context['is_active'] = haus.get_spec_module_params('wetter_pro').get('is_active', False)
        return render_response(request, "m_settings_benachrichtigung.html", context)

    if request.method == "POST":

        # invalidate cache of scheduled email for this entry
        scheduled_email = get_from_cache_or_db(haus, 'scheduled_email')
        if scheduled_email and entityid:
            scheduled_email.pop('%s_%s' % (entityid, 'user'), None)
            set_to_cache_and_db(haus, 'scheduled_email', scheduled_email)

        # preparing data
        data_d = dict()
        data_d['name'] = request.POST.get('name', '')
        user_lst = request.POST.getlist('users')
        data_d['users'] = user_lst
        data_d['notify_mode'] = request.POST.get('notify_mode', 'device-status')
        data_d['email_text_1'] = request.POST.get('email_text_1', '')
        data_d['email_text_2'] = request.POST.get('email_text_2', '')
        data_d['selected_fps'] = int(request.POST.get('selected_fps', 0))
        data_d['send_frequency'] = int(request.POST.get('send_frequency', 0))
        data_d['sensors_on'] = int(request.POST.get('sensors_on', 0))
        data_d['gateways_on'] = int(request.POST.get('gateways_on', 0))

        if entityid is None:
            ids = [0] + [int(k) for k in b_params.keys()]
            entityid = str(max(ids) + 1)
            b_params[entityid] = dict()

        # saving data
        b_params[entityid].update(data_d)
        params['items'] = b_params
        haus.set_spec_module_params('benachrichtigung', params)

        berlin_now = datetime.now(pytz.timezone('Europe/Berlin'))
        berlin_now_str = berlin_now.strftime('%d.%m.%Y-%H:%M:%S')
        # initial last time email sent
        last_time_sent_cache_key = 'last_time_email_sent_%s_%s' % ('user', entityid)
        set_to_cache_and_db(haus, last_time_sent_cache_key, berlin_now_str)
        return render_redirect(request, '/m_setup/%s/benachrichtigung/' % haus.id)


def get_local_settings_link(request, raum):
    pass


def activate(obj):
    if 'benachrichtigung' not in obj.get_modules():
        obj.set_modules(obj.get_modules() + ['benachrichtigung'])


def get_local_settings_page(request, raum):
    pass


def get_local_settings_page_haus(request, haus):
    pass


def get_global_settings_page_help(request, haus):
    return None


def get_local_settings_link_haus(request, haus):
    pass


def notify_users(haus, notification_type, message, user_list=[], *args, **kwargs):
    ret = BaseNotification.notify(notification_type, message, user_list, *args, **kwargs)
    return 0, ret


def get_user_emails_from_id_list(user_id_lst=[]):
    email_lst = list()
    for user in user_id_lst:
        if user not in email_lst:
            try:
                user = User.objects.get(id=int(user))
            except User.DoesNotExist:
                continue
            email_lst.append(user.email)
    return email_lst


def get_rpi_link():
    try:
        mac = get_mac()
        return "%s%s%s" % (mac[9:11], mac[12:14], mac[15:17])
    except:
        return ''


def prepare_fps_summary(haus, message, *args, **kwargs):
    selected_node = kwargs.get('context', {}).get('fps_id', 0)
    key = 'fps_last_val_%s' % selected_node
    last_val = get_from_cache_or_db(haus, key)
    if last_val is None:
        set_to_cache_and_db(haus, key, message)
        return
    if last_val != message:
        set_to_cache_and_db(haus, key, message)
        typ = 'fps'
        prepare_notification_summary(haus, typ, selected_node, message)
        return


def prepare_device_summary(haus, message, *args, **kwargs):
    device_type = kwargs.get('context', {}).get('device_type')
    typ = 'device-status'
    if device_type == 'gateway':
        selected_node = 'gateway'
    else:
        selected_node = 'sensor'

    prepare_notification_summary(haus, typ, selected_node, message)
    return


def get_from_cache_or_db(haus, key):
    ret = ch.get(key)
    if ret is None:
        b_params = haus.get_spec_module_params('benachrichtigung')
        ret = b_params.get(key, {})
    return ret


def set_to_cache_and_db(haus, key, value):
    b_params = haus.get_spec_module_params('benachrichtigung')
    b_params[key] = value
    haus.set_spec_module_params('benachrichtigung', b_params)
    ch.set(key, value)
    return


def check_entry(haus, entry_id, notification_params, user_or_support, typ,
                selected_node, message, *args, **kwargs):

    if notification_params.get('notify_mode') != typ:
        return

    if typ == 'fps':
        selected_fps = int(notification_params.get('selected_fps', 0))
        if selected_fps != selected_node:
            return

    elif typ == 'device':
        device_type = selected_node
        if device_type == 'sensor':
            if not notification_params.get('sensors_on', True):
                return

        if device_type == 'gateway':
            if not notification_params.get('gateways_on', True):
                return

    send_frequency = notification_params.get('send_frequency', 0)
    if send_frequency == 0:
        has_duration = False
    else:
        has_duration = True

    context = dict()
    berlin_now = pytz.timezone('Europe/Berlin')
    berlin_now = datetime.now(berlin_now)
    berlin_now_str = berlin_now.strftime('%d.%m.%Y-%H:%M:%S')
    entry_id = int(entry_id)
    email_for_support = ''
    if 'email_for_support' in notification_params:
        subject = "IDS%s - %s" % (entry_id, notification_params.get('name', ''))
        email_for_support = notification_params.get('email_for_support', '')
    else:
        subject = "ID%s - %s" % (entry_id, notification_params.get('name', ''))

    # populate emails of users of record
    email_lst = get_user_emails_from_id_list(notification_params.get('users', list()))
    email_to_support = True if 'controme_s_email' in notification_params and notification_params['controme_s_email'] else False
    if email_to_support:
        email_lst.append('support@controme.com')

    if notification_params.get('notify_mode') == 'fps':
        notification_purpose = "Statusbenachrichtigung"
    else:
        notification_purpose = "Ausfallbenachrichtigung"

    context.update(dict(notification_purpose=notification_purpose))
    context.update(dict(subject=subject))
    last_time_sent_cache_key = 'last_time_email_sent_%s_%s' % (user_or_support, entry_id)
    if has_duration:
        if user_or_support == 'support':
            entry_link = "support-benachrichtigung"
        else:
            entry_link = "benachrichtigung"

        link = "https://%s.fwd.controme.com/m_setup/%s/%s/report/%s/" % (get_rpi_link(),
                                                                         haus.id,
                                                                         entry_link,
                                                                         entry_id)

        message_to_send = u"""%s \nEs gibt neue Logeinträge zu Ihrer angelegten Benachrichtigung. Diese sind unter nachfolgendem Link einsehbar:\n%s\n""" % (email_for_support, link)

        scheduled_email = get_from_cache_or_db(haus, 'scheduled_email')
        if scheduled_email is None:
            scheduled_email = {}

        dict_key = '%s_%s' % (entry_id, user_or_support)
        if dict_key in scheduled_email:
            return

        last_time_sent = get_from_cache_or_db(haus, last_time_sent_cache_key)
        if last_time_sent:
            last_time_sent = datetime.strptime(last_time_sent.replace(" ", ""), '%d.%m.%Y-%H:%M:%S')
            next_allowed_time = last_time_sent + timedelta(minutes=send_frequency)
        else:
            next_allowed_time = berlin_now + timedelta(minutes=send_frequency)

        dict_key = '%s_%s' % (entry_id, user_or_support)
        scheduled_email[dict_key] = {
            'subject': subject,
            'message': message_to_send,
            'email_to': email_lst,
            'next_allowed_time': next_allowed_time.strftime('%d.%m.%Y-%H:%M:%S'),
            'context': context,
            'entry_id': entry_id,
            'user_or_support': user_or_support,
            'typ': typ,
        }
        set_to_cache_and_db(haus, 'scheduled_email', scheduled_email)
        return

    else:
        if typ == 'fps':
            if message == 0:
                text = notification_params.get('email_text_1', '')
            else:
                text = notification_params.get('email_text_2', '')

            message_to_send = """%s\nName der Benachrichtung: %s\nZeitpunkt: %s\nText: %s\n""" % (email_for_support,
                                                                                                  notification_params.get(
                                                                                                      'name', ''),
                                                                                                  berlin_now_str,
                                                                                                  text)
        else:  # typ == 'device'

            message_to_send = ""

            message_split = message.split('|')
            check, device_id, desc = message_split[0], message_split[1], message_split[2]
            message_to_send += u"""%s \nZeitpunkt: %s\ncheck: %s\nID: %s\nMeldung: %s""" % (email_for_support,
                                                                                            berlin_now_str,
                                                                                            check,
                                                                                            device_id, desc)

    set_to_cache_and_db(haus, last_time_sent_cache_key, berlin_now_str)
    if len(email_lst) and message_to_send:
        try:
            notify_users(haus, 'email', message_to_send, email_lst, **context)
        except Exception as e:
            logging.warning("Error sending email: %s" % e)

    return


def prepare_notification_summary(haus, typ, selected_node, message):
    haus_params = haus.get_module_parameters()
    wetter_pro_params = haus_params.get('wetter_pro', {})
    # check activation before sending email
    if not wetter_pro_params.get('is_active', False):
        return

    b_params = haus_params.get('benachrichtigung', {})
    b_support_params = haus_params.get('support_benachrichtigung', {})
    b_items = b_params.get('items', {})
    b_support_items = b_support_params.get('items', {})
    user_items = list(b_items.items())
    support_items = list(b_support_items.items())
    for entity_id, params in user_items:
        check_entry(haus, entity_id, params, 'user', typ, selected_node, message)

    for entity_id, params in support_items:
        check_entry(haus, entity_id, params, 'support', typ, selected_node, message)

    return True


def send_scheduled_emails():
    haus = Haus.objects.first()
    scheduled_email = get_from_cache_or_db(haus, 'scheduled_email')
    if not scheduled_email:
        return

    for key, email in scheduled_email.items():
        next_allowed_time = email.get('next_allowed_time')
        next_allowed_time = datetime.strptime(next_allowed_time, '%d.%m.%Y-%H:%M:%S')
        berlin_now = datetime.now(pytz.timezone('Europe/Berlin'))
        if next_allowed_time > berlin_now.replace(tzinfo=None):
            continue

        cache_key = 'last_time_email_sent_%s_%s' % (email['user_or_support'], email['entry_id'])
        berlin_now = datetime.now(pytz.timezone('Europe/Berlin'))
        berlin_now_str = berlin_now.strftime('%d.%m.%Y-%H:%M:%S')
        set_to_cache_and_db(haus, cache_key, berlin_now_str)
        try:
            notify_users(haus, 'email', email.get('message', ''), email.get('email_to', []), **email.get('context', {}))
        except Exception as e:
            logging.warning("Error sending email: %s" % e)

        scheduled_email.pop(key, None)

    set_to_cache_and_db(haus, 'scheduled_email', scheduled_email)


def get_fps_duration(haus):
    # default is 24hrs
    return int(haus.get_spec_module_params('benachrichtigung').get('fps_notification_duration', 1440))


def set_jsonapi(data, haus, usr, entityid):
    params = haus.get_spec_module_params('benachrichtigung')
    if 'fps_duration' in data:
        try:
            params['fps_notification_duration'] = int(data['fps_duration'])
        except ValueError:
            return {'error': 'notification_interval must be integer value'}
        haus.set_spec_module_params('benachrichtigung', params)
        return {'message': 'done'}

    return {'error': 'parameters are not provided'}


@csrf_exempt
def config_smtp_data(request, haus):
    haus_data = Haus.objects.first()
    hparams = haus_data.get_module_parameters()
    if not isinstance(haus, Haus):
        haus = Haus.objects.get(pk=long(haus))
    email_setting = hparams.get('email_settings',  dict())

    if request.method == 'GET':
        context = {
            'haus': haus,
            'email_setting': email_setting,
        }
        return render_response(request, "m_settings_benachrichtigung_config.html", context)

    if request.method == 'POST':
        if request.POST.get('send_test_email') == 'smtp':
            try:
                user = hparams.get('email_settings', dict()).get('smtp_testname', '')
                notify_users(haus, 'email', 'Test von der SMTP-Konfigurationsseite', [user], subject='Test')
            except:
                pass
        elif 'send_test_controme_email' in request.POST:
            email = EmailNotification()
            email.send('test message via controme mail service.', to=[request.POST.get('to')], via_controme=True)
            HttpResponse('Done!')
        elif 'send_test_email_smtp' in request.POST:
            email = EmailNotification()
            email.send('test message via smtp.', to=[request.POST.get('to')], via_smtp=True)
            HttpResponse('Done!')
        else:
            smtp_data = request.POST.get('email_data')
            hparams['email_settings'] = eval(smtp_data).get('smtp_data')
            haus_data.set_module_parameters(hparams)
        return HttpResponse('Done!')


@csrf_exempt
def config_device_status(request, haus):
    if not isinstance(haus, Haus):
        haus = Haus.objects.get(pk=long(haus))
    params = haus.get_spec_module_params('benachrichtigung')
    if request.method == 'GET':
        context = dict()
        userlist = UserProfile.objects.all()
        context['haus'] = haus
        context['userlist'] = userlist
        email_list = params.get('device_emails')
        if email_list:
            email_list = email_list.split(',')
        else:
            email_list = []
        context['device_emails'] = email_list
        return render_response(request, "m_settings_benachrichtigung_status.html", context)
    if request.method == 'POST':
        if request.POST.get('cloud') == 'save':
            from dataslave import tasks
            tasks.send_setup.apply()
            return HttpResponse('Done!')
        if 'email_on' in request.POST:
            email_list = params.get('device_emails')
            if email_list:
                email_list = email_list.split(',')
            else:
                email_list = []
            email_list.append(request.POST.get('email_on'))
            result = ','.join(email_list)
            params['device_emails'] = result
        if 'email_off' in request.POST:
            email_list = params.get('device_emails')
            if email_list:
                email_list = email_list.split(',')
            else:
                email_list = []
            if request.POST.get('email_off') in email_list:
                email_list = [s for s in email_list if s != request.POST.get('email_off')]
                result = ','.join(email_list)
                params['device_emails'] = result
        haus.set_spec_module_params('benachrichtigung', params)
        return HttpResponse('Done!')


def send_email(request, action, entityid):
    cache_key = 'scheduled_email'
    haus = Haus.objects.first()
    scheduled_email = get_from_cache_or_db(haus, cache_key)
    scheduled_key = '%s_%s' % (entityid, action)
    if scheduled_key in scheduled_email:
        email = scheduled_email[scheduled_key]
        try:
            notify_users(haus, 'email', email.get('message', ''), email.get('email_to', []), **email.get('context', {}))
        except Exception as e:
            logging.warning("Error sending email: %s" % e)

        scheduled_email.pop(scheduled_key, None)
        set_to_cache_and_db(haus, cache_key, scheduled_email)
        berlin_now = datetime.now(pytz.timezone('Europe/Berlin'))
        berlin_now_str = berlin_now.strftime('%d.%m.%Y-%H:%M:%S')
        cache_key = 'last_time_email_sent_%s_%s' % (action, entityid)
        set_to_cache_and_db(haus, cache_key, berlin_now_str)
    return HttpResponse('done!', content_type="application/json")


