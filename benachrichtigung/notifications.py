# -*- coding: utf-8 -*-
import requests
import json
import logging
from heizmanager.abstracts.base_notification import BaseNotification
from django.core.mail import send_mail
from django.core.mail import get_connection
from heizmanager.network_helper import get_mac
from heizmanager.models import Haus
from django.contrib.auth.models import User

import imp
try:
    mail_service = imp.load_compiled("mail_service","/home/pi/rpi/benachrichtigung/mail_service.py")
except:
    pass


class EmailNotification(BaseNotification):

    def __init__(self):
        pass

    def send(self, message, to=[], *args, **kwargs):
        to = list(set(to))
        haus_data = Haus.objects.first()
        hparams = haus_data.get_module_parameters()
        email_setting = hparams.get('email_settings',  dict())

        notification_purpose = kwargs.get('notification_purpose', '')
        entry_id_name = kwargs.get('subject', '')
        mac = "".join(get_mac().split("-")[3:])

        try:
            user = User.objects.get(id=haus_data.eigentuemer_id)
            user_profile = user.userprofile.get()
            user_name = user_profile.vorname + " " + user_profile.nachname
        except User.DoesNotExist:
            user_name = ""

        subject = "%s (%s) - %s - %s - %s" % (user_name, notification_purpose.lower()[:1],
                                              mac, entry_id_name, notification_purpose)

        message = u"Hallo %s,\nhier ist Ihr Controme-Miniserver :). Es sind Meldungen eines oder mehrerer Komponenten aufgetreten, die ich Ihnen mitteilen möchte:\n%s" % (user_name, message)
        if 'via_controme' in kwargs or email_setting.get('mail_service_use', True):
            filename = '/home/pi/rpi/benachrichtigung/email_cred.yml'
            secret_key = 'JENvbnRyb21lIz8yMDIyJSRlbWFpbF9zZXJ2aWNlXyQmbmV3'
            mail_service.send_mail_service(message, subject, to, secret_key, filename)
            logging.info("subject:%s, message:%s, to:%s" % (subject, message, str(to)))
        if 'via_smtp' in kwargs and 'via_controme' not in kwargs or email_setting.get('smtp_use'):
            email_setting = hparams.get('email_settings', dict())
            authenticate = email_setting.get('smtp_auth', '')
            encrypt_mode = email_setting.get('smtp_encrypt', '')
            tls_mode = False
            ssl_mode = False
            if authenticate == 'TRUE':
                if encrypt_mode == 'TLS':
                    tls_mode = True
                elif encrypt_mode == 'SSL':
                    ssl_mode = True
                from_email = email_setting.get('smtp_name', 'Kein Name')
                connection = get_connection(host=email_setting.get('smtp_host', ''),
                                            port=int(email_setting.get('smtp_port', '')),
                                            username=email_setting.get('smtp_user', ''),
                                            password=email_setting.get('smtp_pass', ''),
                                            use_tls=tls_mode,
                                            use_ssl=ssl_mode)
                logging.info("subject:%s, message:%s, to:%s" % (subject, message, str(to)))
                send_mail(subject, message, from_email, to, connection=connection)

