# -*- coding: utf-8 -*-

from heizmanager.render import render_response, render_redirect
from django.http import HttpResponse
from django.views.decorators.csrf import csrf_exempt
from heizmanager.models import Haus, Regelung
from users.models import UserProfile
import heizmanager.cache_helper as ch
from datetime import datetime
import pytz

from benachrichtigung.views import generate_report, get_from_cache_or_db, set_to_cache_and_db


def get_global_settings_page_support(request, haus, action=None, entityid=None):

    if not isinstance(haus, Haus):
        haus = Haus.objects.get(pk=long(haus))

    context = dict()
    context['haus'] = haus
    params = haus.get_spec_module_params('support_benachrichtigung')
    b_params = params.get('items', dict())

    if request.method == "GET":
        if action == 'edit':
            userlist = UserProfile.objects.filter(user_id__in=haus.besitzer).order_by('id')
            context['userlist'] = userlist
            fps_regs = Regelung.objects.filter(regelung='fps')
            context['fps_lst'] = [(fps.id, fps.get_parameters().get('name', '')) for fps in fps_regs]
            if entityid:
                b_param = b_params.get(entityid)
                context['b_param'] = b_param
                context['b_id'] = entityid
                last_time_email_sent = get_from_cache_or_db(haus, 'last_time_email_sent_support_%s' % entityid)
                context['last_time_email_sent'] = last_time_email_sent
                send_frequency = b_param.get('send_frequency', 0)
                context['send_frequency'] = send_frequency
                if send_frequency > 0:
                    scheduled_email = get_from_cache_or_db(haus, 'scheduled_email')
                    if not scheduled_email:
                        scheduled_email = dict()
                    if '%s_%s' % (entityid, 'support') in scheduled_email:
                        next_allowed_time = scheduled_email['%s_%s' % (entityid, 'support')]['next_allowed_time']
                        context['next_allowed_time'] = next_allowed_time

            return render_response(request, "m_settings_benachrichtigung_edit_support.html", context)

        if action == 'report':
            b_param = b_params.get(entityid)
            filters = {}
            if b_param:
                if b_param.get('notify_mode') == 'fps':
                    selected_fps = b_param.get('selected_fps', 0)
                    if selected_fps:
                        fps_name = Regelung.objects.get(pk=selected_fps).get_parameters().get('name', '')
                        filters = {
                            'selected_fps': selected_fps,
                            'fps_name': fps_name
                        }
                else:
                    if b_param.get('gateways_on', True):
                        filters['gateway'] = True
                    if b_param.get('sensors_on', True):
                        filters['sensor'] = True

                return generate_report(request, filters=filters)

        if action == 'delete':

            # invalidate cache of scheduled email for this entry
            scheduled_email = get_from_cache_or_db(haus, 'scheduled_email')
            scheduled_email.pop('%s_%s' % (entityid, 'support'), None)
            set_to_cache_and_db(haus, 'scheduled_email', scheduled_email)

            b_params.pop(entityid, None)
            params['items'] = b_params
            haus.set_spec_module_params('support_benachrichtigung', params)
            cache_key = "last_time_email_sent_support_%s" % entityid
            last_time_email_sent = get_from_cache_or_db(haus, cache_key)
            if last_time_email_sent:
                set_to_cache_and_db(haus, cache_key, None)
            return render_redirect(request, '/m_setup/%s/support-benachrichtigung/' % haus.id)

        support_exists = False
        for k, v in b_params.items():
            if v.get('name') == 'Support-Ausfallbenachrichtigung':
                support_exists = True
                break

        if not support_exists:
            support_params = b_params.copy()
            b_params_keys = support_params.keys()
            b_params_keys = filter(lambda x: x.isdigit(), b_params_keys)
            new_key = str(int(max(b_params_keys)) + 1) if b_params_keys else '1'
            default_support_item = {
                'status': u'off',
                'name': u'Support-Ausfallbenachrichtigung',
                'selected_fps': 0, 'email_text_2': u'',
                'email_text_1': u'',
                'notify_mode': u'device-status',
                'users': []
            }
            support_params[new_key] = default_support_item
            params['items'] = support_params
            haus.set_spec_module_params('support_benachrichtigung', params)
            return render_redirect(request, '/m_setup/%s/support-benachrichtigung/' % haus.id)

        context['b_params'] = sorted(b_params.items())
        return render_response(request, "m_settings_benachrichtigung_support.html", context)

    if request.method == "POST":

        # invalidate cache of scheduled email for this entry
        scheduled_email = get_from_cache_or_db(haus, 'scheduled_email')
        if entityid:
            scheduled_email.pop('%s_%s' % (entityid, 'support'), None)
            set_to_cache_and_db(haus, 'scheduled_email', scheduled_email)

        # preparing data
        data_d = dict()
        data_d['name'] = request.POST.get('name', '')
        user_lst = request.POST.getlist('users')
        data_d['users'] = user_lst
        data_d['notify_mode'] = request.POST.get('notify_mode', 'device-status')
        data_d['email_text_1'] = request.POST.get('email_text_1', '')
        data_d['email_text_2'] = request.POST.get('email_text_2', '')
        data_d['selected_fps'] = int(request.POST.get('selected_fps', 0))
        data_d['send_frequency'] = int(request.POST.get('send_frequency', 0))
        data_d['email_for_support'] = request.POST.get('email_for_support', '')
        data_d['sensors_on'] = int(request.POST.get('sensors_on', 0))
        data_d['gateways_on'] = int(request.POST.get('gateways_on', 0))
        data_d['controme_s_email'] = request.POST.get('controme_s_email', '')

        if entityid is None:
            ids = [0] + [int(k) for k in b_params.keys()]
            entityid = str(max(ids) + 1)
            b_params[entityid] = dict()

        # saving data
        b_params[entityid].update(data_d)
        params['items'] = b_params
        haus.set_spec_module_params('support_benachrichtigung', params)

        berlin_now = datetime.now(pytz.timezone('Europe/Berlin'))
        berlin_now_str = berlin_now.strftime('%d.%m.%Y-%H:%M:%S')
        # initial last time email sent
        last_time_sent_cache_key = 'last_time_email_sent_%s_%s' % ('support', entityid)
        set_to_cache_and_db(haus, last_time_sent_cache_key, berlin_now_str)

        return render_redirect(request, '/m_setup/%s/support-benachrichtigung/' % haus.id)


def set_sb_status(device_email=None, sensors_on=None, gateways_on=None):
    haus = Haus.objects.first()
    context = dict()
    context['haus'] = haus
    params = haus.get_spec_module_params('support_benachrichtigung')
    b_params = params.get('items', dict())

    support_exists = False
    key = 0
    for k, v in b_params.items():
        if v.get('name') == 'Support-Ausfallbenachrichtigung':
            support_exists = True
            key = k
            break

    if not support_exists:
        b_params_keys = b_params.keys()
        b_params_keys = filter(lambda x: x.isdigit(), b_params_keys)
        new_key = str(int(max(b_params_keys)) + 1) if b_params_keys else '1'
        default_support_item = {
            'status': u'on',
            'name': u'Support-Ausfallbenachrichtigung',
            'selected_fps': 0, 'email_text_2': u'',
            'email_text_1': u'',
            'controme_s_email': '1',
            'notify_mode': u'device-status',
            'users': []
        }
        if sensors_on is not None:
            b_params[key]['sensors_on'] = sensors_on
            if sensors_on:
                b_params[key]['controme_s_email'] = '1'

        if gateways_on is not None:
            b_params[key]['controme_s_email'] = sensors_on
            if gateways_on:
                b_params[key]['controme_s_email'] = '1'

        b_params[new_key] = default_support_item
    else:
        if sensors_on is not None:
            b_params[key]['sensors_on'] = sensors_on
            if sensors_on:
                b_params[key]['controme_s_email'] = '1'
        if gateways_on is not None:
            b_params[key]['gateways_on'] = gateways_on
            if gateways_on:
                b_params[key]['controme_s_email'] = '1'

    params['items'] = b_params

    if device_email is not None:
        if device_email == 1:
            params['device_emails'] = 'support@controme.com'
        else:
            params['device_emails'] = ''
    haus.set_spec_module_params('support_benachrichtigung', params)


def sbon(request):
    set_sb_status(device_email=1, sensors_on=1, gateways_on=1)
    return HttpResponse("Done!")


def sboff(request):
    set_sb_status(device_email=0, sensors_on=0, gateways_on=0)
    return HttpResponse("Done!")


def sbgwon(request):
    set_sb_status(device_email=None, sensors_on=None, gateways_on=1)
    return HttpResponse("Done!")


def sbgwoff(request):
    set_sb_status(device_email=None, sensors_on=None, gateways_on=0)
    return HttpResponse("Done!")


def sbson(request):
    set_sb_status(device_email=None, sensors_on=1, gateways_on=None)
    return HttpResponse("Done!")


def sbsoff(request):
    set_sb_status(device_email=None, sensors_on=0, gateways_on=None)
    return HttpResponse("Done!")


def sbmson(request):
    set_sb_status(device_email=1, sensors_on=None, gateways_on=None)
    return HttpResponse("Done!")


def sbmsoff(request):
    set_sb_status(device_email='off', sensors_on=None, gateways_on=None)
    return HttpResponse("Done!")


@csrf_exempt
def config_device_status(request, haus):
    if not isinstance(haus, Haus):
        haus = Haus.objects.get(pk=long(haus))
    params = haus.get_spec_module_params('support_benachrichtigung')
    if request.method == 'GET':
        context = dict()
        userlist = UserProfile.objects.all()
        context['haus'] = haus
        context['userlist'] = userlist
        email_list = params.get('device_emails')
        if email_list:
            email_list = email_list.split(',')
        else:
            email_list = []
        context['device_emails'] = email_list
        return render_response(request, "m_settings_benachrichtigung_status_support.html", context)
    if request.method == 'POST':
        if request.POST.get('cloud') == 'save':
            from dataslave import tasks
            tasks.send_setup.apply()
            return HttpResponse('Done!')
        if 'email_on' in request.POST:
            email_list = params.get('device_emails')
            if email_list:
                email_list = email_list.split(',')
            else:
                email_list = []
            email_list.append(request.POST.get('email_on'))
            result = ','.join(email_list)
            params['device_emails'] = result
        if 'email_off' in request.POST:
            email_list = params.get('device_emails')
            if email_list:
                email_list = email_list.split(',')
            else:
                email_list = []
            if request.POST.get('email_off') in email_list:
                email_list = [s for s in email_list if s != request.POST.get('email_off')]
                result = ','.join(email_list)
                params['device_emails'] = result
        haus.set_spec_module_params('support_benachrichtigung', params)
        return HttpResponse('Done!')
