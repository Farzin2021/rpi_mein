from django.conf.urls import url
from . import views
from . import supports_views


urlpatterns = [
    url(r'^m_setup/(?P<haus>\d+)/benachrichtigung/status/$', views.config_device_status),
    url(r'^m_setup/(?P<haus>\d+)/benachrichtigung/settings/$', views.config_smtp_data),
    url(r'^m_setup/(?P<haus>\d+)/benachrichtigung/(?P<action>[a-z_]+)/$', views.get_global_settings_page),
    url(r'^m_setup/(?P<haus>\d+)/benachrichtigung/(?P<action>[a-z_]+)/(?P<entityid>\d+)/$', views.get_global_settings_page),
    url(r'^m_setup/benachrichtigung/send_email/(?P<action>[a-z_]+)/(?P<entityid>\d+)/$', views.send_email),
    # support
    url(r'^m_setup/(?P<haus>\d+)/support-benachrichtigung/$',
        supports_views.get_global_settings_page_support),
    url(r'^m_setup/(?P<haus>\d+)/support-benachrichtigung/status/$', supports_views.config_device_status),
    url(r'^m_setup/(?P<haus>\d+)/support-benachrichtigung/(?P<action>[a-z_]+)/$',
        supports_views.get_global_settings_page_support),
    url(r'^m_setup/(?P<haus>\d+)/support-benachrichtigung/(?P<action>[a-z_]+)/(?P<entityid>\d+)/$',
        supports_views.get_global_settings_page_support),
]
