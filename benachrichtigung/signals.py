from django.dispatch import receiver
from heizmanager.models import sig_send_notification
from .tasks import call_method_by_action
import logging

logger = logging.getLogger('logmonitor')


@receiver(sig_send_notification)
def send_notification(sender, **kwargs):
    kwargs.pop('signal')
    call_method_by_action(sender, **kwargs)




