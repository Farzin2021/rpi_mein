from django.apps import AppConfig


class BenachrichtigungConfig(AppConfig):
    name = 'benachrichtigung'
    verbose_name = 'benachrichtigung'

    def ready(self):
        from . import signals
