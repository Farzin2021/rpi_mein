# -*- coding: utf-8 -*-
import re
import logging

from celery import shared_task

from rpi.celery import app
from .views import prepare_device_summary, prepare_fps_summary, send_scheduled_emails
from heizmanager.models import Haus


logger = logging.getLogger('logmonitor')
p_logger = logging.getLogger('servicemonitorlog')


@app.task
def call_method_by_action(sender, **kwargs):
    haus = Haus.objects.get(id=kwargs.pop('haus_id'))
    kwargs['haus'] = haus
    method_factory = {
        'device-stat': prepare_device_summary,
        'fps': prepare_fps_summary,
    }
    return method_factory[sender](**kwargs)


@shared_task
def send_notifications():
    send_scheduled_emails()
