import sys, os, django
from fabric.api import local, lcd
import logging

# Add django settings to run this script seperately!
try:
    sys.path.append('/home/pi/rpi/')
    os.environ.setdefault("DJANGO_SETTINGS_MODULE", "rpi.settings")
    django.setup()
except:
    print('Error in initialize django settings!')
    sys.exit()


# Handle sub proccess
def local_handler(cmd, msg):
    try:
        local(cmd)
    except:
        print(msg)


# main function
def db_replace():
    logger = logging.getLogger("logmonitor")
    try:
        files = os.listdir('/home/pi/uploaded_db/')
        zip_files = [f for f in files if '.zip' in f][0]
        local_handler("cp /home/pi/uploaded_db/* /home/pi/rpi/db.sqlite3 ", 'Error in copy database!')
        local_handler("python /home/pi/rpi/manage.py migrate users", 'Error in mugration users!')
        local_handler("echo 'flush_all' | nc -q 2 localhost 11211", 'Error in flushing!')
        logger.warning("%s|%s" % ('periodischer_reset','Reset auf %s' % zip_files))
    except:
        logger.warning("%s|%s" % ('periodischer_reset','Reset Failed!'))


db_replace()