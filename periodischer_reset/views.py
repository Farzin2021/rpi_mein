# coding=utf-8
from fabric.api import local
from heizmanager.render import render_response, render_redirect
from django.shortcuts import HttpResponse
from django.views.decorators.csrf import csrf_exempt
import pytz
import logging
import os, shutil
import json
import datetime


def get_name():
    return 'PeriodicherReset'


def get_cache_ttl():
    return 1800


def is_togglable():
    return True


def get_global_settings_link(request, haus):
    return "<a href='/m_setup/%s/periodischer_reset/'>Periodischer Reset</a>" % haus.id


def get_global_description_link():
    desc = u"Mit diesem Plugin kann der Miniserver zyklisch auf einen bestimmten Status zurückgesetzt werden"
    desc_link = "https://support.controme.com/periodischer-reset/"
    return desc, desc_link


def write_json(json_file, json_data):
    with open(json_file, 'w') as outfile:
        json.dump(json_data, outfile)


def read_json(json_file):
    with open(json_file) as infile:
        data = json.load(infile)
    return data


def local_handler(cmd):
    try:
        res = local(cmd, capture= True)
    except:
        return
    return res


def get_global_settings_page(request, haus):

    # location of json as db
    filepath = '/home/pi/.peridischer.json'
    # check if periodischer json is avalaible
    if not os.path.isfile(filepath):
        periodischer_db_status = local_handler('echo $(sudo systemctl is-active periodischer_db.timer)')
        periodischer_status = u'on' if periodischer_db_status == 'active' else u'off'
        try:
            periodischer_repeat = str(int(local_handler(
                'cat /etc/systemd/system/periodischer_db.timer | grep OnUnitActiveSec | cut -d "=" -f2 | cut -d " " -f1'
                ))/60)
        except:
            periodischer_repeat = None
        if not periodischer_repeat:
            periodischer_repeat = '30'
        berlin = pytz.timezone('Europe/Berlin')
        now = datetime.datetime.now(berlin)
        datenow = now.ctime()
        json_data = {
            'date': datenow,
            'hour_slider': periodischer_repeat,
            'status': periodischer_status,
        }
        write_json(filepath, json_data)

    # get uploaded zip file name
    try:
        files = os.listdir('/home/pi/uploaded_db/')
        zip_files = [f for f in files if '.zip' in f][0]
    except:
        zip_files = 'Es wurde keine Datenbank ausgewählt!'

    module_params = read_json(filepath)

    if request.method == "GET":
        # get left time 
        left_time = local_handler("echo $(sudo systemctl list-timers periodischer_db.timer | grep -i 'periodischer_db.timer'"
                                  " | grep -o -P '.*(?= left)' | cut -d' ' -f 5-)")
        if left_time == '':
            left_time = 'Warten Sie mal'
        context = {}
        context['haus'] = haus
        context['params'] = module_params
        context['zipdb_name'] = zip_files
        context['left_time'] = left_time
        return render_response(request, 'm_settings_periodischer_reset.html', context)

    elif request.method == "POST":
        if 'hour_slider' in request.POST:
            module_params['hour_slider'] = request.POST['hour_slider']
        if 'set_status' in request.POST:
            module_params['status'] = request.POST['set_status']
            if module_params['status'] == 'on':
                berlin = pytz.timezone('Europe/Berlin')
                now = datetime.datetime.now(berlin)
                datenow = now.ctime()
                module_params['date'] = datenow
        if 'db_backup' in request.POST:
            local_handler('rm -r /home/pi/uploaded_db/ ; mkdir /home/pi/uploaded_db/')
            db = request.FILES.get('db_file', None)
            if db is None:
                return HttpResponse('Error: No zip file selected!')
            with open('/home/pi/uploaded_db/%s' % db.name, 'wb') as dest:
                shutil.copyfileobj(db, dest)
            try:
                if len(os.listdir('/home/pi/uploaded_db/')) != 1 or db.name.split('.')[-1] != 'zip':
                    local_handler('rm -r /home/pi/uploaded_db/ && mkdir /home/pi/uploaded_db/')
                    return HttpResponse('Error: You should add only one database file!')
            except:
                return HttpResponse('Unknown error: Please contact the developer team!')
        if 'db_reset_log' in request.POST:
            local_handler("cat /var/log/uwsgi/uwsgi.log | grep 'periodischer_reset' > /var/log/uwsgi/db_reset.log")
            with open('/var/log/uwsgi/db_reset.log', 'r') as data_file:
                data = data_file.read()
            response = HttpResponse(data, content_type='text/plain')
            response['Content-Disposition'] = 'attachment; filename=db_reset.log'
            return response
        write_json(filepath, module_params)
        status = module_params.get('status')
        if status is not None:
            if status == 'on':
                # kill same existing process
                local_handler("sudo ps -ef | sudo grep 'bash ble_service_create.sh periodischer_db'"
                              " | grep -v grep | awk '{print $2}' | sudo xargs kill > /dev/null 2>&1")
                # update timer according to slider value
                local_handler('cd /home/pi/rpi/heizmanager/scripts/ && sudo bash ble_service_create.sh '
                    'periodischer_db "/usr/bin/sudo -H -u pi /usr/bin/python /home/pi/rpi/periodischer_reset/database_copy.py" %s h'
                    % str(int(module_params.get('hour_slider', '30'))*60))
            else:
                local_handler('sudo systemctl stop periodischer_db.timer && sudo systemctl disable periodischer_db.timer')
        return render_redirect(request, "/m_setup/%s/periodischer_reset/" % haus.id)


def activate(hausoderraum):
    if 'periodischer_reset' not in hausoderraum.get_modules():
        hausoderraum.set_modules(hausoderraum.get_modules() + ['periodischer_reset'])


def deactivate(hausoderraum):
    modules = hausoderraum.get_modules()
    try:
        modules.remove('periodischer_reset')
        hausoderraum.set_modules(modules)
    except ValueError:  # nicht in modules
        pass

